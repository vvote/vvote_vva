﻿using System.Reflection;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyCompany("Victorian Electoral Commission")]
[assembly: AssemblyProduct("Turing")]
[assembly: AssemblyCopyright("Copyright © Victorian Electoral Commission 2011")]
[assembly: AssemblyTrademark("")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
[assembly: AssemblyVersion("2.1.0.1035")]
[assembly: AssemblyFileVersion("2.1.0.1035")]
[assembly: AssemblyInformationalVersion("2.1 Alpha")]