﻿using Autofac;
using Autofac.Integration.Wcf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Vec.Framework.Core.BootStrappers;
using Vec.Properties;

namespace Vec.Wbb.Host
{
    public class Global : System.Web.HttpApplication
    {
        List<IBootStrapper> BootStrappers { get; set; }

        protected void Application_Start(object sender, EventArgs e)
        {
            var builder = new ContainerBuilder();

            BootStrappers = new List<IBootStrapper>();
            BootStrappers.Add(new Vec.Apps.Wbb.BootStrappers.BootStrapper());

            foreach (var bootItem in BootStrappers)
            {
                bootItem.RegisterTypes(builder);
            }

            AutofacHostFactory.Container = builder.Build();
            foreach (var bootItem in BootStrappers)
            {
                bootItem.Start();
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            if (BootStrappers != null && BootStrappers.Count > 0)
                foreach (var bootItem in BootStrappers)
                {
                    bootItem.Start();
                }
        }
    }
}