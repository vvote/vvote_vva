﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using log4net;
using log4net.Appender;
using log4net.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.vVote.Entities;

namespace Vec.Wbb.Host.Apps.vVote.Common
{
    public class ServiceLogAppender : AppenderSkeleton
    {
        const string USER_AGENT = "Mozilla/5.0";
        public string ServerURL { get; set; }
        public string TrustStorePassword { get; set; }
        public string TrustStorePath { get; set; }
        public byte[] TrustStore { get; set; }
        public ILog Logger { get; private set; }

        public override void ActivateOptions()
        {
            Logger = log4net.LogManager.GetLogger(this.GetType());
            base.ActivateOptions();

            TrustStore = File.ReadAllBytes(TrustStorePath);
        }

        protected override void Append(LoggingEvent loggingEvent)
        {
            try
            {
                SendPost(loggingEvent);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private void SendPost(LoggingEvent loggingEvent)
        {
            SecureWebClient webClient = new SecureWebClient() { TrustStorePassword = TrustStorePassword, TrustStore = TrustStore };

            webClient.Headers.Add("User-Agent", USER_AGENT);
            webClient.Headers.Add("Accept", "application/json");
            webClient.Headers.Add("Content-Type", "application/json");

            string[] msgs = loggingEvent.RenderedMessage.Split('#');

            LogEntity logEntity = new LogEntity()
            {
                Level = loggingEvent.Level.Name,
                EventId = Guid.Parse(msgs[0]),
                From = msgs[1],
                MessageType = msgs[2],
                Message = msgs[3]
            };

            string response = webClient.UploadString(ServerURL, "POST", JObject.FromObject(logEntity).ToString(Formatting.None));
        }
    }

    public class SecureWebClient : WebClient
    {
        public string TrustStorePassword { get; set; }
        public byte[] TrustStore { get; set; }

        protected override WebRequest GetWebRequest(Uri address)
        {
            HttpWebRequest request = base.GetWebRequest(address) as HttpWebRequest;
            request.ClientCertificates.Add(new X509Certificate2(TrustStore, TrustStorePassword));
            return request;
        }
    }
}
