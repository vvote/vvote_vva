﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Vec.Wbb.Host.Apps.vVote.Common
{
    public class ServiceLogModule : IHttpModule
    {
        public ILog Logger { get; private set; }

        public string ModuleName
        {
            get { return "ServiceLogModule"; }
        }

        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            Logger = log4net.LogManager.GetLogger(this.GetType());

            context.BeginRequest += context_BeginRequest;
            context.EndRequest += context_EndRequest;
        }

        void context_EndRequest(object sender, EventArgs e)
        {
            HttpApplication application = sender as HttpApplication;

            if (System.IO.File.Exists(application.Context.Request.PhysicalPath))
            {
                string message = string.Format("{0}#{1}#{2}#{3}|{4}",
                    ConfigurationManager.AppSettings["eventId"],
                    application.Context.Request.Headers["deviceId"] ?? application.Context.Request.UserHostAddress,
                    "WBB_RESOURCE_REQUEST",
                    application.Context.Request.Url,
                    application.Context.Response.StatusCode);

                Logger.Info(message);
            }
        }

        void context_BeginRequest(object sender, EventArgs e)
        {
        }
    }
}
