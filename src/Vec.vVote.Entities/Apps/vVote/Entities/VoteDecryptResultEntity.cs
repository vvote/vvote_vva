﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.vVote.Entities
{
    [DataContract]
    public class VoteDecryptResultEntity : BaseVolatileEntity
    {
        public VoteDecryptResultEntity()
        {
            VoteDecryptFiles = new List<VoteDecryptFile>();
        }

        [DataMember]
        public Guid Context { get; set; }

        [DataMember]
        public List<VoteDecryptFile> VoteDecryptFiles { get; set; }
    }

    [DataContract]
    public class VoteDecryptFile : BaseVolatileEntity
    {
        public VoteDecryptFile()
        {
            VoteDecryptRows = new List<VoteDecryptRow>();
        }

        [DataMember]
        public string Region { get; set; }

        [DataMember]
        public string District { get; set; }

        [DataMember]
        public Guid RaceId { get; set; }

        [DataMember]
        public string VoteType { get; set; }
        
        [DataMember]
        public bool IsBlank { get; set; }

        [DataMember]
        public List<VoteDecryptRow> VoteDecryptRows { get; set; }
    }

    [DataContract]
    public class VoteDecryptRow
    {
        public VoteDecryptRow()
        {
            Votes = new List<VoteDecryptCell>();
        }

        [DataMember]
        public List<VoteDecryptCell> Votes { get; set; }
    }

    [DataContract]
    public class VoteDecryptCell
    {
        [DataMember]
        public int CandidateId { get; set; }

        [DataMember]
        public int? Vote { get; set; }
    }
}
