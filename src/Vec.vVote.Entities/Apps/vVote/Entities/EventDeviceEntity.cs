﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.vVote.Entities
{
    [DataContract]
    public class EventDeviceEntity : BaseEntity
    {
        [DataMember]
        public byte TypeId { get; set; }

        [DataMember]
        public int DeviceId { get; set; }

        [DataMember]
        public string TypeName { get; set; }

        [DataMember]
        public EventLocationEntity EventLocationEntity { get; set; }

        [DataMember]
        public EventEntity EventEntity { get; set; }

        [DataMember]
        public byte StageId { get; set; }

        [DataMember]
        public string StageName { get; set; }

        [DataMember]
        public int? QuarantineQuota { get; set; }

        [DataMember]
        public int? DeployedLocationId { get; set; }

        [DataMember(Name = "VPS_Audit")]
        public bool IsAuditEnabled { get; set; }

        [DataMember(Name = "maxBundleSize")]
        public long MaxBundleSize { get; set; }

        [DataMember(Name = "launchURL")]
        public string LaunchUrl { get; set; }

        [DataMember(Name = "heartbeatURL")]
        public string HeartbeatURL { get; set; }

        [DataMember(Name = "connCheckURL")]
        public string ConnectionCheckURL { get; set; }

        [DataMember(Name = "f5LaunchURL")]
        public string F5LaunchURL { get; set; }
    }

    [DataContract]
    public class MonitoredDeviceEntity : EventDeviceEntity
    {
        public MonitoredDeviceEntity()
        {
            VoteStatus = new VoteStatusEntity();
        }

        [DataMember]
        public BundleStatusEntity BundleStatus { get; set; }

        [DataMember]
        public VoteStatusEntity VoteStatus { get; set; }
    }

    [DataContract]
    public class VoteStatusEntity
    {
        [DataMember]
        public int StartCount { get; set; }

        [DataMember]
        public int VoteCount { get; set; }

        [DataMember]
        public int IQCount { get; set; }

        [DataMember]
        public int AuditCount { get; set; }
    }

    [DataContract]
    public class BundleStatusEntity
    {
        [DataMember]
        public string BundleUrl { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public string DateTime { get; set; }
    }
}
