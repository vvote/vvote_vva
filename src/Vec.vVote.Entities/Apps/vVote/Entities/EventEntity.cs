﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.vVote.Entities
{
    [DataContract]
    [Serializable]
    public class EventEntity
    {
        public EventEntity()
        {
            AssociatedElections = new List<ElectionEntity>();
            MBBInternetProtocols = new List<MBBInternetProtocol>();
        }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<ElectionEntity> AssociatedElections { get; set; }

        [DataMember]
        public string WBBServiceHostName { get; set; }

        [DataMember]
        public string WBBMServiceHostName { get; set; }

        [DataMember]
        public string MBBPubKeyPath { get; set; }

        [DataMember]
        public string VVPubKeyPath { get; set; }

        [DataMember]
        public string EBPubKeyPath { get; set; }

        [DataMember]
        public string PollTime { get; set; }

        [DataMember]
        public string ProofTime { get; set; }

        [DataMember]
        public string DraftBundlePath { get; set; }

        [DataMember]
        public string FinalBundlePath { get; set; }

        [DataMember]
        public int MBBTimeout { get; set; }

        [DataMember]
        public List<MBBInternetProtocol> MBBInternetProtocols { get; set; }

        public byte[] SignatureFile { get; set; }

        [DataMember]
        public string WBBPublicKey { get; set; }

        [DataMember]
        public string ReconcileURL { get; set; }
    }
}
