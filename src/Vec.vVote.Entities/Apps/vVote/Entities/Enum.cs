﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.vVote.Entities
{
    public enum EntityType
    {
        Event = 1,
        Party = 2,
        Candidate = 3,
        Location = 4,
        StaticContent = 5,
        Device = 6,
        VpsTemplate = 7,
        EvmTemplate =8
    }

    public enum LocationType
    {
        Region,
        District,
        IS,
        OS,
        SPARE = 10,
        PROOF = 11
    }

    public enum DeviceType
    {
        VPS,
        EVM
    }

    public enum DecryptType
    {
        District = 1,
        ATL = 2,
        BTL = 3
    }

    public enum DeviceStageType
    {
        None,
        FlashDevice,
        Image,
        SetDate,
        ExportCSR,
        ImportKeys,
        Done,
        ImportMix,
        ReplacedBad,
        ReturnedGood,
        BallotGeneration
    }

    public enum DeviceMessageType
    {
        cancelreq
    }

    public enum DeviceMessageResponseType
    {
        Approved,
        Rejected,
        Error
    }

    public enum CommitType
    {
        BallotGenCommit,
        BallotAuditCommit,
        Pod,
        Vote,
        Audit,
        Cancel
    }

    public enum VoteType
    {
        Early,
        Absent,
        Ordinary,
    }
}
