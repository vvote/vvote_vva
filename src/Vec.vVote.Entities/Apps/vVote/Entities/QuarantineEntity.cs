﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.vVote.Entities
{
    [DataContract]
    public class QuarantineEntity : BaseEntity
    {
        [DataMember(Name = "eventId")]
        public Guid EventId { get; set; }

        [DataMember(Name = "type")]
        public string TypeName { get; set; }

        public DeviceMessageType TypeId
        {
            get
            {
                return (DeviceMessageType)Enum.Parse(typeof(DeviceMessageType), TypeName);
            }
        }

        [DataMember(Name = "serialNo")]
        public string SerialNo { get; set; }

        [DataMember(Name = "district")]
        public string District { get; set; }

        [DataMember(Name = "serialSig")]
        public string SerialSig { get; set; }

        [DataMember(Name = "boothID")]
        public int DeviceId { get; set; }

        [DataMember(Name = "boothSig")]
        public string DeviceSig { get; set; }
    }

    [DataContract]
    public class AuthEntity
    {
        [DataMember(Name = "cancelAuthID")]
        public string AuthId { get; set; }

        [DataMember(Name = "cancelAuthSig")]
        public string AuthSig { get; set; }
    }
}
