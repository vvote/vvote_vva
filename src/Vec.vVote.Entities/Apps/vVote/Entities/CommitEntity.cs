﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.vVote.Entities
{
    public class CommitEntity : BaseEntity
    {
        public string DeviceId { get; set; }
        
        public string SerialNo { get; set; }

        public string District { get; set; }

        public string Preferences { get; set; }

        public string BallotReductions { get; set; }

        public int EvmId
        {
            get 
            {
                return Convert.ToInt32(DeviceId);
            }
        }

        public int DeviceLocationCode
        {
            get
            {
                string locCode = DeviceId.Substring(0, DeviceId.Length - 4);
                return Convert.ToInt32(locCode);
            }
        }

        public int DeviceLocationSubCode
        {
            get
            {
                string subCode = DeviceId.Substring(DeviceId.Length - 4, 2);
                return Convert.ToInt32(subCode);
            }
        }

        public int VpsId
        {
            get
            {
                return Convert.ToInt32(SerialNo.Split(':')[0]);
            }
        }

        public int Order
        {
            get
            {
                return Convert.ToInt32(SerialNo.Split(':')[1]);
            }
        }

        public List<string> LaPreferences
        {
            get
            {
                string[] raceVotes = Preferences.Split(new char[1] { ':' }, StringSplitOptions.None);
                return raceVotes[0].Split(new char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
        }
        public List<string> AtlPreferences
        {
            get
            {
                string[] raceVotes = Preferences.Split(new char[1] { ':' }, StringSplitOptions.None);
                return raceVotes[1].Split(new char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
        }
        public List<string> BtlPreferences
        {
            get
            {
                string[] raceVotes = Preferences.Split(new char[1] { ':' }, StringSplitOptions.None);
                return raceVotes[2].Split(new char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
        }

        public string DeviceRaceName { get; set; }

        public Guid VoteDistrictRaceId { get; set; }

        public string VoteDistrictRaceName { get; set; }

        public Guid VoteRegionRaceId { get; set; }

        public string VoteRegionRaceName { get; set; }

        public Guid DeviceRaceId { get; set; }

        public DateTime CommitTime { get; set; }

        public CommitType CommitType { get; set; }

        public VoteType VoteType { get; set; }

        public Guid? Context { get; set; }
    }

    public class Reductions : List<Reduction>
    {
        public Reductions()
            : base()
        { }
    }

    public class Reduction
    {
        public int index { get; set; }
        public int candidateIndex { get; set; }
        public string randomness { get; set; }
    }
}
