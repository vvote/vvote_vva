﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.vVote.Entities
{
    [DataContract]
    public class TelemetryMBBEntity
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string DateTime { get; set; }

        [DataMember]
        public string Level { get; set; }

        [DataMember]
        public string From { get; set; }

        [DataMember]
        public string MessageType { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}
