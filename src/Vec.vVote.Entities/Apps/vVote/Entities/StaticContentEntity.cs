﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.vVote.Entities
{
    [DataContract]
    public class StaticContentsEntity
    {
        public StaticContentsEntity()
        {
            StaticAudioContents = new List<StaticAudioContent>();
            StaticTextContents = new List<StaticTextContent>();
        }

        [DataMember]
        public List<StaticAudioContent> StaticAudioContents { get; set; }

        [DataMember]
        public List<StaticTextContent> StaticTextContents { get; set; }
    }

    [DataContract]
    public class StaticTextContent : BaseEntity
    {
        public StaticTextContent()
        {
            Texts = new List<TextEntity>();
        }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string SizeLimit { get; set; }

        [DataMember]
        public string ConversionLabel { get; set; }

        [DataMember]
        public List<TextEntity> Texts { get; set; }
    }

    [DataContract]
    public class StaticAudioContent : BaseEntity
    {
        public StaticAudioContent()
        {
            Audios = new List<AudioEntity>();
        }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public bool IsLote { get; set; }

        [DataMember]
        public List<AudioEntity> Audios { get; set; }
    }
}
