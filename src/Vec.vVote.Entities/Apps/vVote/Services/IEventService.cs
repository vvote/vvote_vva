﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.vVote.Entities;
using Vec.Apps.vVote.Messages;

namespace Vec.Apps.vVote.Services
{
    [ServiceContract]
    public interface IEventService
    {
        [OperationContract]
        EventEntity GetEvent(Guid eventId);

        [OperationContract]
        GetMbbSettingsResponse GetMbbSettings(Guid eventId);

        [OperationContract]
        List<EventEntity> GetEvents();

        [OperationContract]
        EventConfig GetEventConfig(Guid eventId);

        [OperationContract]
        List<ElectionEntity> GetElections(int electionType, int electionCategory);

        [OperationContract]
        CreateEventResponse CreateEvent(CreateEventRequest request);

        [OperationContract]
        EventEntity UpdateEvent(EventEntity eventEntity);

        [OperationContract]
        List<PartyEntity> GetParties(Guid eventId);

        [OperationContract]
        List<CandidateEntity> GetCandidates(Guid eventId);

        [OperationContract]
        List<LocationEntity> GetLocations(Guid eventId);

        [OperationContract]
        string PrepareExportEvent(Guid eventId);

        [OperationContract]
        string PrepareExportParties(Guid eventId, List<PartyEntity> request);

        [OperationContract]
        string PrepareExportCandidates(Guid eventId, List<CandidateEntity> request);

        [OperationContract]
        string PrepareExportLocations(Guid eventId, List<LocationEntity> request);

        [OperationContract]
        PrepareExportStaticContentsResponse PrepareExportStaticContents(PrepareExportStaticContentsRequest request);

        [OperationContract]
        PrepareExportDevicesResponse PrepareExportDevices(PrepareExportDevicesRequest request);

        [OperationContract]
        Stream Export(Guid context, EntityType type);

        [OperationContract]
        string Import(Stream formContent);

        [OperationContract]
        List<EventDeviceEntity> GetEventDevices(Guid eventId);

        [OperationContract]
        StaticContentsEntity GetStaticContents(Guid eventId);

        [OperationContract]
        void UpdateEventDevice(EventDeviceEntity eventDevice);

        [OperationContract]
        UpdateMbbSettingsResponse UpdateMbbSettings(Stream request);

        [OperationContract]
        Stream GenerateMbbSettingsBundle(Guid eventId);

        [OperationContract]
        Stream ExportEventDeviceLabels(Guid eventId);

        [OperationContract]
        string ImportQuarantine(Stream formContent);

        [OperationContract]
        GetTemplateSettingsResponse GetTemplateSettings(Guid eventId);

        [OperationContract]
        UpdateTemplateSettingsResponse UpdateTemplateSettings(Stream request);

        [OperationContract]
        PrepareExportTemplateResponse PrepareExportTemplate(PrepareExportTemplateRequest request);

        [OperationContract]
        ProcessPublicSigningKeyResponse ProcessPublicSigningKey(Stream request);

        [OperationContract]
        Stream GetProcessedPublicSigningKey(Guid context);

        [OperationContract]
        GetMonitoredDevicesResponse GetMonitoredDevices(Guid eventId);
    }
}
