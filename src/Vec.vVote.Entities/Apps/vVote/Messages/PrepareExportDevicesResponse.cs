﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.vVote.Messages
{
    public class PrepareExportDevicesResponse : Response
    {
        public Guid Context { get; set; }
    }
}
