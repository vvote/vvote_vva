﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.vVote.Entities;

namespace Vec.Apps.vVote.Messages
{
    public class PrepareExportDevicesRequest : Request
    {
        public List<EventDeviceEntity> Devices { get; set; }
    }
}
