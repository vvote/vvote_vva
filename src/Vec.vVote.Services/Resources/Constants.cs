﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Resources
{
    public sealed class Constants
    {
        public const string DefaultAlbum = "© Victorian Electoral Commission";
        public const string DefaultAuthor = "Synthesizer";
        public const string DefaultGenreParty = "Party";
        public const string DefaultGenreElection = "Election";
        public const string DefaultGenreDistrict = "District";
        public const string DefaultGenreRegion = "Region";
        public const string DefaultGenreCandidate = "Candidate";
        public const string DefaultComment = "This is a default synthesized file.";
        public const string DefaultErrorComment = "Unable to synthesize, due to technical issue.";
        public const string DefaultAudioFile = "data/default.mp3";
        public const string GroupPrefix = "Group ";

        public const string EventFileName = "Event.xlsx";
        public const string LocationFileName = "Location.xlsx";
        public const string LocationJsonFileName = "Location.json";
        public const string SpareLocationMapJsonFileName = "SpareLocationMap.json";
        public const string PartyFileName = "Parties.xlsx";
        public const string StaticContentFileName = "StaticContent.xlsx";
        public const string CandidateFileName = "Candidates.xlsx";
        public const string StagingLocationFileName = "StagingLocation.xlsx";
        public const string LabelFileName = "Labels.pdf";
        public const string BundlePath = "bundle/";

        public const string StaticContentFolder = "StaticContent";
        public const string RegionDistrictCandidateFileName = "region_district_candidate_data.txt";
        public const string RegionDistrictTranslationFileName = "region_district_translation_data.txt";
        public const string DistrictConfigFileName = "districtconf.json";
        public const string VersionFileName = "version.json";
        public const string DistrictRegionMapFileName = "DistrictRegionMap.json";
        public const string ReconcileURLFormat = "vVote-VpsTelSummary&rs:Command=Render&rs:Format=IMAGE&EventId={0}&DeviceId={1}&FromDate={2}&ToDate={3}";

        public const string FileService = "FileService.svc";
        public const string VotePackingService = "VotePackingService.svc";
        public const string DataUrl = "data/";
        public const string InternalUrl = "internal/";
        public const string CommitUrl = "commits/";
        public const string VotePackingUrl = @"internal/vote_packing/";
        public const string JointPublicKeyFileName = "peer.jks";
        public const string EvmTemplateFileName = "EVM.zip";
        public const string VpsTemplateFileName = "VPS.zip";
        public const string TrainingEvmTemplateFileName = "TrainingEVM.zip";
        public const string TrainingVpsTemplateFileName = "TrainingVPS.zip";
        public const string EvmClientConfTemplateFileName = "evmClient.conf";
        public const string VpsClientConfTemplateFileName = "vpsClient.conf";
        public const string ClientTemplateFileName = "vvwww.zip";
        public const string EBPublicKeyFileName = "public.bpg";
        public const string EncryptedPaddingPointFileName = "encryptedPaddingPoint.json";
        public const string EvmJksPublicKey = "evm.jks";
        public const string VpsJksPublicKey = "vps.jks";
        public const string CanJksPublicKey = "cancel.jks";
        public const string FmgJksPublicKey = "filemessage.jks";
        public const string ClientTemplateFolderName = "vvoteclient";
        public const string TrainingClientTemplateFolderName = "trainingmode";
        public const string MbbConfigFileName = "MBBConfig.json";
        public const string CertsConfigFileName = "certs.bks";
        public const string QuarantineFileName = "Quarantine.xlsx";
        public const string QuarantineFolderName = "Quarantine";
        public const string QuarantineZipFileName = "Quarantine.zip";
        public const string QuarantineJsonFileName = "Quarantine.json";
        public const string CommitFileName = "commit.json";
        public const string CipherFileFormat = "cipherFile{0}.json";
        public const string Cancel = "cancel";
        public const string CancelAuth = "CancelAuth";
        public const string EvmBlsPublicKey = "evm.bks";
        public const string VpsBlsPublicKey = "vps.bks";
        public const string CanBlsPublicKey = "cancel.bks";
        public const string FmgBlsPublicKey = "filemessage.bks";
        public const string PublicKeyEntry = "pubKeyEntry";
        public const string RaceMap = "race_map.json";

        public const string DisCandidateIdFileName = "LA_candidate_ids_plaintext.json";
        public const string AtlCandidateIdFileName = "LC_ATL_candidate_ids_plaintext.json";
        public const string BtlCandidateIdFileName = "LC_BTL_candidate_ids_plaintext.json";

        public const string MBB_ALL = "MBB_ALL";
        public const string WBB_ALL = "WBB_ALL";
        public const string MSGVOTE_MESSAGE = "MSGVOTE_MESSAGE";
        public const string MSGCANCEL_MESSAGE = "MSGCANCEL_MESSAGE";
        public const string MSGAUDIT_MESSAGE = "MSGAUDIT_MESSAGE";
        public const string MSGPOD_MESSAGE = "MSGPOD_MESSAGE";
        public const string MSGSTARTEVM = "MSGSTARTEVM";
        public const string MSGTELEMETRYINFO = "TELEMETRYINFO";
        public const string MSGCONNFAIL = "CONNFAIL";
        public const string WBB_RESOURCE_REQUEST = "WBB_RESOURCE_REQUEST";

        //TODO: Find out where are the resources in Turing?
        #region Errors (usually these should be in resources
        public const string ERROR_SIGNATURE_NOT_VALID = "Serial signature is not valid.";
        public const string ERROR_IQ_REJECTED = "REJECTED";
        public const string ERROR_DUPLICATE_LOCATION_FILE = "A different location with the same audio file name already exist.";
        public const string ERROR_DUPLICATE_EVENT_FILE = "A different event with the same audio file name already exist.";
        public const string ERROR_DUPLICATE_PARTY_FILE = "A different party with the same audio file name already exist.";
        public const string ERROR_DUPLICATE_STATIC_AUDIO_FILE = "A different property with the same audio file name already exist.";
        public const string ERROR_DUPLICATE_CANDIDATE_FILE = "A different candidate with the same audio file name already exist.";
        public const string ERROR_UNABLE_TO_TRANSFER = "Unable to transfer MP3 file to WBB.";
        public const string ERROR_UNABLE_TO_IMPORT_RESOURCE = "Devices are already staged, resource sheet import is not allowed at this time.";
        public const string ERROR_MISSING_CANDIDATE_DATA = "Unable to import candidate audio file, require Group Description field is missing.";
        #endregion
    }
}
