﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18449
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vec.WbbVotePackingService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Request", Namespace="http://schemas.datacontract.org/2004/07/Vec.Apps.Messages")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Vec.WbbVotePackingService.GetCommitFilesRequest))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Vec.WbbVotePackingService.GetCipherDevicesRequest))]
    public partial class Request : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GetCommitFilesRequest", Namespace="http://schemas.datacontract.org/2004/07/Vec.Apps.Messages")]
    [System.SerializableAttribute()]
    public partial class GetCommitFilesRequest : Vec.WbbVotePackingService.Request {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GetCipherDevicesRequest", Namespace="http://schemas.datacontract.org/2004/07/Vec.Apps.Messages")]
    [System.SerializableAttribute()]
    public partial class GetCipherDevicesRequest : Vec.WbbVotePackingService.Request {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Response", Namespace="http://schemas.datacontract.org/2004/07/Vec.Apps.Messages")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Vec.WbbVotePackingService.GetCommitFilesResponse))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(Vec.WbbVotePackingService.GetCipherDevicesResponse))]
    public partial class Response : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ErrorField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int ErrorCodeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Error {
            get {
                return this.ErrorField;
            }
            set {
                if ((object.ReferenceEquals(this.ErrorField, value) != true)) {
                    this.ErrorField = value;
                    this.RaisePropertyChanged("Error");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ErrorCode {
            get {
                return this.ErrorCodeField;
            }
            set {
                if ((this.ErrorCodeField.Equals(value) != true)) {
                    this.ErrorCodeField = value;
                    this.RaisePropertyChanged("ErrorCode");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GetCommitFilesResponse", Namespace="http://schemas.datacontract.org/2004/07/Vec.Apps.Messages")]
    [System.SerializableAttribute()]
    public partial class GetCommitFilesResponse : Vec.WbbVotePackingService.Response {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private byte[] CommitFileZipField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public byte[] CommitFileZip {
            get {
                return this.CommitFileZipField;
            }
            set {
                if ((object.ReferenceEquals(this.CommitFileZipField, value) != true)) {
                    this.CommitFileZipField = value;
                    this.RaisePropertyChanged("CommitFileZip");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GetCipherDevicesResponse", Namespace="http://schemas.datacontract.org/2004/07/Vec.Apps.Messages")]
    [System.SerializableAttribute()]
    public partial class GetCipherDevicesResponse : Vec.WbbVotePackingService.Response {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int[] DevicesField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int[] Devices {
            get {
                return this.DevicesField;
            }
            set {
                if ((object.ReferenceEquals(this.DevicesField, value) != true)) {
                    this.DevicesField = value;
                    this.RaisePropertyChanged("Devices");
                }
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="WbbVotePackingService.IVotePackingService")]
    public interface IVotePackingService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IVotePackingService/GetCipherDevices", ReplyAction="http://tempuri.org/IVotePackingService/GetCipherDevicesResponse")]
        Vec.WbbVotePackingService.GetCipherDevicesResponse GetCipherDevices(Vec.WbbVotePackingService.GetCipherDevicesRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IVotePackingService/GetCipherDevices", ReplyAction="http://tempuri.org/IVotePackingService/GetCipherDevicesResponse")]
        System.Threading.Tasks.Task<Vec.WbbVotePackingService.GetCipherDevicesResponse> GetCipherDevicesAsync(Vec.WbbVotePackingService.GetCipherDevicesRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IVotePackingService/ExtractCiphers", ReplyAction="http://tempuri.org/IVotePackingService/ExtractCiphersResponse")]
        void ExtractCiphers();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IVotePackingService/ExtractCiphers", ReplyAction="http://tempuri.org/IVotePackingService/ExtractCiphersResponse")]
        System.Threading.Tasks.Task ExtractCiphersAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IVotePackingService/GetCommitFiles", ReplyAction="http://tempuri.org/IVotePackingService/GetCommitFilesResponse")]
        Vec.WbbVotePackingService.GetCommitFilesResponse GetCommitFiles(Vec.WbbVotePackingService.GetCommitFilesRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IVotePackingService/GetCommitFiles", ReplyAction="http://tempuri.org/IVotePackingService/GetCommitFilesResponse")]
        System.Threading.Tasks.Task<Vec.WbbVotePackingService.GetCommitFilesResponse> GetCommitFilesAsync(Vec.WbbVotePackingService.GetCommitFilesRequest request);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IVotePackingServiceChannel : Vec.WbbVotePackingService.IVotePackingService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class VotePackingServiceClient : System.ServiceModel.ClientBase<Vec.WbbVotePackingService.IVotePackingService>, Vec.WbbVotePackingService.IVotePackingService {
        
        public VotePackingServiceClient() {
        }
        
        public VotePackingServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public VotePackingServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public VotePackingServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public VotePackingServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public Vec.WbbVotePackingService.GetCipherDevicesResponse GetCipherDevices(Vec.WbbVotePackingService.GetCipherDevicesRequest request) {
            return base.Channel.GetCipherDevices(request);
        }
        
        public System.Threading.Tasks.Task<Vec.WbbVotePackingService.GetCipherDevicesResponse> GetCipherDevicesAsync(Vec.WbbVotePackingService.GetCipherDevicesRequest request) {
            return base.Channel.GetCipherDevicesAsync(request);
        }
        
        public void ExtractCiphers() {
            base.Channel.ExtractCiphers();
        }
        
        public System.Threading.Tasks.Task ExtractCiphersAsync() {
            return base.Channel.ExtractCiphersAsync();
        }
        
        public Vec.WbbVotePackingService.GetCommitFilesResponse GetCommitFiles(Vec.WbbVotePackingService.GetCommitFilesRequest request) {
            return base.Channel.GetCommitFiles(request);
        }
        
        public System.Threading.Tasks.Task<Vec.WbbVotePackingService.GetCommitFilesResponse> GetCommitFilesAsync(Vec.WbbVotePackingService.GetCommitFilesRequest request) {
            return base.Channel.GetCommitFilesAsync(request);
        }
    }
}
