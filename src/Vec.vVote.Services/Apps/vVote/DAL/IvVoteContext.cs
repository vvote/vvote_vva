﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Data.Entity;

namespace Vec.Apps.vVote.DAL
{
    public interface IvVoteContext
    {
        IDbSet<CandidateAudio> CandidateAudios { get; set; }
        IDbSet<EventLocation> EventLocations { get; set; }
        IDbSet<EventAudio> EventAudios { get; set; }
        IDbSet<EventDevice> EventDevices { get; set; }
        IDbSet<EventElection> EventElections { get; set; }
        IDbSet<EventMBB> EventMBBs { get; set; }
        IDbSet<Event> Events { get; set; }
        IDbSet<EventText> EventTexts { get; set; }
        IDbSet<Language> Languages { get; set; }
        IDbSet<Party> Parties { get; set; }
        IDbSet<PartyAudio> PartyAudios { get; set; }
        IDbSet<PartyText> PartyTexts { get; set; }
        IDbSet<LocationAudio> LocationAudios { get; set; }
        IDbSet<StaticAudio> StaticAudios { get; set; }
        IDbSet<StaticText> StaticTexts { get; set; }
        IDbSet<EventStaticAudio> EventStaticAudios { get; set; }
        IDbSet<EventStaticText> EventStaticTexts { get; set; }
        IDbSet<PackageCommit> PackageCommits { get; set; }
        IDbSet<DeviceMessage> DeviceMessages { get; set; }
        IDbSet<TelemetryWBB> TelemetryWBBs { get; set; }
        IDbSet<TelemetryMBB> TelemetryMBBs { get; set; }
        IDbSet<TelemetryMBBMessage> TelemetryMBBMessages { get; set; }
        IDbSet<Telemetry> Telemetries { get; set; }
        IDbSet<CommitFile> CommitFiles { get; set; }
        IDbSet<ProcessedCommit> ProcessedCommits { get; set; }

        int SaveChanges();

        int usp_Vec_vVote_UpdateTelemetryStatus(string level, string logger, string message, Nullable<Guid> eventId);
    }
}
