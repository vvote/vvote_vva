//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vec.Apps.vVote.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class EventStaticText
    {
        public EventStaticText()
        {
            this.StaticTexts = new HashSet<StaticText>();
        }
    
        public int Id { get; set; }
        public System.Guid EventId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SizeLimit { get; set; }
        public string ConversionLabel { get; set; }
    
        public virtual Event Event { get; set; }
        public virtual ICollection<StaticText> StaticTexts { get; set; }
    }
}
