//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vec.Apps.vVote.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class PartyText
    {
        public int Id { get; set; }
        public int PartyId { get; set; }
        public string Name { get; set; }
        public int LanguageId { get; set; }
        public string Error { get; set; }
        public Nullable<System.DateTime> DateTimeStamp { get; set; }
    
        public virtual Language Language { get; set; }
        public virtual Party Party { get; set; }
    }
}
