//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vec.Apps.vVote.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class TelemetryWBB
    {
        public long Id { get; set; }
        public System.Guid EventId { get; set; }
        public System.DateTime DateTime { get; set; }
        public string Level { get; set; }
        public string From { get; set; }
        public string Resource { get; set; }
        public int Status { get; set; }
    
        public virtual Event Event { get; set; }
    }
}
