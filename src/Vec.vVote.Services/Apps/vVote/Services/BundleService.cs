﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using HttpMultipartParser;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web.Hosting;
using Vec.Apps.vVote.BL;
using Vec.Apps.vVote.BL.Common;
using Vec.Apps.vVote.Common;
using Vec.Apps.vVote.Entities;
using Vec.Apps.vVote.Messages;
using Vec.Properties;
using Vec.Resources;

namespace Vec.Apps.vVote.Services
{

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class BundleService : IBundleService
    {
        public IBundleBL BundleBL { get; set; }

        public BundleService(IBundleBL bundleBL, IFileServiceHelper fileService)
        {
            BundleBL = bundleBL;
        }

        [WebInvoke(Method = "GET", UriTemplate = "bundle?eventId={eventId}")]
        public Stream GetBundle(Guid eventId)
        {
            //TODO: Do not return the bundle for download, Uploading to WBB is sufficient.
            MemoryStream response = null;
            if (!Directory.Exists(Settings.Default.TempWorkingDirectory))
                Directory.CreateDirectory(Settings.Default.TempWorkingDirectory);

            string contextId = Guid.NewGuid().ToString();
            string contextPath = Path.Combine(Settings.Default.TempWorkingDirectory, contextId);

            try
            {
                byte[] fileBytes  = BundleBL.GenerateEventBundle(eventId, contextPath);
                response = new MemoryStream(fileBytes);
            }
            finally
            {
                File.Delete(contextPath + ".zip");
            }

            WebOperationContext.Current.OutgoingResponse.Headers.Add("content-disposition", "attachment; filename=bundle.zip");
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/zip";

            return response;
        }

        [WebInvoke(Method = "POST", UriTemplate = "bundle", ResponseFormat = WebMessageFormat.Json)]
        public string UploadBundle(Stream formContent)
        {
            Guid context = Guid.NewGuid();
            JObject response = new JObject();
            response.Add("Context", context);
            response.Add("Message", "Bundle Uploaded to WBB");
            response.Add("Error", string.Empty);
            DirectoryInfo contextDirectory = Directory.CreateDirectory(Path.Combine(Settings.Default.TempWorkingDirectory, context.ToString()));
            try
            {
                var parser = new MultipartFormDataParser(formContent);
                var file = parser.Files.First();
                string filename = file.FileName;
                bool isFinal = bool.Parse(parser.Parameters["IsFinal"].Data);
                Guid eventId = Guid.Parse(parser.Parameters["EventId"].Data);

                Zipper.Extract(file.Data, contextDirectory.FullName);

                BundleBL.UploadBundle(eventId, isFinal, contextDirectory);
            }
            catch (Exception ex)
            {
                response["Error"] = ex.Message;
            }
            finally
            {
                File.Delete(contextDirectory.FullName + ".zip");
            }
            return response.ToString();
        }

        [WebInvoke(Method = "GET", UriTemplate = "staging-location?eventId={eventId}", ResponseFormat = WebMessageFormat.Json)]
        public GetStagingLocationResponse GetStagingLocation(Guid eventId)
        {
            GetStagingLocationResponse response = new GetStagingLocationResponse();

            response.Locations = BundleBL.GetStagingLocation(eventId);

            return response;
        }

        [WebInvoke(Method = "POST", UriTemplate = "staging-location", ResponseFormat = WebMessageFormat.Json)]
        public string UpdateStagingLocation(Stream formContent)
        {
            Guid context = Guid.NewGuid();
            JObject response = new JObject();
            response.Add("Context", context);
            response.Add("Message", "Staging devices setup completed.");
            response.Add("Error", string.Empty);
            DirectoryInfo contextDirectory = Directory.CreateDirectory(Path.Combine(Settings.Default.TempWorkingDirectory, context.ToString()));
            try
            {
                var parser = new MultipartFormDataParser(formContent);
                var file = parser.Files.First();
                Guid eventId = Guid.Parse(parser.Parameters["EventId"].Data);
                byte[] fileBytes = null;
                using (MemoryStream ms = new MemoryStream())
                {
                    file.Data.CopyTo(ms);
                    fileBytes = ms.ToArray();
                }

                File.WriteAllBytes(Path.Combine(contextDirectory.FullName, Constants.StagingLocationFileName), fileBytes);

                BundleBL.UpdateStagingLocation(eventId,contextDirectory);
            }
            catch (Exception ex)
            {
                response["Error"] = ex.Message;
            }
            finally
            {
                if (contextDirectory.Exists)
                    contextDirectory.Delete(true);
            }
            return response.ToString();
        }

        [WebInvoke(Method = "GET", UriTemplate = "device-config?eventDeviceId={eventDeviceId}")]
        public Stream GetDeviceConfig(int eventDeviceId)
        {
            EventDeviceEntity device = BundleBL.GetDeviceConfig(eventDeviceId);

            MemoryStream stream = new MemoryStream();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(EventDeviceEntity));
            ser.WriteObject(stream, device);
            stream.Position = 0;

            WebOperationContext.Current.OutgoingResponse.Headers.Add("content-disposition", string.Format("attachment; filename={0}.json", device.DeviceId));
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json";
            return stream;
        }

        [WebInvoke(Method = "GET", UriTemplate = "static-contents?eventId={eventId}")]
        public Stream GetStaticContents(Guid eventId)
        {
            MemoryStream response = null;

            string contextId = Guid.NewGuid().ToString();
            string contextPath = Path.Combine(Settings.Default.TempWorkingDirectory, contextId);
            string rootPath = Path.Combine(contextPath, Constants.StaticContentFolder);
            
            try
            {
                BundleBL.GenerateStaticContentBundle(eventId, rootPath);

                Zipper.Archive(rootPath, string.Empty);

                byte[] fileBytes = File.ReadAllBytes(rootPath + ".zip");
                //ITransfer trans = TransferBase.GetTransfer<FtpTransfer>();
                //trans.Put(Settings.Default.DraftBundleUrl, fileBytes);
                response = new MemoryStream(fileBytes);
            }
            finally
            {
                Directory.Delete(contextPath, true);
            }

            WebOperationContext.Current.OutgoingResponse.Headers.Add("content-disposition", "attachment; filename=StaticContent.zip");
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/zip";

            return response;
        }
    }
}
