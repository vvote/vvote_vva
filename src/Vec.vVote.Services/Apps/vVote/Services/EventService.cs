﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using HttpMultipartParser;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using Vec.Apps.vVote.BL;
using Vec.Apps.vVote.Common;
using Vec.Apps.vVote.DAL;
using Vec.Apps.vVote.Entities;
using Vec.Apps.vVote.Messages;
using Vec.Apps.vVote.Reader;
using Vec.Properties;
using Vec.Resources;
using Vec.TuringElectionService;
using Vec.TuringNominationService;

namespace Vec.Apps.vVote.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class EventService : IEventService
    {
        public IEventBL EventBL { get; set; }
        public IElectionBL ElectionBL { get; set; }
        public EventService(IEventBL eventBL, IElectionBL electionBL)
        {
            EventBL = eventBL;
            ElectionBL = electionBL;
        }

        [WebInvoke(Method = "GET", UriTemplate = "events?eventId={eventId}", ResponseFormat = WebMessageFormat.Json)]
        public EventEntity GetEvent(Guid eventId)
        {
            return EventBL.GetEvent(eventId);
        }

        [WebInvoke(Method = "GET", UriTemplate = "mbb-settings?eventId={eventId}", ResponseFormat = WebMessageFormat.Json)]
        public GetMbbSettingsResponse GetMbbSettings(Guid eventId)
        {
            GetMbbSettingsResponse response = new GetMbbSettingsResponse();
            var eve = GetEvent(eventId);
            response.MBBInternetProtocols = eve.MBBInternetProtocols;            
            response.MBBTimeout = eve.MBBTimeout;
            response.MBBPubKeyPath = eve.MBBPubKeyPath;
            response.WBBPublicKey = eve.WBBPublicKey;

            return response;
        }

        [WebInvoke(Method = "GET", UriTemplate = "template-settings?eventId={eventId}", ResponseFormat = WebMessageFormat.Json)]
        public GetTemplateSettingsResponse GetTemplateSettings(Guid eventId)
        {
            GetTemplateSettingsResponse response = new GetTemplateSettingsResponse();
            response.Template = EventBL.GetTemplateSettings(eventId);

            return response;
        }

        [WebInvoke(Method = "POST", UriTemplate = "template-settings", ResponseFormat = WebMessageFormat.Json)]
        public UpdateTemplateSettingsResponse UpdateTemplateSettings(Stream request)
        {
            UpdateTemplateSettingsResponse response = new UpdateTemplateSettingsResponse();
            var parser = new MultipartFormDataParser(request);
            var eventId = Guid.Parse(parser.Parameters["EventId"].Data);
            var files = new Dictionary<string, byte[]>();

            files.Add(Constants.EvmTemplateFileName, parser.Files.Single(f => f.Name == "EvmTemplate").Data.GetBytes());
            files.Add(Constants.VpsTemplateFileName, parser.Files.Single(f => f.Name == "VpsTemplate").Data.GetBytes());
            files.Add(Constants.TrainingEvmTemplateFileName, parser.Files.Single(f => f.Name == "TrainingEvmTemplate").Data.GetBytes());
            files.Add(Constants.TrainingVpsTemplateFileName, parser.Files.Single(f => f.Name == "TrainingVpsTemplate").Data.GetBytes());
            files.Add(Constants.EvmClientConfTemplateFileName, parser.Files.Single(f => f.Name == "EvmClientConfTemplate").Data.GetBytes());
            files.Add(Constants.VpsClientConfTemplateFileName, parser.Files.Single(f => f.Name == "VpsClientConfTemplate").Data.GetBytes());
            files.Add(Constants.ClientTemplateFileName, parser.Files.Single(f => f.Name == "ClientTemplate").Data.GetBytes());
            files.Add(Constants.EBPublicKeyFileName, parser.Files.Single(f => f.Name == "EBPublicKey").Data.GetBytes());
            files.Add(Constants.MbbConfigFileName, parser.Files.Single(f => f.Name == "MBBConfigFile").Data.GetBytes());
            files.Add(Constants.CertsConfigFileName, parser.Files.Single(f => f.Name == "CertsBksFile").Data.GetBytes());

            response.Template = EventBL.UpdateTemplateSettings(eventId, files);

            return response;
        }

        [WebInvoke(Method = "POST", UriTemplate = "mbb-settings", ResponseFormat = WebMessageFormat.Json)]
        public UpdateMbbSettingsResponse UpdateMbbSettings(Stream request)
        {
            UpdateMbbSettingsResponse response = new UpdateMbbSettingsResponse();
            var parser = new MultipartFormDataParser(request);

            byte[] jointPublicKeyFile = null;
            using (MemoryStream ms = new MemoryStream())
            {
                parser.Files.First().Data.CopyTo(ms);
                jointPublicKeyFile = ms.ToArray();
            }
            var mbbPubKeyPath = parser.Parameters["MBBPubKeyPath"].Data;
            var mbbTimeout = Convert.ToInt32(parser.Parameters["MBBTimeout"].Data);
            var mbbInternetProtocols = JArray.Parse(parser.Parameters["MBBInternetProtocols"].Data).ToObject<List<MBBInternetProtocol>>();
            var wbbPublicKey = parser.Parameters["WBBPublicKey"].Data;
            var eventId = Guid.Parse(parser.Parameters["EventId"].Data);

            EventBL.UpdateMbbSettings(eventId, mbbInternetProtocols, mbbPubKeyPath, mbbTimeout, jointPublicKeyFile, wbbPublicKey);

            response.MBBInternetProtocols = EventBL.GetMbbSettings(eventId);
            response.MBBTimeout = mbbTimeout;
            response.MBBPubKeyPath = mbbPubKeyPath;
            response.WBBPublicKey = wbbPublicKey;

            return response;
        }

        [WebInvoke(Method = "GET", UriTemplate = "mbb-settings-bundle?eventId={eventId}")]
        public Stream GenerateMbbSettingsBundle(Guid eventId)
        {
            MemoryStream response = null;
            string contextPath = Path.Combine(Settings.Default.TempWorkingDirectory, Guid.NewGuid().ToString());
            
            if (!Directory.Exists(contextPath))
                Directory.CreateDirectory(contextPath);

            try
            {
                EventBL.GenerateMbbSettingsBundle(eventId, contextPath);

                #region zip content...
                Zipper.Archive(contextPath, string.Empty);
                #endregion zip content...

                byte[] fileBytes = File.ReadAllBytes(contextPath + ".zip");

                response = new MemoryStream(fileBytes);
            }
            finally
            {
                File.Delete(contextPath + ".zip");
            }

            WebOperationContext.Current.OutgoingResponse.Headers.Add("content-disposition", "attachment; filename=mbbSettings.zip");
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/zip";

            return response;
        }

        [WebInvoke(Method = "POST", UriTemplate = "process-public-key", ResponseFormat = WebMessageFormat.Json)]
        public ProcessPublicSigningKeyResponse ProcessPublicSigningKey(Stream request)
        {
            Guid context = Guid.NewGuid();
            ProcessPublicSigningKeyResponse response = new ProcessPublicSigningKeyResponse() { Context = context };
            string contextPath = Path.Combine(Settings.Default.TempWorkingDirectory, context.ToString());

            if (!Directory.Exists(contextPath))
                Directory.CreateDirectory(contextPath);

            var parser = new MultipartFormDataParser(request);

            var eventId = Guid.Parse(parser.Parameters["EventId"].Data);
            Zipper.Extract(parser.Files.First().Data, contextPath);

            EventBL.ProcessPublicSigningKey(eventId, contextPath);

            #region zip content...
            Zipper.Archive(contextPath, string.Empty);
            #endregion zip content...
            
            return response;
        }

        [WebInvoke(Method = "GET", UriTemplate = "process-public-key?context={context}")]
        public Stream GetProcessedPublicSigningKey(Guid context)
        {
            MemoryStream response = null;
            string contextPath = Path.Combine(Settings.Default.TempWorkingDirectory, context.ToString());

            try
            {
                byte[] fileBytes = File.ReadAllBytes(contextPath + ".zip");

                response = new MemoryStream(fileBytes);
            }
            finally
            {
                File.Delete(contextPath + ".zip");
            }

            WebOperationContext.Current.OutgoingResponse.Headers.Add("content-disposition", "attachment; filename=PublicSigningKeys.zip");
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/zip";

            return response;
        }

        [WebInvoke(Method = "GET", UriTemplate = "events", ResponseFormat = WebMessageFormat.Json)]
        public List<EventEntity> GetEvents()
        {
            return EventBL.GetEvents();
        }

        [WebInvoke(Method = "GET", UriTemplate = "event-config?eventId={eventId}", ResponseFormat = WebMessageFormat.Json)]
        public EventConfig GetEventConfig(Guid eventId)
        {
            Guid context = Guid.NewGuid();
            DirectoryInfo contextDirectory = Directory.CreateDirectory(Path.Combine(Settings.Default.TempWorkingDirectory, context.ToString()));

            try
            {
                return EventBL.GetEventConfig(eventId, contextDirectory);
            }
            finally
            {
                if (contextDirectory.Exists)
                    contextDirectory.Delete(true);
            }
        }

        [WebInvoke(Method = "GET", UriTemplate = "election?electionType={electionType}&electionCategory={electionCategory}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public virtual List<ElectionEntity> GetElections(int electionType, int electionCategory)
        {
            List<ElectionEntity> result = new List<ElectionEntity>();

            foreach (var electionItem in ElectionBL.GetElections((ElectionType)electionType))
            {
                result.Add(new ElectionEntity()
                {
                    Id = electionItem.Id,
                    Name = electionItem.Name,
                    ElectionType = (int)electionItem.ElectionType,
                    ElectionDate = electionItem.DateRanges.Single(dr => dr.DateRangeType == ElectionDateRangeType.ElectionDay).StartDateTime.Value.ToLongDateString(),
                });
            }
            return result;
        }

        [WebInvoke(Method = "POST", UriTemplate = "create-event", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public CreateEventResponse CreateEvent(CreateEventRequest request)
        {
            CreateEventResponse response = new CreateEventResponse();

            var electionIds = from election in request.Elections
                              select election.Id;
            try
            {
                response.EventId = EventBL.CreateEvent(electionIds.ToList(), request.Force);
            }
            catch (ArgumentException ae)
            {
                response.ErrorCode = 1;
                response.Error = ae.Message;
            }

            return response;
        }

        [WebInvoke(Method = "POST", UriTemplate = "update-event", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public EventEntity UpdateEvent(EventEntity eventEntity)
        {
            return EventBL.UpdateEvent(eventEntity);
        }

        [WebInvoke(Method = "GET", UriTemplate = "parties?eventId={eventId}", ResponseFormat = WebMessageFormat.Json)]
        public List<PartyEntity> GetParties(Guid eventId)
        {
            DirectoryInfo contextDirectory = Directory.CreateDirectory(Path.Combine(Settings.Default.TempWorkingDirectory, Guid.NewGuid().ToString()));
            try
            {
                return EventBL.GetParties(eventId, contextDirectory);
            }
            finally
            {
                contextDirectory.Delete(true);
            }
        }

        [WebInvoke(Method = "GET", UriTemplate = "staticContents?eventId={eventId}", ResponseFormat = WebMessageFormat.Json)]
        public StaticContentsEntity GetStaticContents(Guid eventId)
        {
            DirectoryInfo contextDirectory = Directory.CreateDirectory(Path.Combine(Settings.Default.TempWorkingDirectory, Guid.NewGuid().ToString()));
            try
            {
                return EventBL.GetStaticContents(eventId, contextDirectory);
            }
            finally
            {
                contextDirectory.Delete(true);
            }
        }

        [WebInvoke(Method = "GET", UriTemplate = "candidates?eventId={eventId}", ResponseFormat = WebMessageFormat.Json)]
        public List<CandidateEntity> GetCandidates(Guid eventId)
        {
            DirectoryInfo contextDirectory = Directory.CreateDirectory(Path.Combine(Settings.Default.TempWorkingDirectory, Guid.NewGuid().ToString()));
            try
            {
                return EventBL.GetCandidates(eventId, contextDirectory);
            }
            finally
            {
                contextDirectory.Delete(true);
            }
        }

        [WebInvoke(Method = "GET", UriTemplate = "locations?eventId={eventId}", ResponseFormat = WebMessageFormat.Json)]
        public List<LocationEntity> GetLocations(Guid eventId)
        {
            DirectoryInfo contextDirectory = Directory.CreateDirectory(Path.Combine(Settings.Default.TempWorkingDirectory, Guid.NewGuid().ToString()));
            try
            {
                return EventBL.GetLocations(eventId, contextDirectory);
            }
            finally
            {
                contextDirectory.Delete(true);
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "pre-export-candidates?eventId={eventId}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public string PrepareExportCandidates(Guid eventId, List<CandidateEntity> request)
        {
            Guid context = Guid.NewGuid();
            DirectoryInfo contextDirectory = Directory.CreateDirectory(Path.Combine(Settings.Default.TempWorkingDirectory, context.ToString()));
            try
            {
                EventBL.PrepareExportCandidates(eventId, request, contextDirectory);
            }
            finally
            {
                if (contextDirectory.Exists)
                    contextDirectory.Delete(true);
            }
            return context.ToString();
        }

        [WebInvoke(Method = "POST", UriTemplate = "pre-export-locations?eventId={eventId}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public string PrepareExportLocations(Guid eventId, List<LocationEntity> request)
        {
            Guid context = Guid.NewGuid();
            DirectoryInfo contextDirectory = Directory.CreateDirectory(Path.Combine(Settings.Default.TempWorkingDirectory, context.ToString()));
            try
            {
                EventBL.PrepareExportLocations(eventId, request, contextDirectory);
            }
            finally
            {
                if (contextDirectory.Exists)
                    contextDirectory.Delete(true);
            }
            return context.ToString();
        }

        [WebInvoke(Method = "POST", UriTemplate = "pre-export-parties?eventId={eventId}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public string PrepareExportParties(Guid eventId, List<PartyEntity> request)
        {
            Guid context = Guid.NewGuid();
            DirectoryInfo contextDirectory = Directory.CreateDirectory(Path.Combine(Settings.Default.TempWorkingDirectory, context.ToString()));
            try
            {
                EventBL.PrepareExportParties(eventId, request, contextDirectory);
            }
            finally
            {
                if (contextDirectory.Exists)
                    contextDirectory.Delete(true);
            }
            return context.ToString();
        }

        [WebInvoke(Method = "POST", UriTemplate = "pre-export-staticContents", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public PrepareExportStaticContentsResponse PrepareExportStaticContents(PrepareExportStaticContentsRequest request)
        {
            Guid context = Guid.NewGuid();
            PrepareExportStaticContentsResponse response = new PrepareExportStaticContentsResponse() { Context = context };

            DirectoryInfo contextDirectory = Directory.CreateDirectory(Path.Combine(Settings.Default.TempWorkingDirectory, context.ToString()));
            try
            {
                EventBL.PrepareExportStaticContents(request.EventId, contextDirectory);
            }
            catch (Exception ex)
            {
                response.ErrorCode = 1;
                response.Error = ex.Message;
            }
            finally
            {
                if (contextDirectory.Exists)
                    contextDirectory.Delete(true);
            }
            return response;
        }

        [WebInvoke(Method = "POST", UriTemplate = "pre-export-devices", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public PrepareExportDevicesResponse PrepareExportDevices(PrepareExportDevicesRequest request)
        {
            Guid context = Guid.NewGuid();
            PrepareExportDevicesResponse response = new PrepareExportDevicesResponse() { Context = context };

            DirectoryInfo contextDirectory = Directory.CreateDirectory(Path.Combine(Settings.Default.TempWorkingDirectory, context.ToString()));
            try
            {
                EventBL.PrepareExportDevices(request.EventId, request.Devices, contextDirectory);
            }
            finally
            {
                if (contextDirectory.Exists)
                    contextDirectory.Delete(true);
            }
            return response;
        }

        [WebInvoke(Method = "POST", UriTemplate = "pre-export-template", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public PrepareExportTemplateResponse PrepareExportTemplate(PrepareExportTemplateRequest request)
        {
            Guid context = Guid.NewGuid();
            PrepareExportTemplateResponse response = new PrepareExportTemplateResponse() { Context = context };
            DirectoryInfo contextDirectory = Directory.CreateDirectory(Path.Combine(Settings.Default.TempWorkingDirectory, context.ToString()));
            try
            {
                EventBL.PrepareExportTemplate(request.DeviceType, request.EventId, contextDirectory);
            }
            finally
            {
                if (contextDirectory.Exists)
                    contextDirectory.Delete(true);
            }
            return response;
        }

        [WebInvoke(Method = "POST", UriTemplate = "pre-export-event?eventId={eventId}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public string PrepareExportEvent(Guid eventId)
        {
            Guid context = Guid.NewGuid();
            DirectoryInfo contextDirectory = Directory.CreateDirectory(Path.Combine(Settings.Default.TempWorkingDirectory, context.ToString()));

            try
            {
                EventBL.PrepareExportEvent(eventId, contextDirectory);
            }
            finally
            {
                if (contextDirectory.Exists)
                    contextDirectory.Delete(true);
            }

            return context.ToString();
        }

        [WebInvoke(Method = "GET", UriTemplate = "export?context={context}&type={type}")]
        public Stream Export(Guid context, EntityType type)
        {
            string filePath = Path.Combine(Settings.Default.TempWorkingDirectory, context + ".zip");
            MemoryStream response;
            try
            {
                string fileName = string.Empty;
                switch (type)
                {
                    case EntityType.Event:
                        fileName = "Event.zip";
                        break;
                    case EntityType.Party:
                        fileName = "Parties.zip";
                        break;
                    case EntityType.Candidate:
                        fileName = "Candidates.zip";
                        break;
                    case EntityType.Location:
                        fileName = "Locations.zip";
                        break;
                    case EntityType.StaticContent:
                        fileName = "StaticContents.zip";
                        break;
                    case EntityType.Device:
                        fileName = "Devices.zip";
                        break;
                    case EntityType.EvmTemplate:
                        fileName = "EVM.zip";
                        break;
                    case EntityType.VpsTemplate:
                        fileName = "VPS.zip";
                        break;
                    default:
                        break;
                }

                WebOperationContext.Current.OutgoingResponse.Headers.Add("content-disposition", "attachment; filename=" + fileName);
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/zip";

                response = new MemoryStream(File.ReadAllBytes(filePath));
                return response;
            }
            finally
            {
                File.Delete(filePath);
            }
        }

        [WebInvoke(Method = "POST", UriTemplate = "import", ResponseFormat = WebMessageFormat.Json)]
        public string Import(Stream formContent)
        {
            Guid context = Guid.NewGuid();
            JObject response = new JObject();
            response.Add("Context", context);
            response.Add("Error", string.Empty);
            DirectoryInfo contextDirectory = Directory.CreateDirectory(Path.Combine(Settings.Default.TempWorkingDirectory, context.ToString()));
            try
            {
                var parser = new MultipartFormDataParser(formContent);
                var file = parser.Files.First();
                string filename = file.FileName;
                var entityType = (EntityType)Enum.Parse(typeof(EntityType), parser.Parameters["Type"].Data);
                Guid eventId = Guid.Parse(parser.Parameters["EventId"].Data);

                Zipper.Extract(file.Data, contextDirectory.FullName);
                switch (entityType)
                {
                    case EntityType.Event:
                        EventBL.UpdateEvent(contextDirectory);
                        break;
                    case EntityType.Party:
                        EventBL.UpdateParty(eventId, contextDirectory);
                        break;
                    case EntityType.Candidate:
                        EventBL.UpdateCandidate(eventId, contextDirectory);
                        break;
                    case EntityType.Location:
                        EventBL.UpdateLocation(eventId, contextDirectory);
                        break;
                    case EntityType.StaticContent:
                        EventBL.UpdateStaticContent(eventId, contextDirectory);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                response["Error"] = ex.Message;
            }
            finally
            {
                contextDirectory.Delete(true);
            }

            return response.ToString();
        }

        [WebInvoke(Method = "GET", UriTemplate = "devices?eventId={eventId}", ResponseFormat = WebMessageFormat.Json)]
        public List<EventDeviceEntity> GetEventDevices(Guid eventId)
        {
            return EventBL.GetEventDevices(eventId);
        }

        [WebInvoke(Method = "GET", UriTemplate = "monitored-devices?eventId={eventId}", ResponseFormat = WebMessageFormat.Json)]
        public GetMonitoredDevicesResponse GetMonitoredDevices(Guid eventId)
        {
            GetMonitoredDevicesResponse response = new GetMonitoredDevicesResponse();
            response.MonitoredDevices = EventBL.GetMonitoredDevices(eventId);
            return response;
        }

        [WebInvoke(Method = "POST", UriTemplate = "update-event-device")]
        public void UpdateEventDevice(EventDeviceEntity eventDevice)
        {
            EventBL.UpdateEventDevice(eventDevice);
        }

        [WebInvoke(Method = "GET", UriTemplate = "export-event-device-labels?eventId={eventId}")]
        public Stream ExportEventDeviceLabels(Guid eventId)
        {
            Guid context = Guid.NewGuid();
            DirectoryInfo contextDirectory = Directory.CreateDirectory(Path.Combine(Settings.Default.TempWorkingDirectory, context.ToString()));

            MemoryStream response;
            try
            {
                string filePath = EventBL.ExportEventDeviceLabels(eventId, contextDirectory);
                response = new MemoryStream(File.ReadAllBytes(filePath));
            }
            finally
            {
                if (contextDirectory.Exists)
                    contextDirectory.Delete(true);
            }

            WebOperationContext.Current.OutgoingResponse.Headers.Add("content-disposition", "attachment; filename=" + Constants.LabelFileName);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/pdf";

            return response;
        }

        [WebInvoke(Method = "POST", UriTemplate = "quarantine", ResponseFormat = WebMessageFormat.Json)]
        public string ImportQuarantine(Stream formContent)
        {
            Guid context = Guid.NewGuid();
            JObject response = new JObject();
            response.Add("Error", string.Empty);
            DirectoryInfo contextDirectory = Directory.CreateDirectory(Path.Combine(Settings.Default.TempWorkingDirectory, context.ToString()));

            try
            {
                var parser = new MultipartFormDataParser(formContent);
                var qFile = parser.Files.Single(f => f.Name == "QuarantineFile");
                string qFileName = Path.Combine(contextDirectory.FullName, qFile.FileName);
                using (MemoryStream ms = new MemoryStream())
                {
                    qFile.Data.CopyTo(ms);
                    File.WriteAllBytes(qFileName, ms.ToArray());
                }
                var sFile = parser.Files.Single(f => f.Name == "SignatureFile");
                string sFileName = Path.Combine(contextDirectory.FullName, qFile.FileName + ".bpg");
                using (MemoryStream ms = new MemoryStream())
                {
                    sFile.Data.CopyTo(ms);
                    File.WriteAllBytes(sFileName, ms.ToArray());
                }
                Guid eventId = Guid.Parse(parser.Parameters["EventId"].Data);

                string uploadUrl = EventBL.UploadQuarantine(eventId, qFileName, sFileName);
                response.Add("Message", "Spreadsheet is validated and uploaded to WBB: " + uploadUrl);
            }
            catch (NullReferenceException)
            {
                response["Error"] = "Unable to verify, the signature file is invalid.";
            }
            catch (Exception ex)
            {
                response["Error"] = ex.Message;
            }
            finally
            {
                contextDirectory.Delete(true);
            }

            return response.ToString();
        }
    }
}