﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.vVote.BL;
using Vec.Apps.vVote.Messages;

namespace Vec.Apps.vVote.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class TelemetryService : ITelemetryService
    {
        public ITelemetryBL TelemetryBL { get; set; }

        public TelemetryService(ITelemetryBL telemetryBL)
        {
            TelemetryBL = telemetryBL;
        }

        [WebInvoke(Method = "GET", UriTemplate = "DeviceReconcile?eveId={eveId}&devId={devId}&fDate={fDate}&tDate={tDate}")]
        public Stream GetDeviceReconcile(Guid eveId, string devId, string fDate, string tDate)
        {
            byte[] image = TelemetryBL.GetDeviceReconcile(eveId, devId, fDate, tDate);

            WebOperationContext.Current.OutgoingResponse.Headers.Add("content-disposition", string.Format("attachment; filename={0}.png", devId));
            WebOperationContext.Current.OutgoingResponse.ContentType = "image/png";

            return new MemoryStream(image);
        }

        [WebInvoke(Method = "POST", UriTemplate = "mbb-telemetry", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public GetMBBTelemetryDataResponse GetMBBTelemetryData(GetMBBTelemetryDataRequest request)
        {
            GetMBBTelemetryDataResponse response = new GetMBBTelemetryDataResponse();
            response.Records = TelemetryBL.GetMBBTelemetryData(request.EventId, request.RecordCount).ToList();

            return response;
        }

        [WebInvoke(Method = "POST", UriTemplate = "wbb-telemetry", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public GetWBBTelemetryDataResponse GetWBBTelemetryData(GetWBBTelemetryDataRequest request)
        {
            GetWBBTelemetryDataResponse response = new GetWBBTelemetryDataResponse();
            response.Records = TelemetryBL.GetWBBTelemetryData(request.EventId, request.RecordCount).ToList();

            return response;
        }
    }
}
