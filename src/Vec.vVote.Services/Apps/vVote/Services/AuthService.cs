﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.vVote.BL;
using Vec.Apps.vVote.Messages;

namespace Vec.Apps.vVote.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AuthService : IAuthService
    {
        public IAuthBL AuthBL { get; set; }

        public AuthService(IAuthBL authBL)
        {
            AuthBL = authBL;
        }

        [WebInvoke(Method = "POST", UriTemplate = "AuthMessage", ResponseFormat = WebMessageFormat.Json)]
        public AuthMessageResponse AuthMessage(AuthMessageRequest request)
        {
            AuthMessageResponse response = new AuthMessageResponse();

            try
            {
                response.Auth = AuthBL.AuthMessage(request.Quarantine);
            }
            catch (Exception ex)
            {
                response.ErrorCode = 1;
                response.Error = ex.Message;
                response.Auth = AuthBL.AuthMessage(ex.Message, request.Quarantine.EventId);
            }

            return response;
        }
    }
}
