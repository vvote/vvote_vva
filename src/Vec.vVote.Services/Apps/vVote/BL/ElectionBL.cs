﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.vVote.DAL;
using Vec.Apps.vVote.Entities;
using Vec.Properties;
using Vec.TuringElectionService;
using Vec.TuringNominationService;

namespace Vec.Apps.vVote.BL
{
    public interface IElectionBL
    {
        List<MElectionLookup> GetElections(ElectionType electionType, ElectionCategory electionCategory = ElectionCategory.Parliamentary);

        List<MElectionLookup> GetElections(List<Guid> electionIds);

        List<MElectorateLookup> GetRaces(List<Guid> electionIds, ElectionElectorateVacancyStatus? status);

        List<MLocationLookup> GetElectionLocations(List<Guid> electionIds);

        List<CandidatesInNominationOrderForElectionElectorateVacancyEntity> GetNominations(List<Guid> electorateVacencyIds);

        List<BallotDrawnCandidatesForElectionElectorateVacancyEntity> BallotDrawCandidates(List<Guid> electorateVacencyIds);

        List<BallotDrawnGroupsForElectionElectorateVacancy> BallotDrawGroups(List<Guid> electorateVacencyIds);
    }

    public class ElectionBL : BaseBL, IElectionBL
    {
        public IElectionLookup ElectionLookup { get; set; }
        public INominationsExternalService NominationLookup { get; set; }
        public IvVoteContext vVoteContext { get; set; }

        public ElectionBL(IElectionLookup electionLookup, INominationsExternalService nominationsLookup, IvVoteContext vVoteContext)
        {
            this.ElectionLookup = electionLookup;
            this.NominationLookup = nominationsLookup;
            this.vVoteContext = vVoteContext;

            Logger.Info("ElectionBL instantiated complete.");
        }

        public virtual List<MElectionLookup> GetElections(ElectionType electionType, ElectionCategory electionCategory = ElectionCategory.Parliamentary)
        {
            List<MElectionLookup> result = ElectionLookup.GetElectionsByTypeAndCategory(electionType, electionCategory).ToList();

            Logger.Info("GetElections complete.");
            return result;
        }

        public virtual List<MElectionLookup> GetElections(List<Guid> electionIds)
        {
            List<MElectionLookup> result = new List<MElectionLookup>();
            var boundries = from eleId in electionIds
                            select new MBoundaryPairs() { Name = "Election", Value = eleId.ToString() };

            result.AddRange(ElectionLookup.GetElections(boundries.ToArray()));

            Logger.Info("GetElections complete.");
            return result;
        }

        public virtual List<MElectorateLookup> GetRaces(List<Guid> electionIds, ElectionElectorateVacancyStatus? status)
        {
            List<MElectorateLookup> result = new List<MElectorateLookup>();

            foreach (var electionIdItem in electionIds)
            {
                var election = ElectionLookup.GetElection(electionIdItem);

                foreach (var parentElectorateItem in election.ParentElectorates)
                {
                    IEnumerable<MElectorateLookup> query;
                    if (status.HasValue)
                        query = parentElectorateItem.Electorates.Where(e => e.ElectorateVacancyConfiguration != null && e.ElectorateVacancyConfiguration.ElectionElectorateVacancyStatus == status.Value);
                    else
                        query = parentElectorateItem.Electorates.Where(e => e.ElectorateVacancyConfiguration != null);

                    foreach (var electorateItem in query)
                    {
                        result.Add(electorateItem);
                    }
                }
            }

            Logger.Info("GetRaces complete.");
            return result;
        }

        public virtual List<MLocationLookup> GetElectionLocations(List<Guid> electionIds)
        {
            List<MLocationLookup> result = new List<MLocationLookup>();

            foreach (var electionIdItem in electionIds)
            {
                var election = ElectionLookup.GetElection(electionIdItem);

                foreach (var parentElectorateItem in election.ParentElectorates)
                    foreach (var electorateItem in parentElectorateItem.Electorates)
                        foreach (var locationItem in electorateItem.Locations)
                        {
                            result.Add(locationItem);
                        }
            }

            Logger.Info("GetElectionLocations complete.");
            return result;
        }

        public virtual List<CandidatesInNominationOrderForElectionElectorateVacancyEntity> GetNominations(List<Guid> electorateVacencyIds)
        {
            List<CandidatesInNominationOrderForElectionElectorateVacancyEntity> result = NominationLookup.GetCandidatesInNominationOrder(electorateVacencyIds.ToArray()).ToList();

            Logger.Info("GetNominations complete.");
            return result;
        }

        public virtual List<BallotDrawnCandidatesForElectionElectorateVacancyEntity> BallotDrawCandidates(List<Guid> electorateVacencyIds)
        {
            List<BallotDrawnCandidatesForElectionElectorateVacancyEntity> result = NominationLookup.GetBallotDrawnCandidatesForElectionElectorateVacancyIds(electorateVacencyIds.ToArray()).ToList();

            Logger.Info("BallotDrawCandidates complete.");
            return result;
        }


        public virtual List<BallotDrawnGroupsForElectionElectorateVacancy> BallotDrawGroups(List<Guid> electorateVacencyIds)
        {
            List<BallotDrawnGroupsForElectionElectorateVacancy> result = NominationLookup.GetBallotDrawnGroupsForElectionElectorateVacancyIds(electorateVacencyIds.ToArray()).ToList();

            Logger.Info("BallotDrawGroups complete.");
            return result;
        }
    }
}
