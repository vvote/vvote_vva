﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using Vec.Apps.vVote.Common.Signature;
using Vec.Apps.vVote.DAL;
using Vec.Apps.vVote.Entities;
using Vec.Properties;
using Vec.Apps.vVote.Common;
using Vec.Resources;
using Vec.Apps.vVote.BL.Common;
using Newtonsoft.Json.Linq;

namespace Vec.Apps.vVote.BL
{
    public interface IAuthBL
    {
        AuthEntity AuthMessage(QuarantineEntity quarantineEntity);

        AuthEntity AuthMessage(string message, Guid eventId);
    }

    public class AuthBL : BaseBL, IAuthBL
    {
        public IvVoteContext vVoteContext { get; set; }
        public IEventBL EventBL { get; set; }
        public IFileServiceHelper FileService { get; set; }

        public AuthBL(IvVoteContext vVoteContext, IEventBL eventBL, IFileServiceHelper fileService)
        {
            this.vVoteContext = vVoteContext;
            this.EventBL = eventBL;
            this.FileService = fileService;
        }

        public AuthEntity AuthMessage(QuarantineEntity quarantineEntity)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == quarantineEntity.EventId);
            var eventDevice = vVoteContext.EventDevices.Single(ed => ed.EventLocation.EventId == quarantineEntity.EventId && ed.DeviceId == quarantineEntity.DeviceId);
            var authEntity = default(AuthEntity);
            var deviceMessage = new DeviceMessage()
            {
                EventDeviceId = eventDevice.Id,
                DeviceId = quarantineEntity.DeviceId,
                DeviceSig = quarantineEntity.DeviceSig,
                District = quarantineEntity.District,
                SerialNo = quarantineEntity.SerialNo,
                SerialSig = quarantineEntity.SerialSig,
                Type = (short)quarantineEntity.TypeId,
                DateTime = DateTime.UtcNow
            };
            vVoteContext.DeviceMessages.Add(deviceMessage);

            {
                var publicKeys = FileService.Get(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.VpsBlsPublicKey);
                var jsonPublicKeys = JObject.Parse(publicKeys.GetString());
                var publicKeyJson = jsonPublicKeys[quarantineEntity.DeviceId.ToString()][Constants.PublicKeyEntry];
                var publicKey = publicKeyJson.ToString(Newtonsoft.Json.Formatting.None).GetBytes();

                string msg = quarantineEntity.TypeName + quarantineEntity.SerialNo + quarantineEntity.District;

                ISignature signature = new Signature<BLSFactory>() { PublicKey = publicKey };

                if (!signature.Verify(msg.GetBytes(), deviceMessage.DeviceSig.GetBytes()))
                {
                    deviceMessage.Response = (short)DeviceMessageResponseType.Error;
                    vVoteContext.SaveChanges();
                    throw new ArgumentException(Constants.ERROR_SIGNATURE_NOT_VALID);
                }
            }

            //Check Quota limit
            var utcDay = DateTime.UtcNow.Date;
            var deviceQuotas = vVoteContext.DeviceMessages.Where(dm => dm.EventDevice.EventLocation.EventId == quarantineEntity.EventId && dm.EventDeviceId == eventDevice.Id && dm.Response == (short)DeviceMessageResponseType.Approved && dm.DateTime >= utcDay && dm.DateTime <= DateTime.UtcNow);
            if (deviceQuotas.Count() < eventDevice.QuarantineQuota.GetValueOrDefault(Properties.Settings.Default.DefaultQuarantineQuota))
            {
                try
                {
                    //cancelreq2010101:01Northcote
                    //Generate Auth signature
                    string msg = (quarantineEntity.SerialNo + Constants.Cancel);

                    authEntity = AuthMessage(msg, quarantineEntity.EventId);
                    deviceMessage.AuthId = authEntity.AuthId;
                    deviceMessage.AuthSig = authEntity.AuthSig;
                    deviceMessage.Response = (short)DeviceMessageResponseType.Approved;
                    vVoteContext.SaveChanges();
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    deviceMessage.Response = (short)DeviceMessageResponseType.Error;
                    vVoteContext.SaveChanges();
                    throw;
                }
            }
            else
            {
                deviceMessage.Response = (short)DeviceMessageResponseType.Rejected;
                vVoteContext.SaveChanges();
                throw new InvalidOperationException(Constants.ERROR_IQ_REJECTED);
            }
            //Update DB

            return authEntity;
        }


        public AuthEntity AuthMessage(string message, Guid eventId)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            var authEntity = new AuthEntity() { AuthId = Constants.CancelAuth };
            var privateKey = FileService.Get(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Settings.Default.AuthPrivateFile);

            ISignature signature = new Signature<BLSFactory>() { PrivateKey = privateKey };
            byte[] sign = signature.Sign(message.GetBytes());

            authEntity.AuthSig = sign.GetString();

            return authEntity;
        }
    }
}
