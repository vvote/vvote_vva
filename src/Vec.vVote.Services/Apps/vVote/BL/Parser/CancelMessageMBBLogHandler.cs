﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Vec.Apps.vVote.DAL;
using Vec.Apps.vVote.Entities;

namespace Vec.Apps.vVote.BL.Handler
{
    public class CancelMessageMBBLogHandler : BaseLogHandler
    {
        public CancelMessageMBBLogHandler(IvVoteContext vVoteContext)
            : base(vVoteContext)
        { }

        //{"boothID":"TestDeviceOne","serialNo":"TestDeviceOne:16","cancelAuthSig":"XGXYI73ep5MZ7sojzZ7Cvl0HPUckUNjsPTN4uQlWz1ZLUE9ZzbAA2Q==","commitTime":"1386180000000","cancelAuthID":"CancelAuth","boothSig":"Ue3tDC2YMMdYYm5Eu6dMUde9lpknRXfN4C263FnTdQLGPChoJe9lxg==","serialSig":"TcyZEoFT24wLJ+tYf3DKPN7k285Ww/2mZpfGwg8S2RN/ore5L0TTgg==","district":"Northcote","type":"cancel"}
        public override void DoHandleLog<T>(T data)
        {
            var msg = JObject.Parse(data.Message);
            var mbbs = new TelemetryMBBMessage()
            {
                EventId = data.EventId,
                SerialNo = msg.Value<string>("serialNo"),
                DateTime = DateTime.UtcNow,
                CommitTime = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(msg.Value<double>("commitTime")).ToUniversalTime(),
                Type = (short)(CommitType)Enum.Parse(typeof(CommitType), msg.Value<string>("type"), true),
                EVMId = msg.Value<string>("boothID"),
                VPSId = msg.Value<string>("serialNo").Split(':')[0],
                VoteDistrict = msg.Value<string>("district")
            };

            vVoteContext.TelemetryMBBMessages.Add(mbbs);
            vVoteContext.SaveChanges();
        }
    }
}
