﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Vec.Apps.vVote.DAL;
using Vec.Apps.vVote.Entities;

namespace Vec.Apps.vVote.BL.Handler
{
    public class ConnectionFailMBBLogHandler : HeartbeatMBBLogHandler
    {
        public ConnectionFailMBBLogHandler(IvVoteContext vVoteContext)
            : base(vVoteContext)
        { }

        //{"Message":"140362ff[SSL_NULL_WITH_NULL_NULL: Socket[unconnected]],IOException:Connection refused: connect","EventId":"98f54476-94b2-4f63-af41-e9a6378823fe","Level":"INFO","MessageType":"CONNFAIL","From":"Peer1"}
        public override void DoHandleLog<T>(T data)
        {
            base.DoHandleLog(data);
        }
    }
}
