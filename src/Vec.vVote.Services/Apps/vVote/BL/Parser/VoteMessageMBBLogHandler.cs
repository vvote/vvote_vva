﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Vec.Apps.vVote.DAL;
using Vec.Apps.vVote.Entities;

namespace Vec.Apps.vVote.BL.Handler
{
    public class VoteMessageMBBLogHandler : BaseLogHandler
    {
        public VoteMessageMBBLogHandler(IvVoteContext vVoteContext)
            : base(vVoteContext)
        { }

        //{"_vPrefs":"2,6,15,7,8,14,13,4,12,3,1,11,9,5,10: ,1, : , , , , , , , , , :","commitTime":"1407913200000","races":[{"preferences":["2","6","15","7","8","14","13","4","12","3","1","11","9","5","10"],"id":"LA"},{"preferences":[" ","1"," "],"id":"LC_ATL"},{"preferences":[" "," "," "," "," "," "," "," "," "," "],"id":"LC_BTL"}],"district":"Bass","boothSig":"LLLhNtbcdzLoo5WygyMrH93zexJRS/R042YTq99c5b6vQ2EDoIXwsA==","serialSig":"VKpn1YtfavZjTrVcXVB1QFkTJ6dGDlXA94k++3GTwYgjbA1KNDSFmQ==","type":"vote","boothID":"5150103","startEVMSig":"WR3cVcPEo7biYwrzcTCK20AG1MgYZjqbWgJ5+UNQHyx+jEtVskJncA==","serialNo":"5150101:13"}
        public override void DoHandleLog<T>(T data)
        {
            var msg = JObject.Parse(data.Message);
            var prefs = msg.Value<string>("_vPrefs").Replace(" ", string.Empty).Split(new char[1] { ':' });
            var laPrefs = prefs[0].Split(new char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var atlPrefs = prefs[1].Split(new char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var btlPrefs = prefs[2].Split(new char[1] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            var mbbs = new TelemetryMBBMessage()
            {
                EventId = data.EventId,
                SerialNo = msg.Value<string>("serialNo"),
                DateTime = DateTime.UtcNow,
                CommitTime = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(msg.Value<double>("commitTime")).ToUniversalTime(),
                Type = (short)(CommitType)Enum.Parse(typeof(CommitType), msg.Value<string>("type"), true),
                EVMId = msg.Value<string>("boothID"),
                VPSId = msg.Value<string>("serialNo").Split(':')[0],
                VoteDistrict = msg.Value<string>("district"),
                IsBlankLA = laPrefs.Length == 0,
                IsBlankATL = atlPrefs.Length == 0,
                IsBlankBTL = btlPrefs.Length == 0
            };

            vVoteContext.TelemetryMBBMessages.Add(mbbs);
            vVoteContext.SaveChanges();
        }
    }
}