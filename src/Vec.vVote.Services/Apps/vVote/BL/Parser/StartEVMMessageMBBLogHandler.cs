﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Vec.Apps.vVote.DAL;
using Vec.Apps.vVote.Entities;

namespace Vec.Apps.vVote.BL.Handler
{
    public class StartEVMMessageMBBLogHandler : BaseLogHandler
    {
        public StartEVMMessageMBBLogHandler(IvVoteContext vVoteContext)
            : base(vVoteContext)
        { }

        //{"serialNo":"TestDeviceOne:9","boothID":"TestEVMOne","commitTime":"1393524000000","boothSig":"ILiIjLOm6OxM6oFmPVPg3QIVXzQUrSc1Fm3VcGmo0vSaAKrQMUwH7A==","serialSig":"BRgOis6u4mJF6Yubrrx03aXeaXs2XbYljtwDgJv1TWD/94hG+VgKQA==","type":"startevm","district":"Altona","_uuid":"ef96cafe-2fdd-46ca-acad-b98b658c7ba5","_fromPeer":"/127.0.0.1:53491"}
        public override void DoHandleLog<T>(T data)
        {
            JObject msg = JObject.Parse(data.Message);
            msg.Remove("boothSig");
            msg.Remove("serialSig");

            TelemetryMBB mbb = new TelemetryMBB()
            {
                Level = data.Level,
                DateTime = DateTime.UtcNow,
                EventId = data.EventId,
                From = data.From,
                MessageType = data.MessageType,
                Message = msg.ToString(Newtonsoft.Json.Formatting.None)
            };

            vVoteContext.TelemetryMBBs.Add(mbb);
            vVoteContext.SaveChanges();
        }
    }
}