﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.vVote.DAL;
using Vec.Apps.vVote.Entities;
using Vec.Resources;

namespace Vec.Apps.vVote.BL.Handler
{
    public interface ILogHandler
    {
        void DoHandleLog<T>(T data) where T : LogEntity;
    }

    public interface ILogHandlerFactory
    {
        ILogHandler[] GetMBBLogHandlers(string key);
        ILogHandler[] GetWBBLogHandlers(string key);
    }

    public class LogHandlerFactory : ILogHandlerFactory
    {
        public IvVoteContext vVoteContext { get; set; }
        public List<LogHandlerPair> MBBLogHandlers = new List<LogHandlerPair>();
        public List<LogHandlerPair> WBBLogHandlers = new List<LogHandlerPair>();

        public LogHandlerFactory(IvVoteContext vVoteContext)
        {
            this.vVoteContext = vVoteContext;

            //WBB log handler...
            WBBLogHandlers.Add(new LogHandlerPair() { Key = Constants.WBB_ALL, Handler = new ILogHandler[1] { new GenericWBBLogHandler(vVoteContext) } });
            WBBLogHandlers.Add(new LogHandlerPair() { Key = Constants.WBB_RESOURCE_REQUEST, Handler = new ILogHandler[1] { new GenericWBBLogHandler(vVoteContext) } });

            //MBB log handler...
            MBBLogHandlers.Add(new LogHandlerPair() { Key = Constants.MSGPOD_MESSAGE, Handler = new ILogHandler[1] { new PODMessageMBBLogHandler(vVoteContext) } });
            MBBLogHandlers.Add(new LogHandlerPair() { Key = Constants.MSGSTARTEVM, Handler = new ILogHandler[1] { new StartEVMMessageMBBLogHandler(vVoteContext) } });
            MBBLogHandlers.Add(new LogHandlerPair() { Key = Constants.MSGVOTE_MESSAGE, Handler = new ILogHandler[1] { new VoteMessageMBBLogHandler(vVoteContext) } });
            MBBLogHandlers.Add(new LogHandlerPair() { Key = Constants.MSGCANCEL_MESSAGE, Handler = new ILogHandler[1] { new CancelMessageMBBLogHandler(vVoteContext) } });
            MBBLogHandlers.Add(new LogHandlerPair() { Key = Constants.MSGAUDIT_MESSAGE, Handler = new ILogHandler[1] { new AuditMessageMBBLogHandler(vVoteContext) } });
            MBBLogHandlers.Add(new LogHandlerPair() { Key = Constants.MSGTELEMETRYINFO, Handler = new ILogHandler[1] { new HeartbeatMBBLogHandler(vVoteContext) } });
            MBBLogHandlers.Add(new LogHandlerPair() { Key = Constants.MSGCONNFAIL, Handler = new ILogHandler[2] { new ConnectionFailMBBLogHandler(vVoteContext), new GenericMBBLogHandler(vVoteContext) } });
            MBBLogHandlers.Add(new LogHandlerPair() { Key = Constants.MBB_ALL, Handler = new ILogHandler[1] { new GenericMBBLogHandler(vVoteContext) } });
        }

        public ILogHandler[] GetMBBLogHandlers(string key)
        {
            var handler = MBBLogHandlers.SingleOrDefault(mp => mp.Key == key);

            if (handler == null)
                handler = MBBLogHandlers.Single(mp => mp.Key == Constants.MBB_ALL);

            return handler.Handler;
        }

        public ILogHandler[] GetWBBLogHandlers(string key)
        {
            var handler = WBBLogHandlers.SingleOrDefault(mp => mp.Key == key);

            if (handler == null)
                handler = WBBLogHandlers.Single(mp => mp.Key == Constants.WBB_ALL);

            return handler.Handler;
        }
    }

    public class LogHandlerPair
    {
        public string Key { get; set; }
        public ILogHandler[] Handler { get; set; }
    }
}
