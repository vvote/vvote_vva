﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Vec.Apps.vVote.DAL;
using Vec.Apps.vVote.Entities;

namespace Vec.Apps.vVote.BL.Handler
{
    public class HeartbeatMBBLogHandler : BaseLogHandler
    {
        public HeartbeatMBBLogHandler(IvVoteContext vVoteContext)
            : base(vVoteContext)
        { }

        //{"Message":"Telemetry Heartbeat#1408410157968#false","EventId":"98f54476-94b2-4f63-af41-e9a6378823fe","Level":"DEBUG","MessageType":"TELEMETRYINFO","From":"PEERNAME_IS_UNDEFINED"}
        public override void DoHandleLog<T>(T data)
        {
            vVoteContext.usp_Vec_vVote_UpdateTelemetryStatus(data.Level, data.MessageType, data.Message, data.EventId);
        }
    }
}
