﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Vec.Apps.vVote.DAL;
using Vec.Apps.vVote.Entities;

namespace Vec.Apps.vVote.BL.Handler
{
    public class PODMessageMBBLogHandler : BaseLogHandler
    {
        public PODMessageMBBLogHandler(IvVoteContext vVoteContext)
            : base(vVoteContext)
        { }

        //"{\"serialNo\":\"TestDeviceOne:9\",\"boothID\":\"TestDeviceOne\",\"ballotReductions\":[[{\"index\":37,\"candidateIndex\":36,\"randomness\":\"39ovSK2HmgIBZZJQ+0v3Vt7flQ9GfQHatImwPYJrCvQ=\"},{\"index\":36,\"candidateIndex\":37,\"randomness\":\"PnjINVtkHfhF0czsHLvN8YB+9P7fRzl5egUPTc1qSoc=\"},{\"index\":35,\"candidateIndex\":35,\"randomness\":\"fqSwqqEVu/qjbDghOJ2yjSVDdkwVJ7akJWEMJAMt3oU=\"},{\"index\":34,\"candidateIndex\":16,\"randomness\":\"qyazU7IhKtPcu9lA0OvvsaDzI+3LJSAmDgOMhgWO9wE=\"},{\"index\":32,\"candidateIndex\":18,\"randomness\":\"Evlukf1GIi0FSFRqd07z6fQfCoUsJoUiaJfcv3ic56E=\"},{\"index\":31,\"candidateIndex\":26,\"randomness\":\"6Q2RBBxqq+LCDIvgeRGZvw9A13dVADZML2SA2jksD/I=\"},{\"index\":30,\"candidateIndex\":27,\"randomness\":\"FVWDr9OMhjR436v5HovrxczakDBG5ArztCd3l7J7Hzc=\"},{\"index\":27,\"candidateIndex\":29,\"randomness\":\"6cfMjTXIhIMjf4zGjNDyyXrTbsvlxajuEjdzcqVO0Mo=\"},{\"index\":26,\"candidateIndex\":21,\"randomness\":\"mnHq8kosBXS0DgLbt8RU9tyPyDIdiK+k3hHtBZL4Z7Q=\"},{\"index\":25,\"candidateIndex\":24,\"randomness\":\"nA9bEHta4ScGBvKP3N83PfZJ7LrkoVImIQfGYX6Hb4Y=\"},{\"index\":24,\"candidateIndex\":19,\"randomness\":\"ARk3otV3HA3OI/OI2PXG1pd5VAemwEhCf1Zc5ZxW8ro=\"},{\"index\":23,\"candidateIndex\":12,\"randomness\":\"L4Apfv7Oj6aJ/1jz6ZDscp+RLAweE2AYNVtxt8Do/Iw=\"},{\"index\":21,\"candidateIndex\":38,\"randomness\":\"O/bgwNwwXK/iRqVGderX44hQB9O9i9LoXqo1BIZMHmk=\"},{\"index\":20,\"candidateIndex\":25,\"randomness\":\"zsBJt5/DgNS5rIV5+wKRtr2hV9JJoUjz+26tNQCQgeQ=\"},{\"index\":19,\"candidateIndex\":34,\"randomness\":\"zATcN7+AxRhBLzqfIxnapAz0bH3c9I8yhrz6RP+mx24=\"},{\"index\":18,\"candidateIndex\":39,\"randomness\":\"73grPGW/KUjelgAEF6nykUmvPDSVB8jOiIA9Jm5eKVU=\"},{\"index\":15,\"candidateIndex\":33,\"randomness\":\"m94DgF5eqzDH6n1zpLGScxNxvXK/HDjDpvd0dweR4ow=\"},{\"index\":14,\"candidateIndex\":13,\"randomness\":\"4KOGXbF1sXka0QtsjIaCFh4llpkpnSXq9T4MzxzfVsc=\"},{\"index\":12,\"candidateIndex\":32,\"randomness\":\"XpB+jYyuB5nLbe+lKkaJy0RRYj3/r1zW70oXzbW1yn8=\"},{\"index\":11,\"candidateIndex\":23,\"randomness\":\"2nYX+AUujRwHJSl1YAS0aPSAzhenHq6jBijcBTxdstU=\"},{\"index\":10,\"candidateIndex\":31,\"randomness\":\"b1r3Jib9pC8oMZv40SWPpOCXIQipn/T2TyVcWdLwbeg=\"},{\"index\":9,\"candidateIndex\":30,\"randomness\":\"PevY+gQqhNqfxO1XpgPOmjAhAN64NJk/nQBBwv7mXkg=\"},{\"index\":8,\"candidateIndex\":15,\"randomness\":\"IJPwT/UOFKt8G+18dXkhhO5Q2JdU07hWbLXtpD4xRGU=\"},{\"index\":5,\"candidateIndex\":14,\"randomness\":\"3rDyBcKj7ZgULGdjULK+mPUWBuq1PBCLRrZf3MLTYXQ=\"},{\"index\":4,\"candidateIndex\":22,\"randomness\":\"HSrtBXg8viaIZMLdiXlAbl+KHIZPe1Bw5s/14v1+67o=\"},{\"index\":3,\"candidateIndex\":17,\"randomness\":\"w2c48Pg17w6cWO4ppyXQDDBYtVurtSpP+1cnhhkK+Ac=\"},{\"index\":2,\"candidateIndex\":28,\"randomness\":\"AQhoiKljhAQ1OzSINnpy45rhs7qKO0sNH2I6ZSP2VDc=\"},{\"index\":1,\"candidateIndex\":20,\"randomness\":\"S/m6a1AqK/0Q7T4xof3NRma6e2GqdLTEznReihFuEN4=\"}],[{\"index\":6,\"candidateIndex\":6,\"randomness\":\"mK7jg7S+FF0kPD7ONSOdCkayh6FwozgCf3gShuuKHB0=\"},{\"index\":2,\"candidateIndex\":4,\"randomness\":\"nKl2f4NvO+W7kF2JQNqfIBFuxjt+QP3YFKxrECQ6mQw=\"},{\"index\":1,\"candidateIndex\":3,\"randomness\":\"kGo04yw23lZf4tWkeeh69Qj1HLUIDkanR9G0hRcZG6g=\"},{\"index\":0,\"candidateIndex\":5,\"randomness\":\"ihk1CI8wvLcQChIhoTqOtBDyVX2rBMyd8BhhtMnSPyI=\"}],[{\"index\":31,\"candidateIndex\":33,\"randomness\":\"6N2C3GTQJ/YGapBnado1twlVTVZcrHPpGowX6aWrERc=\"},{\"index\":29,\"candidateIndex\":31,\"randomness\":\"7bgbpWpYvstnKGY+7QQNRF0TvPE1pukxzUkQMxiUgjg=\"},{\"index\":28,\"candidateIndex\":34,\"randomness\":\"OmY9GVXE4yZr7r6/ZGe4DLCfhCaar+J5qUlYOA2kceg=\"},{\"index\":20,\"candidateIndex\":32,\"randomness\":\"mR+SPsqQCDJn04a0T8TW6zTP2gU0gmCXncQYzz65xVQ=\"},{\"index\":7,\"candidateIndex\":28,\"randomness\":\"saz3X5rI5xn+U1UObEcGvazijatrRNF6cbE53pFRqIQ=\"},{\"index\":6,\"candidateIndex\":29,\"randomness\":\"JPq25SV8dB2K3ZS6e8ufDd70BTlxyGaXecrPq0nW0nI=\"},{\"index\":1,\"candidateIndex\":30,\"randomness\":\"eBzl/bNsltNLod7x5j5VcC7Qswx/uEnTSfN610PKrBw=\"}]],\"commitTime\":\"1393524000000\",\"boothSig\":\"bGwWlt2ZNYMlCNzwTFQVc3Wp5itHefFHmwTSB7lW36QNJxooV9uB3A==\",\"district\":\"Altona\",\"type\":\"pod\",\"_uuid\":\"951c3e5b-602c-4330-ba50-a5d68fee2622\",\"_fromPeer\":\"/127.0.0.1:53449\"}"
        public override void DoHandleLog<T>(T data)
        {
            JObject msg = JObject.Parse(data.Message);
            msg.Remove("ballotReductions");
            msg.Remove("boothSig");

            TelemetryMBB mbb = new TelemetryMBB()
            {
                Level = data.Level,
                DateTime = DateTime.UtcNow,
                EventId = data.EventId,
                From = data.From,
                MessageType = data.MessageType,
                Message = msg.ToString(Newtonsoft.Json.Formatting.None)
            };

            vVoteContext.TelemetryMBBs.Add(mbb);
            vVoteContext.SaveChanges();
        }
    }
}
