﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vec.Properties;
using Vec.WbbVotePackingService;

namespace Vec.Apps.vVote.BL.Common
{
    public interface IVotePackingServiceHelper
    {
        void ExtractCiphers(string remoteAddress);

        List<int> GetCipherDevices(string remoteAddress);

        byte[] GetCommitFiles(string remoteAddress);
    }

    public class VotePackingServiceHelper: IVotePackingServiceHelper
    {
        public byte[] GetCommitFiles(string remoteAddress)
        {
            var request = new GetCommitFilesRequest();
            VotePackingServiceClient vpSvcClient = new VotePackingServiceClient(Settings.Default.WbbVotePackingServiceEndpoint, remoteAddress);
            var response = vpSvcClient.GetCommitFiles(request);

            return response.CommitFileZip;
        }

        public void ExtractCiphers(string remoteAddress)
        {
            VotePackingServiceClient vpSvcClient = new VotePackingServiceClient(Settings.Default.WbbVotePackingServiceEndpoint, remoteAddress);
            vpSvcClient.ExtractCiphers();
        }

        public List<int> GetCipherDevices(string remoteAddress)
        {
            var request = new GetCipherDevicesRequest();
            VotePackingServiceClient vpSvcClient = new VotePackingServiceClient(Settings.Default.WbbVotePackingServiceEndpoint, remoteAddress);
            var response = vpSvcClient.GetCipherDevices(request);

            return response.Devices.ToList();
        }
    }
}
