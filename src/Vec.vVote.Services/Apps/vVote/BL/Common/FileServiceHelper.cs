﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vec.Properties;
using Vec.WbbFileService;

namespace Vec.Apps.vVote.BL.Common
{
    public interface IFileServiceHelper
    {
        byte[] Get(string remoteAddress, string filePath);

        void Put(string remoteAddress, string filePath, byte[] content);

        bool Exist(string remoteAddress, string filePath);

        List<string> List(string remoteAddress, string folderPath, string pattern);

        byte[] GetListed(string remoteAddress, List<string> relativeFilePaths);
    }

    public class FileServiceHelper : IFileServiceHelper, IDisposable
    {
        public byte[] Get(string remoteAddress, string filePath)
        {
            FileServiceClient fileSvcClient = new FileServiceClient(Settings.Default.WbbFileServiceEndpoint, remoteAddress);
            return fileSvcClient.Get(filePath);
        }

        public void Put(string remoteAddress, string filePath, byte[] content)
        {
            FileServiceClient fileSvcClient = new FileServiceClient(Settings.Default.WbbFileServiceEndpoint, remoteAddress);
            fileSvcClient.Put(filePath, content);
        }

        public bool Exist(string remoteAddress, string filePath)
        {
            FileServiceClient fileSvcClient = new FileServiceClient(Settings.Default.WbbFileServiceEndpoint, remoteAddress);
            return fileSvcClient.Exist(filePath);
        }

        public List<string> List(string remoteAddress, string folderPath, string pattern)
        {
            FileServiceClient fileSvcClient = new FileServiceClient(Settings.Default.WbbFileServiceEndpoint, remoteAddress);
            return fileSvcClient.List(folderPath, pattern).ToList();
        }

        public byte[] GetListed(string remoteAddress, List<string> relativeFilePaths)
        {
            FileServiceClient fileSvcClient = new FileServiceClient(Settings.Default.WbbFileServiceEndpoint, remoteAddress);
            return fileSvcClient.GetListed(relativeFilePaths.ToArray());
        }

        public void Dispose()
        {
        }
    }
}
