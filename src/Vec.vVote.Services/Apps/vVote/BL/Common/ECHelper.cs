﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using Org.BouncyCastle.Asn1.Nist;
using Org.BouncyCastle.Asn1.X9;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Math.EC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.vVote.Entities;

namespace Vec.Apps.vVote.BL.Common
{
    public interface IECHelper
    {
        /// <summary>
        /// Coverts the encoded integer-16 string  to EC point pair.
        /// </summary>
        /// <param name="cipher">Cipher containing alpha and beta.</param>
        /// <returns>EC point conversion of alpha and beta.</returns>
        ECPoint[] CipherToECPoint(Cipher cipher);
    }

    public class ECHelper : IECHelper
    {
        const string DEFAULT_CURVE = "P-256";

        ECDomainParameters Parameters { get; set; }

        public ECHelper()
        {
            X9ECParameters p = NistNamedCurves.GetByName(DEFAULT_CURVE); //use the default curve.
            Parameters = new ECDomainParameters(p.Curve, p.G, p.N, p.H);
        }

        /// <summary>
        /// Coverts the encoded integer-16 string  to EC point pair.
        /// </summary>
        /// <param name="cipher">Cipher containing alpha and beta.</param>
        /// <returns>EC point conversion of alpha and beta.</returns>
        public ECPoint[] CipherToECPoint(Cipher cipher)
        {
            ECPoint[] cipherPoint = new ECPoint[2];
            cipherPoint[0] = Parameters.Curve.CreatePoint(new BigInteger(cipher.gr.x, 16), new BigInteger(cipher.gr.y, 16), false);
            cipherPoint[1] = Parameters.Curve.CreatePoint(new BigInteger(cipher.myr.x, 16), new BigInteger(cipher.myr.y, 16), false);

            return cipherPoint;
        }
    }
}
