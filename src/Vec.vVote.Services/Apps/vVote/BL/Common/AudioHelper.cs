﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using Vec.Apps.vVote.Common;
using Vec.Resources;

namespace Vec.Apps.vVote.BL.Common
{
    public interface IAudioHelper
    {
        byte[] GetDefaultMP3File(string synthTxt, ref ID3TagEntity tag);
    }

    public class AudioHelper : IAudioHelper
    {
        public byte[] GetDefaultMP3File(string synthTxt, ref ID3TagEntity tag)
        {
            byte[] content = null;
            bool isSynthSuccess = false;

            try
            {
                content = Synthesizer.SynthesizeText(synthTxt);
                isSynthSuccess = true;
            }
            catch
            {
                content = File.ReadAllBytes(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, Constants.DefaultAudioFile));
            }

            tag.Comment = isSynthSuccess ? Constants.DefaultComment : Constants.DefaultErrorComment;
            tag.Author = Constants.DefaultAuthor;
            tag.Year = (uint)DateTime.Now.Year;
            tag.Album = Constants.DefaultAlbum;

            return ID3Tag.SetMP3Tags(tag, content);
        }
    }
}
