﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using Vec.Apps.vVote.Common.Signature;
using Vec.Apps.vVote.DAL;
using Vec.Apps.vVote.Entities;
using Vec.Properties;
using Vec.Apps.vVote.Common;
using Vec.Resources;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;

namespace Vec.Apps.vVote.BL
{
    public interface ITelemetryBL
    {
        byte[] GetDeviceReconcile(Guid eventId, string deviceId, string fromDate, string toDate);

        IEnumerable<TelemetryMBBEntity> GetMBBTelemetryData(Guid eventId, int recordCount);

        IEnumerable<TelemetryWBBEntity> GetWBBTelemetryData(Guid eventId, int recordCount);
    }

    public class TelemetryBL : BaseBL, ITelemetryBL
    {
        public IvVoteContext vVoteContext { get; set; }
        public IEventBL EventBL { get; set; }

        public TelemetryBL(IvVoteContext vVoteContext, IEventBL eventBL)
        {
            this.vVoteContext = vVoteContext;
            this.EventBL = eventBL;
        }

        public byte[] GetDeviceReconcile(Guid eventId, string deviceId, string fromDate, string toDate)
        {
            byte[] file = null;
            string remoteUri = string.Format(Settings.Default.ReportingServiceUrl + Constants.ReconcileURLFormat, eventId, deviceId, fromDate, toDate);

            using (WebClient myWebClient = new WebClient() { UseDefaultCredentials = true })
            {
                file = myWebClient.DownloadData(remoteUri);
            }

            var images = GetAllPages(file);

            using (Bitmap bitmap = new Bitmap(images.Max(i => i.Width), images.Sum(i => i.Height)))
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.Clear(SystemColors.AppWorkspace);
                    var imgHeight = 0;
                    foreach (var img in images)
                    {
                        g.DrawImage(img, new Point(0, imgHeight));
                        imgHeight += img.Height;
                        
                        img.Dispose();
                    }
                }

                using (var pngStream = new MemoryStream())
                {
                    bitmap.Save(pngStream, ImageFormat.Png);
                    file = pngStream.ToArray();
                }
            }

            return file;
        }

        private List<Image> GetAllPages(byte[] file)
        {
            List<Image> images = new List<Image>();
            using (Bitmap bitmap = (Bitmap)Image.FromStream(new MemoryStream(file)))
            {
                int count = bitmap.GetFrameCount(FrameDimension.Page);
                for (int idx = 0; idx < count; idx++)
                {
                    // save each frame to a bytestream
                    bitmap.SelectActiveFrame(FrameDimension.Page, idx);
                    MemoryStream byteStream = new MemoryStream();
                    bitmap.Save(byteStream, ImageFormat.Tiff);

                    // and then create a new Image from it
                    images.Add(Image.FromStream(byteStream));
                }
            }
            return images;
        }


        public IEnumerable<TelemetryMBBEntity> GetMBBTelemetryData(Guid eventId, int recordCount)
        {
            foreach (var item in vVoteContext.TelemetryMBBs.Where(mbb => mbb.EventId == eventId).OrderByDescending(mbb => mbb.DateTime).Take(recordCount))
            {
                yield return new TelemetryMBBEntity()
                {
                    Id = item.Id,
                    DateTime = item.DateTime.ToLocalTime().ToString("g"),
                    Level = item.Level,
                    From = item.From,
                    MessageType = item.MessageType,
                    Message = item.Message
                };
            }
        }

        public IEnumerable<TelemetryWBBEntity> GetWBBTelemetryData(Guid eventId, int recordCount)
        {
            foreach (var item in vVoteContext.TelemetryWBBs.Where(wbb => wbb.EventId == eventId).OrderByDescending(mbb => mbb.DateTime).Take(recordCount))
            {
                yield return new TelemetryWBBEntity()
                {
                    Id = item.Id,
                    DateTime = item.DateTime.ToLocalTime().ToString("g"),
                    Level = item.Level,
                    From = item.From,
                    Resource = item.Resource,
                    Status = item.Status
                };
            }
        }
    }
}
