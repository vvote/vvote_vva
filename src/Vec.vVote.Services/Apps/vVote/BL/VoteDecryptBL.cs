﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Math.EC;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Hosting;
using Vec.Apps.vVote.BL.Common;
using Vec.Apps.vVote.BL.PdfGenerator;
using Vec.Apps.vVote.BL.VoteDecrypter;
using Vec.Apps.vVote.Common;
using Vec.Apps.vVote.DAL;
using Vec.Apps.vVote.Entities;
using Vec.Apps.vVote.Reader;
using Vec.Resources;
using Vec.WbbVotePackingService;

namespace Vec.Apps.vVote.BL
{
    public interface IVoteDecryptBL
    {
        /// <summary>
        /// Gets the context directory where the CSVs are placed and process them by getting the ballot order from Turing services.
        /// </summary>
        /// <param name="contextDirectory">Context directory containing the CSVs, Signature.png and DistrictVoteTemplate.pdf</param>
        /// <returns>Vote decrypt result entity that contains the files processed and indicates the number of votes in each file.</returns>
        VoteDecryptResultEntity DecryptDistrictVotes(DirectoryInfo contextDirectory);

        /// <summary>
        /// Gets the context directory where the CSVs are placed and process them by getting the ballot order from Turing services.
        /// </summary>
        /// <param name="contextDirectory">Context directory containing the CSVs, Signature.png and ATLVoteTemplate.pdf</param>
        /// <returns>Vote decrypt result entity that contains the files processed and indicates the number of votes in each file.</returns>
        VoteDecryptResultEntity DecryptAtlVotes(DirectoryInfo contextDirectory);

        /// <summary>
        /// Gets the context directory where the CSVs are placed and process them by getting the ballot order from Turing services.
        /// </summary>
        /// <param name="contextDirectory">Context directory containing the CSVs, Signature.png and BTLVoteTemplate/BTLVoteTemplateA3.pdf</param>
        /// <returns>Vote decrypt result entity that contains the files processed and indicates the number of votes in each file.</returns>
        VoteDecryptResultEntity DecryptBtlVotes(DirectoryInfo contextDirectory);

        /// <summary>
        /// Generates ballots after the user confirms the files that are to be processed.
        /// </summary>
        /// <param name="contextDirectory">Context directory containing the CSVs, Signature.png and DistrictVoteTemplate.pdf</param>
        /// <param name="fileIds">A list of file IDs that needs to be processed.</param>
        void GenerateDistrictVotePDFs(DirectoryInfo contextDirectory, List<int> fileIds);

        /// <summary>
        /// Generates ballots after the user confirms the files that are to be processed.
        /// </summary>
        /// <param name="contextDirectory">Context directory containing the CSVs, Signature.png and ATLVoteTemplate.pdf</param>
        /// <param name="fileIds">A list of file IDs that needs to be processed.</param>
        void GenerateAtlVotePDFs(DirectoryInfo contextDirectory, List<int> fileIds);

        /// <summary>
        /// Generates ballots after the user confirms the files that are to be processed.
        /// </summary>
        /// <param name="contextDirectory">Context directory containing the CSVs, Signature.png and BTLVoteTemplate/BTLVoteTemplateA3.pdf</param>
        /// <param name="fileIds">A list of file IDs that needs to be processed.</param>
        void GenerateBtlVotePDFs(DirectoryInfo contextDirectory, List<int> fileIds);

        /// <summary>
        /// Processes the commit file and prepares the data for packing.
        /// </summary>
        /// <param name="context">The context created for processing.</param>
        /// <param name="contextDirectory">Context directory containing the commit files, candidate Ids and ciphers.</param>
        /// <param name="eventId">The event ID of the election being processed.</param>
        List<string> ProcessCommitVotes(Guid context, DirectoryInfo contextDirectory, Guid eventId);

        /// <summary>
        /// Packs the votes that are processed and updated into vVote database.
        /// </summary>
        /// <param name="context">The context created for processing.</param>
        /// <param name="contextDirectory">Context directory containing the commit files, candidate Ids and ciphers.</param>
        /// <param name="eventId">The event ID of the election being processed.</param>
        void PackProcessedVotes(Guid context, DirectoryInfo contextDirectory, Guid eventId, int[] maxVotes);

        void CancelPackageProcess(Guid context, Guid eventId);

        void ExtractCiphers(Guid eventId);

        List<int> GetCipherDevices(List<EventDeviceEntity> vpsDevices, Guid eventId);

        PackingMiscEntity GetPackingMiscSettings(Guid eventId);

        PackingMiscEntity UpdatePackingMiscSettings(Guid eventId, Dictionary<string, byte[]> files);

        List<CommitFileEntity> GetCommitFiles(Guid eventId);

        void ProcessCommitFiles(Guid eventId, List<CommitFileEntity> commitFiles);
    }

    public class VoteDecryptBL : BaseBL, IVoteDecryptBL
    {
        public IvVoteContext vVoteContext { get; set; }
        public IEventBL EventBL { get; set; }
        public IElectionBL ElectionBL { get; set; }
        public IFileServiceHelper FileService { get; set; }
        public IECHelper ECHelper { get; set; }
        public IVotePackingServiceHelper VotePackingServiceHelper { get; set; }

        /// <summary>
        /// Injected constructor.
        /// </summary>
        /// <param name="vVoteContext">vVote EntityFramework context</param>
        /// <param name="eventBL">Event business logic.</param>
        /// <param name="electionBL">Election business logic.</param>
        /// <param name="fileService">File service to transfer files to and from WBB.</param>
        /// <param name="ecHelper">EC helper, manages and converts JSON to EC point.</param>
        public VoteDecryptBL(IvVoteContext vVoteContext, IEventBL eventBL, IElectionBL electionBL, IFileServiceHelper fileService, IVotePackingServiceHelper votePackingServiceHelper, IECHelper ecHelper)
        {
            this.vVoteContext = vVoteContext;
            this.EventBL = eventBL;
            this.ElectionBL = electionBL;
            this.FileService = fileService;
            this.ECHelper = ecHelper;
            this.VotePackingServiceHelper = votePackingServiceHelper;
        }

        /// <summary>
        /// Gets the context directory where the CSVs are placed and process them by getting the ballot order from Turing services.
        /// </summary>
        /// <param name="contextDirectory">Context directory containing the CSVs, Signature.png and DistrictVoteTemplate.pdf</param>
        /// <returns>Vote decrypt result entity that contains the files processed and indicates the number of votes in each file.</returns>
        public VoteDecryptResultEntity DecryptDistrictVotes(DirectoryInfo contextDirectory)
        {
            VoteDecryptResultEntity result = new VoteDecryptResultEntity();
            List<RaceMap> raceMap = new List<RaceMap>();
            if (File.Exists(Path.Combine(contextDirectory.FullName, Constants.RaceMap)))
                raceMap = ReadRaceMapFile(File.ReadAllBytes(Path.Combine(contextDirectory.FullName, Constants.RaceMap)));
            else
                throw new ArgumentNullException("Upload race_map.json file along with the csvs");

            //The CSVs imported should match the regular expression file name, it contains metadata information on district, race and vote type.
            Regex fileExpression = new Regex(@"(?<race>([A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}))_LA_(?<blank>([BLANK_]*))(?<voteType>([AE0-9]+))..csv", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            int fileId = 1;
            foreach (var file in contextDirectory.EnumerateFiles("*.csv", SearchOption.TopDirectoryOnly))
            {
                VoteDecryptFile decryptFile = new VoteDecryptFile() { Id = fileId, Name = file.Name }; //Create a decrypt file object for each file processed, weather success or failure.
                Match match = fileExpression.Match(file.Name);

                if (match.Success)
                {
                    decryptFile.RaceId = Guid.Parse(match.Groups["race"].Value);
                    decryptFile.District = raceMap.Single(rm => rm.Id == decryptFile.RaceId).Name; //match.Groups["district"].Value; 
                    decryptFile.VoteType = match.Groups["voteType"].Value;
                    decryptFile.IsBlank = !string.IsNullOrEmpty(match.Groups["blank"].Value);
                    var raceLookupFile = Path.Combine(contextDirectory.FullName, string.Format("{0}_lookup.json", decryptFile.RaceId));

                    List<Guid> races = new List<Guid>();
                    races.Add(decryptFile.RaceId);
                    //TODO: check the eev status instead of the candidate count & typo.
                    var candidtes = ElectionBL.BallotDrawCandidates(races); //Get the ballot draw candidates for the race in current file being processed.
                    if (candidtes.Count == 0 || candidtes[0].BallotDrawnCandidates.Count() == 0)
                    {
                        decryptFile.Error = "The race is invalid or uncontested.";
                    }
                    else
                    {
                        List<CandidateEntity> contextCandidates = new List<CandidateEntity>();
                        if (File.Exists(raceLookupFile))
                        {
                            JArray lookupJson = JArray.Parse(File.ReadAllText(raceLookupFile));
                            contextCandidates = lookupJson.ToObject<List<CandidateEntity>>();
                        }
                        else
                        {
                            foreach (var candidate in candidtes[0].BallotDrawnCandidates.OrderBy(o => o.BallotPaperPosition)) //Get the ballot draw candidates in ballot position order.
                            {
                                CandidateEntity contextCandidate = new CandidateEntity()
                                {
                                    Id = candidate.Id,
                                    Name = candidate.BallotPaperName,
                                    PartyName = candidate.PreferredRPPName,
                                    Region = candidate.BallotPaperLocality,
                                    BallotPosition = candidate.BallotPaperPosition,
                                    BallotSequence = candidate.OrderWithinGroupOrTeam
                                };

                                //Add all candidates to the lookup so that it could avoid additional processing while generating PDFs.
                                contextCandidates.Add(contextCandidate);
                            }

                            //save the candidates lookup for further processing while PDF generation.
                            JArray lookupJson = JArray.FromObject(contextCandidates);
                            File.WriteAllText(raceLookupFile, lookupJson.ToString(Formatting.Indented));
                        }

                        using (DelimitedFileReader fileReader = new DelimitedFileReader())
                        {
                            List<string[]> fileContent = fileReader.ImportCsvFile(file.FullName); //Get the CSVs in the form of string array.

                            try
                            {
                                if (fileContent.Count == 0)
                                    throw new ArgumentException("The file does not contain any rows.");

                                for (int i = 0; i < fileContent.Count; i++)
                                {
                                    VoteDecryptRow decryptRow = new VoteDecryptRow();
                                    if (fileContent[i].Count() == contextCandidates.Count)
                                    {
                                        for (int j = 0; j < fileContent[i].Count(); j++) //for each vote preference associate the preference to the candidate Id for processing later while generating PDF.
                                        {
                                            int vote;
                                            VoteDecryptCell decryptCell = new VoteDecryptCell() { CandidateId = contextCandidates[j].Id };
                                            if (string.IsNullOrEmpty(fileContent[i][j]))
                                            {
                                                decryptCell.Vote = null; //informal...
                                            }
                                            else if (int.TryParse(fileContent[i][j], out vote) && vote > 0 && vote <= contextCandidates.Count)
                                                decryptCell.Vote = vote; //full formal vote, if try parse is successful.
                                            else
                                                throw new ArgumentException(string.Format("Row {0} contains invalid vote.", i));
                                            decryptRow.Votes.Add(decryptCell);
                                        }
                                    }
                                    else
                                        throw new ArgumentException(string.Format("Row {0} contains invalid column count.", i));
                                    decryptFile.VoteDecryptRows.Add(decryptRow);
                                }
                            }
                            catch (Exception ex)
                            {
                                decryptFile.Error = ex.Message;
                            }
                        }
                    }
                }
                else
                {
                    decryptFile.Error = "The file name is invalid.";
                }

                result.VoteDecryptFiles.Add(decryptFile);
                fileId++; //increment file ID.
            }

            //save the parsed vote for further processing while PDF generation.
            JObject parsedVotesJson = JObject.FromObject(result);
            File.WriteAllText(Path.Combine(contextDirectory.FullName, "ParsedVotes.json"), parsedVotesJson.ToString());

            return result;
        }

        /// <summary>
        /// Gets the context directory where the CSVs are placed and process them by getting the ballot order from Turing services.
        /// </summary>
        /// <param name="contextDirectory">Context directory containing the CSVs, Signature.png and ATLVoteTemplate.pdf</param>
        /// <returns>Vote decrypt result entity that contains the files processed and indicates the number of votes in each file.</returns>
        public VoteDecryptResultEntity DecryptAtlVotes(DirectoryInfo contextDirectory)
        {
            VoteDecryptResultEntity result = new VoteDecryptResultEntity();
            List<RaceMap> raceMap = new List<RaceMap>();
            if (File.Exists(Path.Combine(contextDirectory.FullName, Constants.RaceMap)))
                raceMap = ReadRaceMapFile(File.ReadAllBytes(Path.Combine(contextDirectory.FullName, Constants.RaceMap)));
            else
                throw new ArgumentNullException("Upload race_map.json file along with the csvs");

            //The CSVs imported should match the regular expression file name, it contains metadata information on district, race and vote type.
            Regex fileExpression = new Regex(@"(?<id>([A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}))_ATL_(?<blank>([BLANK_]*))(?<voteType>([AE0-9]+))..csv", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            int fileId = 1;
            foreach (var file in contextDirectory.EnumerateFiles("*.csv", SearchOption.TopDirectoryOnly))
            {
                VoteDecryptFile decryptFile = new VoteDecryptFile() { Id = fileId, Name = file.Name };
                Match match = fileExpression.Match(file.Name);

                if (match.Success)
                {
                    var id = Guid.Parse(match.Groups["id"].Value);
                    decryptFile.Region = raceMap.Single(rm => rm.Id == id).Name; //match.Groups["region"].Value;
                    decryptFile.District = raceMap.Single(rm => rm.Id == id).District; //match.Groups["district"].Value;
                    decryptFile.RaceId = raceMap.Single(rm => rm.Id == id).RaceId; //Guid.Parse(match.Groups["race"].Value);
                    decryptFile.VoteType = match.Groups["voteType"].Value;
                    decryptFile.IsBlank = !string.IsNullOrEmpty(match.Groups["blank"].Value);
                    var raceLookupFile = Path.Combine(contextDirectory.FullName, string.Format("{0}_lookup.json", decryptFile.RaceId));

                    List<Guid> races = new List<Guid>();
                    races.Add(decryptFile.RaceId);
                    //TODO: check the eev status instead of the candidate count & typo.
                    var groups = ElectionBL.BallotDrawGroups(races); //Get the ballot draw groups for the race in current file being processed.
                    if (groups.Count == 0 || groups[0].BallotDrawnGroups.Count() == 0)
                    {
                        decryptFile.Error = "The race is invalid or uncontested.";
                    }
                    else
                    {
                        List<PartyEntity> contextParties = new List<PartyEntity>();
                        if (File.Exists(raceLookupFile))
                        {
                            JArray lookupJson = JArray.Parse(File.ReadAllText(raceLookupFile));
                            contextParties = lookupJson.ToObject<List<PartyEntity>>();
                        }
                        else
                        {
                            foreach (var group in groups)
                            {
                                foreach (var party in group.BallotDrawnGroups.OrderBy(o => o.BallotPaperPosition)) //Get the ballot draw candidates in ballot position order.
                                {
                                    if (string.IsNullOrEmpty(party.BallotPaperLetter)) //Skip the ones that are un-ticketed.
                                        continue;

                                    PartyEntity contextParty = new PartyEntity()
                                    {
                                        Id = party.Id,
                                        Name = party.Unnamed ? string.Empty : party.Name,
                                        BallotBoxLetter = party.BallotPaperLetter ?? string.Empty,
                                        GroupBallotPosition = party.BallotPaperPosition,
                                        HasBallotBox = !string.IsNullOrEmpty(party.BallotPaperLetter),
                                        Ungrouped = party.Ungrouped,
                                        Unnamed = party.Unnamed,
                                    };

                                    //Add all parties and candidates to the lookup so that it could avoid additional processing while generating PDFs.
                                    contextParties.Add(contextParty);
                                }
                            }

                            //save the parties lookup for further processing while PDF generation.
                            JArray lookupJson = JArray.FromObject(contextParties);
                            File.WriteAllText(raceLookupFile, lookupJson.ToString(Formatting.Indented));
                        }

                        using (DelimitedFileReader fileReader = new DelimitedFileReader())
                        {
                            List<string[]> fileContent = fileReader.ImportCsvFile(file.FullName); //Get the CSVs in the form of string array.

                            try
                            {
                                if (fileContent.Count == 0)
                                    throw new ArgumentException("The file does not contain any rows.");

                                for (int i = 0; i < fileContent.Count; i++)
                                {
                                    VoteDecryptRow decryptRow = new VoteDecryptRow();
                                    if (fileContent[i].Count() == contextParties.Count)
                                    {
                                        for (int j = 0; j < fileContent[i].Count(); j++)
                                        {
                                            int vote;
                                            VoteDecryptCell decryptCell = new VoteDecryptCell() { CandidateId = contextParties[j].Id };
                                            if (string.IsNullOrWhiteSpace(fileContent[i][j]))
                                            {
                                                decryptCell.Vote = null; //informal...
                                            }
                                            else if (int.TryParse(fileContent[i][j], out vote) && vote > 0 && vote <= contextParties.Count)
                                                decryptCell.Vote = vote; //full formal vote, if try parse is successful.
                                            else
                                                throw new ArgumentException(string.Format("Row {0} contains invalid vote.", i));
                                            decryptRow.Votes.Add(decryptCell);
                                        }
                                    }
                                    else
                                        throw new ArgumentException(string.Format("Row {0} contains invalid column count.", i));
                                    decryptFile.VoteDecryptRows.Add(decryptRow);
                                }
                            }
                            catch (Exception ex)
                            {
                                decryptFile.Error = ex.Message;
                            }
                        }
                    }
                }
                else
                {
                    decryptFile.Error = "The file name is invalid.";
                }

                result.VoteDecryptFiles.Add(decryptFile);
                fileId++; //increment file ID.
            }

            //save the parsed vote for further processing while PDF generation.
            JObject parsedVotesJson = JObject.FromObject(result);
            File.WriteAllText(Path.Combine(contextDirectory.FullName, "ParsedVotes.json"), parsedVotesJson.ToString());

            return result;
        }

        /// <summary>
        /// Gets the context directory where the CSVs are placed and process them by getting the ballot order from Turing services.
        /// </summary>
        /// <param name="contextDirectory">Context directory containing the CSVs, Signature.png and BTLVoteTemplate/BTLVoteTemplateA3.pdf</param>
        /// <returns>Vote decrypt result entity that contains the files processed and indicates the number of votes in each file.</returns>
        public VoteDecryptResultEntity DecryptBtlVotes(DirectoryInfo contextDirectory)
        {
            VoteDecryptResultEntity result = new VoteDecryptResultEntity();
            List<RaceMap> raceMap = new List<RaceMap>();
            if (File.Exists(Path.Combine(contextDirectory.FullName, Constants.RaceMap)))
                raceMap = ReadRaceMapFile(File.ReadAllBytes(Path.Combine(contextDirectory.FullName, Constants.RaceMap)));
            else
                throw new ArgumentNullException("Upload race_map.json file along with the csvs");

            //The CSVs imported should match the regular expression file name, it contains metadata information on district, race and vote type.
            Regex fileExpression = new Regex(@"(?<id>([A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}))_BTL_(?<voteType>([AE0-9]+))..csv", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            int fileId = 1;
            foreach (var file in contextDirectory.EnumerateFiles("*.csv", SearchOption.TopDirectoryOnly))
            {
                VoteDecryptFile decryptFile = new VoteDecryptFile() { Id = fileId, Name = file.Name }; //Create a decrypt file object for each file processed, weather success or failure.
                Match match = fileExpression.Match(file.Name);

                if (match.Success)
                {
                    var id = Guid.Parse(match.Groups["id"].Value);
                    decryptFile.Region = raceMap.Single(rm => rm.Id == id).Name; //match.Groups["region"].Value;
                    decryptFile.District = raceMap.Single(rm => rm.Id == id).District; //match.Groups["district"].Value;
                    decryptFile.RaceId = raceMap.Single(rm => rm.Id == id).RaceId; //Guid.Parse(match.Groups["race"].Value);
                    decryptFile.VoteType = match.Groups["voteType"].Value;
                    var raceLookupFile = Path.Combine(contextDirectory.FullName, string.Format("{0}_lookup.json", decryptFile.RaceId));

                    List<Guid> races = new List<Guid>();
                    races.Add(decryptFile.RaceId);
                    //TODO: check the eev status instead of the candidate count & typo.
                    var groups = ElectionBL.BallotDrawGroups(races); //Get the ballot draw groups for the race in current file being processed.
                    if (groups.Count == 0 || groups[0].BallotDrawnGroups.Count() == 0)
                    {
                        decryptFile.Error = "The race is invalid or uncontested.";
                    }
                    else
                    {
                        List<PartyEntity> contextParties = new List<PartyEntity>();
                        if (File.Exists(raceLookupFile))
                        {
                            JArray lookupJson = JArray.Parse(File.ReadAllText(raceLookupFile));
                            contextParties = lookupJson.ToObject<List<PartyEntity>>();
                        }
                        else
                        {
                            foreach (var group in groups)
                            {
                                foreach (var party in group.BallotDrawnGroups.OrderBy(o => o.BallotPaperPosition)) //Get the ballot draw candidates in ballot position order.
                                {
                                    PartyEntity contextParty = new PartyEntity()
                                    {
                                        Id = party.Id,
                                        Name = party.Unnamed ? string.Empty : party.Name,
                                        BallotBoxLetter = party.BallotPaperLetter ?? string.Empty,
                                        GroupBallotPosition = party.BallotPaperPosition,
                                        HasBallotBox = !string.IsNullOrEmpty(party.BallotPaperLetter),
                                        Ungrouped = party.Ungrouped,
                                        Unnamed = party.Unnamed,
                                    };

                                    foreach (var candidate in party.Nominations.OrderBy(o => o.BallotPaperPosition))
                                    {
                                        CandidateEntity contextCandidate = new CandidateEntity()
                                        {
                                            Id = candidate.Id,
                                            Name = candidate.BallotPaperName,
                                            PartyName = candidate.PreferredRPPName,
                                            Region = candidate.BallotPaperLocality,
                                            BallotPosition = candidate.BallotPaperPosition,
                                            BallotSequence = candidate.OrderWithinGroupOrTeam
                                        };

                                        contextParty.Candidates.Add(contextCandidate);
                                    }

                                    //Add all parties and candidates to the lookup so that it could avoid additional processing while generating PDFs.
                                    contextParties.Add(contextParty);
                                }
                            }

                            //save the parties and its candidate lookup for further processing while PDF generation.
                            JArray lookupJson = JArray.FromObject(contextParties);
                            File.WriteAllText(raceLookupFile, lookupJson.ToString(Formatting.Indented));
                        }

                        var contextCandidates = (from party in contextParties
                                                 from candidate in party.Candidates
                                                 select candidate).ToList();

                        using (DelimitedFileReader fileReader = new DelimitedFileReader())
                        {
                            List<string[]> fileContent = fileReader.ImportCsvFile(file.FullName);  //Get the CSVs in the form of string array.

                            try
                            {
                                if (fileContent.Count == 0)
                                    throw new ArgumentException("The file does not contain any rows.");

                                for (int i = 0; i < fileContent.Count; i++)
                                {
                                    VoteDecryptRow decryptRow = new VoteDecryptRow();
                                    if (fileContent[i].Count() == contextCandidates.Count)
                                    {
                                        for (int j = 0; j < fileContent[i].Count(); j++)
                                        {
                                            int vote;
                                            VoteDecryptCell decryptCell = new VoteDecryptCell() { CandidateId = contextCandidates[j].Id };
                                            if (string.IsNullOrEmpty(fileContent[i][j]))
                                            {
                                                decryptCell.Vote = null; //informal...
                                            }
                                            else if (int.TryParse(fileContent[i][j], out vote) && vote > 0 && vote <= contextCandidates.Count)
                                                decryptCell.Vote = vote; //full formal vote, if try parse is successful.
                                            else
                                                throw new ArgumentException(string.Format("Row {0} contains invalid vote.", i));
                                            decryptRow.Votes.Add(decryptCell);
                                        }
                                    }
                                    else
                                        throw new ArgumentException(string.Format("Row {0} contains invalid column count.", i));
                                    decryptFile.VoteDecryptRows.Add(decryptRow);
                                }
                            }
                            catch (Exception ex)
                            {
                                decryptFile.Error = ex.Message;
                            }
                        }
                    }
                }
                else
                {
                    decryptFile.Error = "The file name is invalid.";
                }

                result.VoteDecryptFiles.Add(decryptFile);
                fileId++; //increment file ID.
            }

            //save the parsed vote for further processing while PDF generation.
            JObject parsedVotesJson = JObject.FromObject(result);
            File.WriteAllText(Path.Combine(contextDirectory.FullName, "ParsedVotes.json"), parsedVotesJson.ToString());

            return result;
        }

        /// <summary>
        /// Generates ballots after the user confirms the files that are to be processed.
        /// </summary>
        /// <param name="contextDirectory">Context directory containing the CSVs, Signature.png and DistrictVoteTemplate.pdf</param>
        /// <param name="fileIds">A list of file IDs that needs to be processed.</param>
        public void GenerateDistrictVotePDFs(DirectoryInfo contextDirectory, List<int> fileIds)
        {
            //Read the parsed votes.
            JObject parsedVotesJson = JObject.Parse(File.ReadAllText(Path.Combine(contextDirectory.FullName, "ParsedVotes.json")));
            VoteDecryptResultEntity voteDecrypt = parsedVotesJson.ToObject<VoteDecryptResultEntity>();

            //Group the files by district so that processing per district is easier and faster.
            var files = voteDecrypt.VoteDecryptFiles.Where(vfs => fileIds.Contains(vfs.Id)).OrderBy(vfso => vfso.Id).GroupBy(vfsg => new { RaceName = vfsg.District, RaceId = vfsg.RaceId });

            foreach (var fileItem in files)//for each file generate PDF file through VoteDecrypter interface.
            {
                IVoteDecrypter districtVoteDecrypter = VoteDecrypterBase.GetVoteDecrypter<DistrictVoteDecrypter>();
                districtVoteDecrypter.ContextPath = contextDirectory.FullName;
                districtVoteDecrypter.FileName = fileItem.Key.RaceName;
                districtVoteDecrypter.Context = fileItem.Key.RaceId;

                var disVoteDecryptFile = new List<VoteDecryptFile>();
                var typeGroup = fileItem.GroupBy(typ => typ.VoteType);
                foreach (var typeItem in typeGroup)
                {
                    var first = typeItem.OrderBy(typo => typo.IsBlank).First();

                    foreach (var typItem in typeItem)
                    {
                        if (typItem == first)
                            continue;

                        first.VoteDecryptRows.AddRange(typItem.VoteDecryptRows);
                    }

                    disVoteDecryptFile.Add(first);
                }
                districtVoteDecrypter.Files = disVoteDecryptFile.OrderByDescending(dvdf => dvdf.VoteType).ToList();
                districtVoteDecrypter.GeneratePDF();
            }
        }

        /// <summary>
        /// Generates ballots after the user confirms the files that are to be processed.
        /// </summary>
        /// <param name="contextDirectory">Context directory containing the CSVs, Signature.png and ATLVoteTemplate.pdf</param>
        /// <param name="fileIds">A list of file IDs that needs to be processed.</param>
        public void GenerateAtlVotePDFs(DirectoryInfo contextDirectory, List<int> fileIds)
        {
            //Read the parsed votes.
            JObject parsedVotesJson = JObject.Parse(File.ReadAllText(Path.Combine(contextDirectory.FullName, "ParsedVotes.json")));
            VoteDecryptResultEntity voteDecrypt = parsedVotesJson.ToObject<VoteDecryptResultEntity>();

            //Group the files by district so that processing per region is easier and faster.
            var files = voteDecrypt.VoteDecryptFiles.Where(vfs => fileIds.Contains(vfs.Id)).OrderBy(vfso => vfso.Id).GroupBy(vfsg => new { RaceName = vfsg.Region, RaceId = vfsg.RaceId });

            foreach (var fileItem in files)//for each file generate PDF file through VoteDecrypter interface.
            {
                IVoteDecrypter atlVoteDecrypter = VoteDecrypterBase.GetVoteDecrypter<AtlVoteDecrypter>();
                atlVoteDecrypter.ContextPath = contextDirectory.FullName;
                atlVoteDecrypter.FileName = fileItem.Key.RaceName;
                atlVoteDecrypter.Context = fileItem.Key.RaceId;

                var atlVoteDecryptFile = new List<VoteDecryptFile>();
                var typeGroup = fileItem.GroupBy(typ => new { VoteType = typ.VoteType, District = typ.District });
                foreach (var typeItem in typeGroup)
                {
                    var first = typeItem.OrderBy(typo => typo.IsBlank).First();

                    foreach (var typItem in typeItem)
                    {
                        if (typItem == first)
                            continue;

                        first.VoteDecryptRows.AddRange(typItem.VoteDecryptRows);
                    }

                    atlVoteDecryptFile.Add(first);
                }
                atlVoteDecrypter.Files = atlVoteDecryptFile.OrderByDescending(dvdf => dvdf.VoteType).ToList();
                atlVoteDecrypter.GeneratePDF();
            }
        }

        /// <summary>
        /// Generates ballots after the user confirms the files that are to be processed.
        /// </summary>
        /// <param name="contextDirectory">Context directory containing the CSVs, Signature.png and BTLVoteTemplate/BTLVoteTemplateA3.pdf</param>
        /// <param name="fileIds">A list of file IDs that needs to be processed.</param>
        public void GenerateBtlVotePDFs(DirectoryInfo contextDirectory, List<int> fileIds)
        {
            //Read the parsed votes.
            JObject parsedVotesJson = JObject.Parse(File.ReadAllText(Path.Combine(contextDirectory.FullName, "ParsedVotes.json")));
            VoteDecryptResultEntity voteDecrypt = parsedVotesJson.ToObject<VoteDecryptResultEntity>();

            //Group the files by district so that processing per region is easier and faster.
            var files = voteDecrypt.VoteDecryptFiles.Where(vfs => fileIds.Contains(vfs.Id)).OrderBy(vfso => vfso.Id).GroupBy(vfsg => new { RaceName = vfsg.Region, RaceId = vfsg.RaceId });

            foreach (var fileItem in files)//for each file generate PDF file through VoteDecrypter interface.
            {
                IVoteDecrypter btlVoteDecrypter = VoteDecrypterBase.GetVoteDecrypter<BtlVoteDecrypter>();
                btlVoteDecrypter.ContextPath = contextDirectory.FullName;
                btlVoteDecrypter.Context = fileItem.Key.RaceId;
                btlVoteDecrypter.FileName = fileItem.Key.RaceName;
                btlVoteDecrypter.Files = fileItem.OrderByDescending(fi => fi.VoteType).ToList();

                btlVoteDecrypter.GeneratePDF();
            }
        }

        /// <summary>
        /// Processes the commit file and prepares the data for packing.
        /// </summary>
        /// <param name="context">The context created for processing.</param>
        /// <param name="contextDirectory">Context directory containing the commit files, candidate Ids and ciphers.</param>
        /// <param name="eventId">The event ID of the election being processed.</param>
        public List<string> ProcessCommitVotes(Guid context, DirectoryInfo contextDirectory, Guid eventId)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            List<string> processLogs = new List<string>();
            List<string> qSerialNos = new List<string>();
            List<EventLocationEntity> locations = new List<EventLocationEntity>();
            Dictionary<Guid, string> races = new Dictionary<Guid, string>();

            //Load Quarantine file, for which serial numbers has to be ignored while packing.
            if (FileService.Exist(eve.GetWbbFileServiceAddress(), Constants.DataUrl + Constants.QuarantineJsonFileName))
            {
                var qFile = FileService.Get(eve.GetWbbFileServiceAddress(), Constants.DataUrl + Constants.QuarantineJsonFileName);
                var qFileJson = qFile.GetString();
                var qFileJObj = JObject.Parse(qFileJson);

                qSerialNos = qFileJObj["Serials"].ToObject<List<string>>();
            }
            processLogs.Add(string.Format("Found {0} BQ", qSerialNos.Count));

            //Load location and races to track and associate serial numbers with device locations.
            {
                foreach (var locationItem in eve.EventLocations)
                {
                    EventLocationEntity locEntity = new EventLocationEntity()
                    {
                        Id = locationItem.Id,
                        Name = locationItem.LocationName,
                        LocationCode = locationItem.LocationCode,
                        LocationSubCode = locationItem.LocationSubCode,
                        Type = (LocationType)locationItem.Type.Value,
                        RaceName = locationItem.RaceName,
                        ParentRaceName = locationItem.ParentRaceName
                    };
                    locations.Add(locEntity);
                }
            }
            processLogs.Add(string.Format("Found {0} locations", locations.Count));

            //Get commit files from WBB...
            {
                var cFile = VotePackingServiceHelper.GetCommitFiles(eve.GetWbbVotePackingServiceAddress());
                Zipper.Extract(new MemoryStream(cFile), Path.Combine(contextDirectory.FullName, "Commits"));

                processLogs.Add(string.Format("Found {0} commit files for processing", Directory.GetFiles(Path.Combine(contextDirectory.FullName, "Commits")).Length));
            }

            {
                var electionIds = from e in eve.EventElections
                                  select e.ElectionId;
                var elections = ElectionBL.GetElections(electionIds.ToList()); // Get the election to match the race names with the race IDs
                //Get the vote district and region names and IDs.
                foreach (var electionItem in elections)

                    foreach (var electorateItem in electionItem.ParentElectorates)
                        foreach (var electionElectorate in electorateItem.Electorates.Where(e => e.ElectorateVacancyConfiguration != null))
                        {
                            switch (electionElectorate.Type)
                            {
                                case Vec.TuringElectionService.ElectorateType.District:
                                    races.Add(electionElectorate.ElectorateVacancyConfiguration.ElectionElectorateVacancyId, electionElectorate.Name);
                                    break;
                                case Vec.TuringElectionService.ElectorateType.Region:
                                    races.Add(electionElectorate.ElectorateVacancyConfiguration.ElectionElectorateVacancyId, electionElectorate.Name);
                                    break;
                                default:
                                    break;
                            }
                        }
            }


            //fetch relevant file for packing
            {
                var districtCandidateIdsFile = FileService.Get(eve.GetWbbFileServiceAddress(), Constants.VotePackingUrl + Constants.DisCandidateIdFileName);
                File.WriteAllBytes(Path.Combine(contextDirectory.FullName, Constants.DisCandidateIdFileName), districtCandidateIdsFile);
                var regionAtlCandidateIdsFile = FileService.Get(eve.GetWbbFileServiceAddress(), Constants.VotePackingUrl + Constants.AtlCandidateIdFileName);
                File.WriteAllBytes(Path.Combine(contextDirectory.FullName, Constants.AtlCandidateIdFileName), regionAtlCandidateIdsFile);
                var regionBTLCandidateIdsFile = FileService.Get(eve.GetWbbFileServiceAddress(), Constants.VotePackingUrl + Constants.BtlCandidateIdFileName);
                File.WriteAllBytes(Path.Combine(contextDirectory.FullName, Constants.BtlCandidateIdFileName), regionBTLCandidateIdsFile);
                var encryptedPaddingPointFile = FileService.Get(eve.GetWbbFileServiceAddress(), Constants.VotePackingUrl + Constants.EncryptedPaddingPointFileName);
                File.WriteAllBytes(Path.Combine(contextDirectory.FullName, Constants.EncryptedPaddingPointFileName), encryptedPaddingPointFile);
            }

            ICommitFileProcessor cfp = new CommitFileProcessor()
            {
                BulkQuarantineSerials = qSerialNos,
                Context = context,
                Locations = locations,
                ProcessLogs = processLogs
            };

            cfp.ReadCommitFiles(Path.Combine(contextDirectory.FullName, "Commits"));

            cfp.UpdatePackageCommitDetails(races);

            List<PackageCommit> pgkCommit = cfp.SavePackageCommits(eve);
            vVoteContext.SaveChanges();

            GeneratePreMixReport(contextDirectory, locations, eve, pgkCommit);

            return processLogs;
        }

        /// <summary>
        /// Generates a pre mix report before sending the packages to Mix-Net.
        /// </summary>
        /// <param name="context">The context created for processing.</param>
        /// <param name="contextDirectory">Context directory containing the commit files, candidate Ids and ciphers.</param>
        /// <param name="eve">The event to which this commit is associated.</param>
        /// <param name="locations">List of location and associated races.</param>
        private void GeneratePreMixReport(DirectoryInfo contextDirectory, List<EventLocationEntity> locations, Event eve, List<PackageCommit> pkgCommit)
        {
            var districtCommits = pkgCommit.GroupBy(dc => new { VoteDistrictRaceId = dc.VoteDistrictRaceId, VoteDistrictRaceName = dc.VoteDistrictRaceName });

            foreach (var commitGroup in districtCommits)
            {
                PreMixEntity preMix = new PreMixEntity()
                {
                    EventId = eve.Id,
                    EventName = eve.Name,
                    CommitDateTime = commitGroup.First().CommitTime.Value.ToLocalTime(),
                    ReportDateTime = DateTime.Now,
                    DistrictName = commitGroup.Key.VoteDistrictRaceName,
                    DistrictId = commitGroup.Key.VoteDistrictRaceId,
                    Locations = locations
                };

                foreach (var vote in commitGroup)
                {
                    preMix.Commits.Add(new CommitEntity()
                       {
                           DeviceId = vote.DeviceId,
                           SerialNo = vote.SerialNo,
                           District = vote.District,
                           Preferences = vote.Preferences,
                           DeviceRaceId = vote.DeviceRaceId.Value,
                           DeviceRaceName = vote.DeviceRaceName,
                           VoteDistrictRaceId = vote.VoteDistrictRaceId.Value,
                           VoteDistrictRaceName = vote.VoteDistrictRaceName,
                           VoteRegionRaceId = vote.VoteRegionRaceId.Value,
                           VoteRegionRaceName = vote.VoteRegionRaceName,
                           CommitTime = vote.CommitTime.Value,
                           CommitType = (CommitType)vote.Type,
                           VoteType = (VoteType)vote.VoteType,
                           Context = vote.Context
                       });
                }

                //Get an instance of PreMix report generator which generates PDF and process the commits in the context.
                IPdfGenerator<PreMixEntity> pdfGenerator = PdfGeneratorBase<PreMixEntity>.GetGeneratorInstance<PreMixReportGenerator>();
                pdfGenerator.ContextDirectory = contextDirectory;
                pdfGenerator.Entity = preMix;
                pdfGenerator.FileName = commitGroup.Key.VoteDistrictRaceName;
                pdfGenerator.GeneratePDF();
            }
        }

        /// <summary>
        /// Packs the votes that are processed and updated into vVote database.
        /// </summary>
        /// <param name="context">The context created for processing.</param>
        /// <param name="contextDirectory">Context directory containing the commit files, candidate Ids and ciphers.</param>
        /// <param name="eventId">The event ID of the election being processed.</param>
        public void PackProcessedVotes(Guid context, DirectoryInfo contextDirectory, Guid eventId, int[] maxVotes)
        {
            //Get all the candidate IDs.
            List<Coordinate> laCandidateIds = JArray.Parse(File.ReadAllText(Path.Combine(contextDirectory.FullName, Constants.DisCandidateIdFileName))).ToObject<List<Coordinate>>();
            List<Coordinate> atlCandidateIds = JArray.Parse(File.ReadAllText(Path.Combine(contextDirectory.FullName, Constants.AtlCandidateIdFileName))).ToObject<List<Coordinate>>();
            List<Coordinate> btlCandidateIds = JArray.Parse(File.ReadAllText(Path.Combine(contextDirectory.FullName, Constants.BtlCandidateIdFileName))).ToObject<List<Coordinate>>();
            JObject nullPoint = JObject.Parse(File.ReadAllText(Path.Combine(contextDirectory.FullName, Constants.EncryptedPaddingPointFileName))); //Get the encrypted null EC point.

            //Identify the maximum number of candidates for ballot reduction.
            if (maxVotes.Sum() == 0)
                maxVotes = new int[3] { laCandidateIds.Count, atlCandidateIds.Count, btlCandidateIds.Count };
            int packSize = Properties.Settings.Default.PackSize; //Pack size default being 3.

            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            var commits = from vote in eve.PackageCommits.Where(pc => pc.Context == context)
                          select new CommitEntity()
                          {
                              DeviceId = vote.DeviceId,
                              SerialNo = vote.SerialNo,
                              District = vote.District,
                              BallotReductions = vote.BallotReductions,
                              Preferences = vote.Preferences,
                              DeviceRaceId = vote.DeviceRaceId.Value,
                              DeviceRaceName = vote.DeviceRaceName,
                              VoteDistrictRaceId = vote.VoteDistrictRaceId.Value,
                              VoteDistrictRaceName = vote.VoteDistrictRaceName,
                              VoteRegionRaceId = vote.VoteRegionRaceId.Value,
                              VoteRegionRaceName = vote.VoteRegionRaceName,
                              CommitTime = vote.CommitTime.Value,
                              CommitType = (CommitType)vote.Type,
                              VoteType = (VoteType)vote.VoteType,
                              Context = vote.Context
                          };

            var districtCommits = commits.GroupBy(rc => new { VoteDistrictRaceId = rc.VoteDistrictRaceId, VoteDistrictRaceName = rc.VoteDistrictRaceName });
            List<string> raceJsonStr = new List<string>();

            //TODO: comeback and redo this stuff (USE BUILDER PATTERN HERE)
            foreach (var districtCommit in districtCommits)
            {
                byte[] packedFile;
                string bltFileFormat = "{0}_{1}_{2}.blt";
                string cidFileFormat = "{0}_{1}_{2}.cid";
                string blankFileFormat = "{0}_{1}_BLANK_{2}..csv";
                var disPackedVotes = new VotePack();
                var atlPackedVotes = new VotePack();
                var btlPackedVotes = new VotePack();

                //group the commits by Device IDs so that the processing of cipher files is within the context of the loop.
                var vpsGroups = districtCommit.GroupBy(g => g.VpsId);

                foreach (var vpsGroup in vpsGroups)
                {
                    var vpsId = vpsGroup.Key;
                    var cipherFile = FileService.Get(eve.GetWbbFileServiceAddress(), string.Format("{0}ciphers/{1}.json", Constants.VotePackingUrl, vpsId));
                    List<DevicePermutationEntity> devicePerumtations = ReadGenCipherFile(cipherFile); //Read cipher file.

                    foreach (var commit in vpsGroup)
                    {
                        Logger.InfoFormat("Performing packing for {0}", commit.SerialNo);

                        //foreach commit perform ballot reduction.
                        var ballotReduction = JArray.Parse(commit.BallotReductions).ToObject<List<Reductions>>();
                        var devicePerm = devicePerumtations.Single(gc => gc.serialNo == commit.SerialNo);

                        var laCiphers = devicePerm.ciphers.ToList();
                        var atlCiphers = devicePerm.ciphers.ToList();
                        var btlCiphers = devicePerm.ciphers.ToList();

                        laCiphers.RemoveRange(maxVotes[0], maxVotes.Sum() - maxVotes[0]);
                        atlCiphers.RemoveRange(0, maxVotes[0]);
                        atlCiphers.RemoveRange(maxVotes[1], atlCiphers.Count - maxVotes[1]);
                        btlCiphers.RemoveRange(0, maxVotes.Sum() - maxVotes[2]);

                        foreach (var item in ballotReduction[0])
                            laCiphers.RemoveAt(item.index);
                        foreach (var item in ballotReduction[1])
                            atlCiphers.RemoveAt(item.index);
                        foreach (var item in ballotReduction[2])
                            btlCiphers.RemoveAt(item.index);

                        Logger.InfoFormat("LA candidates for district {0} after reduction: {1}", commit.District, laCiphers.Count);
                        Logger.InfoFormat("ATL candidates for district {0} after reduction: {1}", commit.District, atlCiphers.Count);
                        Logger.InfoFormat("BTL candidates for district {0} after reduction: {1}", commit.District, btlCiphers.Count);

                        //perform vote packing.
                        var disPackCiphers = PackageProcess(commit.LaPreferences, laCiphers, packSize);
                        var atlPackCiphers = PackageProcess(commit.AtlPreferences, atlCiphers, packSize);
                        var btlPackCiphers = PackageProcess(commit.BtlPreferences, btlCiphers, packSize);

                        //classify the vote packs into Early/Absent/Ordinary, the file name contains this information so that it will be easy to generate PDF votes using this meta information.
                        switch (commit.VoteType)
                        {
                            case VoteType.Early:
                                if (disPackCiphers.PrefCount > 0)
                                    disPackedVotes.EarlyPackCiphers.Add(disPackCiphers);
                                else
                                    disPackedVotes.EarlyBlanks.Add(string.Join(",", commit.LaPreferences).Replace(" ", string.Empty));

                                if (atlPackCiphers.PrefCount > 0)
                                    atlPackedVotes.EarlyPackCiphers.Add(atlPackCiphers);
                                if (btlPackCiphers.PrefCount > 0)
                                    btlPackedVotes.EarlyPackCiphers.Add(btlPackCiphers);

                                if (atlPackCiphers.PrefCount == 0 && btlPackCiphers.PrefCount == 0)
                                    atlPackedVotes.EarlyBlanks.Add(string.Join(",", commit.AtlPreferences).Replace(" ", string.Empty));
                                break;
                            case VoteType.Absent:
                                if (disPackCiphers.PrefCount > 0)
                                    disPackedVotes.AbsentPackCiphers.Add(disPackCiphers);
                                else
                                    disPackedVotes.AbsentBlanks.Add(string.Join(",", commit.LaPreferences).Replace(" ", string.Empty));
                                if (atlPackCiphers.PrefCount > 0)
                                    atlPackedVotes.AbsentPackCiphers.Add(atlPackCiphers);
                                if (btlPackCiphers.PrefCount > 0)
                                    btlPackedVotes.AbsentPackCiphers.Add(btlPackCiphers);

                                if (atlPackCiphers.PrefCount == 0 && btlPackCiphers.PrefCount == 0)
                                    atlPackedVotes.AbsentBlanks.Add(string.Join(",", commit.AtlPreferences).Replace(" ", string.Empty));
                                break;
                            case VoteType.Ordinary:
                            default:
                                if (disPackCiphers.PrefCount > 0)
                                    disPackedVotes.OrdinaryPackCiphers.Add(disPackCiphers);
                                else
                                    disPackedVotes.OrdinaryBlanks.Add(string.Join(",", commit.LaPreferences).Replace(" ", string.Empty));
                                if (atlPackCiphers.PrefCount > 0)
                                    atlPackedVotes.OrdinaryPackCiphers.Add(atlPackCiphers);
                                if (btlPackCiphers.PrefCount > 0)
                                    btlPackedVotes.OrdinaryPackCiphers.Add(btlPackCiphers);

                                if (atlPackCiphers.PrefCount == 0 && btlPackCiphers.PrefCount == 0)
                                    atlPackedVotes.OrdinaryBlanks.Add(string.Join(",", commit.AtlPreferences).Replace(" ", string.Empty));
                                break;
                        }
                    }
                }

                Logger.Info("Packing LA...");
                var first = districtCommit.First();
                var dcm = new RaceCandidateMap() { RaceId = districtCommit.Key.VoteDistrictRaceId, RaceType = "LA" };
                //The race candidate map file should be generated only once per race. This file should then be copied across for different vote types,
                //such as early, absent or ordinary for mix-net to lookup the candidates after decrypting.
                for (int i = 0; i < first.LaPreferences.Count; i++)
                    dcm.CandidateIds.Add(laCandidateIds[i]);
                var raceMap = new RaceMap() { Id = districtCommit.Key.VoteDistrictRaceId, Name = districtCommit.Key.VoteDistrictRaceName, RaceId = districtCommit.Key.VoteDistrictRaceId, District = districtCommit.Key.VoteDistrictRaceName };
                raceJsonStr.Add(JObject.FromObject(raceMap).ToString(Formatting.None));
                if (disPackedVotes.EarlyPackCiphers.Count > 0)
                {
                    PadVotes(disPackedVotes.EarlyPackCiphers, nullPoint); //Pad votes.
                    packedFile = GetEncodedPackage(disPackedVotes.EarlyPackCiphers);//Perform an ASN1 encoding of EC points. 
                    File.WriteAllBytes(Path.Combine(contextDirectory.FullName, string.Format(bltFileFormat, raceMap.Id, dcm.RaceType, "E")), packedFile);
                    File.WriteAllText(Path.Combine(contextDirectory.FullName, string.Format(cidFileFormat, raceMap.Id, dcm.RaceType, "E")), JObject.FromObject(dcm).ToString());
                }
                if (disPackedVotes.EarlyBlanks.Count > 0)
                    File.WriteAllLines(Path.Combine(contextDirectory.FullName, string.Format(blankFileFormat, raceMap.Id, dcm.RaceType, "E")), disPackedVotes.EarlyBlanks);

                if (disPackedVotes.AbsentPackCiphers.Count > 0)
                {
                    PadVotes(disPackedVotes.AbsentPackCiphers, nullPoint); //Pad votes.
                    packedFile = GetEncodedPackage(disPackedVotes.AbsentPackCiphers);//Perform an ASN1 encoding of EC points. 
                    File.WriteAllBytes(Path.Combine(contextDirectory.FullName, string.Format(bltFileFormat, raceMap.Id, dcm.RaceType, "A")), packedFile);
                    File.WriteAllText(Path.Combine(contextDirectory.FullName, string.Format(cidFileFormat, raceMap.Id, dcm.RaceType, "A")), JObject.FromObject(dcm).ToString());
                }
                if (disPackedVotes.AbsentBlanks.Count > 0)
                    File.WriteAllLines(Path.Combine(contextDirectory.FullName, string.Format(blankFileFormat, raceMap.Id, dcm.RaceType, "A")), disPackedVotes.AbsentBlanks);

                if (disPackedVotes.OrdinaryPackCiphers.Count > 0)
                {
                    PadVotes(disPackedVotes.OrdinaryPackCiphers, nullPoint); //Pad votes.
                    packedFile = GetEncodedPackage(disPackedVotes.OrdinaryPackCiphers);//Perform an ASN1 encoding of EC points. 
                    File.WriteAllBytes(Path.Combine(contextDirectory.FullName, string.Format(bltFileFormat, raceMap.Id, dcm.RaceType, first.DeviceLocationCode)), packedFile);
                    File.WriteAllText(Path.Combine(contextDirectory.FullName, string.Format(cidFileFormat, raceMap.Id, dcm.RaceType, first.DeviceLocationCode)), JObject.FromObject(dcm).ToString());
                }
                if (disPackedVotes.OrdinaryBlanks.Count > 0)
                    File.WriteAllLines(Path.Combine(contextDirectory.FullName, string.Format(blankFileFormat, raceMap.Id, dcm.RaceType, first.DeviceLocationCode)), disPackedVotes.OrdinaryBlanks);

                Logger.Info("Packing ATL...");
                var atlRcm = new RaceCandidateMap() { RaceId = first.VoteRegionRaceId, RaceType = "ATL" };
                //The race candidate map file should be generated only once per race. This file should then be copied across for different vote types,
                //such as early, absent or ordinary for mix-net to lookup the candidates after decrypting.
                for (int i = 0; i < first.AtlPreferences.Count; i++)
                    atlRcm.CandidateIds.Add(atlCandidateIds[i]);
                raceMap = new RaceMap() { Id = Guid.NewGuid(), Name = first.VoteRegionRaceName, RaceId = first.VoteRegionRaceId, District = districtCommit.Key.VoteDistrictRaceName };
                raceJsonStr.Add(JObject.FromObject(raceMap).ToString(Formatting.None));
                if (atlPackedVotes.EarlyPackCiphers.Count > 0)
                {
                    PadVotes(atlPackedVotes.EarlyPackCiphers, nullPoint); //Pad votes.
                    packedFile = GetEncodedPackage(atlPackedVotes.EarlyPackCiphers);//Perform an ASN1 encoding of EC points. 
                    File.WriteAllBytes(Path.Combine(contextDirectory.FullName, string.Format(bltFileFormat, raceMap.Id, atlRcm.RaceType, "E")), packedFile);
                    File.WriteAllText(Path.Combine(contextDirectory.FullName, string.Format(cidFileFormat, raceMap.Id, atlRcm.RaceType, "E")), JObject.FromObject(atlRcm).ToString());

                }
                if (atlPackedVotes.EarlyBlanks.Count > 0)
                    File.WriteAllLines(Path.Combine(contextDirectory.FullName, string.Format(blankFileFormat, raceMap.Id, atlRcm.RaceType, "E")), atlPackedVotes.EarlyBlanks);

                if (atlPackedVotes.AbsentPackCiphers.Count > 0)
                {
                    PadVotes(atlPackedVotes.AbsentPackCiphers, nullPoint); //Pad votes.
                    packedFile = GetEncodedPackage(atlPackedVotes.AbsentPackCiphers);//Perform an ASN1 encoding of EC points. 
                    File.WriteAllBytes(Path.Combine(contextDirectory.FullName, string.Format(bltFileFormat, raceMap.Id, atlRcm.RaceType, "A")), packedFile);
                    File.WriteAllText(Path.Combine(contextDirectory.FullName, string.Format(cidFileFormat, raceMap.Id, atlRcm.RaceType, "A")), JObject.FromObject(atlRcm).ToString());
                }
                if (atlPackedVotes.AbsentBlanks.Count > 0)
                    File.WriteAllLines(Path.Combine(contextDirectory.FullName, string.Format(blankFileFormat, raceMap.Id, atlRcm.RaceType, "A")), atlPackedVotes.AbsentBlanks);

                if (atlPackedVotes.OrdinaryPackCiphers.Count > 0)
                {
                    PadVotes(atlPackedVotes.OrdinaryPackCiphers, nullPoint); //Pad votes.
                    packedFile = GetEncodedPackage(atlPackedVotes.OrdinaryPackCiphers);//Perform an ASN1 encoding of EC points. 
                    File.WriteAllBytes(Path.Combine(contextDirectory.FullName, string.Format(bltFileFormat, raceMap.Id, atlRcm.RaceType, first.DeviceLocationCode)), packedFile);
                    File.WriteAllText(Path.Combine(contextDirectory.FullName, string.Format(cidFileFormat, raceMap.Id, atlRcm.RaceType, first.DeviceLocationCode)), JObject.FromObject(atlRcm).ToString());
                }
                if (atlPackedVotes.OrdinaryBlanks.Count > 0)
                    File.WriteAllLines(Path.Combine(contextDirectory.FullName, string.Format(blankFileFormat, raceMap.Id, atlRcm.RaceType, first.DeviceLocationCode)), atlPackedVotes.OrdinaryBlanks);

                Logger.Info("Packing BTL...");
                var btlRcm = new RaceCandidateMap() { RaceId = first.VoteRegionRaceId, RaceType = "BTL" };
                //The race candidate map file should be generated only once per race. This file should then be copied across for different vote types,
                //such as early, absent or ordinary for mix-net to lookup the candidates after decrypting.
                for (int i = 0; i < first.BtlPreferences.Count; i++)
                    btlRcm.CandidateIds.Add(btlCandidateIds[i]);
                raceMap = new RaceMap() { Id = Guid.NewGuid(), Name = first.VoteRegionRaceName, RaceId = first.VoteRegionRaceId, District = districtCommit.Key.VoteDistrictRaceName };
                raceJsonStr.Add(JObject.FromObject(raceMap).ToString(Formatting.None));
                if (btlPackedVotes.EarlyPackCiphers.Count > 0)
                {
                    PadVotes(btlPackedVotes.EarlyPackCiphers, nullPoint); //Pad votes.
                    packedFile = GetEncodedPackage(btlPackedVotes.EarlyPackCiphers);//Perform an ASN1 encoding of EC points. 
                    File.WriteAllBytes(Path.Combine(contextDirectory.FullName, string.Format(bltFileFormat, raceMap.Id, btlRcm.RaceType, "E")), packedFile);
                    File.WriteAllText(Path.Combine(contextDirectory.FullName, string.Format(cidFileFormat, raceMap.Id, btlRcm.RaceType, "E")), JObject.FromObject(btlRcm).ToString());
                }
                if (btlPackedVotes.AbsentPackCiphers.Count > 0)
                {
                    PadVotes(btlPackedVotes.AbsentPackCiphers, nullPoint); //Pad votes.
                    packedFile = GetEncodedPackage(btlPackedVotes.AbsentPackCiphers);//Perform an ASN1 encoding of EC points. 
                    File.WriteAllBytes(Path.Combine(contextDirectory.FullName, string.Format(bltFileFormat, raceMap.Id, btlRcm.RaceType, "A")), packedFile);
                    File.WriteAllText(Path.Combine(contextDirectory.FullName, string.Format(cidFileFormat, raceMap.Id, btlRcm.RaceType, "A")), JObject.FromObject(btlRcm).ToString());
                }
                if (btlPackedVotes.OrdinaryPackCiphers.Count > 0)
                {
                    PadVotes(btlPackedVotes.OrdinaryPackCiphers, nullPoint); //Pad votes.
                    packedFile = GetEncodedPackage(btlPackedVotes.OrdinaryPackCiphers);//Perform an ASN1 encoding of EC points. 
                    File.WriteAllBytes(Path.Combine(contextDirectory.FullName, string.Format(bltFileFormat, raceMap.Id, btlRcm.RaceType, first.DeviceLocationCode)), packedFile);
                    File.WriteAllText(Path.Combine(contextDirectory.FullName, string.Format(cidFileFormat, raceMap.Id, btlRcm.RaceType, first.DeviceLocationCode)), JObject.FromObject(btlRcm).ToString());
                }
                Logger.InfoFormat("Performing packing for {0} - Complete", districtCommit.Key.VoteDistrictRaceName);
            }
            File.WriteAllLines(Path.Combine(contextDirectory.FullName, Constants.RaceMap), raceJsonStr);
        }

        public void CancelPackageProcess(Guid context, Guid eventId)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            var pkgCommits = eve.PackageCommits.Where(pc => pc.Context == context);

            while (pkgCommits.Count() > 0)
                vVoteContext.PackageCommits.Remove(pkgCommits.First());

            vVoteContext.SaveChanges();
        }

        public void ExtractCiphers(Guid eventId)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            VotePackingServiceHelper.ExtractCiphers(eve.GetWbbVotePackingServiceAddress());
        }

        public List<int> GetCipherDevices(List<EventDeviceEntity> vpsDevices, Guid eventId)
        {
            var result = new List<int>();
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            var foundDevices = VotePackingServiceHelper.GetCipherDevices(eve.GetWbbVotePackingServiceAddress());

            var missingDevicesGroup = from sd in vpsDevices
                                      join fd in foundDevices on sd.DeviceId equals fd into subGrp
                                      from sub in subGrp.DefaultIfEmpty()
                                      select new { Device = sd, CipherDevice = sub };

            foreach (var missingDevices in missingDevicesGroup.Where(md => md.CipherDevice == 0))
            {
                result.Add(missingDevices.Device.DeviceId);
            }


            return result;
        }

        public PackingMiscEntity GetPackingMiscSettings(Guid eventId)
        {
            PackingMiscEntity packingMiscEntity = new PackingMiscEntity();
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            packingMiscEntity.DistrictCandidateIds = new TemplateItem()
            {
                Name = Constants.DisCandidateIdFileName,
                Path = eve.GetWbbFileUrl(Constants.VotePackingUrl + Constants.DisCandidateIdFileName),
                IsUploaded = FileService.Exist(eve.GetWbbFileServiceAddress(), Constants.VotePackingUrl + Constants.DisCandidateIdFileName)
            };

            packingMiscEntity.RegionATLCandidateIds = new TemplateItem()
            {
                Name = Constants.AtlCandidateIdFileName,
                Path = eve.GetWbbFileUrl(Constants.VotePackingUrl + Constants.AtlCandidateIdFileName),
                IsUploaded = FileService.Exist(eve.GetWbbFileServiceAddress(), Constants.VotePackingUrl + Constants.AtlCandidateIdFileName)
            };

            packingMiscEntity.RegionBTLCandidateIds = new TemplateItem()
            {
                Name = Constants.BtlCandidateIdFileName,
                Path = eve.GetWbbFileUrl(Constants.VotePackingUrl + Constants.BtlCandidateIdFileName),
                IsUploaded = FileService.Exist(eve.GetWbbFileServiceAddress(), Constants.VotePackingUrl + Constants.BtlCandidateIdFileName)
            };

            packingMiscEntity.EncryptedPaddingPoint = new TemplateItem()
            {
                Name = Constants.EncryptedPaddingPointFileName,
                Path = eve.GetWbbFileUrl(Constants.VotePackingUrl + Constants.EncryptedPaddingPointFileName),
                IsUploaded = FileService.Exist(eve.GetWbbFileServiceAddress(), Constants.VotePackingUrl + Constants.EncryptedPaddingPointFileName)
            };

            return packingMiscEntity;
        }

        public PackingMiscEntity UpdatePackingMiscSettings(Guid eventId, Dictionary<string, byte[]> files)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            foreach (var file in files)
                FileService.Put(eve.GetWbbFileServiceAddress(), Constants.VotePackingUrl + file.Key, file.Value);

            return GetPackingMiscSettings(eventId);
        }

        public List<CommitFileEntity> GetCommitFiles(Guid eventId)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            var commitFileList = FileService.List(eve.GetWbbFileServiceAddress(), Constants.CommitUrl, "*.json");
            Regex fileExpression = new Regex(@"(?<commitTime>([0-9]{13}))(?<extraCommit>([A-Za-z0-9]*)).json", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
            var filteredCommitFiles = new List<CommitFileEntity>();
            foreach (var file in commitFileList)
            {
                Match match = fileExpression.Match(file);
                if (match.Success)
                {
                    filteredCommitFiles.Add(new CommitFileEntity()
                    {
                        Name = file,
                        UnixCommitDateTime = match.Groups["commitTime"].Value,
                        CommitDateTimeString = match.Groups["commitTime"].Value.GetDateTime().ToLocalTime().ToString("g")
                    });
                }
            }

            var x = from r in filteredCommitFiles
                    join cf in eve.CommitFiles on r.Name equals cf.Name into cGroup
                    from item in cGroup.DefaultIfEmpty()
                    select new CommitFileEntity()
                    {
                        Id = item == null ? null : new Nullable<int>(item.Id),
                        Name = r.Name,
                        UnixCommitDateTime = r.UnixCommitDateTime,
                        CommitDateTimeString = r.CommitDateTimeString,
                        ProcessedDateTime = item == null ? null : new Nullable<DateTime>(item.ProcessedDateTime),
                        ProcessedDateTimeString = item == null ? null : (item.ProcessedDateTime.ToLocalTime().ToString("g"))
                    };

            return x.OrderByDescending(X => X.CommitDateTimeString).ToList();
        }

        public void ProcessCommitFiles(Guid eventId, List<CommitFileEntity> commitFiles)
        {
            if (commitFiles.Count > 0)
            {
                var eve = vVoteContext.Events.Single(e => e.Id == eventId);
                var commitFileNames = commitFiles.Select(cf => cf.Name).ToList();
                var relativeCommitFileNames = commitFiles.Select(cf => Constants.CommitUrl + cf.Name).ToList();
                var listedFiles = FileService.GetListed(eve.GetWbbFileServiceAddress(), relativeCommitFileNames);
                var commitFileContent = Zipper.Extract(new MemoryStream(listedFiles), commitFileNames);

                foreach (var file in commitFiles)
                {
                    var content = commitFileContent[file.Name];
                    if (content == null)
                        continue;

                    var commitFile = new CommitFile()
                    {
                        Name = file.Name,
                        CommitDateTime = file.UnixCommitDateTime.GetDateTime(),
                        ProcessedDateTime = DateTime.UtcNow
                    };

                    ReadCommitFiles(eve.ProcessedCommits, ref content);

                    eve.CommitFiles.Add(commitFile);
                }
                vVoteContext.SaveChanges();
            }
        }

        private void ReadCommitFiles(ICollection<ProcessedCommit> processedCommits, ref byte[] content)
        {
            using (TextReader fs = new StreamReader(new MemoryStream(content)))
            {
                while (fs.Peek() >= 0)
                {
                    JObject jo = JObject.Parse(fs.ReadLine());

                    if (jo["serialNo"] != null)
                    {
                        var serialNo = jo["serialNo"].Value<string>();
                        var commitType = (CommitType)Enum.Parse(typeof(CommitType), jo["type"].Value<string>(), true);

                        switch (commitType)
                        {
                            case CommitType.Vote:
                            case CommitType.Audit:
                            case CommitType.Cancel:
                                ProcessedCommit pc = processedCommits.SingleOrDefault(c => c.SerialNo == serialNo && c.Type == (short)commitType) ?? new ProcessedCommit();
                                //Read only the necessary fields.
                                pc.DeviceId = serialNo.Split(':')[0];
                                pc.SerialNo = serialNo;
                                pc.Type = (short)commitType;
                                pc.District = jo["district"].Value<string>();
                                pc.CommitDateTime = jo["commitTime"].Value<double>().GetDateTime();
                                processedCommits.Add(pc);
                                break;
                            case CommitType.BallotGenCommit:
                            case CommitType.BallotAuditCommit:
                            case CommitType.Pod:
                            default:
                                break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// When vote preference are smaller in size the input of mix-net is ensured of same length by padding with null EC point.
        /// </summary>
        /// <param name="packs">The packed ciphers under consideration.</param>
        private void PadVotes(List<PackCiphers> packs, JObject nullPoint)
        {
            var p = from pac in packs
                    group pac by pac.Ciphers.Count into pacGroup
                    orderby pacGroup.Key descending
                    select pacGroup;

            int? maxLength = null; //Get the maximum preference and pad the rest till it reaches the max preference.
            foreach (var item in p)
            {
                if (maxLength == null)
                {
                    maxLength = item.Key;
                    continue;
                }

                foreach (var cipherItem in item)
                {
                    for (int i = cipherItem.Ciphers.Count; i < maxLength; i++)
                    {
                        Cipher cipher = nullPoint.ToObject<Cipher>();
                        ECPoint[] cipherPoint = ECHelper.CipherToECPoint(cipher);
                        cipherItem.Ciphers.Add(new PackCipher() { GYR = cipherPoint[0], MYR = cipherPoint[1] });
                    }
                }
            }
        }

        /// <summary>
        /// Encode the packed ciphers to ANS1 as it is the format which Mix-Net expects.
        /// </summary>
        /// <param name="packs">The packed ciphers under consideration.</param>
        /// <returns>Byte array of ANS1 encoding.</returns>
        private byte[] GetEncodedPackage(List<PackCiphers> packs)
        {
            byte[] package;

            using (MemoryStream ms = new MemoryStream())
            using (BinaryWriter bw = new BinaryWriter(ms))
            {
                foreach (var outerPack in packs)
                {
                    Asn1EncodableVector outer = new Asn1EncodableVector();
                    foreach (var innerPack in outerPack.Ciphers)
                    {
                        Asn1EncodableVector inner = new Asn1EncodableVector();
                        inner.Add(new DerOctetString(innerPack.GYR.GetEncoded()));
                        inner.Add(new DerOctetString(innerPack.MYR.GetEncoded()));

                        outer.Add(new DerSequence(inner));
                    }

                    bw.Write(new DerSequence(outer).GetDerEncoded());
                }
                bw.Flush();

                package = ms.ToArray();
            }

            return package;
        }

        /*
        /// <summary>
        /// Update the commits for further processing with is vote type, device location, device race, vote race.
        /// </summary>
        /// <param name="context">The context for the processing.</param>
        /// <param name="eve">The event to which this commit is associated.</param>
        /// <param name="locations">List of location and associated races.</param>
        private void UpdateCommitRaceDetails(Guid context, Event eve, List<EventLocationEntity> locations)
        {
            var electionIds = from e in eve.EventElections
                              select e.ElectionId;
            var elections = ElectionBL.GetElections(electionIds.ToList()); // Get the election to match the race names with the race IDs

            //Get the commits for the context that are processed in groups of district names.
            var districtCommits = eve.PackageCommits.Where(pc => pc.Context == context && pc.Type == (short)CommitType.Vote).GroupBy(g => g.District);

            foreach (var districtCommit in districtCommits)
            {
                var districtName = districtCommit.Key;
                string voteRegionName = null, voteDistrictName = null;
                Guid voteRegionId = Guid.Empty, voteDistrictId = Guid.Empty;

                //Get the vote district and region names and IDs.
                foreach (var election in elections)
                {
                    foreach (var electorate in election.ParentElectorates)
                    {
                        var elerate = electorate.Electorates.SingleOrDefault(e => e.Name.StartsWith(districtName, StringComparison.CurrentCultureIgnoreCase));

                        if (elerate != null)
                        {
                            voteDistrictName = elerate.Name;
                            voteDistrictId = elerate.ElectorateVacancyConfiguration.ElectionElectorateVacancyId;
                            var eleRateRegion = electorate.Electorates.SingleOrDefault(e => e.Type == TuringElectionService.ElectorateType.Region);
                            voteRegionId = eleRateRegion.ElectorateVacancyConfiguration.ElectionElectorateVacancyId;
                            voteRegionName = eleRateRegion.Name;

                            break;
                        }
                    }

                    if (!string.IsNullOrEmpty(voteDistrictName))
                        break;
                }

                //Update the commits with associated vote raceIds
                foreach (var commit in districtCommit)
                {
                    commit.VoteRegionRaceId = voteRegionId;
                    commit.VoteRegionRaceName = Regex.Replace(voteRegionName, @"\sRegion\z", string.Empty, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                    commit.VoteDistrictRaceId = voteDistrictId;
                    commit.VoteDistrictRaceName = Regex.Replace(voteDistrictName, @"\sDistrict\z", string.Empty, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                }
            }

            //Get the device race Ids to identify Early/Absent/Ordinary votes.
            districtCommits = eve.PackageCommits.Where(pc => pc.Context == context && pc.Type == (short)CommitType.Vote).GroupBy(g => g.DeviceId.Substring(0, g.DeviceId.Length - 4));
            foreach (var districtCommit in districtCommits)
            {
                var locCode = Convert.ToInt32(districtCommit.Key);
                var raceName = eve.EventLocations.First(e => e.LocationCode == locCode).RaceName;
                string deviceDistrictName = null;
                Guid deviceDistrictId = Guid.Empty;

                foreach (var election in elections)
                {
                    foreach (var electorate in election.ParentElectorates)
                    {
                        var elerate = electorate.Electorates.SingleOrDefault(e => e.Name.StartsWith(raceName));

                        if (elerate != null)
                        {
                            deviceDistrictName = Regex.Replace(elerate.Name, @"\sDistrict\z", string.Empty, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase); ;
                            deviceDistrictId = elerate.ElectorateVacancyConfiguration.ElectionElectorateVacancyId;

                            break;
                        }
                    }

                    if (!string.IsNullOrEmpty(deviceDistrictName))
                        break;
                }

                //based on the location types, classify the votes as Early, Ordinary and Absent votes.
                foreach (var commit in districtCommit)
                {
                    commit.DeviceRaceId = deviceDistrictId;
                    commit.DeviceRaceName = deviceDistrictName;

                    int locSubCode = Convert.ToInt32(commit.DeviceId.Substring(commit.DeviceId.Length - 4, 2));
                    var evc = locations.Single(l => l.LocationCode == locCode && l.LocationSubCode == locSubCode);

                    if (evc.Name.Contains("EVC"))
                        commit.VoteType = (short)VoteType.Early;
                    else if (commit.DeviceRaceId == commit.VoteDistrictRaceId)
                        commit.VoteType = (short)VoteType.Ordinary;
                    else
                        commit.VoteType = (short)VoteType.Absent;
                }
            }

            vVoteContext.SaveChanges();
        }
        */

        /// <summary>
        /// Packs the vote preferences and its ciphers, for the mix-net.
        /// </summary>
        /// <param name="votePref">List of comma separated vote preferences.</param>
        /// <param name="ciphers">List of ciphers after ballot reduction.</param>
        /// <param name="blockSize">The block size for packing.</param>
        /// <returns>Packed ciphers.</returns>
        private PackCiphers PackageProcess(List<string> votePref, List<Cipher> ciphers, int blockSize)
        {
            Logger.InfoFormat("Preferences count: {0}, Cipher count: {1}", votePref.Count, ciphers.Count);
            Dictionary<int, Cipher> pairSequence = new Dictionary<int, Cipher>();
            for (int i = 0; i < votePref.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(votePref[i]))
                    continue;

                pairSequence.Add(Convert.ToInt32(votePref[i]), ciphers[i]);
            }
            var sequence = pairSequence.OrderBy(so => so.Key).ToList(); //Pair the sequence and ciphers and order them by preference in ascending order.

            PackCiphers pcs = new PackCiphers() { BallotLength = votePref.Count, PrefCount = sequence.Count };
            PackCipher pc = default(PackCipher);

            for (int i = 0, p = 1; i < sequence.Count; i++, p++)
            {
                ECPoint[] ecPoint = ECHelper.CipherToECPoint(sequence[i].Value); //Get the EC point through EC util.
                // Pack the vote
                if (pc == null)
                {
                    pc = new PackCipher();
                    pc.GYR = ecPoint[0].Multiply(BigInteger.ValueOf(p));//Multiply the cipher by its preference.
                    pc.MYR = ecPoint[1].Multiply(BigInteger.ValueOf(p));//Multiply the cipher by its preference.
                }
                else
                {
                    pcs.IsPacked = true;
                    pc.GYR = pc.GYR.Add(ecPoint[0].Multiply(BigInteger.ValueOf(p)));
                    pc.MYR = pc.MYR.Add(ecPoint[1].Multiply(BigInteger.ValueOf(p)));
                }
                if (!pcs.Ciphers.Contains(pc))
                    pcs.Ciphers.Add(pc);
                if ((i + 1) % blockSize == 0)// if pack is greater than block size, create a new pack.
                {
                    pc = null;
                    p = 0;
                }
            }
            Logger.InfoFormat("Total ciphers packed: {0}", pcs.Ciphers.Count);

            return pcs;
        }

        /// <summary>
        /// Read the permutations form the cipher files.
        /// </summary>
        /// <param name="path">Path of cipher file.</param>
        /// <returns>List of device permutations.</returns>
        private List<DevicePermutationEntity> ReadGenCipherFile(byte[] cipherFile)
        {
            List<DevicePermutationEntity> result = new List<DevicePermutationEntity>();

            using (TextReader fs = new StreamReader(new MemoryStream(cipherFile)))
            {
                while (fs.Peek() >= 0)
                {
                    JObject jo = JObject.Parse(fs.ReadLine());
                    DevicePermutationEntity dp = jo.ToObject<DevicePermutationEntity>();
                    result.Add(dp);
                }
            }

            return result;
        }

        private List<RaceMap> ReadRaceMapFile(byte[] raceMapFile)
        {
            List<RaceMap> result = new List<RaceMap>();

            using (TextReader fs = new StreamReader(new MemoryStream(raceMapFile)))
            {
                while (fs.Peek() >= 0)
                {
                    JObject jo = JObject.Parse(fs.ReadLine());
                    RaceMap rm = jo.ToObject<RaceMap>();
                    result.Add(rm);
                }
            }

            return result;
        }

        /*
        /// <summary>
        /// Reads the commit file present in the context folder, normalizes it into the database.
        /// </summary>
        /// <param name="context">The context for the processing.</param>
        /// <param name="eve">The event to which this commit is associated.</param>
        /// <param name="path">Path of the commit file.</param>
        /// <param name="Quarantine">List of serial numbers to ignore.</param>
        private void ReadCommitFile(Guid context, Event eve, string path, List<string> Quarantine)
        {
            foreach (var file in Directory.GetFiles(path))
            {
                using (TextReader fs = new StreamReader(file))
                {
                    while (fs.Peek() >= 0)
                    {
                        JObject jo = JObject.Parse(fs.ReadLine());

                        if (jo["serialNo"] != null)
                        {
                            if (Quarantine.Contains(jo["serialNo"].Value<string>()))
                                continue;
                            //Read only the necessary fields.
                            eve.PackageCommits.Add(new PackageCommit()
                            {
                                SerialNo = jo["serialNo"].Value<string>(),
                                DeviceId = jo["boothID"].Value<string>(),
                                District = jo["district"].Value<string>(),
                                Type = (short)(CommitType)Enum.Parse(typeof(CommitType), jo["type"].Value<string>(), true),
                                BallotReductions = jo["ballotReductions"] == null ? null : jo["ballotReductions"].ToString(),
                                Preferences = jo["_vPrefs"] == null ? null : jo["_vPrefs"].ToString(),
                                CommitTime = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(jo["commitTime"].Value<double>()).ToUniversalTime(),
                                Context = context
                            });
                        }
                    }
                    vVoteContext.SaveChanges();
                }
            }
        }
        */
    }
}
