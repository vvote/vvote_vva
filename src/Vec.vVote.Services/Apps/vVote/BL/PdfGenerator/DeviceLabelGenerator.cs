﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.vVote.Entities;

namespace Vec.Apps.vVote.BL.PdfGenerator
{
    public interface IPdfGenerator<T>
    {
        void GeneratePDF();

        DirectoryInfo ContextDirectory { get; set; }

        T Entity { get; set; }

        string FileName { get; set; }
    }

    public abstract class PdfGeneratorBase<T> : IPdfGenerator<T>
    {
        public static I GetGeneratorInstance<I>() where I : IPdfGenerator<T>, new()
        {
            return new I();
        }

        public abstract void GeneratePDF();

        public DirectoryInfo ContextDirectory { get; set; }

        public T Entity { get; set; }

        public string FileName { get; set; }
    }

    public class DeviceLabelGenerator : PdfGeneratorBase<List<EventDeviceEntity>>
    {
        public override void GeneratePDF()
        {
            string pdfpath = Path.Combine(ContextDirectory.FullName, FileName);
            int maxLabelCount = 21;
            Document document = new Document();
            PdfSmartCopy copy = new PdfSmartCopy(document, new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, pdfpath), FileMode.Create));
            try
            {
                document.Open();

                for (int i = 0; i < Math.Ceiling((decimal)Entity.Count / maxLabelCount); i++)
                {
                    byte[] page = null;
                    {
                        string pdfTemplatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "LabelTemplate.pdf");
                        using (PdfReader templateReader = new PdfReader(pdfTemplatePath))
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                PdfStamper stamper = new PdfStamper(templateReader, ms);
                                AcroFields form = stamper.AcroFields;

                                for (int j = 0; j < maxLabelCount; j++)
                                {
                                    if ((maxLabelCount * i + j) >= Entity.Count)
                                        break;
                                    int currentIndex = maxLabelCount * i + j;
                                    form.SetField("Texta" + (j + 1), Entity[currentIndex].EventLocationEntity.Name + Chunk.NEWLINE + string.Format("Device {0}", Entity[currentIndex].DeviceId));
                                    form.SetField("Textb" + (j + 1) , Entity[currentIndex].Name);
                                }

                                stamper.FormFlattening = true;

                                stamper.Close();
                                page = ms.ToArray();
                            }
                        }
                    }

                    PdfReader reader = new PdfReader(page);
                    copy.AddPage(copy.GetImportedPage(reader, 1));
                    reader.Close();
                }
            }
            finally
            {
                copy.Close();
                document.Close();
            }
        }
    }
}
