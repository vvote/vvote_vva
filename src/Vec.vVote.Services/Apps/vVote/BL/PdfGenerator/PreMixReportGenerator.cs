﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.vVote.Entities;

namespace Vec.Apps.vVote.BL.PdfGenerator
{
    public class PreMixReportGenerator : PdfGeneratorBase<PreMixEntity>
    {
        public override void GeneratePDF()
        {
            string pdfpath = Path.Combine(ContextDirectory.FullName, string.Format("{0}.pdf", FileName.Replace(" ", "_")));
            string pdfTemplatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PreMixTemplate.pdf");

            Document document = new Document();
            PdfSmartCopy copy = new PdfSmartCopy(document, new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, pdfpath), FileMode.Create)); //TODO: change all the Application.Hostpath to use BaseDirectory to makes sure the dll coudl be used from any app.
            try
            {
                document.Open();

                byte[] page = null;

                using (PdfReader templateReader = new PdfReader(pdfTemplatePath))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        PdfStamper stamper = new PdfStamper(templateReader, ms);
                        AcroFields form = stamper.AcroFields;
                        form.SetField("Title", "Electronic Votes");
                        form.SetField("PrintDateTimeLabel", "Print Date & Time");
                        form.SetField("PrintDateTimeValue", Entity.ReportDateTime.ToString());
                        form.SetField("EventName", Entity.EventName);
                        form.SetField("DistrictName", Entity.DistrictName);
                        form.SetField("CommitDateTimeLabel", "Commit Date & Time");
                        form.SetField("CommitDateTimeValue", Entity.CommitDateTime.ToString());

                        form.SetField("CommitDateTimeLabel", "Commit Date & Time");
                        form.SetField("CommitDateTimeValue", Entity.CommitDateTime.ToString());


                        form.SetField("EarlyName", "Early");
                        form.SetField("AbsentName", "Absent");
                        form.SetField("TotalLabel", "Total");
                        form.SetField("TotalValue", Entity.Commits.Count.ToString());

                        form.SetField("EarlyTotal", Entity.Commits.Count(c => c.VoteType == VoteType.Early).ToString());
                        form.SetField("AbsentTotal", Entity.Commits.Count(c => c.VoteType == VoteType.Absent).ToString());

                        var oCommit = Entity.Commits
                            .Where(c => c.VoteType == VoteType.Ordinary)
                            .GroupBy(g => new { LocationCode = g.DeviceLocationCode, LocationSubCode = g.DeviceLocationSubCode });

                        int i = 1;
                        foreach (var oCommitItem in oCommit)
                        {
                            string locationName = Entity.Locations.Single(l => l.LocationCode == oCommitItem.Key.LocationCode && l.LocationSubCode == oCommitItem.Key.LocationSubCode).Name;
                            form.SetField("OName" + i, string.Format("Ordinary - {0}", locationName));
                            form.SetField("OTotal" + i, oCommitItem.Count().ToString());
                            i++;
                        }

                        stamper.FormFlattening = true;

                        stamper.Close();
                        page = ms.ToArray();
                    }
                }

                PdfReader reader = new PdfReader(page);
                copy.AddPage(copy.GetImportedPage(reader, 1));
                reader.Close();
            }
            finally
            {
                copy.Close();
                document.Close();
            }
        }
    }
}
