﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Hosting;
using Vec.Apps.vVote.BL.Common;
using Vec.Apps.vVote.Common;
using Vec.Apps.vVote.Common.Signature;
using Vec.Apps.vVote.Converter;
using Vec.Apps.vVote.DAL;
using Vec.Apps.vVote.Entities;
using Vec.Apps.vVote.Reader;
using Vec.Properties;
using Vec.Resources;
using Vec.TuringElectionService;
using Vec.TuringNominationService;

namespace Vec.Apps.vVote.BL
{
    public interface IBundleBL
    {
        byte[] GenerateEventBundle(Guid eventId, string contextPath);

        void UpdateStagingLocation(Guid eventId, DirectoryInfo contextDirectory);

        EventDeviceEntity GetDeviceConfig(int eventDeviceId);

        void GenerateStaticContentBundle(Guid eventId, string rootPath);

        void UploadBundle(Guid eventId, bool isFinal, DirectoryInfo contextDirectory);

        List<EventLocationEntity> GetStagingLocation(Guid eventId);
    }

    public class BundleBL : BaseBL, IBundleBL
    {
        public IvVoteContext vVoteContext { get; set; }
        public IElectionBL ElectionBL { get; set; }
        public IFileServiceHelper FileService { get; set; }
        public IEventBL EventBL { get; set; }
        public IAudioConverter AudioConverter { get; set; }

        public BundleBL(IvVoteContext vVoteContext, IElectionBL electionBL, IEventBL eventBL, IFileServiceHelper fileService, IAudioConverter audioConverter)
        {
            this.vVoteContext = vVoteContext;
            this.ElectionBL = electionBL;
            this.EventBL = eventBL;
            this.FileService = fileService;
            this.AudioConverter = audioConverter;
        }

        public void GenerateStaticContentBundle(Guid eventId, string rootPath)
        {
            EventBL.GenerateStaticContentBundle(eventId, rootPath);
        }

        public byte[] GenerateEventBundle(Guid eventId, string contextPath)
        {
            #region Setup the bundle folder...
            Directory.CreateDirectory(Path.Combine(contextPath, Settings.Default.ElectionAudioPath.Replace(Constants.BundlePath, string.Empty)));
            Directory.CreateDirectory(Path.Combine(contextPath, Settings.Default.RegionAudioPath.Replace(Constants.BundlePath, string.Empty)));
            Directory.CreateDirectory(Path.Combine(contextPath, Settings.Default.PartyAudioPath.Replace(Constants.BundlePath, string.Empty)));
            Directory.CreateDirectory(Path.Combine(contextPath, Settings.Default.DistrictAudioPath.Replace(Constants.BundlePath, string.Empty)));
            Directory.CreateDirectory(Path.Combine(contextPath, Settings.Default.CandidateAudioPath.Replace(Constants.BundlePath, string.Empty)));
            #endregion Setup the bundle folder...

            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            var electionIds = from eveElection in eve.EventElections
                              select eveElection.ElectionId;

            var ballotOrder = GetBallotOrder(eve, electionIds.ToList());

            //Generate the region district translation file.
            JObject translation = GenerateTranslation(eve);

            //Generate the region district candidate file.
            JArray ballot = GenerateBallot(ballotOrder);

            //Generate the district configuration file for MBB.
            JObject districtConfig = GenerateDistrictConfig(ballotOrder);

            //Generate the version file.
            JObject version = GenerateVersion(eve);

            CopyAudioFiles(eve, ballotOrder, contextPath); //TODO: Make this more generic...pls..

            //Convert mp3 to ogg
            {
                Logger.Debug(string.Format("Reading from directory: {0}", contextPath));
                DirectoryInfo di = new DirectoryInfo(contextPath);
                foreach (var file in di.GetFiles("*.mp3", SearchOption.AllDirectories))
                {
                    var outFile = Path.Combine(file.DirectoryName, Path.GetFileNameWithoutExtension(file.FullName) + ".ogg");
                    var result = AudioConverter.Convert(file.FullName, outFile);
                    Logger.Debug(string.Format("{0} -> {1} = {2}{3}{4}", result.InFile, result.OutFile, result.IsSuccess, Environment.NewLine, result.IsSuccess == false ? result.ExecutionLog : string.Empty));
                    file.Delete();
                }
            }

            var convertedBallot = ballot.ToString().Replace(".mp3", ".ogg");
            var convertedTranslation = translation.ToString().Replace(".mp3", ".ogg");

            File.WriteAllText(Path.Combine(contextPath, Constants.RegionDistrictCandidateFileName), convertedBallot);

            File.WriteAllText(Path.Combine(contextPath, Constants.RegionDistrictTranslationFileName), convertedTranslation);

            File.WriteAllText(Path.Combine(contextPath, Constants.DistrictConfigFileName), districtConfig.ToString());

            File.WriteAllText(Path.Combine(contextPath, Constants.VersionFileName), version.ToString());

            //District region map & candidate ballot order needed for the WBB to lookup the receipts...
            {
                JObject districtRegionMap = new JObject();
                foreach (var region in ballotOrder)
                    foreach (var district in region.Districts)
                        districtRegionMap.Add(district.Name, region.Name);

                FileService.Put(eve.GetWbbFileServiceAddress(), Constants.DataUrl + Constants.DistrictRegionMapFileName, districtRegionMap.ToString(Newtonsoft.Json.Formatting.Indented).GetBytes());
                FileService.Put(eve.GetWbbFileServiceAddress(), Constants.DataUrl + Constants.RegionDistrictCandidateFileName, convertedBallot.GetBytes());
            }

            #region Signing files...
            {
                string[] fileFilter = Settings.Default.BundleFileFilter.Split(';');
                foreach (var fileItem in Directory.GetFiles(contextPath, "*.*", SearchOption.AllDirectories).Where(s => fileFilter.Any(f => s.EndsWith(f))))
                {
                    PGP.SignFile(fileItem, Path.Combine(HostingEnvironment.ApplicationPhysicalPath, Settings.Default.PrivateFile), Settings.Default.Passphrase.ToCharArray());
                }
            }
            #endregion Signing files...

            Zipper.Archive(contextPath, string.Empty);

            byte[] fileBytes = File.ReadAllBytes(contextPath + ".zip");
            FileService.Put(eve.GetWbbFileServiceAddress(), Settings.Default.DraftBundleUrl, fileBytes);

            vVoteContext.SaveChanges();

            Logger.Info("GenerateEventBundle Complete.");

            return fileBytes;
        }

        private JObject GenerateVersion(Event eve)
        {
            JObject result = new JObject();
            result.Add("Environment", Settings.Default.Environment);
            result.Add("EventId", eve.Id);
            result.Add("EventName", eve.Name);
            result.Add("Build", "DRAFT");
            result.Add("Revision", ++eve.BundleRevision);
            result.Add("DateTime", DateTime.UtcNow.ToString("o"));

            return result;
        }

        public void UploadBundle(Guid eventId, bool isFinal, DirectoryInfo contextDirectory)
        {
            byte[] fileBytes = null;
            string uploadPath = Settings.Default.DraftBundleUrl;
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            if (isFinal)
            {
                uploadPath = Settings.Default.FinalBundleUrl;

                Zipper.Archive(contextDirectory.FullName, string.Empty);
                fileBytes = File.ReadAllBytes(contextDirectory.FullName + ".zip");
            }
            else
            {
                #region Update revision...
                string versionFile = Path.Combine(contextDirectory.FullName, Constants.VersionFileName);

                if (File.Exists(versionFile))
                {
                    JObject version = JObject.Parse(File.ReadAllText(versionFile));

                    version["Revision"] = ++eve.BundleRevision;
                    version["Build"] = "DRAFT";
                    version["DateTime"] = DateTime.UtcNow.ToString("o");

                    File.WriteAllText(Path.Combine(contextDirectory.FullName, Constants.VersionFileName), version.ToString());
                }
                #endregion Update revision...

                #region Signing files...
                {
                    string[] fileFilter = Settings.Default.BundleFileFilter.Split(';');
                    foreach (var fileItem in Directory.GetFiles(contextDirectory.FullName, "*.*", SearchOption.AllDirectories).Where(s => fileFilter.Any(f => s.EndsWith(f))))
                    {
                        PGP.SignFile(fileItem, Path.Combine(HostingEnvironment.ApplicationPhysicalPath, Settings.Default.PrivateFile), Settings.Default.Passphrase.ToCharArray());
                    }
                }
                #endregion Signing files...

                Zipper.Archive(contextDirectory.FullName, string.Empty);
                fileBytes = File.ReadAllBytes(contextDirectory.FullName + ".zip");
            }

            FileService.Put(eve.GetWbbFileServiceAddress(), uploadPath, fileBytes);

            vVoteContext.SaveChanges();
        }

        public void UpdateStagingLocation(Guid eventId, DirectoryInfo contextDirectory)
        {
            Event eve = vVoteContext.Events.Single(e => e.Id == eventId);

            if (eve.EventLocations.Any(eveLoc => eveLoc.EventDevices.Count(el => el.Stage > 0) > 0))
                throw new VoteException(Constants.ERROR_UNABLE_TO_IMPORT_RESOURCE, 100);
            //TODO: used typeddataset...
            DataSet sheetData;
            string stagingLocationFilePath = Path.Combine(contextDirectory.FullName, Constants.StagingLocationFileName);
            using (SpreadsheetReader ssr = new SpreadsheetReader(stagingLocationFilePath))
            {
                sheetData = ssr.GetDataSet("District", "ISOS");
            }

            Dictionary<string, string> eveRaces = new Dictionary<string, string>();
            {//Check if races exist in turing...
                var eveElection = from election in eve.EventElections
                                  select election.ElectionId;
                var elections = ElectionBL.GetElections(eveElection.ToList());
                IEnumerable<MElectorateLookup> electorates = new List<MElectorateLookup>();
                foreach (var electionItem in elections)
                    electorates = electorates.Union(electionItem.ParentElectorates);

                var locRaceGroup = sheetData.Tables["District"].AsEnumerable().Union(sheetData.Tables["ISOS"].AsEnumerable()).GroupBy(g => g.Field<string>("Race"));

                foreach (var locRaceItem in locRaceGroup)
                {
                    if (locRaceItem.Key == "Any")
                    {
                        eveRaces.Add(locRaceItem.Key, null);
                        continue;
                    }

                    var parent = electorates.SingleOrDefault(e => e.Electorates.Any(dis => dis.Name.StartsWith(locRaceItem.Key, StringComparison.InvariantCultureIgnoreCase)));
                    if (parent == null)
                        throw new VoteException(string.Format("The race '{0}' is not found in Turing for this event.", locRaceItem.Key), 200);
                    var child = parent.Electorates.Single(e => e.Name.StartsWith(locRaceItem.Key, StringComparison.InvariantCultureIgnoreCase));

                    eveRaces.Add(child.Name.RegexReplace(@"\sDistrict.*\z"), parent.Name.RegexReplace(@"\sRegion.*\z"));
                }
            }

            List<EventLocationEntity> deviceLocations = new List<EventLocationEntity>();
            foreach (DataTable dataTable in sheetData.Tables)
            {
                foreach (DataRow dRow in dataTable.Rows)
                {
                    var locationCode = Convert.ToInt32(dRow["LocationCode"]);
                    var location = dRow["Location"].ToString();
                    var evmCount = Convert.ToInt32(dRow["EVM"]);
                    var vpsCount = Convert.ToInt32(dRow["VPS"]);
                    var raceName = dRow["Race"].ToString();
                    var type = string.IsNullOrWhiteSpace(Convert.ToString(dRow["Type"])) ? Vec.Apps.vVote.Entities.LocationType.District : (Entities.LocationType)Enum.Parse(typeof(Entities.LocationType), dRow["Type"].ToString());

                    deviceLocations.Add(new EventLocationEntity()
                    {
                        Name = location,
                        LocationCode = locationCode,
                        EvmCount = evmCount,
                        VpsCount = vpsCount,
                        RaceName = raceName,
                        ParentRaceName = eveRaces.Single(er => er.Key == raceName).Value,
                        Type = type
                    });
                }
            }

            while (eve.EventLocations.Count > 0)
                vVoteContext.EventLocations.Remove(eve.EventLocations.First());
            vVoteContext.SaveChanges();

            var deviceLocGroup = from deviceLoc in deviceLocations
                                 group deviceLoc by deviceLoc.LocationCode;

            foreach (var locGroup in deviceLocGroup)
            {
                var locationCode = locGroup.Key;
                int locSubId = 1;
                foreach (var locItem in locGroup)
                {
                    EventLocation eventLocation = new EventLocation()
                    {
                        LocationCode = locationCode,
                        LocationName = locItem.Name,
                        RaceName = locItem.RaceName,
                        ParentRaceName = locItem.ParentRaceName,
                        Type = (int)locItem.Type,
                        LocationSubCode = locItem.LocationSubCode = locSubId
                    };
                    eve.EventLocations.Add(eventLocation);

                    int rollon = 1;
                    for (int i = 1; i <= locItem.VpsCount; i++, rollon++)
                    {
                        eventLocation.EventDevices.Add(new EventDevice()
                        {
                            DeviceId = Convert.ToInt32(string.Format("{0}{1:d2}{2:d2}", locationCode, locSubId, rollon)),
                            Type = (int)Entities.DeviceType.VPS,
                            QuarantineQuota = Settings.Default.DefaultQuarantineQuota,
                            IsAuditEnabled = Settings.Default.IsAuditEnabled
                        });
                    }

                    for (int i = 1; i <= locItem.EvmCount; i++, rollon++)
                    {
                        eventLocation.EventDevices.Add(new EventDevice()
                        {
                            DeviceId = Convert.ToInt32(string.Format("{0}{1:d2}{2:d2}", locationCode, locSubId, rollon)),
                            Type = (int)Entities.DeviceType.EVM
                        });
                    }

                    locSubId++;
                }
            }

            PGP.SignFile(stagingLocationFilePath, Path.Combine(HostingEnvironment.ApplicationPhysicalPath, Settings.Default.PrivateFile), Settings.Default.Passphrase.ToCharArray());
            FileService.Put(eve.GetWbbFileServiceAddress(), Constants.DataUrl + Constants.LocationFileName, File.ReadAllBytes(stagingLocationFilePath));
            FileService.Put(eve.GetWbbFileServiceAddress(), Constants.DataUrl + Constants.LocationFileName + ".bpg", File.ReadAllBytes(stagingLocationFilePath + ".bpg"));

            JArray deviceLocationJson = JArray.FromObject(deviceLocations);
            string stagingLocationJsonFilePath = Path.Combine(contextDirectory.FullName, Constants.LocationJsonFileName);
            File.WriteAllText(stagingLocationJsonFilePath, deviceLocationJson.ToString());
            PGP.SignFile(stagingLocationJsonFilePath, Path.Combine(HostingEnvironment.ApplicationPhysicalPath, Settings.Default.PrivateFile), Settings.Default.Passphrase.ToCharArray());
            FileService.Put(eve.GetWbbFileServiceAddress(), Constants.DataUrl + Constants.LocationJsonFileName, File.ReadAllBytes(stagingLocationJsonFilePath));
            FileService.Put(eve.GetWbbFileServiceAddress(), Constants.DataUrl + Constants.LocationJsonFileName + ".bpg", File.ReadAllBytes(stagingLocationJsonFilePath + ".bpg"));

            vVoteContext.SaveChanges();
        }

        public EventDeviceEntity GetDeviceConfig(int eventDeviceId)
        {
            EventDeviceEntity result = new EventDeviceEntity();

            var eveDevice = vVoteContext.EventDevices.Single(e => e.Id == eventDeviceId);
            result.Id = eveDevice.Id;
            result.DeviceId = eveDevice.DeviceId;
            result.TypeId = eveDevice.Type;
            result.TypeName = Enum.GetName(typeof(DeviceType), eveDevice.Type);
            result.StageId = eveDevice.Stage;
            result.StageName = Enum.GetName(typeof(DeviceStageType), eveDevice.Stage);
            result.QuarantineQuota = eveDevice.QuarantineQuota;
            result.DeployedLocationId = eveDevice.DeployedLocationId;
            result.IsAuditEnabled = eveDevice.IsAuditEnabled;
            result.MaxBundleSize = Settings.Default.MaxBundleSize;
            result.LaunchUrl = eveDevice.Type == (byte)DeviceType.VPS ? Settings.Default.VpsLaunchUrl : Settings.Default.EvmLaunchUrl;
            result.HeartbeatURL = eveDevice.Type == (byte)DeviceType.VPS ? Settings.Default.VpsHeartbeatUrl : Settings.Default.EvmHeartbeatUrl;
            result.ConnectionCheckURL = Settings.Default.ConnectionCheckUrl;
            result.F5LaunchURL = Settings.Default.F5LaunchUrl;
            result.EventLocationEntity = new EventLocationEntity()
            {
                Id = eveDevice.EventLocationId,
                Name = eveDevice.EventLocation.LocationName,
                LocationCode = eveDevice.EventLocation.LocationCode,
                LocationSubCode = eveDevice.EventLocation.LocationSubCode,
                RaceName = eveDevice.EventLocation.RaceName,
                ParentRaceName = eveDevice.EventLocation.ParentRaceName,
                Type = (Entities.LocationType)eveDevice.EventLocation.Type.Value,
            };
            result.EventEntity = EventBL.GetEvent(eveDevice.EventLocation.EventId);

            return result;
        }

        public List<EventLocationEntity> GetStagingLocation(Guid eventId)
        {
            List<EventLocationEntity> result = new List<EventLocationEntity>();
            Event eve = vVoteContext.Events.Single(e => e.Id == eventId);

            foreach (var loc in eve.EventLocations.Where(el => el.Type < 10))
            {
                result.Add(new EventLocationEntity()
                {
                    Id = loc.Id,
                    Name = loc.LocationName,
                    LocationCode = loc.LocationCode,
                    LocationSubCode = loc.LocationSubCode,
                    RaceName = loc.RaceName,
                    ParentRaceName = loc.ParentRaceName,
                    Type = (Entities.LocationType)loc.Type.Value,
                });
            }

            return result;
        }

        #region private methods
        protected virtual BallotOrderEntity GetBallotOrder(Event eve, List<Guid> electionIds)
        {
            BallotOrderEntity result = new BallotOrderEntity();

            foreach (var electionItem in ElectionBL.GetElections(electionIds))
            {
                foreach (var electorateItem in electionItem.ParentElectorates)
                {
                    var electionElectorate = electorateItem.Electorates.FirstOrDefault(e => e.Type == ElectorateType.Region && e.ElectorateVacancyConfiguration != null);// && e.ElectorateVacancyConfiguration.ElectionElectorateVacancyStatus == ElectionElectorateVacancyStatus.Contested);
                    RegionEntity region = default(RegionEntity);

                    if (electionElectorate == null && electionItem.ElectionType == ElectionType.ByElection)
                    {
                        region = new RegionEntity()
                        {
                            Id = Guid.Empty,
                            Name = electorateItem.Name.RegexReplace(@"\sRegion.*\z"),
                            ElectionElectorateVacancyId = Guid.Empty,
                            NoRace = true,
                            Audio = string.Empty
                        };
                    }
                    else
                    {

                        region = new RegionEntity()
                        {
                            Id = electionElectorate.ElectorateVacancyConfiguration.ElectionElectorateVacancyId,
                            Name = electionElectorate.Name.RegexReplace(@"\sRegion.*\z"),
                            ElectionElectorateVacancyId = electionElectorate.ElectorateVacancyConfiguration.ElectionElectorateVacancyId,
                            NoRace = electionElectorate.ElectorateVacancyConfiguration.ElectionElectorateVacancyStatus != ElectionElectorateVacancyStatus.Contested,
                            Audio = Settings.Default.RegionAudioPath + eve.LocationAudios.Single(r => r.LocationId == electionElectorate.ElectorateVacancyConfiguration.ElectionElectorateVacancyId).FileName
                        };
                    }

                    foreach (var districtItem in electorateItem.Electorates.Where(e => e.Type == ElectorateType.District && e.ElectorateVacancyConfiguration != null))// && e.ElectorateVacancyConfiguration.ElectionElectorateVacancyStatus == ElectionElectorateVacancyStatus.Contested))
                    {
                        var district = new DistrictEntity()
                        {
                            Id = districtItem.ElectorateVacancyConfiguration.ElectionElectorateVacancyId,
                            Name = districtItem.Name.RegexReplace(@"\sDistrict.*\z"),
                            ElectionElectorateVacancyId = districtItem.ElectorateVacancyConfiguration.ElectionElectorateVacancyId,
                            Uncontested = districtItem.ElectorateVacancyConfiguration.ElectionElectorateVacancyStatus != ElectionElectorateVacancyStatus.Contested,
                            Audio = Settings.Default.DistrictAudioPath + eve.LocationAudios.Single(d => d.LocationId == districtItem.ElectorateVacancyConfiguration.ElectionElectorateVacancyId).FileName
                        };

                        region.Districts.Add(district);
                    };

                    PopulateDistrictCandidates(eve, region.Districts);

                    result.Add(region);
                }

                PopulateRegionParties(eve, result);
            }

            Logger.Info("GetBallotOrder Complete.");

            return result;
        }

        protected virtual void PopulateDistrictCandidates(Event eve, List<DistrictEntity> districts)
        {
            var electorateVacencyIds = from district in districts
                                       select district.ElectionElectorateVacancyId;

            var entities = ElectionBL.BallotDrawCandidates(electorateVacencyIds.ToList());

            foreach (var entityItem in entities)
            {
                var distric = districts.First(d => d.ElectionElectorateVacancyId == entityItem.ElectionElectorateVacancyId);

                //TODO: put the ballot paper positing order and send out the warning.
                var bdne = entityItem.BallotDrawnCandidates.OrderBy(o => o.BallotPaperPosition).ToArray();

                CopyCandidates(eve, bdne, distric.Candidates);
            }

            Logger.Info("PopulateDistrictCandidates Complete.");
        }

        protected virtual void CopyCandidates(Event eve, BallotDrawnNominationEntity[] source, List<CandidateEntity> destination)
        {
            for (int i = 0, pos = 1; i < source.Length; i++)
            {
                int candidateId = source[i].Id;

                //TODO: send out a warning since candidate is not in system.
                if (eve.CandidateAudios.SingleOrDefault(c => c.CandidateId == candidateId) == null) //TODO: remove this condition, it should fail if the candidate audio does not exist.
                    continue;

                CandidateEntity candidate = new CandidateEntity()
                {
                    Id = candidateId,
                    Name = source[i].BallotPaperName,
                    BallotSequence = source[i].OrderWithinGroupOrTeam.HasValue ? source[i].OrderWithinGroupOrTeam.Value : pos++, //Order within group is null for ungrouped and district so, generate sequence now.
                    BallotPosition = source[i].BallotPaperPosition, //Ballot position is restarted to ensure it is properly listed as LC_BTL -- Convert.ToString(source[i].BallotPaperPosition) ?? string.Empty,
                    PartyName = source[i].PreferredRPPName ?? string.Empty,
                    Region = source[i].BallotPaperLocality ?? string.Empty,
                    Audio = new AudioEntity() { FilePath = Settings.Default.CandidateAudioPath + eve.CandidateAudios.Single(c => c.CandidateId == candidateId).FileName }
                };

                destination.Add(candidate);
            }

            Logger.Info("PopulateCandidates Complete.");
        }

        protected virtual void PopulateRegionParties(Event eve, List<RegionEntity> regions)
        {
            var electorateVacencyIds = from region in regions
                                       where region.ElectionElectorateVacancyId != Guid.Empty
                                       select region.ElectionElectorateVacancyId;

            var entities = ElectionBL.BallotDrawGroups(electorateVacencyIds.ToList());

            foreach (var entityItem in entities)
            {
                var region = regions.First(r => r.ElectionElectorateVacancyId == entityItem.ElectionElectorateVacancyId);

                //TODO: put the ballot paper positing order and send out the warning.
                var bdg = entityItem.BallotDrawnGroups.OrderBy(o => o.BallotPaperPosition).ToArray();

                foreach (var ballotGroup in bdg)
                {
                    Party par = eve.Parties.Single(p => p.Name == ballotGroup.Name);
                    PartyEntity party = new PartyEntity()
                    {
                        Id = par.Id,
                        Name = ballotGroup.Name,
                        BallotBoxLetter = ballotGroup.BallotPaperLetter ?? string.Empty,
                        GroupBallotPosition = ballotGroup.BallotPaperPosition,
                        HasBallotBox = !string.IsNullOrEmpty(ballotGroup.BallotPaperLetter),
                        Ungrouped = ballotGroup.Ungrouped,
                        Unnamed = ballotGroup.Unnamed,
                        Initials = string.Empty, //TODO: populate Initials once data is received
                        DefaultAudio = new AudioEntity() { FilePath = Settings.Default.PartyAudioPath + par.PartyAudios.Single(p => p.LanguageId == 1).FileName }
                    };

                    //TODO: put the ballot paper positing order and send out the warning.
                    var bdne = ballotGroup.Nominations.OrderBy(o => o.BallotPaperPosition).ToArray();

                    CopyCandidates(eve, bdne, party.Candidates);

                    region.Parties.Add(party);
                }
            }

            Logger.Info("PopulateRegionParties Complete.");
        }

        private JArray GenerateBallot(BallotOrderEntity ballotOrder)
        {
            Func<List<CandidateEntity>, JArray> candidateArray = delegate(List<CandidateEntity> candidateList)
            {
                JArray candidates = new JArray();

                foreach (var candidateItem in candidateList)
                {
                    JObject candidate = new JObject();
                    candidate.Add("id", candidateItem.Id);
                    candidate.Add("name", candidateItem.Name);
                    candidate.Add("region", candidateItem.Region);
                    candidate.Add("partyName", candidateItem.PartyName);
                    candidate.Add("ballotSequence", candidateItem.BallotSequence);
                    candidate.Add("ballotPosition", candidateItem.BallotPosition);
                    candidate.Add("audio", candidateItem.Audio.FilePath);
                    candidates.Add(candidate);
                }

                Logger.Info("GenerateBallot Complete.");
                return candidates;
            };

            Func<List<PartyEntity>, JArray> partieArray = delegate(List<PartyEntity> partyList)
            {
                JArray parties = new JArray();

                foreach (var partyItem in partyList)
                {
                    JObject party = new JObject();
                    party.Add("id", partyItem.Id);
                    party.Add("preferredName", partyItem.Name);
                    party.Add("initials", partyItem.Initials);
                    party.Add("ballotBoxLetter", partyItem.BallotBoxLetter);
                    party.Add("groupBallotPosition", partyItem.GroupBallotPosition);
                    party.Add("ungrouped", partyItem.Ungrouped);
                    party.Add("unnamed", partyItem.Unnamed);
                    party.Add("hasBallotBox", partyItem.HasBallotBox);
                    party.Add("audio", partyItem.DefaultAudio.FilePath);
                    party.Add("candidates", candidateArray(partyItem.Candidates));
                    parties.Add(party);
                }

                return parties;
            };

            Func<List<DistrictEntity>, JArray> districtArray = delegate(List<DistrictEntity> districtList)
            {
                JArray districts = new JArray();

                foreach (var districtItem in districtList)
                {
                    JObject district = new JObject();
                    district.Add("district", districtItem.Name);
                    district.Add("uncontested", districtItem.Uncontested);
                    district.Add("locationAudio", districtItem.Audio);
                    district.Add("candidates", candidateArray(districtItem.Candidates));
                    districts.Add(district);
                }

                return districts;
            };

            JArray ballot = new JArray();
            foreach (var regionItem in ballotOrder)
            {
                JObject region = new JObject();
                region.Add("region", regionItem.Name);
                region.Add("locationAudio", regionItem.Audio);
                region.Add("no_race", regionItem.NoRace);
                region.Add("parties", partieArray(regionItem.Parties));
                region.Add("districts", districtArray(regionItem.Districts));

                ballot.Add(region);
            }

            return ballot;
        }

        private void CopyAudioFiles(Event eve, BallotOrderEntity ballotOrder, string bundlePath)
        {
            byte[] fileContent;

            foreach (var item in eve.EventAudios)
            {
                fileContent = FileService.Get(eve.GetWbbFileServiceAddress(), Settings.Default.ElectionAudioPath + item.FileName);
                File.WriteAllBytes(Path.Combine(bundlePath, Settings.Default.ElectionAudioPath.Replace(Constants.BundlePath, string.Empty), item.FileName), fileContent);
            }

            foreach (var party in eve.Parties)
                foreach (var partyAudio in party.PartyAudios.Where(pa => pa.PartyId == party.Id))
                {
                    fileContent = FileService.Get(eve.GetWbbFileServiceAddress(), Settings.Default.PartyAudioPath + partyAudio.FileName);
                    File.WriteAllBytes(Path.Combine(bundlePath, Settings.Default.PartyAudioPath.Replace(Constants.BundlePath, string.Empty), partyAudio.FileName), fileContent);
                }

            foreach (var regionItem in ballotOrder)
            {
                if (regionItem.Id != Guid.Empty) //Check for By-Election Region.
                {
                    fileContent = FileService.Get(eve.GetWbbFileServiceAddress(), Settings.Default.RegionAudioPath + string.Format(Settings.Default.DefaultAudioFileFormat, regionItem.Id));
                    File.WriteAllBytes(Path.Combine(bundlePath, Settings.Default.RegionAudioPath.Replace(Constants.BundlePath, string.Empty), string.Format(Settings.Default.DefaultAudioFileFormat, regionItem.Id)), fileContent);
                }

                foreach (var partyItem in regionItem.Parties)
                {
                    foreach (var candidateItem in partyItem.Candidates)
                    {
                        string fileName = eve.CandidateAudios.Single(c => c.CandidateId == candidateItem.Id).FileName;
                        fileContent = FileService.Get(eve.GetWbbFileServiceAddress(), Settings.Default.CandidateAudioPath + fileName);
                        File.WriteAllBytes(Path.Combine(bundlePath, Settings.Default.CandidateAudioPath.Replace(Constants.BundlePath, string.Empty), fileName), fileContent);
                    }
                }

                foreach (var districtItem in regionItem.Districts)
                {
                    fileContent = FileService.Get(eve.GetWbbFileServiceAddress(), Settings.Default.DistrictAudioPath + string.Format(Settings.Default.DefaultAudioFileFormat, districtItem.Id));
                    File.WriteAllBytes(Path.Combine(bundlePath, Settings.Default.DistrictAudioPath.Replace(Constants.BundlePath, string.Empty), string.Format(Settings.Default.DefaultAudioFileFormat, districtItem.Id)), fileContent);
                    foreach (var candidateItem in districtItem.Candidates)
                    {
                        string fileName = eve.CandidateAudios.Single(c => c.CandidateId == candidateItem.Id).FileName;
                        fileContent = FileService.Get(eve.GetWbbFileServiceAddress(), Settings.Default.CandidateAudioPath + fileName);
                        File.WriteAllBytes(Path.Combine(bundlePath, Settings.Default.CandidateAudioPath.Replace(Constants.BundlePath, string.Empty), fileName), fileContent);
                    }
                }
            }
            Logger.Info("CopyAudioFiles Complete.");
        }

        private JObject GenerateTranslation(Event eve)
        {
            JObject ballotTranslation = new JObject();

            {
                JArray parties = new JArray();
                ballotTranslation.Add("parties", parties);

                foreach (var partyItem in eve.Parties)
                {
                    JObject party = new JObject();
                    party.Add("id", partyItem.Name);
                    JObject preferredNameObj = new JObject();
                    JObject audioObj = new JObject();

                    foreach (var audioItem in partyItem.PartyAudios)
                    {
                        audioObj.Add(audioItem.Language.Name.ToLowerInvariant(), Settings.Default.PartyAudioPath + audioItem.FileName);
                    }
                    foreach (var textItem in partyItem.PartyTexts)
                    {
                        preferredNameObj.Add(textItem.Language.Name.ToLowerInvariant(), textItem.Name);
                    }

                    party.Add("preferredName", preferredNameObj);
                    party.Add("audio", audioObj);
                    parties.Add(party);
                }
            }

            {
                JObject electionTranslation;
                JObject preferredNameObj = new JObject();
                JObject audioObj = new JObject();

                foreach (var audioItem in eve.EventAudios.OrderBy(o => o.LanguageId))
                {
                    audioObj.Add(audioItem.Language.Name.ToLowerInvariant(), Settings.Default.ElectionAudioPath + audioItem.FileName);
                }
                foreach (var textItem in eve.EventTexts.OrderBy(o => o.LanguageId))
                {
                    preferredNameObj.Add(textItem.Language.Name.ToLowerInvariant(), textItem.Name);
                }

                electionTranslation = JObject.FromObject(new { preferredName = preferredNameObj, audio = audioObj });
                ballotTranslation.Add("election", electionTranslation);
            }

            Logger.Info("GenerateBallotTranslation Complete.");

            return ballotTranslation;
        }

        private JObject GenerateDistrictConfig(BallotOrderEntity ballotOrder)
        {
            JObject districtConfig = new JObject();
            foreach (var region in ballotOrder)
            {
                int atl = region.Parties.Where(p => p.HasBallotBox).Count();
                int btl = 0;
                foreach (var party in region.Parties)
                    btl += party.Candidates.Count;

                foreach (var district in region.Districts)
                {
                    districtConfig.Add(district.Name, JObject.FromObject(new { la = district.Candidates.Count, lc_atl = atl, lc_btl = btl }));
                }
            }

            Logger.Info("GenerateDistrictConfig Complete.");
            return districtConfig;
        }
        #endregion private methods
    }
}
