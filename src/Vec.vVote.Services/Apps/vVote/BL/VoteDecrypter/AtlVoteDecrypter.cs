﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.vVote.Entities;
using Vec.Apps.vVote.Common;
using System.Text.RegularExpressions;

namespace Vec.Apps.vVote.BL.VoteDecrypter
{
    /// <summary>
    /// Generates a PDF file from the input that has to be processed.
    /// </summary>
    public class AtlVoteDecrypter : VoteDecrypterBase
    {
        public const int MaxRegionAtlCandidateLabels = 28;
        /// <summary>
        /// Generates a PDF file by using its context properties.
        /// </summary>
        public override void GeneratePDF()
        {
            string pdfpath = Path.Combine(ContextPath, string.Format("{0}-ATL.pdf", FileName.GetValidFileName()));
            string pdfTemplatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ATLVoteTemplate.pdf");
            string imagepath = Path.Combine(ContextPath, "Signature.png");
            var lookupFile = Path.Combine(ContextPath, string.Format("{0}_lookup.json", Context));
            JArray lookupJson = JArray.Parse(File.ReadAllText(lookupFile));
            List<PartyEntity> contextParties = lookupJson.ToObject<List<PartyEntity>>();            

            Document document = new Document();
            PdfSmartCopy copy = new PdfSmartCopy(document, new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, pdfpath), FileMode.Create));
            try
            {
                document.Open();

                foreach (var fileItem in Files)
                {
                    for (int i = 0; i < fileItem.VoteDecryptRows.Count; i++)
                    {
                        byte[] page = null;
                        {
                            using (PdfReader templateReader = new PdfReader(pdfTemplatePath))
                            {
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    PdfStamper stamper = new PdfStamper(templateReader, ms);
                                    AcroFields form = stamper.AcroFields;
                                    form.SetField("Region", fileItem.Region.RegexReplace(@"\sRegion.*\z"));
                                    form.SetField("RegionLabel", "Region");
                                    form.SetField("OfLabel", "Of");
                                    form.SetField("BallotPaper", "Ballot Paper");
                                    form.SetField("ElectronicVote", "ELECTRONIC VOTE");
                                    form.SetField("VoteType", fileItem.VoteType == "E" ? string.Empty : fileItem.VoteType);
                                    form.SetField("VoteCount", string.Format("{0}/{1}", i + 1, fileItem.VoteDecryptRows.Count));
                                    form.SetField("CastedAt", string.Format("(District of {0})", fileItem.District.RegexReplace(@"\sDistrict.*\z")));

                                    //Add signature...
                                    {
                                        var pos = form.GetFieldPositions("Signature")[0].position;
                                        PdfContentByte content = stamper.GetOverContent(1);
                                        Image image = Image.GetInstance(imagepath);
                                        image.SetAbsolutePosition(pos.Left, pos.Top - 75f);
                                        image.ScaleAbsolute(pos.Width, pos.Height);
                                        content.AddImage(image);
                                        form.RemoveField("Signature");
                                    }

                                    for (int j = 0; j < MaxRegionAtlCandidateLabels; j++)
                                    {
                                        if (j >= fileItem.VoteDecryptRows[i].Votes.Count)
                                            form.RemoveField("Vote" + (j + 1));
                                        else
                                        {
                                            var party = contextParties.Single(c => c.Id == fileItem.VoteDecryptRows[i].Votes[j].CandidateId);
                                            form.SetField("Vote" + (j + 1), fileItem.VoteDecryptRows[i].Votes[j].Vote.HasValue ? fileItem.VoteDecryptRows[i].Votes[j].Vote.Value.ToString() : string.Empty);
                                            form.SetField("Box" + (j + 1), party.BallotBoxLetter);
                                            form.SetField("Party" + (j + 1), party.Name);
                                        }
                                    }

                                    stamper.FormFlattening = true;

                                    stamper.Close();
                                    page = ms.ToArray();
                                }
                            }
                        }

                        PdfReader reader = new PdfReader(page);
                        copy.AddPage(copy.GetImportedPage(reader, 1));
                        reader.Close();

                    }
                }
            }
            finally
            {
                copy.Close();
                document.Close();
            }
        }
    }
}
