﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.vVote.Entities;
using Vec.Apps.vVote.Common;
using System.Text.RegularExpressions;

namespace Vec.Apps.vVote.BL.VoteDecrypter
{
    /// <summary>
    /// Generates a PDF file from the input that has to be processed.
    /// </summary>
    public class BtlVoteDecrypter : VoteDecrypterBase
    {
        public const int MaxA4Length = 66;
        public const int MaxA4PerColumn = 22;
        public const int MaxA3Length = 120;
        public const int MaxA3PerColumn = 20;
        /// <summary>
        /// Generates a PDF file by using its context properties.
        /// </summary>
        public override void GeneratePDF()
        {
            string pdfpath = Path.Combine(ContextPath, string.Format("{0}-BTL.pdf", FileName.GetValidFileName()));
            string imagepath = Path.Combine(ContextPath, "Signature.png");
            var lookupFile = Path.Combine(ContextPath, string.Format("{0}_lookup.json", Context));
            JArray lookupJson = JArray.Parse(File.ReadAllText(lookupFile));
            List<PartyEntity> contextParties = lookupJson.ToObject<List<PartyEntity>>();
            List<CandidateEntity> contextCandidates = (from party in contextParties
                                                       from candidate in party.Candidates
                                                       select candidate).ToList();

            //Pre determine the columns that are going to be used when the PDF is rendered, if the column count is greater than the max columns
            //for an A4 sheet, then switch to A3 PDF.
            var columns = 1;
            for (int i = 0, rows = 0; i < contextParties.Count; i++)
            {
                var groupLen = contextParties[i].Candidates.Count + 1;
                rows += groupLen;

                if (rows > MaxA4PerColumn)
                {
                    i -= 1;
                    rows = 0;
                    columns++;
                }
            }

            int maxFieldLength = MaxA4Length;
            int maxFieldPerColumn = MaxA4PerColumn;
            string pdfTemplatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "BTLVoteTemplate.pdf");
            if (columns > MaxA4Length / MaxA4PerColumn) //switch to A3 happens here.
            {
                pdfTemplatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "BTLVoteTemplateA3.pdf");
                maxFieldLength = MaxA3Length;
                maxFieldPerColumn = MaxA3PerColumn;
            }

            Document document = new Document();
            PdfSmartCopy copy = new PdfSmartCopy(document, new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, pdfpath), FileMode.Create));
            try
            {
                document.Open();

                foreach (var fileItem in Files)
                {
                    for (int i = 0; i < fileItem.VoteDecryptRows.Count; i++)
                    {
                        byte[] page = null;
                        {
                            using (PdfReader templateReader = new PdfReader(pdfTemplatePath))
                            {
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    PdfStamper stamper = new PdfStamper(templateReader, ms);
                                    AcroFields form = stamper.AcroFields;
                                    form.SetField("Region", fileItem.Region.RegexReplace(@"\sRegion.*\z"));
                                    form.SetField("RegionLabel", "Region");
                                    form.SetField("OfLabel", "Of");
                                    form.SetField("BallotPaper", "Ballot Paper");
                                    form.SetField("ElectronicVote", "ELECTRONIC VOTE");
                                    form.SetField("VoteType", fileItem.VoteType == "E" ? string.Empty : fileItem.VoteType);
                                    form.SetField("VoteCount", string.Format("{0}/{1}", i + 1, fileItem.VoteDecryptRows.Count));
                                    form.SetField("CastedAt", string.Format("(District of {0})", fileItem.District.RegexReplace(@"\sDistrict.*\z")));

                                    //Add signature...
                                    {
                                        var pos = form.GetFieldPositions("Signature")[0].position;
                                        PdfContentByte content = stamper.GetOverContent(1);
                                        Image image = Image.GetInstance(imagepath);
                                        image.SetAbsolutePosition(pos.Left, pos.Top - 75f);
                                        image.ScaleAbsolute(pos.Width, pos.Height);
                                        content.AddImage(image);
                                        form.RemoveField("Signature");
                                    }

                                    List<PartyEntity> renderedParties = new List<PartyEntity>();
                                    for (int j = 0, k = 0, l = maxFieldPerColumn; j < maxFieldLength; j++)
                                    {
                                        if (k >= fileItem.VoteDecryptRows[i].Votes.Count)
                                        {
                                            form.RemoveField("Vote" + (j + 1));
                                            form.RemoveField("Candidate" + (j + 1));
                                            form.RemoveField("Party" + (j + 1));
                                            form.RemoveField("Box" + (j + 1));
                                            form.RemoveField("BoxParty" + (j + 1));
                                        }
                                        else
                                        {
                                            var party = (from par in contextParties
                                                         where par.Candidates.SingleOrDefault(c => c.Id == fileItem.VoteDecryptRows[i].Votes[k].CandidateId) != null
                                                         select par).First();

                                            var renderedParty = renderedParties.SingleOrDefault(p => p.Id == party.Id);
                                            if (renderedParty == null)
                                            {
                                                if ((j + party.Candidates.Count + 1) / l == 1)
                                                {
                                                    while (j % l > 0)
                                                    {
                                                        form.RemoveField("Box" + (j + 1));
                                                        form.RemoveField("BoxParty" + (j + 1));
                                                        form.RemoveField("Vote" + (j + 1));
                                                        form.RemoveField("Candidate" + (j + 1));
                                                        form.RemoveField("Party" + (j + 1));
                                                        j++;
                                                    }
                                                    l += maxFieldPerColumn;
                                                }

                                                if (party.HasBallotBox || party.Ungrouped)
                                                {
                                                    form.SetField("Box" + (j + 1), party.BallotBoxLetter);
                                                    form.SetField("BoxParty" + (j + 1), party.Name);
                                                }
                                                else
                                                {
                                                    form.RemoveField("Box" + (j + 1));
                                                    form.RemoveField("BoxParty" + (j + 1));
                                                }

                                                form.RemoveField("Vote" + (j + 1));
                                                form.RemoveField("Candidate" + (j + 1));
                                                form.RemoveField("Party" + (j + 1));

                                                renderedParties.Add(party);
                                            }
                                            else
                                            {
                                                var candidate = contextCandidates.Single(c => c.Id == fileItem.VoteDecryptRows[i].Votes[k].CandidateId);
                                                form.SetField("Vote" + (j + 1), fileItem.VoteDecryptRows[i].Votes[k].Vote.HasValue ? fileItem.VoteDecryptRows[i].Votes[k].Vote.Value.ToString() : string.Empty);
                                                form.SetField("Candidate" + (j + 1), candidate.Name);
                                                form.SetField("Party" + (j + 1), string.IsNullOrEmpty(candidate.PartyName) ? candidate.Region : candidate.PartyName + Chunk.NEWLINE + candidate.Region);
                                                form.RemoveField("Box" + (j + 1));
                                                form.RemoveField("BoxParty" + (j + 1));
                                                k++;
                                            }
                                        }
                                    }

                                    stamper.FormFlattening = true;

                                    stamper.Close();
                                    page = ms.ToArray();
                                }
                            }
                        }

                        PdfReader reader = new PdfReader(page);
                        copy.AddPage(copy.GetImportedPage(reader, 1));
                        reader.Close();

                    }
                }
            }
            finally
            {
                copy.Close();
                document.Close();
            }
        }
    }
}
