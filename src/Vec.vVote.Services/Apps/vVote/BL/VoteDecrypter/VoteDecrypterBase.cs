﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.vVote.Entities;

namespace Vec.Apps.vVote.BL.VoteDecrypter
{
    public interface IVoteDecrypter
    {
        void GeneratePDF();

        string ContextPath { get; set; }

        List<VoteDecryptFile> Files { get; set; }

        string FileName { get; set; }

        Guid Context { get; set; }
    }

    public abstract class VoteDecrypterBase : IVoteDecrypter
    {
        public static T GetVoteDecrypter<T>() where T : IVoteDecrypter, new()
        {
            return new T();
        }

        public abstract void GeneratePDF();

        public Guid Context { get; set; }

        public string ContextPath { get; set; }

        public List<VoteDecryptFile> Files { get; set; }

        public string FileName { get; set; }
    }
}
