﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.vVote.DAL;
using Vec.Apps.vVote.Entities;

namespace Vec.Apps.vVote.BL.VoteDecrypter
{
    public interface ICommitFileProcessor
    {
        Guid Context { get; set; }
        List<string> BulkQuarantineSerials { get; set; }
        List<string> IndividualQuarantineSerials { get; set; }
        List<EventLocationEntity> Locations { get; set; }
        List<string> ProcessLogs { get; set; }

        void ReadCommitFiles(string commitFilePath);

        void UpdatePackageCommitDetails(Dictionary<Guid, string> races);

        List<PackageCommit> SavePackageCommits(Event eve);
    }

    public class CommitFileProcessor : ICommitFileProcessor
    {
        public List<string> BulkQuarantineSerials { get; set; }
        public List<string> IndividualQuarantineSerials { get; set; }
        public List<EventLocationEntity> Locations { get; set; }
        public List<string> ProcessLogs { get; set; }
        public Guid Context { get; set; }

        private Dictionary<string, string> BallotReductions { get; set; }
        private List<PackageCommit> PackageCommitVotes { get; set; }

        public CommitFileProcessor()
        {
            BulkQuarantineSerials = new List<string>();
            IndividualQuarantineSerials = new List<string>();
            BallotReductions = new Dictionary<string, string>();

            PackageCommitVotes = new List<PackageCommit>();
        }

        public void ReadCommitFiles(string commitFilePath)
        {
            foreach (var file in Directory.GetFiles(commitFilePath))
            {
                using (TextReader fs = new StreamReader(file))
                {
                    while (fs.Peek() >= 0)
                    {
                        JObject jo = JObject.Parse(fs.ReadLine());

                        if (jo["serialNo"] != null)
                        {
                            var serialNo = jo["serialNo"].Value<string>();
                            if (BulkQuarantineSerials.Contains(serialNo))
                                continue;

                            var commitType = (CommitType)Enum.Parse(typeof(CommitType), jo["type"].Value<string>(), true);

                            switch (commitType)
                            {
                                case CommitType.BallotGenCommit:
                                    break;
                                case CommitType.BallotAuditCommit:
                                    break;
                                case CommitType.Pod:
                                    if (!BallotReductions.ContainsKey(serialNo))
                                        BallotReductions.Add(serialNo, jo["ballotReductions"].ToString());
                                    break;
                                case CommitType.Vote:
                                    //Read only the necessary fields.
                                    PackageCommitVotes.Add(new PackageCommit()
                                    {
                                        SerialNo = serialNo,
                                        Type = (short)CommitType.Vote,
                                        DeviceId = serialNo.Split(':')[0], //jo["boothID"].Value<string>(),
                                        District = jo["district"].Value<string>(),
                                        Preferences = jo["_vPrefs"].ToString(),
                                        CommitTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(jo["commitTime"].Value<double>())
                                    });
                                    break;
                                case CommitType.Audit:
                                    break;
                                case CommitType.Cancel:
                                    if (!IndividualQuarantineSerials.Contains(serialNo))
                                        IndividualQuarantineSerials.Add(serialNo);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }

            ProcessLogs.Add(string.Format("Found {0} IQ", IndividualQuarantineSerials.Count));
            ProcessLogs.Add(string.Format("Found {0} votes in commit files", PackageCommitVotes.Count));
            //Remove all IQed Votes...
            {
                PackageCommitVotes.RemoveAll(pcv => IndividualQuarantineSerials.Contains(pcv.SerialNo));
            }
            ProcessLogs.Add(string.Format("Total votes after filtering from BQ & IQ {0}", PackageCommitVotes.Count));
        }

        public void UpdatePackageCommitDetails(Dictionary<Guid, string> races)
        {
            var districtGroup = PackageCommitVotes.GroupBy(g => g.District);
            //Update the commits with associated vote raceIds
            foreach (var districtGroupItem in districtGroup)
            {
                var districtName = districtGroupItem.Key;

                var race = races.Single(r => r.Value.StartsWith(districtName, StringComparison.InvariantCultureIgnoreCase));
                var location = Locations.First(l => l.RaceName.StartsWith(districtName, StringComparison.InvariantCultureIgnoreCase));
                var parentRace = races.Single(r => r.Value.StartsWith(location.ParentRaceName, StringComparison.InvariantCultureIgnoreCase));

                foreach (var commit in districtGroupItem)
                {
                    commit.VoteRegionRaceId = parentRace.Key;
                    commit.VoteRegionRaceName = parentRace.Value;
                    commit.VoteDistrictRaceId = race.Key;
                    commit.VoteDistrictRaceName = race.Value;
                }
            }

            var deviceGroup = PackageCommitVotes.GroupBy(g => g.DeviceId.Substring(0, g.DeviceId.Length - 4));
            foreach (var deviceGroupItem in deviceGroup)
            {
                var locationCodeString = deviceGroupItem.Key;
                var locCode = Convert.ToInt32(locationCodeString);
                var location = Locations.First(l => l.LocationCode == locCode);
                var race = races.SingleOrDefault(r => r.Value.StartsWith(location.RaceName, StringComparison.InvariantCultureIgnoreCase));

                foreach (var commit in deviceGroupItem)
                {
                    //associate race Ids
                    commit.DeviceRaceId = race.Key;
                    commit.DeviceRaceName = race.Value;

                    //associate race types
                    int locSubCode = Convert.ToInt32(commit.DeviceId.Substring(commit.DeviceId.Length - 4, 2));
                    var evc = Locations.Single(l => l.LocationCode == locCode && l.LocationSubCode == locSubCode);

                    if (evc.Name.Contains("EVC"))
                        commit.VoteType = (short)VoteType.Early;
                    else if (commit.DeviceRaceId == commit.VoteDistrictRaceId)
                        commit.VoteType = (short)VoteType.Ordinary;
                    else
                        commit.VoteType = (short)VoteType.Absent;
                }
            }
        }

        public List<PackageCommit> SavePackageCommits(Event eve)
        {
            var pkgJoin = from pkg in PackageCommitVotes
                          join balReduction in BallotReductions on pkg.SerialNo equals balReduction.Key
                          select new { PC = pkg, BL = balReduction.Value };

            foreach (var item in pkgJoin)
            {
                item.PC.Context = Context;
                item.PC.BallotReductions = item.BL;

                eve.PackageCommits.Add(item.PC);
            }

            return PackageCommitVotes;
        }
    }
}
