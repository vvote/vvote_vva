﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Vec.Apps.vVote.Entities;
using Vec.Apps.vVote.Common;

namespace Vec.Apps.vVote.BL.VoteDecrypter
{
    /// <summary>
    /// Generates a PDF file from the input that has to be processed.
    /// </summary>
    public class DistrictVoteDecrypter : VoteDecrypterBase
    {
        public const int MaxDistrictCandidateLabels = 28;//Candidate count can never be more than 28, clear the fields if exceeds the max of candidate count.
        /// <summary>
        /// Generates a PDF file by using its context properties.
        /// </summary>
        public override void GeneratePDF()
        {
            string pdfpath = Path.Combine(ContextPath, string.Format("{0}.pdf", FileName.GetValidFileName()));
            string imagepath = Path.Combine(ContextPath, "Signature.png");
            var lookupFile = Path.Combine(ContextPath, string.Format("{0}_lookup.json", Context));
            JArray lookupJson = JArray.Parse(File.ReadAllText(lookupFile));
            List<CandidateEntity> contextCandidates = lookupJson.ToObject<List<CandidateEntity>>();

            string pdfTemplatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "DistrictVoteTemplate.pdf");

            Document document = new Document();
            PdfSmartCopy copy = new PdfSmartCopy(document, new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, pdfpath), FileMode.Create));
            try
            {
                document.Open();

                foreach (var fileItem in Files)
                {
                    for (int i = 0; i < fileItem.VoteDecryptRows.Count; i++)
                    {
                        byte[] page = null;
                        {
                            using (PdfReader templateReader = new PdfReader(pdfTemplatePath))
                            {
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    PdfStamper stamper = new PdfStamper(templateReader, ms);
                                    AcroFields form = stamper.AcroFields;
                                    form.SetField("District", fileItem.District.RegexReplace(@"\sDistrict.*\z"));
                                    form.SetField("DistrictLabel", "District");
                                    form.SetField("OfLabel", "Of");
                                    form.SetField("BallotPaper", "Ballot Paper");
                                    form.SetField("ElectronicVote", "ELECTRONIC VOTE");
                                    form.SetField("VoteType", fileItem.VoteType == "E" ? string.Empty : fileItem.VoteType);
                                    form.SetField("VoteCount", string.Format("{0}/{1}", i + 1, fileItem.VoteDecryptRows.Count));

                                    //Add signature...
                                    {
                                        var pos = form.GetFieldPositions("Signature")[0].position;
                                        PdfContentByte content = stamper.GetOverContent(1);
                                        Image image = Image.GetInstance(imagepath);
                                        image.SetAbsolutePosition(pos.Left, pos.Top - 75f);
                                        image.ScaleAbsolute(pos.Width, pos.Height);
                                        content.AddImage(image);
                                        form.RemoveField("Signature");
                                    }

                                    for (int j = 0; j < MaxDistrictCandidateLabels; j++)
                                    {
                                        if (j >= fileItem.VoteDecryptRows[i].Votes.Count)
                                            form.RemoveField("Vote" + (j + 1));
                                        else
                                        {
                                            var candidate = contextCandidates.Single(c => c.Id == fileItem.VoteDecryptRows[i].Votes[j].CandidateId);
                                            form.SetField("Vote" + (j + 1), fileItem.VoteDecryptRows[i].Votes[j].Vote.HasValue ? fileItem.VoteDecryptRows[i].Votes[j].Vote.Value.ToString() : string.Empty);
                                            form.SetField("Candidate" + (j + 1), candidate.Name);
                                            form.SetField("Party" + (j + 1), candidate.PartyName);
                                        }
                                    }

                                    stamper.FormFlattening = true;

                                    stamper.Close();
                                    page = ms.ToArray();
                                }
                            }
                        }

                        PdfReader reader = new PdfReader(page);
                        copy.AddPage(copy.GetImportedPage(reader, 1));
                        reader.Close();

                    }
                }
            }
            finally
            {
                copy.Close();
                document.Close();
            }
        }

    }
}
