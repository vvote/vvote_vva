﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.vVote.BL.Handler;
using Vec.Apps.vVote.DAL;
using Vec.Apps.vVote.Entities;

namespace Vec.Apps.vVote.BL
{
    public interface ILogBL
    {
        void PostWBBLogData(LogEntity data);

        void PostMBBLogData(LogEntity data);
    }

    public class LogBL : ILogBL
    {
        public IvVoteContext vVoteContext { get; set; }

        public ILogHandlerFactory LogHandlerFactory { get; set; }

        public LogBL(IvVoteContext vVoteContext, ILogHandlerFactory logHandlerFactory)
        {
            this.vVoteContext = vVoteContext;
            this.LogHandlerFactory = logHandlerFactory;
        }

        public void PostWBBLogData(LogEntity data)
        {
            var handler = LogHandlerFactory.GetWBBLogHandlers(data.MessageType);
            foreach (var currentHandler in handler)
                currentHandler.DoHandleLog(data);
        }

        public void PostMBBLogData(LogEntity data)
        {
            var handler = LogHandlerFactory.GetMBBLogHandlers(data.MessageType);
            foreach (var currentHandler in handler)
                currentHandler.DoHandleLog(data);
        }
    }
}
