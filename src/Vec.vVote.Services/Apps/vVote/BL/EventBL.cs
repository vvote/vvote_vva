﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using Vec.Apps.vVote.BL.Common;
using Vec.Apps.vVote.BL.PdfGenerator;
using Vec.Apps.vVote.Common;
using Vec.Apps.vVote.Common.Signature;
using Vec.Apps.vVote.DAL;
using Vec.Apps.vVote.Entities;
using Vec.Apps.vVote.Reader;
using Vec.Apps.vVote.Writer;
using Vec.Properties;
using Vec.Resources;
using Vec.TuringElectionService;
using Vec.TuringNominationService;

namespace Vec.Apps.vVote.BL
{
    public interface IEventBL
    {
        List<EventEntity> GetEvents();

        EventEntity GetEvent(Guid eventId);

        Guid CreateEvent(List<Guid> electionIds, bool force);

        EventEntity UpdateEvent(EventEntity eventEntity);

        List<PartyEntity> GetParties(Guid eventId, DirectoryInfo contextDirectory);

        List<CandidateEntity> GetCandidates(Guid eventId, DirectoryInfo contextDirectory);

        List<LocationEntity> GetLocations(Guid eventId, DirectoryInfo contextDirectory);

        void PrepareExportCandidates(Guid eventId, List<CandidateEntity> candidates, DirectoryInfo contextDirectory);

        void PrepareExportParties(Guid eventId, List<PartyEntity> parties, DirectoryInfo contextDirectory);

        void PrepareExportEvent(Guid eventId, DirectoryInfo contextDirectory);

        void PrepareExportLocations(Guid eventId, List<LocationEntity> locations, DirectoryInfo contextDirectory);

        void PrepareExportStaticContents(Guid eventId, DirectoryInfo contextDirectory);

        void PrepareExportDevices(Guid eventId, List<EventDeviceEntity> eventDevices, DirectoryInfo contextDirectory);

        void UpdateEvent(DirectoryInfo contextDirectory);

        void UpdateParty(Guid eventId, DirectoryInfo contextDirectory);

        void UpdateCandidate(Guid eventId, DirectoryInfo contextDirectory);

        void UpdateLocation(Guid eventId, DirectoryInfo contextDirectory);

        void UpdateStaticContent(Guid eventId, DirectoryInfo contextDirectory);

        List<EventDeviceEntity> GetEventDevices(Guid eventId);

        void UpdateEventSignature(Guid eventId, byte[] fileBytes);

        EventConfig GetEventConfig(Guid eventId, DirectoryInfo contextDirectory);

        StaticContentsEntity GetStaticContents(Guid eventId, DirectoryInfo contextDirectory);

        void UpdateEventDevice(EventDeviceEntity eventDevice);

        string ExportEventDeviceLabels(Guid eventId, DirectoryInfo contextDirectory);

        string UploadQuarantine(Guid eventId, string qFileName, string sFileName);

        List<MBBInternetProtocol> GetMbbSettings(Guid eventId);

        void UpdateMbbSettings(Guid eventId, List<MBBInternetProtocol> mbbInternetProtocols, string mbbPubKeyPath, int mbbTimeout, byte[] jointPublicKeyFile, string wbbPublicKey);

        void GenerateMbbSettingsBundle(Guid eventId, string contextPath);

        Dictionary<string, byte[]> GetMbbSettingFiles(Guid eventId);

        TemplateEntity GetTemplateSettings(Guid eventId);

        TemplateEntity UpdateTemplateSettings(Guid eventId, Dictionary<string, byte[]> files);

        void PrepareExportTemplate(DeviceType deviceType, Guid eventId, DirectoryInfo contextDirectory);

        void GenerateStaticContentBundle(Guid eventId, string rootPath);

        void ProcessPublicSigningKey(Guid eventId, string contextPath);

        List<MonitoredDeviceEntity> GetMonitoredDevices(Guid eventId);
    }

    public class EventBL : BaseBL, IEventBL
    {
        public IvVoteContext vVoteContext { get; set; }
        public IElectionBL ElectionBL { get; set; }
        public IFileServiceHelper FileService { get; set; }
        public IAudioHelper AudioHelper { get; set; }

        public EventBL(IvVoteContext vVoteContext, IElectionBL electionBL, IFileServiceHelper fileService, IAudioHelper audioHelper)
        {
            this.vVoteContext = vVoteContext;
            this.ElectionBL = electionBL;
            this.FileService = fileService;
            this.AudioHelper = audioHelper;
        }

        public List<EventEntity> GetEvents()
        {
            List<EventEntity> result = new List<EventEntity>();

            foreach (var eventItem in vVoteContext.Events)
            {
                result.Add(new EventEntity()
                {
                    Id = eventItem.Id,
                    Name = eventItem.Name
                });
            }

            Logger.Info("GetEvents complete.");
            return result;
        }

        public EventEntity GetEvent(Guid eventId)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            EventEntity result = new EventEntity()
            {
                Id = eve.Id,
                Name = eve.Name,
                WBBServiceHostName = eve.WBBServiceHostName,
                WBBMServiceHostName = eve.WBBMServiceHostName,
                MBBPubKeyPath = eve.MBBPubKeyPath,
                VVPubKeyPath = eve.VVPubKeyPath,
                EBPubKeyPath = eve.EBPubKeyPath,
                PollTime = eve.PollTime,
                ProofTime = eve.ProofTime,
                DraftBundlePath = eve.DraftBundlePath,
                FinalBundlePath = eve.FinalBundlePath,
                SignatureFile = eve.SignatureFile,
                MBBTimeout = eve.MBBTimeout,
                WBBPublicKey = eve.WBBPublicKey,
                ReconcileURL = eve.ReconcileURL
            };

            var associatedElectionIds = from eventItem in eve.EventElections
                                        select eventItem.ElectionId;
            var elections = ElectionBL.GetElections(associatedElectionIds.ToList());

            foreach (var electionItem in elections)
            {
                result.AssociatedElections.Add(new ElectionEntity()
                {
                    Id = electionItem.Id,
                    Name = electionItem.Name,
                    ElectionDate = electionItem.DateRanges.Single(d => d.DateRangeType == ElectionDateRangeType.ElectionDay).StartDateTime.Value.ToLongDateString(),
                    ElectionType = (int)electionItem.ElectionType
                });
            }

            result.MBBInternetProtocols = GetMbbSettings(eventId);

            Logger.Info("GetEvent Complete.");
            return result;
        }

        public List<MBBInternetProtocol> GetMbbSettings(Guid eventId)
        {
            List<MBBInternetProtocol> peers = new List<MBBInternetProtocol>();
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            foreach (var mbbPeer in eve.EventMBBs)
            {
                peers.Add(new MBBInternetProtocol()
                {
                    Id = mbbPeer.Id,
                    IPAddress = mbbPeer.IP,
                    Name = mbbPeer.Name,
                    PublicKeyEntry = mbbPeer.PublicKeyEntry,
                    Port = mbbPeer.Port
                });
            }

            return peers;
        }

        public TemplateEntity GetTemplateSettings(Guid eventId)
        {
            TemplateEntity template = new TemplateEntity();
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            template.EVM = new TemplateItem()
            {
                Name = Constants.EvmTemplateFileName,
                Path = eve.GetWbbFileUrl(Constants.InternalUrl + Constants.EvmTemplateFileName),
                IsUploaded = FileService.Exist(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.EvmTemplateFileName)
            };

            template.VPS = new TemplateItem()
            {
                Name = Constants.VpsTemplateFileName,
                Path = eve.GetWbbFileUrl(Constants.InternalUrl + Constants.VpsTemplateFileName),
                IsUploaded = FileService.Exist(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.VpsTemplateFileName)
            };

            template.TrainingEVM = new TemplateItem()
            {
                Name = Constants.TrainingEvmTemplateFileName,
                Path = eve.GetWbbFileUrl(Constants.InternalUrl + Constants.TrainingEvmTemplateFileName),
                IsUploaded = FileService.Exist(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.TrainingEvmTemplateFileName)
            };

            template.TrainingVPS = new TemplateItem()
            {
                Name = Constants.TrainingVpsTemplateFileName,
                Path = eve.GetWbbFileUrl(Constants.InternalUrl + Constants.TrainingVpsTemplateFileName),
                IsUploaded = FileService.Exist(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.TrainingVpsTemplateFileName)
            };

            template.EvmClientConf = new TemplateItem()
            {
                Name = Constants.EvmClientConfTemplateFileName,
                Path = eve.GetWbbFileUrl(Constants.InternalUrl + Constants.EvmClientConfTemplateFileName),
                IsUploaded = FileService.Exist(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.EvmClientConfTemplateFileName)
            };

            template.VpsClientConf = new TemplateItem()
            {
                Name = Constants.VpsClientConfTemplateFileName,
                Path = eve.GetWbbFileUrl(Constants.InternalUrl + Constants.VpsClientConfTemplateFileName),
                IsUploaded = FileService.Exist(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.VpsClientConfTemplateFileName)
            };

            template.Client = new TemplateItem()
            {
                Name = Constants.ClientTemplateFileName,
                Path = eve.GetWbbFileUrl(Constants.InternalUrl + Constants.ClientTemplateFileName),
                IsUploaded = FileService.Exist(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.ClientTemplateFileName)
            };

            template.EBPublicKey = new TemplateItem()
            {
                Name = Constants.EBPublicKeyFileName,
                Path = eve.GetWbbFileUrl(Constants.InternalUrl + Constants.EBPublicKeyFileName),
                IsUploaded = FileService.Exist(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.EBPublicKeyFileName)
            };

            template.MBBConfigFile = new TemplateItem()
            {
                Name = Constants.MbbConfigFileName,
                Path = eve.GetWbbFileUrl(Constants.InternalUrl + Constants.MbbConfigFileName),
                IsUploaded = FileService.Exist(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.MbbConfigFileName)
            };

            template.CertsBksFile = new TemplateItem()
            {
                Name = Constants.CertsConfigFileName,
                Path = eve.GetWbbFileUrl(Constants.InternalUrl + Constants.CertsConfigFileName),
                IsUploaded = FileService.Exist(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.CertsConfigFileName)
            };

            return template;
        }

        public TemplateEntity UpdateTemplateSettings(Guid eventId, Dictionary<string, byte[]> files)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            foreach (var file in files)
            {
                FileService.Put(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + file.Key, file.Value);
            }

            return GetTemplateSettings(eventId);
        }

        public void UpdateMbbSettings(Guid eventId, List<MBBInternetProtocol> mbbInternetProtocols, string mbbPubKeyPath, int mbbTimeout, byte[] jointPublicKeyFile, string wbbPublicKey)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            eve.MBBTimeout = mbbTimeout;
            eve.MBBPubKeyPath = mbbPubKeyPath;
            eve.WBBPublicKey = wbbPublicKey;
            FileService.Put(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.JointPublicKeyFileName, jointPublicKeyFile);

            while (eve.EventMBBs.Count > 0)
                vVoteContext.EventMBBs.Remove(eve.EventMBBs.First());

            foreach (var mbbItem in mbbInternetProtocols)
            {
                eve.EventMBBs.Add(new EventMBB()
                {
                    Name = mbbItem.Name,
                    IP = mbbItem.IPAddress,
                    Port = mbbItem.Port,
                    PublicKeyEntry = mbbItem.PublicKeyEntry
                });
            }
            vVoteContext.SaveChanges();
        }

        public Dictionary<string, byte[]> GetMbbSettingFiles(Guid eventId)
        {
            var result = new Dictionary<string, byte[]>(2);
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            result.Add(Constants.MbbConfigFileName, FileService.Get(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.MbbConfigFileName));
            result.Add(Constants.CertsConfigFileName, FileService.Get(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.CertsConfigFileName));

            return result;
        }

        public void GenerateMbbSettingsBundle(Guid eventId, string contextPath)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            #region generate MBBConfig.json
            JObject mbbConfig = new JObject();
            JArray mbbArray = new JArray();
            JObject certsConfig = new JObject();
            foreach (var mbbItem in eve.EventMBBs)
            {
                JObject mbbJsonItem = new JObject();
                mbbJsonItem.Add("address", mbbItem.IP);
                mbbJsonItem.Add("port", mbbItem.Port);
                mbbJsonItem.Add("id", mbbItem.Name + "_SSL");

                mbbArray.Add(mbbJsonItem);

                JObject publicKeyEntry = new JObject();
                publicKeyEntry.Add("pubKeyEntry", JObject.Parse(mbbItem.PublicKeyEntry));

                certsConfig.Add(mbbItem.Name + "_SigningSK2", publicKeyEntry);
            }
            mbbConfig.Add("peers", mbbArray);
            mbbConfig.Add("MBBTimeOut", eve.MBBTimeout <= 0 ? Settings.Default.DefaultMBBTimeOut : eve.MBBTimeout);
            mbbConfig.Add("MBBFileUploadTimeOut", Settings.Default.DefaultMBBFileUploadTimeOut);

            JObject wbbPubKeyEntry = new JObject();
            wbbPubKeyEntry.Add("pubKeyEntry", JObject.Parse(eve.WBBPublicKey));
            certsConfig.Add("WBB", wbbPubKeyEntry);

            certsConfig.Add("jksPath", eve.MBBPubKeyPath);

            File.WriteAllText(Path.Combine(contextPath, Constants.MbbConfigFileName), mbbConfig.ToString());
            File.WriteAllText(Path.Combine(contextPath, Constants.CertsConfigFileName), certsConfig.ToString());

            var jointPublicKey = FileService.Get(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.JointPublicKeyFileName);
            File.WriteAllBytes(Path.Combine(contextPath, Constants.JointPublicKeyFileName), jointPublicKey);
            #endregion generate MBBConfig.json
        }

        public EventConfig GetEventConfig(Guid eventId, DirectoryInfo contextDirectory)
        {
            EventConfig result = new EventConfig();
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            var eventAudioContent = eve.EventAudios;
            var eventTextContent = eve.EventTexts;
            if (eventAudioContent.Count == 0 && eventTextContent.Count == 0)
            {
                ID3TagEntity tag = new ID3TagEntity() { Title = eve.Name, Genre = Constants.DefaultGenreElection };
                byte[] content = AudioHelper.GetDefaultMP3File(eve.Name, ref tag);

                foreach (var LanguageItem in vVoteContext.Languages)
                {
                    EventAudio eventAudio = new EventAudio()
                    {
                        EventId = eve.Id,
                        FileName = string.Format(Settings.Default.DefaultAudioFileFormat, eve.Id + "_" + LanguageItem.ShortName),
                        Title = eve.Name,
                        Author = tag.Author,
                        Genre = tag.Genre,
                        Comment = tag.Comment,
                        LanguageId = LanguageItem.Id
                    };

                    FileService.Put(eve.GetWbbFileServiceAddress(), Settings.Default.ElectionAudioPath + eventAudio.FileName, content);
                    eve.EventAudios.Add(eventAudio);

                    string defaultText = string.Empty;
                    if (LanguageItem.Id == 1)
                        defaultText = eve.Name;
                    EventText eventText = new EventText()
                    {
                        EventId = eve.Id,
                        Name = defaultText,
                        LanguageId = LanguageItem.Id,
                    };
                    eve.EventTexts.Add(eventText);
                }
                vVoteContext.SaveChanges();
            }

            foreach (var eventAudioItem in eve.EventAudios.OrderBy(o => o.LanguageId))
            {
                AudioEntity eventAudioEntity = new AudioEntity()
                {
                    Id = eventAudioItem.Id,
                    Name = eventAudioItem.FileName,
                    Title = eventAudioItem.Title,
                    Author = eventAudioItem.Author,
                    Comment = eventAudioItem.Comment,
                    Genre = eventAudioItem.Genre,
                    Error = eventAudioItem.Error,
                    LanguageId = eventAudioItem.LanguageId
                };

                eventAudioEntity.FilePath = Settings.Default.ElectionAudioPath + eventAudioItem.FileName;
                result.Audios.Add(eventAudioEntity);

                if (eventAudioEntity.IsModified = eventAudioItem.DateTimeStamp == null)
                {
                    eventAudioItem.DateTimeStamp = DateTime.UtcNow;
                    eventAudioItem.Error = null;
                }
            }

            foreach (var eventTextItem in eve.EventTexts.OrderBy(o => o.LanguageId))
            {
                TextEntity eventTextEntity = new TextEntity()
                {
                    Id = eventTextItem.Id,
                    Name = eventTextItem.Name,
                    LanguageId = eventTextItem.LanguageId,
                    Error = string.IsNullOrEmpty(eventTextItem.Name) ? "Missing" : null
                };

                result.Texts.Add(eventTextEntity);

                if (eventTextEntity.IsModified = eventTextItem.DateTimeStamp == null)
                {
                    eventTextItem.DateTimeStamp = DateTime.UtcNow;
                    eventTextItem.Error = null;
                }
            }
            vVoteContext.SaveChanges();

            return result;
        }

        public Guid CreateEvent(List<Guid> electionIds, bool force)
        {
            Guid result = Guid.Empty;

            Event eve = null;

            if (!force)
                eve = (from eveEle in vVoteContext.EventElections
                       join electionId in electionIds on eveEle.ElectionId equals electionId
                       select eveEle.Event).FirstOrDefault();

            if (eve == null)
            {
                var electionList = ElectionBL.GetElections(electionIds);

                var dbEve = new Event() { Id = Guid.NewGuid(), Name = electionList.Count() == 1 ? electionList.First().Name : "Multiple By-Election" };
                vVoteContext.Events.Add(dbEve);

                foreach (var electionItem in electionList)
                    dbEve.EventElections.Add(new EventElection() { ElectionId = electionItem.Id });
                vVoteContext.SaveChanges();

                result = dbEve.Id;
            }
            else
                throw new ArgumentException("Event for this election is already created.");

            return result;
        }

        public EventEntity UpdateEvent(EventEntity eventEntity)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == eventEntity.Id);
            eve.Name = eventEntity.Name;
            eve.WBBServiceHostName = eventEntity.WBBServiceHostName;
            eve.WBBMServiceHostName = eventEntity.WBBMServiceHostName;
            eve.MBBPubKeyPath = eventEntity.MBBPubKeyPath;
            eve.VVPubKeyPath = eventEntity.VVPubKeyPath;
            eve.EBPubKeyPath = eventEntity.EBPubKeyPath;
            eve.PollTime = eventEntity.PollTime;
            eve.ProofTime = eventEntity.ProofTime;
            eve.DraftBundlePath = eventEntity.DraftBundlePath;
            eve.FinalBundlePath = eventEntity.FinalBundlePath;
            eve.ReconcileURL = eventEntity.ReconcileURL;

            vVoteContext.SaveChanges();
            Logger.Info("UpdateEvent Complete.");

            return GetEvent(eventEntity.Id);
        }

        public void UpdateEventSignature(Guid eventId, byte[] fileBytes)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            eve.SignatureFile = fileBytes;

            vVoteContext.SaveChanges();
            Logger.Info("UpdateEventSignature Complete.");
        }

        private PartyEntity GetOrCreateParty(Event eve, string partyName)
        {
            Party party = eve.Parties.SingleOrDefault(p => p.Name == partyName);

            if (party == null)
            {
                party = new Party() { Name = partyName };
                eve.Parties.Add(party);
                vVoteContext.SaveChanges();

                ID3TagEntity tag = new ID3TagEntity() { Title = partyName, Genre = Constants.DefaultGenreParty };
                byte[] content = AudioHelper.GetDefaultMP3File(partyName, ref tag);

                foreach (var LanguageItem in vVoteContext.Languages)
                {
                    PartyAudio partyAudio = new PartyAudio()
                    {
                        FileName = string.Format(Settings.Default.DefaultAudioFileFormat, party.Id + "_" + LanguageItem.ShortName),
                        Title = tag.Title,
                        Author = tag.Author,
                        Genre = tag.Genre,
                        Comment = tag.Comment,
                        LanguageId = LanguageItem.Id,
                    };

                    FileService.Put(eve.GetWbbFileServiceAddress(), Settings.Default.PartyAudioPath + partyAudio.FileName, content);
                    party.PartyAudios.Add(partyAudio);

                    string defaultText = string.Empty;
                    if (LanguageItem.Id == 1)
                        defaultText = party.Name;
                    PartyText partyText = new PartyText()
                    {
                        Name = defaultText,
                        LanguageId = LanguageItem.Id,
                    };
                    party.PartyTexts.Add(partyText);
                }
                vVoteContext.SaveChanges();
            }

            PartyEntity partyEntity = new PartyEntity() { Id = party.Id, Name = party.Name };

            if (partyEntity.IsModified = party.DateTimeStamp == null)
            {
                party.DateTimeStamp = DateTime.UtcNow;
            }

            foreach (var partyAudioItem in party.PartyAudios.OrderBy(o => o.LanguageId))
            {
                AudioEntity partyAudioEntity = new AudioEntity()
                {
                    Id = partyAudioItem.Id,
                    Name = partyAudioItem.FileName,
                    Title = partyAudioItem.Title,
                    Author = partyAudioItem.Author,
                    Comment = partyAudioItem.Comment,
                    Genre = partyAudioItem.Genre,
                    Error = partyAudioItem.Error,
                    LanguageId = partyAudioItem.LanguageId
                };

                partyAudioEntity.FilePath = Settings.Default.PartyAudioPath + partyAudioItem.FileName;
                partyEntity.Audios.Add(partyAudioEntity);

                if (partyAudioEntity.IsModified = partyAudioItem.DateTimeStamp == null)
                {
                    partyAudioItem.DateTimeStamp = DateTime.UtcNow;
                    partyAudioItem.Error = null;
                }
            }

            foreach (var partyTextItem in party.PartyTexts.OrderBy(o => o.LanguageId))
            {
                TextEntity partyTextEntity = new TextEntity()
                {
                    Id = partyTextItem.Id,
                    Name = partyTextItem.Name,
                    LanguageId = partyTextItem.LanguageId,
                    Error = string.IsNullOrEmpty(partyTextItem.Name) ? "Missing" : null
                };

                partyEntity.Texts.Add(partyTextEntity);

                if (partyTextEntity.IsModified = partyTextItem.DateTimeStamp == null)
                {
                    partyTextItem.DateTimeStamp = DateTime.UtcNow;
                    partyTextItem.Error = null;
                }
            }

            vVoteContext.SaveChanges();

            return partyEntity;
        }

        public List<PartyEntity> GetParties(Guid eventId, DirectoryInfo contextDirectory)
        {
            List<PartyEntity> result = new List<PartyEntity>();

            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            var eveElection = from election in eve.EventElections
                              select election.ElectionId;
            var eveElectionRaces = from race in ElectionBL.GetRaces(eveElection.ToList(), null)
                                   select race.ElectorateVacancyConfiguration.ElectionElectorateVacancyId;
            var ballotDrawGroups = ElectionBL.BallotDrawGroups(eveElectionRaces.ToList());

            foreach (var ballotDrawItem in ballotDrawGroups)
                foreach (var ballotGroupItem in ballotDrawItem.BallotDrawnGroups)
                {
                    if (!result.Exists(p => p.Name == ballotGroupItem.Name))
                        result.Add(GetOrCreateParty(eve, ballotGroupItem.Name));
                }

            var preferredRPPs = eve.Parties.Where(p => result.SingleOrDefault(rp => rp.Id == p.Id) == null);
            foreach (var rppItem in preferredRPPs)
            {
                result.Add(GetOrCreateParty(eve, rppItem.Name));
            }

            return result;
        }

        public StaticContentsEntity GetStaticContents(Guid eventId, DirectoryInfo contextDirectory)
        {
            StaticContentsEntity result = new StaticContentsEntity();

            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            foreach (var eveStaTxtItem in eve.EventStaticTexts)
            {
                StaticTextContent staTxtCon = new StaticTextContent()
                {
                    Id = eveStaTxtItem.Id,
                    Name = eveStaTxtItem.Name,
                    Description = eveStaTxtItem.Description,
                    ConversionLabel = eveStaTxtItem.ConversionLabel,
                    SizeLimit = eveStaTxtItem.SizeLimit
                };

                foreach (var staTxtConItem in eveStaTxtItem.StaticTexts.OrderBy(o => o.LanguageId))
                {
                    staTxtCon.Texts.Add(new TextEntity()
                    {
                        Id = staTxtConItem.Id,
                        Name = staTxtConItem.Name,
                        LanguageId = staTxtConItem.LanguageId,
                        IsModified = staTxtConItem.DateTimeStamp == null,
                        Error = string.IsNullOrEmpty(staTxtConItem.Name) ? "Missing" : null
                    });

                    if (staTxtConItem.DateTimeStamp == null)
                    {
                        staTxtConItem.DateTimeStamp = DateTime.UtcNow;
                        //staTxtConItem.Error = null;
                    }
                }
                result.StaticTextContents.Add(staTxtCon);
            }

            foreach (var eveStaAudItem in eve.EventStaticAudios)
            {
                StaticAudioContent staAudCon = new StaticAudioContent()
                {
                    Id = eveStaAudItem.Id,
                    Name = eveStaAudItem.Name,
                    Description = eveStaAudItem.Description,
                    IsLote = eveStaAudItem.IsLOTE
                };

                foreach (var staAudConItem in eveStaAudItem.StaticAudios.OrderBy(o => o.LanguageId))
                {
                    staAudCon.Audios.Add(new AudioEntity()
                    {
                        Id = staAudConItem.Id,
                        Name = staAudConItem.FileName,
                        Title = staAudConItem.Title,
                        Author = staAudConItem.Author,
                        Comment = staAudConItem.Comment,
                        Genre = staAudConItem.Genre,
                        LanguageId = staAudConItem.LanguageId,
                        IsModified = staAudConItem.DateTimeStamp == null,
                        Error = staAudConItem.Error
                    });

                    if (staAudConItem.DateTimeStamp == null)
                    {
                        staAudConItem.DateTimeStamp = DateTime.UtcNow;
                        //staAudConItem.Error = null;
                    }
                }

                result.StaticAudioContents.Add(staAudCon);
            }

            vVoteContext.SaveChanges();
            return result;
        }

        public List<LocationEntity> GetLocations(Guid eventId, DirectoryInfo contextDirectory)
        {
            List<LocationEntity> result = new List<LocationEntity>();

            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            var eveElection = from election in eve.EventElections
                              select election.ElectionId;
            var electionList = ElectionBL.GetElections(eveElection.ToList());

            foreach (var electionItem in electionList)
            {
                foreach (var electorateItem in electionItem.ParentElectorates)
                {
                    foreach (var electionElectorate in electorateItem.Electorates.Where(e => e.ElectorateVacancyConfiguration != null))
                    {
                        switch (electionElectorate.Type)
                        {
                            case ElectorateType.Unknown:
                                break;
                            case ElectorateType.District:
                                LocationAudio districtAudio = eve.LocationAudios.SingleOrDefault(da => da.LocationId == electionElectorate.ElectorateVacancyConfiguration.ElectionElectorateVacancyId);
                                if (districtAudio == null)
                                {
                                    ID3TagEntity tag = new ID3TagEntity() { Title = electionElectorate.Name, Genre = Constants.DefaultGenreDistrict };
                                    byte[] content = AudioHelper.GetDefaultMP3File(electionElectorate.Name, ref tag);

                                    districtAudio = new LocationAudio()
                                        {
                                            LocationId = electionElectorate.ElectorateVacancyConfiguration.ElectionElectorateVacancyId,
                                            FileName = string.Format(Settings.Default.DefaultAudioFileFormat, electionElectorate.ElectorateVacancyConfiguration.ElectionElectorateVacancyId),
                                            Title = tag.Title,
                                            Author = tag.Author,
                                            Genre = tag.Genre,
                                            Comment = tag.Comment
                                        };

                                    FileService.Put(eve.GetWbbFileServiceAddress(), Settings.Default.DistrictAudioPath + string.Format(Settings.Default.DefaultAudioFileFormat, electionElectorate.ElectorateVacancyConfiguration.ElectionElectorateVacancyId), content);
                                    eve.LocationAudios.Add(districtAudio);
                                    vVoteContext.SaveChanges();
                                }

                                LocationEntity districtLocationEntity = new LocationEntity()
                                {
                                    LocationId = electionElectorate.ElectorateVacancyConfiguration.ElectionElectorateVacancyId,
                                    Name = electionElectorate.Name,
                                    LocationType = (int)Vec.Apps.vVote.Entities.LocationType.District,
                                    Error = districtAudio.Error,
                                    Audio = new AudioEntity()
                                    {
                                        Id = districtAudio.Id,
                                        Name = districtAudio.FileName,
                                        Title = districtAudio.Title,
                                        Author = districtAudio.Author,
                                        Comment = districtAudio.Comment,
                                        Genre = districtAudio.Genre,
                                        Error = districtAudio.Error,
                                        FilePath = Settings.Default.DistrictAudioPath + districtAudio.FileName
                                    }
                                };

                                if (districtLocationEntity.IsModified = districtLocationEntity.Audio.IsModified = districtAudio.DateTimeStamp == null)
                                {
                                    districtAudio.DateTimeStamp = DateTime.UtcNow;
                                    districtAudio.Error = null;
                                }
                                vVoteContext.SaveChanges();
                                result.Add(districtLocationEntity);

                                break;
                            case ElectorateType.Region:
                                LocationAudio regionAudio = eve.LocationAudios.SingleOrDefault(ra => ra.LocationId == electionElectorate.ElectorateVacancyConfiguration.ElectionElectorateVacancyId);
                                if (regionAudio == null)
                                {
                                    ID3TagEntity tag = new ID3TagEntity() { Title = electionElectorate.Name, Genre = Constants.DefaultGenreRegion };
                                    byte[] content = AudioHelper.GetDefaultMP3File(electionElectorate.Name, ref tag);

                                    regionAudio = new LocationAudio()
                                        {
                                            LocationId = electionElectorate.ElectorateVacancyConfiguration.ElectionElectorateVacancyId,
                                            FileName = string.Format(Settings.Default.DefaultAudioFileFormat, electionElectorate.ElectorateVacancyConfiguration.ElectionElectorateVacancyId),
                                            Title = tag.Title,
                                            Author = tag.Author,
                                            Genre = tag.Genre,
                                            Comment = tag.Comment
                                        };

                                    FileService.Put(eve.GetWbbFileServiceAddress(), Settings.Default.RegionAudioPath + string.Format(Settings.Default.DefaultAudioFileFormat, electionElectorate.ElectorateVacancyConfiguration.ElectionElectorateVacancyId), content);
                                    eve.LocationAudios.Add(regionAudio);
                                    vVoteContext.SaveChanges();
                                }

                                LocationEntity regionLocationEntity = new LocationEntity()
                                {
                                    LocationId = electionElectorate.ElectorateVacancyConfiguration.ElectionElectorateVacancyId,
                                    Name = electionElectorate.Name,
                                    LocationType = (int)Vec.Apps.vVote.Entities.LocationType.Region,
                                    Error = regionAudio.Error,
                                    Audio = new AudioEntity()
                                    {
                                        Id = regionAudio.Id,
                                        Name = regionAudio.FileName,
                                        Title = regionAudio.Title,
                                        Author = regionAudio.Author,
                                        Comment = regionAudio.Comment,
                                        Genre = regionAudio.Genre,
                                        Error = regionAudio.Error,
                                        FilePath = Settings.Default.RegionAudioPath + regionAudio.FileName
                                    }
                                };

                                if (regionLocationEntity.IsModified = regionLocationEntity.Audio.IsModified = regionAudio.DateTimeStamp == null)
                                {
                                    regionAudio.DateTimeStamp = DateTime.UtcNow;
                                    regionAudio.Error = null;
                                }
                                vVoteContext.SaveChanges();
                                result.Add(regionLocationEntity);

                                break;
                            case ElectorateType.Municipality:
                            case ElectorateType.Ward:
                            default:
                                break;
                        }
                    }
                }
            }

            return result;
        }

        public List<CandidateEntity> GetCandidates(Guid eventId, DirectoryInfo contextDirectory)
        {
            List<CandidateEntity> result = new List<CandidateEntity>();

            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            var eveElection = from election in eve.EventElections
                              select election.ElectionId;
            var eveElectionRaces = from race in ElectionBL.GetRaces(eveElection.ToList(), null)
                                   select new KeyValuePair<Guid, string>(race.ElectorateVacancyConfiguration.ElectionElectorateVacancyId, race.Name);

            var races = ElectionBL.GetNominations(eveElectionRaces.Select(t => t.Key).ToList());
            foreach (var ballotDrawCandidates in races)
                foreach (var nomination in ballotDrawCandidates.CandidatesInNominationOrder.Where(n => n.ProcessingStatus == PS.QualityAssured || n.ProcessingStatus == PS.Finalised))
                {
                    CandidateAudio candidateAudio = eve.CandidateAudios.SingleOrDefault(c => c.CandidateId == nomination.Id);

                    if (candidateAudio == null)
                    {
                        var race =  eveElectionRaces.Single(eer => eer.Key == ballotDrawCandidates.ElectionElectorateVacancyId).Value;

                        JObject userDefined = new JObject();
                        userDefined.Add("race", race);
                        userDefined.Add("local", nomination.BallotPaperLocality ?? string.Empty);
                        userDefined.Add("party", nomination.PreferredRPPName ?? string.Empty);
                        ID3TagEntity tag = new ID3TagEntity()
                        {
                            Title = nomination.BallotPaperName,
                            Genre = race,
                            UserDefined = userDefined.ToString(Newtonsoft.Json.Formatting.None)
                        };

                        byte[] content = AudioHelper.GetDefaultMP3File(nomination.BallotPaperName, ref tag);

                        candidateAudio = new CandidateAudio()
                        {
                            CandidateId = nomination.Id,
                            FileName = string.Format(Settings.Default.DefaultAudioFileFormat, nomination.Id),
                            Title = tag.Title,
                            Author = tag.Author,
                            Genre = tag.Genre,
                            Comment = tag.Comment
                        };

                        FileService.Put(eve.GetWbbFileServiceAddress(), Settings.Default.CandidateAudioPath + candidateAudio.FileName, content);
                        eve.CandidateAudios.Add(candidateAudio);
                        vVoteContext.SaveChanges();

                        if (!string.IsNullOrEmpty(nomination.PreferredRPPName))
                            GetOrCreateParty(eve, nomination.PreferredRPPName);
                    }

                    CandidateEntity candidateEntity = new CandidateEntity()
                    {
                        Id = nomination.Id,
                        Name = nomination.BallotPaperName,
                        BallotPosition = nomination.BallotPaperPosition,
                        BallotSequence = nomination.OrderWithinGroupOrTeam,
                        PartyName = nomination.PreferredRPPName ?? string.Empty,
                        Region = nomination.BallotPaperLocality ?? string.Empty,
                        NominationDateTime = nomination.NominationDateTime.Value.ToString(),
                        NominationStatusText = Enum.GetName(typeof(NS), nomination.NominationStatus),
                        ProcessStatusText = Enum.GetName(typeof(PS), nomination.ProcessingStatus),
                        Error = candidateAudio.Error,
                        Audio = new AudioEntity()
                        {
                            Id = candidateAudio.Id,
                            Name = candidateAudio.FileName,
                            Title = nomination.BallotPaperName,
                            Author = candidateAudio.Author,
                            Comment = candidateAudio.Comment,
                            Genre = candidateAudio.Genre,
                            Error = candidateAudio.Error,
                            FilePath = Settings.Default.CandidateAudioPath + candidateAudio.FileName
                        }
                    };

                    if (candidateEntity.IsModified = candidateEntity.Audio.IsModified = candidateAudio.DateTimeStamp == null)
                    {
                        candidateAudio.DateTimeStamp = DateTime.UtcNow;
                        candidateAudio.Error = null;
                    }
                    vVoteContext.SaveChanges();
                    result.Add(candidateEntity);
                }

            return result;
        }

        public void PrepareExportParties(Guid eventId, List<PartyEntity> parties, DirectoryInfo contextDirectory)
        {
            string fileName = Constants.PartyFileName;
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            #region create schema...
            DataSet sheetData = new DataSet();
            DataTable audioTable = new DataTable("Audio");
            audioTable.Columns.Add("Id", typeof(int));
            audioTable.Columns.Add("Name", typeof(string));
            foreach (var languageItem in vVoteContext.Languages.OrderBy(o => o.Id))
            {
                audioTable.Columns.Add(languageItem.Name, typeof(string));
            }
            DataTable textTable = audioTable.Clone();
            textTable.TableName = "Text";
            #endregion

            foreach (var partyItem in parties)
            {
                DataRow aRow = audioTable.NewRow();
                aRow["Id"] = partyItem.Id;
                aRow["Name"] = partyItem.Name;

                foreach (var partyAudioItem in vVoteContext.PartyAudios.Where(pa => pa.PartyId == partyItem.Id).OrderBy(o => o.LanguageId))
                {
                    byte[] fileContent = FileService.Get(eve.GetWbbFileServiceAddress(), Settings.Default.PartyAudioPath + partyAudioItem.FileName);
                    File.WriteAllBytes(Path.Combine(contextDirectory.FullName, partyAudioItem.FileName), fileContent);

                    aRow[partyAudioItem.Language.Name] = partyAudioItem.FileName;
                }
                audioTable.Rows.Add(aRow);

                DataRow tRow = textTable.NewRow();
                tRow["Id"] = partyItem.Id;
                tRow["Name"] = partyItem.Name;

                foreach (var partyTextItem in vVoteContext.PartyTexts.Where(pt => pt.PartyId == partyItem.Id).OrderBy(o => o.LanguageId))
                {
                    tRow[partyTextItem.Language.Name] = partyTextItem.Name;
                }
                textTable.Rows.Add(tRow);
            }

            sheetData.Tables.Add(audioTable);
            sheetData.Tables.Add(textTable);


            using (SpreadsheetWriter writer = new SpreadsheetWriter(Path.Combine(contextDirectory.FullName, fileName)))
            {
                writer.SetDataSet(sheetData);
            }

            Zipper.Archive(contextDirectory.FullName, "");
        }

        public void PrepareExportEvent(Guid eventId, DirectoryInfo contextDirectory)
        {
            string fileName = Constants.EventFileName;
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            #region create schema...
            DataSet sheetData = new DataSet();
            DataTable audioTable = new DataTable("Audio");
            audioTable.Columns.Add("Id", typeof(Guid));
            audioTable.Columns.Add("Name", typeof(string));
            foreach (var languageItem in vVoteContext.Languages.OrderBy(o => o.Id))
            {
                audioTable.Columns.Add(languageItem.Name, typeof(string));
            }
            DataTable textTable = audioTable.Clone();
            textTable.TableName = "Text";
            #endregion

            DataRow aRow = audioTable.NewRow();
            aRow["Id"] = eve.Id;
            aRow["Name"] = eve.Name;

            foreach (var eventAudioItem in eve.EventAudios.OrderBy(o => o.LanguageId))
            {
                byte[] fileContent = FileService.Get(eve.GetWbbFileServiceAddress(), Settings.Default.ElectionAudioPath + eventAudioItem.FileName);
                File.WriteAllBytes(Path.Combine(contextDirectory.FullName, eventAudioItem.FileName), fileContent);

                aRow[eventAudioItem.Language.Name] = eventAudioItem.FileName;
            }
            audioTable.Rows.Add(aRow);

            DataRow tRow = textTable.NewRow();
            tRow["Id"] = eve.Id;
            tRow["Name"] = eve.Name;

            foreach (var eventTextItem in eve.EventTexts.OrderBy(o => o.LanguageId))
            {
                tRow[eventTextItem.Language.Name] = eventTextItem.Name;
            }
            textTable.Rows.Add(tRow);

            sheetData.Tables.Add(audioTable);
            sheetData.Tables.Add(textTable);


            using (SpreadsheetWriter writer = new SpreadsheetWriter(Path.Combine(contextDirectory.FullName, fileName)))
            {
                writer.SetDataSet(sheetData);
            }

            Zipper.Archive(contextDirectory.FullName, "");
        }

        public void PrepareExportCandidates(Guid eventId, List<CandidateEntity> candidates, DirectoryInfo contextDirectory)
        {
            DataSet sheetData = new DataSet();
            string fileName = Constants.CandidateFileName;
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            #region create schema...
            DataTable audioTable = new DataTable("Audio");
            audioTable.Columns.Add("Id", typeof(int));
            audioTable.Columns.Add("Name", typeof(string));
            audioTable.Columns.Add("Locality", typeof(string));
            audioTable.Columns.Add("Party", typeof(string));
            audioTable.Columns.Add("AudioFileName", typeof(string));
            #endregion

            foreach (var candidateItem in candidates)
            {
                DataRow aRow = audioTable.NewRow();
                aRow["Id"] = candidateItem.Id;
                aRow["Name"] = candidateItem.Name;
                aRow["Locality"] = candidateItem.Region;
                aRow["Party"] = candidateItem.PartyName;
                string candidateAudioFileName = eve.CandidateAudios.Single(c => c.CandidateId == candidateItem.Id).FileName;

                byte[] fileContent = FileService.Get(eve.GetWbbFileServiceAddress(), Settings.Default.CandidateAudioPath + candidateAudioFileName);
                File.WriteAllBytes(Path.Combine(contextDirectory.FullName, candidateAudioFileName), fileContent);

                aRow["AudioFileName"] = candidateAudioFileName;

                audioTable.Rows.Add(aRow);
            }

            sheetData.Tables.Add(audioTable);

            using (SpreadsheetWriter writer = new SpreadsheetWriter(Path.Combine(contextDirectory.FullName, fileName)))
            {
                writer.SetDataSet(sheetData);
            }

            Zipper.Archive(contextDirectory.FullName, "");
        }

        public void PrepareExportLocations(Guid eventId, List<LocationEntity> locations, DirectoryInfo contextDirectory)
        {
            DataSet sheetData = new DataSet();
            string fileName = Constants.LocationFileName;
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            #region create schema...
            DataTable audioTable = new DataTable("Audio");
            audioTable.Columns.Add("Id", typeof(Guid));
            audioTable.Columns.Add("Type", typeof(int));
            audioTable.Columns.Add("Name", typeof(string));
            audioTable.Columns.Add("AudioFileName", typeof(string));
            #endregion

            foreach (var locationItem in locations)
            {
                DataRow aRow = audioTable.NewRow();
                aRow["Id"] = locationItem.LocationId;
                aRow["Type"] = locationItem.LocationType;
                aRow["Name"] = locationItem.Name;

                string audioFileName = eve.LocationAudios.Single(c => c.LocationId == locationItem.LocationId).FileName;
                byte[] fileContent;

                switch ((Vec.Apps.vVote.Entities.LocationType)locationItem.LocationType)
                {
                    case Entities.LocationType.District:
                        fileContent = FileService.Get(eve.GetWbbFileServiceAddress(), Settings.Default.DistrictAudioPath + audioFileName);
                        break;
                    case Entities.LocationType.Region:
                        fileContent = FileService.Get(eve.GetWbbFileServiceAddress(), Settings.Default.RegionAudioPath + audioFileName);
                        break;
                    default:
                        fileContent = null;
                        break;
                }
                File.WriteAllBytes(Path.Combine(contextDirectory.FullName, audioFileName), fileContent);

                aRow["AudioFileName"] = audioFileName;

                audioTable.Rows.Add(aRow);
            }

            sheetData.Tables.Add(audioTable);


            using (SpreadsheetWriter writer = new SpreadsheetWriter(Path.Combine(contextDirectory.FullName, fileName)))
            {
                writer.SetDataSet(sheetData);
            }

            Zipper.Archive(contextDirectory.FullName, "");
        }

        public void PrepareExportStaticContents(Guid eventId, DirectoryInfo contextDirectory)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            foreach (var staticAudioContentItem in eve.EventStaticAudios)
            {
                foreach (var sacItem in staticAudioContentItem.StaticAudios.Where(sa => sa.EventStaticAudioId == staticAudioContentItem.Id).OrderBy(o => o.LanguageId))
                {
                    if (sacItem.FileContent != null)
                    {
                        FileInfo fi = new FileInfo(Path.Combine(contextDirectory.FullName, sacItem.FileName));

                        if (!fi.Directory.Exists)
                            fi.Directory.Create();

                        File.WriteAllBytes(Path.Combine(contextDirectory.FullName, sacItem.FileName), sacItem.FileContent);
                    }
                }
            }

            byte[] fileContent = FileService.Get(eve.GetWbbFileServiceAddress(), Constants.DataUrl + Constants.StaticContentFileName);
            File.WriteAllBytes(Path.Combine(contextDirectory.FullName, Constants.StaticContentFileName), fileContent);

            Zipper.Archive(contextDirectory.FullName, "");
        }

        public void PrepareExportDevices(Guid eventId, List<EventDeviceEntity> eventDevices, DirectoryInfo contextDirectory)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            EventEntity eventEntity = GetEvent(eventId);

            foreach (var eventDeviceItem in eventDevices)
            {
                EventDeviceEntity result = new EventDeviceEntity();
                EventDevice eveDevice = vVoteContext.EventDevices.Single(e => e.Id == eventDeviceItem.Id);

                result.Id = eveDevice.Id;
                result.DeviceId = eveDevice.DeviceId;
                result.TypeId = eveDevice.Type;
                result.TypeName = Enum.GetName(typeof(DeviceType), eveDevice.Type);
                result.StageId = eveDevice.Stage;
                result.StageName = Enum.GetName(typeof(DeviceStageType), eveDevice.Stage);
                result.QuarantineQuota = eveDevice.QuarantineQuota;
                result.DeployedLocationId = eveDevice.DeployedLocationId;
                result.IsAuditEnabled = eveDevice.IsAuditEnabled;
                result.MaxBundleSize = Settings.Default.MaxBundleSize;
                result.LaunchUrl = eveDevice.Type == (byte)DeviceType.VPS ? Settings.Default.VpsLaunchUrl : Settings.Default.EvmLaunchUrl;
                result.HeartbeatURL = eveDevice.Type == (byte)DeviceType.VPS ? Settings.Default.VpsHeartbeatUrl : Settings.Default.EvmHeartbeatUrl;
                result.ConnectionCheckURL = Settings.Default.ConnectionCheckUrl;
                result.F5LaunchURL = Settings.Default.F5LaunchUrl;
                result.EventEntity = eventEntity;
                result.EventLocationEntity = new EventLocationEntity()
                {
                    Id = eveDevice.EventLocationId,
                    Name = eveDevice.EventLocation.LocationName,
                    LocationCode = eveDevice.EventLocation.LocationCode,
                    LocationSubCode = eveDevice.EventLocation.LocationSubCode,
                    RaceName = eveDevice.EventLocation.RaceName,
                    ParentRaceName = eveDevice.EventLocation.ParentRaceName,
                    Type = (Entities.LocationType)eveDevice.EventLocation.Type.Value,
                };


                var clientConf = FileService.Get(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + (eveDevice.Type == (byte)DeviceType.VPS ? Constants.VpsClientConfTemplateFileName : Constants.EvmClientConfTemplateFileName));
                var clientConfStr = clientConf.GetString();
                JObject clientConfObj = JObject.Parse(clientConfStr);
                clientConfObj["id"] = Convert.ToString(eveDevice.DeviceId);

                File.WriteAllText(Path.Combine(contextDirectory.FullName, string.Format("{0}.json", result.DeviceId)), JObject.FromObject(result).ToString());
                File.WriteAllText(Path.Combine(contextDirectory.FullName, string.Format("{0}client.conf", result.DeviceId)), clientConfObj.ToString());
            }

            Zipper.Archive(contextDirectory.FullName, "");
        }

        public void UpdateEvent(DirectoryInfo contextDirectory)
        {
            DataSet sheetData;
            using (SpreadsheetReader ssr = new SpreadsheetReader(Path.Combine(contextDirectory.FullName, Constants.EventFileName)))
            {
                sheetData = ssr.GetDataSet("Audio", "Text");
            }

            foreach (DataRow rowItem in sheetData.Tables["Audio"].Rows)
            {
                Guid eventId = Guid.Parse(rowItem["Id"].ToString());
                var eve = vVoteContext.Events.Single(e => e.Id == eventId);
                string eventName = rowItem["Name"].ToString();

                foreach (var languageItem in vVoteContext.Languages)
                {
                    string fileName = rowItem[languageItem.Name].ToString();

                    EventAudio eventAudio = vVoteContext.EventAudios.Single(ea => ea.EventId == eventId && ea.LanguageId == languageItem.Id);
                    eventAudio.DateTimeStamp = null;
                    try
                    {
                        string audioFilePath = Path.Combine(contextDirectory.FullName, fileName);
                        var tag = ID3Tag.GetMP3Tags(audioFilePath);

                        FileService.Put(eve.GetWbbFileServiceAddress(), Settings.Default.ElectionAudioPath + fileName, File.ReadAllBytes(audioFilePath));

                        eventAudio.FileName = fileName;
                        eventAudio.Author = tag.Author;
                        eventAudio.Title = tag.Title;
                        eventAudio.Comment = tag.Comment;
                        eventAudio.Genre = tag.Genre;
                    }
                    catch (Exception ex)
                    {
                        eventAudio.Error = ex.Message;
                    }
                }
            }

            foreach (DataRow rowItem in sheetData.Tables["Text"].Rows)
            {
                Guid eventId = Guid.Parse(rowItem["Id"].ToString());
                string eventName = rowItem["Name"].ToString();

                foreach (var languageItem in vVoteContext.Languages)
                {
                    string languageText = rowItem[languageItem.Name].ToString();

                    EventText eventText = vVoteContext.EventTexts.Single(et => et.EventId == eventId && et.LanguageId == languageItem.Id);
                    try
                    {
                        if (eventText == null)
                        {
                            eventText = new EventText()
                            {
                                Name = languageText,
                                EventId = eventId,
                                LanguageId = languageItem.Id
                            };
                            vVoteContext.EventTexts.Add(eventText);
                        }
                        else
                        {
                            eventText.DateTimeStamp = null;
                            eventText.Name = languageText;
                        }
                    }
                    catch (Exception ex)
                    {
                        eventText.Error = ex.Message;
                    }
                }
            }

            vVoteContext.SaveChanges();
        }

        public void UpdateParty(Guid eventId, DirectoryInfo contextDirectory)
        {
            DataSet sheetData;
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            using (SpreadsheetReader ssr = new SpreadsheetReader(Path.Combine(contextDirectory.FullName, Constants.PartyFileName)))
            {
                sheetData = ssr.GetDataSet("Audio", "Text");
            }

            foreach (DataRow rowItem in sheetData.Tables["Audio"].Rows)
            {
                int partyId = int.Parse(rowItem["Id"].ToString());
                string partyName = rowItem["Name"].ToString();

                foreach (var languageItem in vVoteContext.Languages)
                {
                    string fileName = rowItem[languageItem.Name].ToString();

                    bool isDuplicate = vVoteContext.PartyAudios.Any(pa => pa.Party.EventId == eventId && pa.PartyId != partyId && string.Compare(pa.FileName, fileName, true) == 0);

                    PartyAudio partyAudio = vVoteContext.PartyAudios.Single(pa => pa.PartyId == partyId && pa.LanguageId == languageItem.Id);
                    partyAudio.DateTimeStamp = null;
                    try
                    {
                        if (isDuplicate)
                            throw new ArgumentException(Constants.ERROR_DUPLICATE_PARTY_FILE);

                        string audioFilePath = Path.Combine(contextDirectory.FullName, fileName);
                        var tag = ID3Tag.GetMP3Tags(audioFilePath);

                        FileService.Put(eve.GetWbbFileServiceAddress(), Settings.Default.PartyAudioPath + fileName, File.ReadAllBytes(audioFilePath));

                        partyAudio.FileName = fileName;
                        partyAudio.Author = tag.Author;
                        partyAudio.Title = tag.Title;
                        partyAudio.Comment = tag.Comment;
                        partyAudio.Genre = tag.Genre;
                    }
                    catch (Exception ex)
                    {
                        partyAudio.Error = ex.Message;
                    }
                }
            }

            foreach (DataRow rowItem in sheetData.Tables["Text"].Rows)
            {
                int partyId = int.Parse(rowItem["Id"].ToString());
                string partyName = rowItem["Name"].ToString();

                foreach (var languageItem in vVoteContext.Languages)
                {
                    string languageText = rowItem[languageItem.Name].ToString();

                    PartyText partyText = vVoteContext.PartyTexts.Single(pt => pt.PartyId == partyId && pt.LanguageId == languageItem.Id);
                    try
                    {
                        if (partyText == null)
                        {
                            partyText = new PartyText()
                            {
                                Name = languageText,
                                PartyId = partyId,
                                LanguageId = languageItem.Id
                            };
                            vVoteContext.PartyTexts.Add(partyText);
                        }
                        else
                        {
                            partyText.DateTimeStamp = null;
                            partyText.Name = languageText;
                        }
                    }
                    catch (Exception ex)
                    {
                        partyText.Error = ex.Message;
                    }
                }
            }

            vVoteContext.SaveChanges();
        }

        public void UpdateCandidate(Guid eventId, DirectoryInfo contextDirectory)
        {
            DataSet sheetData;
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            using (SpreadsheetReader ssr = new SpreadsheetReader(Path.Combine(contextDirectory.FullName, Constants.CandidateFileName)))
            {
                sheetData = ssr.GetDataSet("Audio");
            }

            foreach (DataRow rowItem in sheetData.Tables["Audio"].Rows)
            {
                int id = int.Parse(rowItem["Id"].ToString());
                string audioFileName = rowItem["AudioFileName"].ToString();
                bool isDuplicate = eve.CandidateAudios.Any(c => c.CandidateId != id && string.Compare(c.FileName, audioFileName, true) == 0);
                CandidateAudio candidateAudio = eve.CandidateAudios.Single(c => c.CandidateId == id);
                candidateAudio.DateTimeStamp = null;
                try
                {
                    if (isDuplicate)
                        throw new ArgumentException(Constants.ERROR_DUPLICATE_CANDIDATE_FILE);

                    string audioFilePath = Path.Combine(contextDirectory.FullName, rowItem["AudioFileName"].ToString());
                    var tag = ID3Tag.GetMP3Tags(audioFilePath);

                    if (string.IsNullOrWhiteSpace(tag.UserDefined))
                        throw new ArgumentException(Constants.ERROR_MISSING_CANDIDATE_DATA);

                    FileService.Put(eve.GetWbbFileServiceAddress(), Settings.Default.CandidateAudioPath + rowItem["AudioFileName"].ToString(), File.ReadAllBytes(audioFilePath));

                    candidateAudio.FileName = rowItem["AudioFileName"].ToString();
                    candidateAudio.Author = tag.Author;
                    candidateAudio.Title = tag.Title;
                    candidateAudio.Comment = tag.Comment;
                    candidateAudio.Genre = tag.Genre;
                }
                catch (Exception ex)
                {
                    candidateAudio.Error = ex.Message;
                }
            }
            vVoteContext.SaveChanges();
        }

        public void UpdateLocation(Guid eventId, DirectoryInfo contextDirectory)
        {
            DataSet sheetData;
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            using (SpreadsheetReader ssr = new SpreadsheetReader(Path.Combine(contextDirectory.FullName, Constants.LocationFileName)))
            {
                sheetData = ssr.GetDataSet("Audio");
            }


            foreach (DataRow rowItem in sheetData.Tables["Audio"].Rows)
            {
                Guid id = Guid.Parse(rowItem["Id"].ToString());
                Vec.Apps.vVote.Entities.LocationType locType = (Vec.Apps.vVote.Entities.LocationType)Enum.Parse(typeof(Vec.Apps.vVote.Entities.LocationType), rowItem["Type"].ToString());
                string audioFileName = rowItem["AudioFileName"].ToString();

                bool isDuplicate = eve.LocationAudios.Any(c => c.LocationId != id && string.Compare(c.FileName, audioFileName, true) == 0);
                LocationAudio locationAudio = eve.LocationAudios.Single(c => c.LocationId == id);
                locationAudio.DateTimeStamp = null;
                try
                {
                    if (isDuplicate)
                        throw new ArgumentException(Constants.ERROR_DUPLICATE_LOCATION_FILE);

                    string audioFilePath = Path.Combine(contextDirectory.FullName, rowItem["AudioFileName"].ToString());
                    var tag = ID3Tag.GetMP3Tags(audioFilePath);

                    switch (locType)
                    {
                        case Vec.Apps.vVote.Entities.LocationType.Region:
                            FileService.Put(eve.GetWbbFileServiceAddress(), Settings.Default.RegionAudioPath + rowItem["AudioFileName"].ToString(), File.ReadAllBytes(audioFilePath));
                            break;
                        case Vec.Apps.vVote.Entities.LocationType.District:
                            FileService.Put(eve.GetWbbFileServiceAddress(), Settings.Default.DistrictAudioPath + rowItem["AudioFileName"].ToString(), File.ReadAllBytes(audioFilePath));
                            break;
                        case Vec.Apps.vVote.Entities.LocationType.IS:
                            break;
                        case Vec.Apps.vVote.Entities.LocationType.OS:
                            break;
                        default:
                            break;
                    }

                    locationAudio.FileName = rowItem["AudioFileName"].ToString();
                    locationAudio.Author = tag.Author;
                    locationAudio.Title = tag.Title;
                    locationAudio.Comment = tag.Comment;
                    locationAudio.Genre = tag.Genre;
                }
                catch (Exception ex)
                {
                    locationAudio.Error = ex.Message;
                }
            }
            vVoteContext.SaveChanges();
        }

        public void UpdateStaticContent(Guid eventId, DirectoryInfo contextDirectory)
        {
            DataSet sheetData;
            using (SpreadsheetReader ssr = new SpreadsheetReader(Path.Combine(contextDirectory.FullName, Constants.StaticContentFileName)))
            {
                sheetData = ssr.GetDataSet("GVS-audio", "TVS-audio", "Counters-audio", "VIS-audio", "VIS-text", "System Languages");
            }

            foreach (DataRow rowItem in sheetData.Tables["System Languages"].Rows)
            {
                int id = Convert.ToInt32(rowItem["Id"]);
                string languageName = rowItem["Language"].ToString();
                string longName = rowItem["Long Name"].ToString();
                string shortName = rowItem["Short Name"].ToString();
                string localName = rowItem["Local Name"].ToString();
                string direction = rowItem["Direction"].ToString();
                string font = rowItem["Font"].ToString();
                string fontSize = rowItem["FontSize"].ToString();
                string styleSheetLarge = rowItem["Style Sheet Large"].ToString();
                string styleSheetMedium = rowItem["Style Sheet Medium"].ToString();
                int sortOrder = Convert.ToInt32(rowItem["Sort Order"]);

                var language = vVoteContext.Languages.Single(l => l.Id == id);
                language.LongName = longName;
                language.ShortName = shortName;
                language.LocalName = localName;
                language.Direction = direction;
                language.Font = font;
                language.FontSize = fontSize;
                language.StyleSheetLarge = styleSheetLarge;
                language.StyleSheetMedium = styleSheetMedium;
                language.SortOrder = sortOrder;
            }
            vVoteContext.SaveChanges();

            Event eve = vVoteContext.Events.Single(e => e.Id == eventId);
            while (eve.EventStaticAudios.Count > 0)
                vVoteContext.EventStaticAudios.Remove(eve.EventStaticAudios.First());
            while (eve.EventStaticTexts.Count > 0)
                vVoteContext.EventStaticTexts.Remove(eve.EventStaticTexts.First());
            vVoteContext.SaveChanges();

            AddAudioFromSheet(eve, sheetData.Tables["GVS-audio"], contextDirectory, false);
            AddAudioFromSheet(eve, sheetData.Tables["TVS-audio"], contextDirectory, false);
            AddAudioFromSheet(eve, sheetData.Tables["Counters-audio"], contextDirectory, false);
            AddAudioFromSheet(eve, sheetData.Tables["VIS-audio"], contextDirectory, true);

            foreach (DataRow rowItem in sheetData.Tables["VIS-text"].Rows)
            {
                string propertyName = rowItem["Field Name"].ToString();

                if (string.IsNullOrWhiteSpace(propertyName))
                    continue;

                if (eve.EventStaticTexts.SingleOrDefault(est => est.Name == propertyName) == null)
                {
                    string propertyConversionLabel = rowItem["Conversion Label"].ToString();
                    string propertySizeLimit = rowItem["Size Limit"].ToString();
                    string propertyDescription = rowItem["Description"].ToString();

                    EventStaticText eventStaticText = new EventStaticText()
                    {
                        Name = propertyName,
                        Description = propertyDescription,
                        SizeLimit = propertySizeLimit,
                        ConversionLabel = propertyConversionLabel
                    };
                    eve.EventStaticTexts.Add(eventStaticText);

                    foreach (var languageItem in vVoteContext.Languages)
                    {
                        StaticText staticText = new StaticText()
                        {
                            LanguageId = languageItem.Id,
                            DateTimeStamp = null
                        };

                        try
                        {
                            eventStaticText.StaticTexts.Add(staticText);

                            staticText.Name = rowItem[languageItem.Name].ToString();
                        }
                        catch (Exception ex)
                        {
                            staticText.Error = ex.Message;
                        }
                    }
                }
            }

            FileService.Put(eve.GetWbbFileServiceAddress(), Constants.DataUrl + Constants.StaticContentFileName, File.ReadAllBytes(Path.Combine(contextDirectory.FullName, Constants.StaticContentFileName)));

            vVoteContext.SaveChanges();
        }

        private void AddAudioFromSheet(Event eve, DataTable sheetData, DirectoryInfo contextDirectory, bool isLote)
        {
            foreach (DataRow rowItem in sheetData.Rows)
            {
                string name = rowItem["TAG"].ToString();

                if (string.IsNullOrWhiteSpace(name))
                    continue;

                if (eve.EventStaticAudios.SingleOrDefault(esa => esa.Name == name && esa.IsLOTE == isLote) == null)
                {
                    string description = rowItem["Context"].ToString();

                    EventStaticAudio eventStaticAudio = new EventStaticAudio()
                    {
                        Name = name,
                        Description = description,
                        IsLOTE = isLote
                    };
                    eve.EventStaticAudios.Add(eventStaticAudio);

                    if (isLote)
                    {
                        IEnumerable<Language> languages = vVoteContext.Languages.OrderBy(o => o.Id);
                        foreach (var languageItem in languages)
                        {
                            StaticAudio staticAudio = new StaticAudio()
                            {
                                LanguageId = languageItem.Id,
                                DateTimeStamp = null
                            };
                            try
                            {
                                eventStaticAudio.StaticAudios.Add(staticAudio);

                                string fileName = rowItem[languageItem.Name + " File"].ToString();
                                string audioFilePath = Path.Combine(contextDirectory.FullName, fileName);

                                if (File.Exists(audioFilePath))
                                {
                                    var tag = ID3Tag.GetMP3Tags(audioFilePath);
                                    staticAudio.Author = tag.Author;
                                    staticAudio.Title = tag.Title;
                                    staticAudio.Comment = tag.Comment;
                                    staticAudio.Genre = tag.Genre;
                                    staticAudio.FileName = fileName;
                                    staticAudio.FileContent = File.ReadAllBytes(audioFilePath);
                                }
                                else
                                    staticAudio.Error = "File does not exist.";
                            }
                            catch (Exception ex)
                            {
                                staticAudio.Error = ex.Message;
                            }
                        }
                    }
                    else
                    {
                        StaticAudio staticAudio = new StaticAudio()
                        {
                            LanguageId = 1,
                            DateTimeStamp = null
                        };
                        try
                        {
                            eventStaticAudio.StaticAudios.Add(staticAudio);

                            string fileName = rowItem["FILE"].ToString();
                            string audioFilePath = Path.Combine(contextDirectory.FullName, fileName);

                            if (File.Exists(audioFilePath))
                            {
                                var tag = ID3Tag.GetMP3Tags(audioFilePath);
                                staticAudio.Author = tag.Author;
                                staticAudio.Title = tag.Title;
                                staticAudio.Comment = tag.Comment;
                                staticAudio.Genre = tag.Genre;
                                staticAudio.FileName = fileName;
                                staticAudio.FileContent = File.ReadAllBytes(audioFilePath);
                            }
                            else
                                staticAudio.Error = "File does not exist.";
                        }
                        catch (Exception ex)
                        {
                            staticAudio.Error = ex.Message;
                        }
                    }
                }
            }
        }

        public List<EventDeviceEntity> GetEventDevices(Guid eventId)
        {
            List<EventDeviceEntity> result = new List<EventDeviceEntity>();

            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            foreach (var eveLocation in eve.EventLocations)
            {
                foreach (var eveDevice in eveLocation.EventDevices)
                {
                    result.Add(new EventDeviceEntity()
                    {
                        Id = eveDevice.Id,
                        DeviceId = eveDevice.DeviceId,
                        TypeId = eveDevice.Type,
                        TypeName = Enum.GetName(typeof(DeviceType), eveDevice.Type),
                        StageId = eveDevice.Stage,
                        StageName = Enum.GetName(typeof(DeviceStageType), eveDevice.Stage),
                        QuarantineQuota = eveDevice.QuarantineQuota,
                        DeployedLocationId = eveDevice.DeployedLocationId,
                        IsAuditEnabled = eveDevice.IsAuditEnabled,
                        MaxBundleSize = Settings.Default.MaxBundleSize,
                        LaunchUrl = eveDevice.Type == (byte)DeviceType.VPS ? Settings.Default.VpsLaunchUrl : Settings.Default.EvmLaunchUrl,
                        HeartbeatURL = eveDevice.Type == (byte)DeviceType.VPS ? Settings.Default.VpsHeartbeatUrl : Settings.Default.EvmHeartbeatUrl,
                        ConnectionCheckURL = Settings.Default.ConnectionCheckUrl,
                        F5LaunchURL = Settings.Default.F5LaunchUrl,
                        EventLocationEntity = new EventLocationEntity()
                        {
                            Id = eveLocation.Id,
                            Name = eveLocation.LocationName,
                            LocationCode = eveLocation.LocationCode,
                            LocationSubCode = eveLocation.LocationSubCode,
                            RaceName = eveLocation.RaceName,
                            ParentRaceName = eveLocation.ParentRaceName,
                            Type = (Entities.LocationType)eveLocation.Type.Value,
                        }
                    });
                }
            }

            return result;
        }

        public List<MonitoredDeviceEntity> GetMonitoredDevices(Guid eventId)
        {
            List<MonitoredDeviceEntity> result = new List<MonitoredDeviceEntity>();

            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            var telemetryWbb = eve.TelemetryWBBs;
            var telemetryMbb = eve.TelemetryMBBs;
            var telemetryMbbMsg = eve.TelemetryMBBMessages;

            foreach (var eveLocation in eve.EventLocations)
            {
                foreach (var eveDevice in eveLocation.EventDevices)
                {
                    var monDevEntity = new MonitoredDeviceEntity()
                    {
                        Id = eveDevice.Id,
                        DeviceId = eveDevice.DeviceId,
                        TypeId = eveDevice.Type,
                        TypeName = Enum.GetName(typeof(DeviceType), eveDevice.Type),
                        StageId = eveDevice.Stage,
                        StageName = Enum.GetName(typeof(DeviceStageType), eveDevice.Stage),
                        QuarantineQuota = eveDevice.QuarantineQuota,
                        DeployedLocationId = eveDevice.DeployedLocationId,
                        IsAuditEnabled = eveDevice.IsAuditEnabled,
                        MaxBundleSize = Settings.Default.MaxBundleSize,
                        LaunchUrl = eveDevice.Type == (byte)DeviceType.VPS ? Settings.Default.VpsLaunchUrl : Settings.Default.EvmLaunchUrl,
                        HeartbeatURL = eveDevice.Type == (byte)DeviceType.VPS ? Settings.Default.VpsHeartbeatUrl : Settings.Default.EvmHeartbeatUrl,
                        ConnectionCheckURL = Settings.Default.ConnectionCheckUrl,
                        F5LaunchURL = Settings.Default.F5LaunchUrl,
                        EventLocationEntity = new EventLocationEntity()
                        {
                            Id = eveLocation.Id,
                            Name = eveLocation.LocationName,
                            LocationCode = eveLocation.LocationCode,
                            LocationSubCode = eveLocation.LocationSubCode,
                            RaceName = eveLocation.RaceName,
                            ParentRaceName = eveLocation.ParentRaceName,
                            Type = (Entities.LocationType)eveLocation.Type.Value,
                        }
                    };

                    if (monDevEntity.DeployedLocationId.HasValue)
                        monDevEntity.EventLocationEntity.Name += string.Format("-({0})", eve.EventLocations.Single(el => el.Id == monDevEntity.DeployedLocationId).LocationName);

                    var deviceId = monDevEntity.DeviceId.ToString();
                    //Get bundle status
                    {
                        var bundleInfoGroup = telemetryWbb.Where(telWbb => telWbb.From == deviceId).OrderByDescending(o => o.DateTime);
                        var bundleInfo = bundleInfoGroup.FirstOrDefault();

                        if (bundleInfo != null)
                            monDevEntity.BundleStatus = new BundleStatusEntity()
                            {
                                BundleUrl = bundleInfo.Resource,
                                DateTime = bundleInfo.DateTime.ToLocalTime().ToString("g"),
                                Status = bundleInfo.Status
                            };
                    }

                    //Get Start EVM/POD count
                    {
                        var startPodEvm = default(IEnumerable<TelemetryMBB>);
                        if (monDevEntity.TypeId == (byte)DeviceType.VPS)
                            startPodEvm = telemetryMbb.Where(tmbb => tmbb.MessageType == Constants.MSGPOD_MESSAGE && tmbb.Message.Contains(deviceId));
                        else
                            startPodEvm = telemetryMbb.Where(tmbb => tmbb.MessageType == Constants.MSGSTARTEVM && tmbb.Message.Contains(deviceId));

                        foreach (var groupItem in startPodEvm.GroupBy(g => g.From))
                        {
                            monDevEntity.VoteStatus.StartCount = groupItem.Count();
                            break;
                        }
                    }

                    //Get the vote count
                    if (monDevEntity.TypeId == (byte)DeviceType.VPS)
                    {
                        monDevEntity.VoteStatus.IQCount = telemetryMbbMsg.
                           Where(tmms => tmms.VPSId == deviceId && tmms.Type == (short)CommitType.Cancel).
                           GroupBy(g => g.SerialNo).
                           Select(grp => new { SerialNo = grp.Key, VoteCount = 1 }).
                           Sum(s => s.VoteCount);

                        monDevEntity.VoteStatus.AuditCount = telemetryMbbMsg.
                           Where(tmms => tmms.VPSId == deviceId && tmms.Type == (short)CommitType.Audit).
                           GroupBy(g => g.SerialNo).
                           Select(grp => new { SerialNo = grp.Key, VoteCount = 1 }).
                           Sum(s => s.VoteCount);
                    }
                    else
                    {
                        monDevEntity.VoteStatus.VoteCount = telemetryMbbMsg.
                            Where(tmms => tmms.EVMId == deviceId && tmms.Type == (short)CommitType.Vote).
                            GroupBy(g => g.SerialNo).
                            Select(grp => new { SerialNo = grp.Key, VoteCount = 1 }).
                            Sum(s => s.VoteCount);
                    }

                    result.Add(monDevEntity);
                }
            }

            return result;
        }

        public void UpdateEventDevice(EventDeviceEntity eventDevice)
        {
            var eveDevice = vVoteContext.EventDevices.Single(ed => ed.Id == eventDevice.Id);
            eveDevice.Stage = eventDevice.StageId;
            eveDevice.QuarantineQuota = eventDevice.QuarantineQuota;
            eveDevice.IsAuditEnabled = eventDevice.IsAuditEnabled;

            var assignedSpareLocation = eveDevice.DeployedLocationId != eventDevice.DeployedLocationId;

            eveDevice.DeployedLocationId = eventDevice.DeployedLocationId;
            vVoteContext.SaveChanges();

            if (assignedSpareLocation)
                GenerateSpareMap(eveDevice.EventLocation.EventId);
        }

        private void GenerateSpareMap(Guid eventId)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            var spareMap = from el in eve.EventLocations
                           join sd in vVoteContext.EventDevices.Where(ed => ed.EventLocation.EventId == eventId && ed.DeployedLocationId.HasValue) on el.Id equals sd.DeployedLocationId
                           select new { SpareDevice = sd, DeployedLocation = el };

            JObject spareMapObject = new JObject();
            foreach (var spare in spareMap)
            {
                spareMapObject.Add(spare.SpareDevice.DeviceId.ToString(), spare.DeployedLocation.LocationName);
            }

            FileService.Put(eve.GetWbbFileServiceAddress(), Constants.DataUrl + Constants.SpareLocationMapJsonFileName, spareMapObject.ToString().GetBytes());
        }

        public string ExportEventDeviceLabels(Guid eventId, DirectoryInfo contextDirectory)
        {
            var eventDevices = GetEventDevices(eventId);

            var orderedList = eventDevices.OrderBy(o => o.Id).OrderBy(o => o.TypeId).GroupBy(g => g.EventLocationEntity.Name);

            foreach (var grpItem in orderedList)
            {
                int vpsCount = 1, evmCount = 1;
                foreach (var item in grpItem)
                {
                    if (item.TypeId == (byte)DeviceType.VPS)
                        item.Name = string.Format("VPS tablet {0:d2}", vpsCount++);
                    else
                        item.Name = string.Format("EVM tablet {0:d2}", evmCount++);
                }
            }

            string filePath = Path.Combine(contextDirectory.FullName, Constants.LabelFileName);
            File.Copy(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "LabelTemplate.pdf"), Path.Combine(contextDirectory.FullName, "LabelTemplate.pdf"));

            IPdfGenerator<List<EventDeviceEntity>> pdfGenerator = PdfGeneratorBase<List<EventDeviceEntity>>.GetGeneratorInstance<DeviceLabelGenerator>();
            pdfGenerator.ContextDirectory = contextDirectory;
            pdfGenerator.Entity = eventDevices;
            pdfGenerator.FileName = Constants.LabelFileName;
            pdfGenerator.GeneratePDF();

            return filePath;
        }

        public string UploadQuarantine(Guid eventId, string qFileName, string sFileName)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            var qFileInfo = new FileInfo(qFileName);
            var ebPubFileName = (Path.Combine(qFileInfo.DirectoryName, Constants.EBPublicKeyFileName));
            byte[] ebPublicKeyBytes = FileService.Get(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.EBPublicKeyFileName);
            File.WriteAllBytes(ebPubFileName, ebPublicKeyBytes);

            if (PGP.VerifyFile(qFileName, ebPubFileName))
            {
                DataSet sheetData;
                using (SpreadsheetReader ssr = new SpreadsheetReader(qFileName))
                {
                    sheetData = ssr.GetDataSet("Receipts");
                }

                JObject quarantineObj = new JObject();
                JArray quarantineArray = new JArray();
                for (int i = 0; i < sheetData.Tables["Receipts"].Rows.Count; i++)
                {
                    string serial = sheetData.Tables["Receipts"].Rows[i].Field<string>("Serial Number");
                    int deviceId, serialNumber;

                    if (!int.TryParse(serial.Split(':')[0], out deviceId))
                        throw new ArgumentException(string.Format("The serial number '{0}' at row '{1}' is not valid.", serial, i + 1));
                    if (!int.TryParse(serial.Split(':')[1], out serialNumber))
                        throw new ArgumentException(string.Format("The serial number '{0}' at row '{1}' is not valid.", serial, i + 1));

                    quarantineArray.Add(serial);
                }
                quarantineObj.Add("Serials", quarantineArray);

                var sFileInfo = new FileInfo(sFileName);
                using (StreamReader sr = sFileInfo.OpenText())
                {
                    var qSignature = sr.ReadToEnd();
                    qSignature = qSignature.Replace("\r\n", string.Empty);
                    quarantineObj.Add("Signature", qSignature.RegexReplace(@".*\.\d{5}").RegexReplace(@"-.*"));
                }

                var contextDirectory = qFileInfo.DirectoryName;
                var quarantineDirectory = Directory.CreateDirectory(Path.Combine(contextDirectory, Constants.QuarantineFolderName)).FullName;
                qFileInfo.MoveTo(Path.Combine(quarantineDirectory, qFileInfo.Name));
                sFileInfo.MoveTo(Path.Combine(quarantineDirectory, sFileInfo.Name));
                Zipper.Archive(quarantineDirectory, string.Empty);

                FileService.Put(eve.GetWbbFileServiceAddress(), Constants.DataUrl + Constants.QuarantineZipFileName, File.ReadAllBytes(Path.Combine(contextDirectory, Constants.QuarantineZipFileName)));
                FileService.Put(eve.GetWbbFileServiceAddress(), Constants.DataUrl + Constants.QuarantineJsonFileName, quarantineObj.ToString(Newtonsoft.Json.Formatting.None).GetBytes());

                return eve.GetWbbFileUrl(Constants.DataUrl + Constants.QuarantineZipFileName);
            }
            else
                throw new ArgumentException("The uploaded file does not verify against its signature.");
        }

        public void GenerateStaticContentBundle(Guid eventId, string rootPath)
        {
            Directory.CreateDirectory(Path.Combine(rootPath, Constants.DataUrl));

            JObject languageConfig = new JObject();
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            JArray languages = new JArray();
            JArray dictionaries = new JArray();
            JObject audioConfig = new JObject();
            JObject clips = new JObject();
            JObject sizeLimit = new JObject();
            JObject conversionLabels = new JObject();

            foreach (var langItem in vVoteContext.Languages.OrderBy(o => o.SortOrder))
            {
                JObject language = new JObject();
                language.Add("language", langItem.Name.ToLowerInvariant());
                language.Add("name", langItem.LocalName);
                language.Add("englishName", langItem.LongName);
                language.Add("dictionary", langItem.Name.ToLowerInvariant());
                language.Add("direction", langItem.Direction);
                language.Add("font", langItem.Font);
                language.Add("default_font_size", langItem.FontSize);
                language.Add("stylesheet_large", langItem.StyleSheetLarge);
                language.Add("stylesheet_medium", langItem.StyleSheetMedium);
                languages.Add(language);

                JObject translation = new JObject();
                foreach (var stcItem in eve.EventStaticTexts)
                    foreach (var item in stcItem.StaticTexts.Where(st => st.LanguageId == langItem.Id))
                    {
                        translation.Add(stcItem.Name, item.Name);
                    }
                JObject dictionary = new JObject();
                dictionary.Add("dictionary", langItem.Name.ToLowerInvariant());
                dictionary.Add("translations", translation);
                dictionaries.Add(dictionary);

                JObject audioTranslation = new JObject();
                foreach (var sacItem in eve.EventStaticAudios.Where(est => est.IsLOTE))
                    foreach (var item in sacItem.StaticAudios.Where(sa => sa.LanguageId == langItem.Id))
                    {
                        if (string.IsNullOrEmpty(item.FileName))
                        {
                            audioTranslation.Add(sacItem.Name, string.Empty);
                        }
                        else
                        {
                            audioTranslation.Add(sacItem.Name, item.FileName);

                            if (item.FileContent != null)
                            {
                                FileInfo fi = new FileInfo(Path.Combine(rootPath, item.FileName));

                                if (!fi.Directory.Exists)
                                    fi.Directory.Create();

                                File.WriteAllBytes(Path.Combine(rootPath, item.FileName), item.FileContent);
                            }
                        }
                    }
                clips.Add(langItem.Name.ToLowerInvariant(), audioTranslation);
            }

            foreach (var sacItem in eve.EventStaticAudios.Where(est => !est.IsLOTE))
                foreach (var item in sacItem.StaticAudios.Where(sa => sa.LanguageId == 1))
                {
                    if (string.IsNullOrEmpty(item.FileName))
                    {
                        clips.Add(sacItem.Name, string.Empty);
                    }
                    else
                    {
                        clips.Add(sacItem.Name, item.FileName);

                        if (item.FileContent != null)
                        {
                            FileInfo fi = new FileInfo(Path.Combine(rootPath, item.FileName));

                            if (!fi.Directory.Exists)
                                fi.Directory.Create();

                            File.WriteAllBytes(Path.Combine(rootPath, item.FileName), item.FileContent);
                        }
                    }
                }

            foreach (var stcItem in eve.EventStaticTexts)
            {
                sizeLimit.Add(stcItem.Name, stcItem.SizeLimit);
                if (!string.IsNullOrEmpty(stcItem.ConversionLabel))
                    conversionLabels.Add(stcItem.Name, stcItem.ConversionLabel);
            }

            languageConfig.Add("languages", languages);
            languageConfig.Add("dictionaries", dictionaries);
            languageConfig.Add("size_limits", sizeLimit);
            languageConfig.Add("conversion_labels", conversionLabels);
            audioConfig.Add("clips", clips);

            File.WriteAllText(Path.Combine(rootPath, Constants.DataUrl, "language_config.txt"), languageConfig.ToString());
            File.WriteAllText(Path.Combine(rootPath, Constants.DataUrl, "audio_config.txt"), audioConfig.ToString());

            #region Signing files...
            //{
            //    string[] fileFilter = Settings.Default.BundleFileFilter.Split(';');
            //    foreach (var fileItem in Directory.GetFiles(rootPath, "*.*", SearchOption.AllDirectories).Where(s => fileFilter.Any(f => s.EndsWith(f))))
            //    {
            //        PGP.SignFile(fileItem, Path.Combine(HostingEnvironment.ApplicationPhysicalPath, Settings.Default.PrivateFile), Settings.Default.Passphrase.ToCharArray());
            //    }
            //}
            #endregion Signing files...

            Logger.Info("GenerateStaticContentBundle Complete.");
        }

        public void PrepareExportTemplate(DeviceType deviceType, Guid eventId, DirectoryInfo contextDirectory)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);

            byte[] file = FileService.Get(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + (deviceType == DeviceType.EVM ? Constants.EvmTemplateFileName : Constants.VpsTemplateFileName));
            Zipper.Extract(new MemoryStream(file), contextDirectory.FullName);

            byte[] trainingFile = FileService.Get(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + (deviceType == DeviceType.EVM ? Constants.TrainingEvmTemplateFileName : Constants.TrainingVpsTemplateFileName));
            Zipper.Extract(new MemoryStream(trainingFile), contextDirectory.FullName);

            var clientFolder = Path.Combine(contextDirectory.FullName, Constants.ClientTemplateFolderName);
            var trainingFolder = Path.Combine(contextDirectory.FullName, Constants.TrainingClientTemplateFolderName);
            var mbbSettings = GetMbbSettingFiles(eventId);

            foreach (var item in mbbSettings)
                File.WriteAllBytes(Path.Combine(clientFolder, item.Key), item.Value);

            byte[] vVoteFile = FileService.Get(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.ClientTemplateFileName);
            Zipper.Extract(new MemoryStream(vVoteFile), Path.Combine(clientFolder, "vvwww"));
            Zipper.Extract(new MemoryStream(vVoteFile), Path.Combine(trainingFolder, "vvwww"));

            byte[] ebPublicKeyFile = FileService.Get(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.EBPublicKeyFileName);
            File.WriteAllBytes(Path.Combine(clientFolder, Constants.EBPublicKeyFileName), ebPublicKeyFile);

            GenerateStaticContentBundle(eventId, Path.Combine(clientFolder, "vvwww"));
            GenerateStaticContentBundle(eventId, Path.Combine(trainingFolder, "vvwww"));

            Zipper.Archive(contextDirectory.FullName, string.Empty);
        }

        public void ProcessPublicSigningKey(Guid eventId, string contextPath)
        {
            var eve = vVoteContext.Events.Single(e => e.Id == eventId);
            var fileSuffix = "_PublicSigningKey.json";
            DirectoryInfo di = new DirectoryInfo(contextPath);

            JObject fmg = new JObject();
            JObject evm = new JObject();
            JObject vps = new JObject();
            JObject can = new JObject();

            if (File.Exists(Path.Combine(contextPath, Constants.FmgBlsPublicKey)))
            {
                fmg = JObject.Parse(File.ReadAllText(Path.Combine(contextPath, Constants.FmgBlsPublicKey)));
                fmg.Remove("jksPath");
                File.Delete(Path.Combine(contextPath, Constants.FmgBlsPublicKey));
            }

            var publicFiles = di.GetFiles("*" + fileSuffix, SearchOption.TopDirectoryOnly);
            foreach (var file in publicFiles)
            {
                var deviceIdStr = file.Name.Replace(fileSuffix, string.Empty);
                var deviceId = Convert.ToInt32(deviceIdStr);
                var fileContent = File.ReadAllText(file.FullName);

                var device = vVoteContext.EventDevices.Single(ed => ed.EventLocation.EventId == eventId && ed.DeviceId == deviceId);

                if (device.Type == (byte)DeviceType.EVM)
                {
                    evm.Add(deviceIdStr, JObject.Parse(fileContent)[deviceIdStr]);
                }
                else
                {
                    vps.Add(deviceIdStr, JObject.Parse(fileContent)[deviceIdStr]);
                    fmg.Add(deviceIdStr, JObject.Parse(fileContent)[deviceIdStr]);
                    can.Add(deviceIdStr, JObject.Parse(fileContent)[deviceIdStr]);
                }
                File.Delete(file.FullName);
            }

            FileService.Put(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.EvmBlsPublicKey, evm.ToString(Newtonsoft.Json.Formatting.Indented).GetBytes());
            FileService.Put(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.VpsBlsPublicKey, vps.ToString(Newtonsoft.Json.Formatting.Indented).GetBytes());

            var mbbConfigFile = FileService.Get(eve.GetWbbFileServiceAddress(), Constants.InternalUrl + Constants.MbbConfigFileName);
            var mbbConfigJson = JObject.Parse(mbbConfigFile.GetString());

            foreach (var mbb in mbbConfigJson["peers"])
            {
                var mbbName = mbb["id"].Value<string>().Replace("_SSL", string.Empty);

                DirectoryInfo dir = new DirectoryInfo(Path.Combine(contextPath, string.Format(Settings.Default.PublicKeyRootPathFormat, mbbName)));
                dir.Create();

                evm["jksPath"] = string.Format(Settings.Default.PublicKeyRootPathFormat, mbbName) + Constants.EvmJksPublicKey;
                vps["jksPath"] = string.Format(Settings.Default.PublicKeyRootPathFormat, mbbName) + Constants.VpsJksPublicKey;
                fmg["jksPath"] = string.Format(Settings.Default.PublicKeyRootPathFormat, mbbName) + Constants.FmgJksPublicKey;
                can["jksPath"] = string.Format(Settings.Default.PublicKeyRootPathFormat, mbbName) + Constants.CanJksPublicKey;

                File.WriteAllText(Path.Combine(dir.FullName, Constants.EvmBlsPublicKey), evm.ToString());
                File.WriteAllText(Path.Combine(dir.FullName, Constants.VpsBlsPublicKey), vps.ToString());
                File.WriteAllText(Path.Combine(dir.FullName, Constants.CanBlsPublicKey), can.ToString());
                File.WriteAllText(Path.Combine(dir.FullName, Constants.FmgBlsPublicKey), fmg.ToString());
            }
        }
    }
}
