﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using Org.BouncyCastle.Math.EC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.vVote.Entities
{
    public class DevicePermutationEntity
    {
        public DevicePermutationEntity()
        {
            ciphers = new List<Cipher>();
        }

        public string serialNo { get; set; }

        public string permutation { get; set; }

        public List<Cipher> ciphers { get; set; }
    }

    public class Cipher
    {
        public Coordinate myr { get; set; }

        public Coordinate gr { get; set; }
    }

    public class Coordinate
    {
        public string x { get; set; }

        public string y { get; set; }

    }

    public class VotePack
    {
        public List<PackCiphers> EarlyPackCiphers { get; set; }
        public List<PackCiphers> OrdinaryPackCiphers { get; set; }
        public List<PackCiphers> AbsentPackCiphers { get; set; }
        internal VotePack()
        {
            EarlyPackCiphers = new List<PackCiphers>();
            OrdinaryPackCiphers = new List<PackCiphers>();
            AbsentPackCiphers = new List<PackCiphers>();

            EarlyBlanks = new List<string>();
            OrdinaryBlanks = new List<string>();
            AbsentBlanks = new List<string>();
        }

        public List<string> EarlyBlanks { get; set; }
        public List<string> OrdinaryBlanks { get; set; }
        public List<string> AbsentBlanks { get; set; }
    }

    public class PackCiphers
    {
        public List<PackCipher> Ciphers { get; set; }
        public int PrefCount { get; set; }
        public int BallotLength { get; set; }
        public bool IsPacked { get; set; }

        internal PackCiphers()
        {
            Ciphers = new List<PackCipher>();
        }
    }

    public class PackCipher
    {
        public ECPoint GYR { get; set; }

        public ECPoint MYR { get; set; }
    }

    public class RaceCandidateMap
    {
        public Guid RaceId { get; set; }

        public string RaceName { get; set; }

        public string RaceType { get; set; }

        public List<Coordinate> CandidateIds { get; set; }

        public string DistrictName { get; set; }

        public RaceCandidateMap()
        {
            CandidateIds = new List<Coordinate>();
            RaceName = string.Empty;
            DistrictName = string.Empty;
        }
    }

    public class RaceMap
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid RaceId { get; set; }

        public string District { get; set; }
    }
}
