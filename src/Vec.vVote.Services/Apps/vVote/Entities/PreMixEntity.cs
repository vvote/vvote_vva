﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.vVote.Entities
{
    public class PreMixEntity
    {
        public Guid EventId { get; set; }

        public string EventName { get; set; }

        public List<CommitEntity> Commits { get; set; }

        public List<EventLocationEntity> Locations { get; set; }

        public DateTime ReportDateTime { get; set; }

        public DateTime CommitDateTime { get; set; }

        public PreMixEntity()
        {
            Commits = new List<CommitEntity>();
            Locations = new List<EventLocationEntity>();
        }

        public string DistrictName { get; set; }

        public Guid? DistrictId { get; set; }
    }
}
