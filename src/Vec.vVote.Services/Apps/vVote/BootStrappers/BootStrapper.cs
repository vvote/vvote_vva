﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.vVote.BL;
using Vec.Apps.vVote.BL.Common;
using Vec.Apps.vVote.BL.Handler;
using Vec.Apps.vVote.Converter;
using Vec.Apps.vVote.DAL;
using Vec.Apps.vVote.Services;
using Vec.Framework.Core.BootStrappers;
using Vec.Properties;
using Vec.TuringElectionService;
using Vec.TuringNominationService;
using Vec.WbbVotePackingService;

namespace Vec.Apps.vVote.BootStrappers
{
    public class BootStrapper : IBootStrapper
    {
        public void RegisterTypes(ContainerBuilder builder)
        {
            //DAL
            builder.Register(c => new vVoteEntities()).As<IvVoteContext>().InstancePerLifetimeScope();

            //Common
            builder.RegisterType<AudioHelper>().As<IAudioHelper>().SingleInstance();
            builder.RegisterType<ECHelper>().As<IECHelper>().SingleInstance();
            builder.RegisterType<LogHandlerFactory>().As<ILogHandlerFactory>();
            builder.RegisterType<MP3toOggConverter>().As<IAudioConverter>();

            //BL
            builder.RegisterType<ElectionBL>().AsSelf().As<IElectionBL>();
            builder.RegisterType<EventBL>().AsSelf().As<IEventBL>();
            builder.RegisterType<BundleBL>().AsSelf().As<IBundleBL>();
            builder.RegisterType<VoteDecryptBL>().AsSelf().As<IVoteDecryptBL>();
            builder.RegisterType<LogBL>().AsSelf().As<ILogBL>();
            builder.RegisterType<AuthBL>().AsSelf().As<IAuthBL>();
            builder.RegisterType<TelemetryBL>().AsSelf().As<ITelemetryBL>();

            //External Services
            builder.Register(elc => new ElectionLookupClient(Settings.Default.ElectionServiceEndpoint)).AsSelf().As<IElectionLookup>();
            builder.Register(nesc => new NominationsExternalServiceClient(Settings.Default.NominationServiceEndpoint)).AsSelf().As<INominationsExternalService>();
            builder.Register(nesc => new VotePackingServiceHelper()).AsSelf().As<IVotePackingServiceHelper>();
            builder.Register(wfs => new FileServiceHelper()).AsSelf().As<IFileServiceHelper>();

            //Internal Services
            builder.RegisterType<EventService>().AsSelf().As<IEventService>();
            builder.RegisterType<BundleService>().AsSelf().As<IBundleService>();
            builder.RegisterType<VoteDecryptService>().AsSelf().As<IVoteDecryptService>();
            builder.RegisterType<LogService>().AsSelf().As<ILogService>();
            builder.RegisterType<AuthService>().AsSelf().As<IAuthService>();
            builder.RegisterType<TelemetryService>().AsSelf().As<ITelemetryService>();
        }

        public void Start()
        {
        }

        public void Stop()
        {
        }
    }
}
