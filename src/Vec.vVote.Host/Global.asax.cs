﻿using Autofac;
using Autofac.Integration.Wcf;
using System;
using System.Collections.Generic;
using System.IdentityModel.Services;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Vec.Framework.Core.BootStrappers;

namespace Vec.vVote.Host
{
    public class Global : System.Web.HttpApplication
    {
        List<IBootStrapper> BootStrappers { get; set; }

        protected void Application_Start(object sender, EventArgs e)
        {
            var builder = new ContainerBuilder();

            BootStrappers = new List<IBootStrapper>();
            BootStrappers.Add(new Vec.Apps.vVote.BootStrappers.BootStrapper());

            foreach (var bootItem in BootStrappers)
            {
                bootItem.RegisterTypes(builder);
            }

            AutofacHostFactory.Container = builder.Build();
            foreach (var bootItem in BootStrappers)
            {
                bootItem.Start();
            }
        }

        void SessionAuthenticationModule_SessionSecurityTokenReceived(object sender, SessionSecurityTokenReceivedEventArgs e)
        {
            System.Diagnostics.Trace.WriteLine("Handling SessionSecurityTokenReceived event");
            SessionSecurityToken sessionSecurityToken = e.SessionToken;
            if (FederatedAuthentication.SessionAuthenticationModule != null)
            {
                FederatedAuthentication.SessionAuthenticationModule.WriteSessionTokenToCookie(sessionSecurityToken);
                System.Web.HttpContext.Current.User = Thread.CurrentPrincipal = sessionSecurityToken.ClaimsPrincipal;
            }
        }


        void SessionAuthenticationModule_SessionSecurityTokenCreated(object sender, SessionSecurityTokenCreatedEventArgs e)
        {
            System.Diagnostics.Trace.WriteLine("Handling SessionSecurityTokenCreated event");
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            if (BootStrappers != null && BootStrappers.Count > 0)
                foreach (var bootItem in BootStrappers)
                {
                    bootItem.Start();
                }
        }
    }
}