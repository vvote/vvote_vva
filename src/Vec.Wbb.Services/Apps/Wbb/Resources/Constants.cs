﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.Wbb.Resources
{
    public class Constants
    {
        public const string LocationJsonFileName = "Location.json";
        public const string DistrictRegionMapFileName = "DistrictRegionMap.json";
        public const string CiphersFileName = "ciphers.json";

        public const string DataUrl = "data/";
        public const string CommitsFolder = @"commits/";
        public const string CiphersFolder = @"internal/vote_packing/ciphers/";
    }
}
