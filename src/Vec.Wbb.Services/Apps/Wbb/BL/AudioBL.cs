﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using Vec.Apps.Entities;
using Vec.Properties;

namespace Vec.Apps.Wbb.BL
{
    public interface IAudioBL
    {
        List<Entities.File> GetAudioPropertyList(EntityType entityType);
    }

    public class AudioBL : IAudioBL
    {
        public IFileBL FileBL { get; set; }

        public AudioBL(IFileBL fileBL)
        {
            FileBL = fileBL;
        }

        public List<Entities.File> GetAudioPropertyList(EntityType entityType)
        {
            List<Entities.File> result = new List<Entities.File>();
            DirectoryInfo di = null;
            string relativePath = string.Empty;
            switch (entityType)
            {
                case EntityType.Candidate:
                    relativePath = Settings.Default.CandidateAudioPath;
                    di = new DirectoryInfo(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, relativePath));

                    break;
                case EntityType.Party:
                    relativePath = Settings.Default.PartyAudioPath;
                    di = new DirectoryInfo(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, relativePath));
                    break;
                default:
                    break;
            }

            if (di != null)
                foreach (var item in di.EnumerateFiles("*.mp3"))
                {
                    var file = new Entities.File();
                    file.Properties.Add(new FileProperty() { Key = "name", Value = item.Name });
                    file.Properties.Add(new FileProperty() { Key = "path", Value = Path.Combine(relativePath, item.Name) });
                    file.Properties.AddRange(FileBL.GetID3Tags(item.FullName));
                    result.Add(file);
                }

            return result;
        }
    }
}
