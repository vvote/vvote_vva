﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Hosting;
using Vec.Apps.Entities;
using Vec.Apps.vVote;
using Vec.Properties;

namespace Vec.Apps.Wbb.BL
{
    public interface IFileBL
    {
        byte[] Get(string relativeFilePath);

        void Put(string relativeFilePath, byte[] content);

        bool Exist(string relativeFilePath);

        List<FileProperty> GetID3Tags(string filePath);
    }

    public class FileBL : IFileBL
    {
        public byte[] Get(string relativeFilePath)
        {
            string absolutePath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, relativeFilePath);
            return System.IO.File.ReadAllBytes(absolutePath);
        }

        public void Put(string relativeFilePath, byte[] content)
        {
            string absolutePath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, relativeFilePath);
            FileInfo fi = new FileInfo(absolutePath);

            if (!fi.Directory.Exists)
                fi.Directory.Create();

            System.IO.File.WriteAllBytes(absolutePath, content);
        }

        public bool Exist(string relativeFilePath)
        {
            string absolutePath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, relativeFilePath);
            return System.IO.File.Exists(absolutePath);
        }

        public List<FileProperty> GetID3Tags(string filePath)
        {
            List<FileProperty> result = new List<FileProperty>();

            var id3Tag = ID3Tag.GetMP3Tags(filePath);
            result.Add(new FileProperty() { Key = "album", Value = id3Tag.Album });
            result.Add(new FileProperty() { Key = "author", Value = id3Tag.Author });
            result.Add(new FileProperty() { Key = "comment", Value = id3Tag.Comment });
            result.Add(new FileProperty() { Key = "genre", Value = id3Tag.Genre });
            result.Add(new FileProperty() { Key = "title", Value = id3Tag.Title });
            result.Add(new FileProperty() { Key = "year", Value = Convert.ToString(id3Tag.Year) });
            result.Add(new FileProperty() { Key = "userDefined", Value = id3Tag.UserDefined });

            return result;
        }
    }
}
