﻿using Ionic.Zip;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using Vec.Apps.Wbb.Resources;

namespace Vec.Apps.Wbb.BL
{
    public interface IVotePackingBL
    {
        List<int> GetCipherDevices(string basePath);

        void ExtractCiphers(string basePath);

        byte[] GetCommitFiles(string basePath);
    }

    public class VotePackingBL : IVotePackingBL
    {
        public byte[] GetCommitFiles(string basePath)
        {
            var result = default(byte[]);
            var allCommitFiles = new DirectoryInfo(Path.Combine(basePath, Constants.CommitsFolder)).GetFiles("*.json", SearchOption.TopDirectoryOnly);

            using (ZipFile zf = new ZipFile())
            {
                foreach (var deviceFile in allCommitFiles.Where(acf => acf.Length > 0).OrderBy(azf => azf.CreationTimeUtc))
                {
                    zf.AddFile(deviceFile.FullName, "");
                }

                using (MemoryStream ms = new MemoryStream())
                {
                    zf.Save(ms);

                    result = ms.ToArray();
                }
            }

            return result;
        }

        public List<int> GetCipherDevices(string basePath)
        {
            var result = new List<int>();
            var allCipherFiles = new DirectoryInfo(Path.Combine(basePath, Constants.CiphersFolder)).GetFiles("*.json", SearchOption.TopDirectoryOnly);

            foreach (var deviceFile in allCipherFiles)
                result.Add(Convert.ToInt32(Path.GetFileNameWithoutExtension(deviceFile.Name)));

            return result;
        }

        public void ExtractCiphers(string basePath)
        {
            var allZipFiles = new DirectoryInfo(Path.Combine(basePath, Constants.CommitsFolder)).GetFiles("*.zip", SearchOption.TopDirectoryOnly);

            foreach (var zipFile in allZipFiles.OrderBy(azf => azf.CreationTimeUtc))
            {
                ZipFile zi = ZipFile.Read(zipFile.FullName);

                foreach (var zipEntry in zi.Entries)
                {
                    if (zipEntry.FileName.EndsWith("zip", StringComparison.InvariantCultureIgnoreCase))
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            zipEntry.Extract(ms);
                            ms.Position = 0;

                            var innerZipFile = ZipFile.Read(ms);
                            var cipherZipEntry = innerZipFile.Entries.SingleOrDefault(ze => ze.FileName == Constants.CiphersFileName);

                            if (cipherZipEntry != null)
                                using (MemoryStream msCipher = new MemoryStream())
                                {
                                    cipherZipEntry.Extract(msCipher);
                                    msCipher.Position = 0;

                                    using (TextReader fs = new StreamReader(msCipher))
                                    {
                                        JObject jo = JObject.Parse(fs.ReadLine());
                                        var serialNo = jo.Value<string>("serialNo");
                                        var deviceId = serialNo.Split(':')[0];

                                        msCipher.Position = 0;
                                        File.WriteAllBytes(Path.Combine(basePath, Constants.CiphersFolder, string.Format("{0}.json", deviceId)), msCipher.ToArray());
                                    }
                                }
                        }
                    }
                }
            }
        }
    }
}
