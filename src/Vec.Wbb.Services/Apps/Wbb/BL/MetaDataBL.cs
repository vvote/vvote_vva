﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using Vec.Apps.Entities;
using Vec.Apps.Wbb.Resources;
using Vec.Properties;

namespace Vec.Apps.Wbb.BL
{
    public interface IMetaDataBL
    {
        Dictionary<string, string> DistrictRegions { get; set; }

        List<VotingCenter> VotingCenters { get; set; }

        Dictionary<string, JObject> Receipts { get; set; }
    }

    public sealed class MetaDataBL : IMetaDataBL
    {
        public Dictionary<string, string> DistrictRegions { get; set; }
        public List<VotingCenter> VotingCenters { get; set; }
        public Dictionary<string, JObject> Receipts { get; set; }

        public MetaDataBL()
        {
            LoadVotingCenters();
            LoadDistrictRegions();
            LoadReceipts();
        }

        private void LoadDistrictRegions()
        {
            JObject districtRegionsJson = JObject.Parse(System.IO.File.ReadAllText(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, Constants.DataUrl, Constants.DistrictRegionMapFileName)));

            DistrictRegions = new Dictionary<string, string>();
            foreach (var item in districtRegionsJson)
            {
                DistrictRegions.Add(item.Key, item.Value.ToString());
            }
        }

        private void LoadVotingCenters()
        {
            JArray votingCentersJson = JArray.Parse(System.IO.File.ReadAllText(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, Constants.DataUrl, Constants.LocationJsonFileName)));

            VotingCenters = new List<VotingCenter>();
            foreach (var item in votingCentersJson)
            {
                VotingCenters.Add(new VotingCenter()
                {
                    Id = Convert.ToInt32(string.Format("{0}{1:d2}", item["LocationCode"], item["LocationSubCode"])),
                    Name = item["Name"].Value<string>()
                });
            }
        }

        private void LoadReceipts()
        {
            Receipts = new Dictionary<string, JObject>();
            foreach (var file in Directory.GetFiles(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, Constants.CommitsFolder), "*.json"))
            {
                foreach (var item in System.IO.File.ReadAllLines(file))
                {
                    JObject receiptJson = JObject.Parse(item);
                    if (receiptJson["type"].Value<string>() == "vote" || receiptJson["type"].Value<string>() == "audit" || receiptJson["type"].Value<string>() == "cancel")
                    {
                        string serialNo = receiptJson["serialNo"].Value<string>();
                        string[] device = serialNo.Split(':');
                        int locCode = Convert.ToInt32(device[0].Substring(0, device[0].Length - 2));
                        string district = receiptJson["district"].Value<string>();


                        receiptJson.Add("voting_centre", VotingCenters.Single(vc => vc.Id == locCode).Name);
                        receiptJson.Add("region", DistrictRegions.Single(dr => dr.Key == district).Value);

                        if (!Receipts.ContainsKey(serialNo))
                            Receipts.Add(serialNo, receiptJson);
                    }
                }
            }
        }
    }
}
