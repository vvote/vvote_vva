﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.Entities;
using Vec.Apps.vVote;
using Vec.Apps.Wbb.Resources;
using Vec.Properties;

namespace Vec.Apps.Wbb.BL
{
    public interface IReceiptBL
    {
        string GetReceipt(string serialNumber);
    }

    public class ReceiptBL : IReceiptBL
    {
        public IMetaDataBL MetaDataBL { get; set; }

        public ReceiptBL(IMetaDataBL metaDataBL)
        {
            MetaDataBL = metaDataBL;
        }

        public string GetReceipt(string serialNumber)
        {
            var receipt = MetaDataBL.Receipts.SingleOrDefault(r => r.Key == serialNumber);
            if (receipt.Key == null)
                throw new VoteException("Receipt does not exist.", 1);

            return receipt.Value.ToString(Newtonsoft.Json.Formatting.None);
        }
    }
}
