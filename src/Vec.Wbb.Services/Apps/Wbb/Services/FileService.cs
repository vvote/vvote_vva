﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.Entities;
using Vec.Apps.Services;
using Vec.Apps.Wbb.BL;

namespace Vec.Apps.Wbb.Services
{
    public class FileService : IFileService
    {
        public IFileBL FileManagerBL { get; set; }

        public FileService(IFileBL fileManagerBL)
        {
            FileManagerBL = fileManagerBL;
        }

        public byte[] Get(string relativeFilePath)
        {
            return FileManagerBL.Get(relativeFilePath);
        }

        public void Put(string relativeFilePath, byte[] content)
        {
            FileManagerBL.Put(relativeFilePath, content);
        }

        public bool Exist(string relativeFilePath)
        {
            return FileManagerBL.Exist(relativeFilePath);
        }

        public List<FileProperty> GetID3Tags(string relativeFilePath)
        {
            return FileManagerBL.GetID3Tags(relativeFilePath);
        }
    }
}