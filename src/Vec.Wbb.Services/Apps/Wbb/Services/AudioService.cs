﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.Messages;
using Vec.Apps.Services;
using Vec.Apps.Wbb.BL;
using Vec.Properties;

namespace Vec.Apps.Wbb.Services
{
    public class AudioService : IAudioService
    {
        public IAudioBL AudioBL { get; set; }

        public AudioService(IAudioBL audioBL)
        {
            AudioBL = audioBL;    
        }

        [WebInvoke(Method = "POST", UriTemplate = "GetAudio", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public GetAudioResponse GetAudioList(GetAudioRequest request)
        {
            GetAudioResponse response = new GetAudioResponse();

            response.File = AudioBL.GetAudioPropertyList(request.EntityType);

            return response;
        }
    }
}
