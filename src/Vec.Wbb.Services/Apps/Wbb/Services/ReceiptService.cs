﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.Messages;
using Vec.Apps.Services;
using Vec.Apps.vVote;
using Vec.Apps.Wbb.BL;

namespace Vec.Apps.Wbb.Services
{
    public class ReceiptService : IReceiptService
    {
        public IReceiptBL ReceiptBL { get; set; }

        public ReceiptService(IReceiptBL receiptBL)
        {
            ReceiptBL = receiptBL;
        }

        [WebInvoke(Method = "POST", UriTemplate = "GetReceipt", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public GetReceiptResponse GetReceipt(GetReceiptRequest request)
        {
            GetReceiptResponse response = new GetReceiptResponse();
            try
            {
                response.Receipt = ReceiptBL.GetReceipt(request.SerialNumber);
            }
            catch (VoteException ve)
            {
                response.ErrorCode = ve.ErrorCode;
                response.Error = ve.Message;
            }

            return response;
        }
    }
}
