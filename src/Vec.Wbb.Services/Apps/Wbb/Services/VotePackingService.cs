﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using Vec.Apps.Messages;
using Vec.Apps.Services;
using Vec.Apps.vVote;
using Vec.Apps.Wbb.BL;

namespace Vec.Apps.Wbb.Services
{
    public class VotePackingService : IVotePackingService
    {
        public IVotePackingBL VotePackingBL { get; set; }

        public VotePackingService(IVotePackingBL votePackingBL)
        {
            VotePackingBL = votePackingBL;
        }

        [WebInvoke(Method = "POST", UriTemplate = "GetCipherDevices", RequestFormat=WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public GetCipherDevicesResponse GetCipherDevices(GetCipherDevicesRequest request)
        {
            GetCipherDevicesResponse response = new GetCipherDevicesResponse();
            response.Devices = VotePackingBL.GetCipherDevices(HostingEnvironment.ApplicationPhysicalPath);

            return response;
        }

        [WebInvoke(Method = "POST", UriTemplate = "ExtractCiphers", ResponseFormat = WebMessageFormat.Json)]
        public void ExtractCiphers()
        {
            VotePackingBL.ExtractCiphers(HostingEnvironment.ApplicationPhysicalPath);
        }

        [WebInvoke(Method = "POST", UriTemplate = "GetCommitFiles", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public GetCommitFilesResponse GetCommitFiles(GetCommitFilesRequest request)
        {
            GetCommitFilesResponse response = new GetCommitFilesResponse();
            response.CommitFileZip = VotePackingBL.GetCommitFiles(HostingEnvironment.ApplicationPhysicalPath);

            return response;
        }
    }
}
