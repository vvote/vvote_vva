﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.Services;
using Vec.Apps.Wbb.BL;
using Vec.Apps.Wbb.Services;
using Vec.Framework.Core.BootStrappers;

namespace Vec.Apps.Wbb.BootStrappers
{
    public class BootStrapper : IBootStrapper
    {
        public void RegisterTypes(ContainerBuilder builder)
        {

            //BL
            builder.RegisterType<FileBL>().AsSelf().As<IFileBL>();
            builder.RegisterType<AudioBL>().AsSelf().As<IAudioBL>();
            builder.RegisterType<ReceiptBL>().AsSelf().As<IReceiptBL>();
            builder.RegisterType<VotePackingBL>().AsSelf().As<IVotePackingBL>();
            builder.RegisterType<MetaDataBL>().As<IMetaDataBL>().SingleInstance();

            //Internal Services
            builder.RegisterType<FileService>().AsSelf().As<IFileService>();
            builder.RegisterType<ReceiptService>().AsSelf().As<IReceiptService>();
            builder.RegisterType<AudioService>().AsSelf().As<IAudioService>();
            builder.RegisterType<VotePackingService>().AsSelf().As<IVotePackingService>();
        }

        public void Start()
        {
        }

        public void Stop()
        {
        }
    }
}
