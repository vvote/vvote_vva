﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using TagLib;

namespace Vec.Apps.vVote
{
    public sealed class ID3Tag
    {
        public static ID3TagEntity GetMP3Tags(string filePath)
        {
            ID3TagEntity result = new ID3TagEntity();

            using (File file = File.Create(filePath))
            {
                Tag tag = file.Tag as CombinedTag;
                if (tag == null)
                    tag = file.Tag;
                else
                    tag = (tag as CombinedTag).Tags.OfType<TagLib.Id3v2.Tag>().FirstOrDefault() ?? file.Tag;

                if (tag is TagLib.Id3v2.Tag)
                {
                    bool isDirty = false;
                    var x = (tag as TagLib.Id3v2.Tag).OfType<TagLib.Id3v2.UserTextInformationFrame>();
                    var albumArtists = x.SingleOrDefault(y => y.Description == "Band");
                    var groupDescription = x.SingleOrDefault(y => y.Description == "Content group description");

                    if (tag.AlbumArtists.Count() == 0 && albumArtists != null)
                    {
                        tag.AlbumArtists = albumArtists.Text;
                        isDirty = true;
                    }

                    if (tag.Grouping == null && groupDescription != null)
                    {
                        tag.Grouping = groupDescription.Text.First();
                        isDirty = true;
                    }

                    if (isDirty)
                        file.Save();
                }

                if (tag.AlbumArtists.Count() == 0)
                    throw new ArgumentException("The ID3 Album Artists tag is not defined.");
                else if (string.IsNullOrEmpty(tag.Title))
                    throw new ArgumentException("The ID3 Title tag is not defined.");


                result.Author = string.Join(";", tag.AlbumArtists);
                result.Title = tag.Title;
                result.Comment = tag.Comment;
                result.Genre = tag.Genres.Length == 0 ? string.Empty : tag.Genres[0];
                result.Year = tag.Year;
                result.Album = tag.Album;
                result.UserDefined = tag.Grouping;
            }

            return result;
        }

        public static ID3TagEntity GetMP3Tags(byte[] content)
        {
            ID3TagEntity result = new ID3TagEntity();

            using (File file = File.Create(new MemoryFileAbstraction(content)))
            {
                Tag tag = file.Tag as CombinedTag;
                if (tag == null)
                    tag = file.Tag;
                else
                    tag = (tag as CombinedTag).Tags.OfType<TagLib.Id3v2.Tag>().FirstOrDefault() ?? file.Tag;

                if (tag is TagLib.Id3v2.Tag)
                {
                    bool isDirty = false;
                    var x = (tag as TagLib.Id3v2.Tag).OfType<TagLib.Id3v2.UserTextInformationFrame>();
                    var albumArtists = x.SingleOrDefault(y => y.Description == "Band");
                    var groupDescription = x.SingleOrDefault(y => y.Description == "Content group description");

                    if (tag.AlbumArtists.Count() == 0 && albumArtists != null)
                    {
                        tag.AlbumArtists = albumArtists.Text;
                        isDirty = true;
                    }

                    if (tag.Grouping == null && groupDescription != null)
                    {
                        tag.Grouping = groupDescription.Text.First();
                        isDirty = true;
                    }

                    if (isDirty)
                        file.Save();
                }

                if (tag.AlbumArtists.Count() == 0)
                    throw new ArgumentException("The ID3 Album Artists tag is not defined.");
                else if (string.IsNullOrEmpty(tag.Title))
                    throw new ArgumentException("The ID3 Title tag is not defined.");

                result.Author = string.Join(";", tag.AlbumArtists);
                result.Title = tag.Title;
                result.Comment = tag.Comment;
                result.Genre = tag.Genres.Length == 0 ? string.Empty : tag.Genres[0];
                result.Year = tag.Year;
                result.Album = tag.Album;
                result.UserDefined = tag.Grouping;
            }

            return result;
        }

        public static void SetMP3Tags(ID3TagEntity tagEntity, string filePath)
        {
            using (File file = File.Create(filePath))
            {
                if (string.IsNullOrEmpty(tagEntity.Author))
                    throw new ArgumentException("The ID3 Author tag is not defined.");
                else if (string.IsNullOrEmpty(tagEntity.Title))
                    throw new ArgumentException("The ID3 Title tag is not defined.");

                file.Tag.AlbumArtists = tagEntity.Author.Split(new string[1] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                file.Tag.Title = tagEntity.Title;
                file.Tag.Comment = tagEntity.Comment;
                file.Tag.Genres = tagEntity.Genre.Split(new string[1] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                file.Tag.Year = tagEntity.Year;
                file.Tag.Album = tagEntity.Album;
                file.Tag.Grouping = tagEntity.UserDefined;
                file.Save();
            }
        }


        public static byte[] SetMP3Tags(ID3TagEntity tagEntity, byte[] content)
        {
            byte[] result = null;
            using (MemoryFileAbstraction mfa = new MemoryFileAbstraction(content))
            {
                using (File file = File.Create(mfa))
                {
                    if (string.IsNullOrEmpty(tagEntity.Author))
                        throw new ArgumentException("The ID3 Author tag is not defined.");
                    else if (string.IsNullOrEmpty(tagEntity.Title))
                        throw new ArgumentException("The ID3 Title tag is not defined.");

                    file.Tag.AlbumArtists = tagEntity.Author.Split(new string[1] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                    file.Tag.Title = tagEntity.Title;
                    file.Tag.Comment = tagEntity.Comment;
                    file.Tag.Genres = tagEntity.Genre.Split(new string[1] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                    file.Tag.Year = tagEntity.Year;
                    file.Tag.Album = tagEntity.Album;
                    file.Tag.Grouping = tagEntity.UserDefined;
                    file.Save();
                }
                result = mfa.ToArray();
            }
            return result;
        }
    }

    [DataContract]
    public sealed class ID3TagEntity
    {
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Author { get; set; }

        [DataMember]
        public string Album { get; set; }

        [DataMember]
        public uint Year { get; set; }

        [DataMember]
        public string Genre { get; set; }

        [DataMember]
        public string Comment { get; set; }

        [DataMember]
        public string UserDefined { get; set; }
    }

    internal sealed class MemoryFileAbstraction : TagLib.File.IFileAbstraction, IDisposable
    {
        private System.IO.MemoryStream _stream { get; set; }

        public MemoryFileAbstraction(byte[] content)
        {
            _stream = new System.IO.MemoryStream();
            _stream.Write(content, 0, content.Length);
            Name = System.IO.Path.GetTempFileName() + ".mp3";
        }

        public void CloseStream(System.IO.Stream stream)
        {
            stream.Flush();
            stream.Position = 0;
        }

        public string Name
        {
            get;
            private set;
        }

        public System.IO.Stream ReadStream
        {
            get { return _stream; }
        }

        public System.IO.Stream WriteStream
        {
            get { return _stream; }
        }

        public void Dispose()
        {
            _stream.Dispose();
        }

        public byte[] ToArray()
        {
            return _stream.ToArray();
        }
    }

}
