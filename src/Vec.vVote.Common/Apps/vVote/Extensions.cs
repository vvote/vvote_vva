﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Vec.Apps.vVote.Common
{
    public static class Extensions
    {
        public static string GetValidFileName(this string fileName)
        {
            string regexSearch = new string(Path.GetInvalidFileNameChars());
            Regex regx = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            return regx.Replace(fileName, "");
        }

        public static string RegexReplace(this string input, string pattern, string replacement = "", RegexOptions options = RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase)
        {
            return Regex.Replace(input, pattern, replacement, options);
        }

        public static byte[] GetBytes(this string data)
        {
            return Encoding.UTF8.GetBytes(data);
        }

        public static string GetString(this byte[] encodedData)
        {
            return Encoding.UTF8.GetString(encodedData);
        }

        public static byte[] GetBytes(this Stream stream)
        {
            byte[] bytes = null;
            using (MemoryStream ms = new MemoryStream())
            {
                stream.CopyTo(ms);
                bytes = ms.ToArray();
            }
            return bytes;
        }

        public static DateTime GetDateTime(this double unixTime)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(unixTime);
        }
        
        public static DateTime GetDateTime(this string unixTimeStr)
        {
            return GetDateTime(Convert.ToDouble(unixTimeStr));
        }
    }
}
