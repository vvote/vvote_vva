﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vec.Properties;

namespace Vec.Apps.vVote.Common
{
    public sealed class Zipper
    {
        public static void Archive(string directoryPath, string directoryPathInArchive)
        {
            string fileName = directoryPath + ".zip";
            using (ZipFile zip = new ZipFile(fileName))
            {
                if (string.IsNullOrEmpty(directoryPathInArchive))
                    zip.AddDirectory(directoryPath);
                else
                    zip.AddDirectory(directoryPath, directoryPathInArchive);
                zip.Save();
            }
            Directory.Delete(directoryPath, true);
        }

        public static void Extract(Stream zipFile, string extractPath)
        {
            using (ZipFile zf = ZipFile.Read(zipFile))
            {
                zf.ExtractAll(extractPath);
            }
        }

        public static Dictionary<string, byte[]> Extract(Stream zipFile, List<string> files)
        {
            var result = new Dictionary<string, byte[]>();
            using (ZipFile zf = ZipFile.Read(zipFile))
            {
                foreach (var file in files)
                {
                    var entry = zf.Entries.SingleOrDefault(ze => ze.FileName == file);
                    if (entry == null)
                        result.Add(file, null);
                    else
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            entry.Extract(ms);
                            result.Add(file, ms.ToArray());
                        }
                    }
                }
            }
            return result;
        }
    }
}
