﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.vVote.Converter
{
    public interface IAudioConverter
    {
        ConversionResult Convert(string inFile, string outFile);
    }

    public class MP3toOggConverter : IAudioConverter
    {
        public const string FFMPEG = "ffmpeg.exe";
        public const string FFMPEG_ARGS_FORMAT = "-i \"{0}\" -acodec libvorbis -y \"{1}\"";

        public ConversionResult Convert(string input, string output)
        {
            ConversionResult result = new ConversionResult() { InFile = input, OutFile = output };

            try
            {
                ProcessStartInfo psi = new ProcessStartInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FFMPEG), string.Format(FFMPEG_ARGS_FORMAT, input, output))
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    RedirectStandardError = true,
                    UseShellExecute = false
                };

                using (Process p = Process.Start(psi))
                {
                    result.ExecutionLog = p.StandardError.ReadToEnd();

                    result.IsSuccess = p.ExitCode == 0;
                }
            }
            catch (Exception ex)
            {
                result.ExecutionLog = ex.Message;
                result.IsSuccess = false;
            }

            return result;
        }
    }

    public struct ConversionResult
    {
        public bool IsSuccess { get; set; }
        public string InFile { get; set; }
        public string OutFile { get; set; }
        public string ExecutionLog { get; set; }
    }
}
