﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data;

namespace Vec.Apps.vVote.Reader
{
    public sealed class SpreadsheetReader : IDisposable
    {
        WorkbookPart _workBookPart { get; set; }
        SpreadsheetDocument _document { get; set; }
        SharedStringTablePart _stringTable { get; set; }

        public SpreadsheetReader(string filePath)
        {
            _document = SpreadsheetDocument.Open(filePath, false);
            _workBookPart = _document.WorkbookPart;
            _stringTable = _workBookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
        }

        string GetValue(Cell theCell)
        {
            string result;

            if (theCell.DataType == null)
                result = theCell.InnerText;
            else
                switch (theCell.DataType.Value)
                {
                    case CellValues.Boolean:
                        switch (theCell.InnerText)
                        {
                            case "0":
                                result = "false";
                                break;
                            default:
                                result = "true";
                                break;
                        }
                        break;
                    case CellValues.SharedString:
                        result = _stringTable.SharedStringTable.ElementAt(int.Parse(theCell.InnerText)).InnerText.Trim();
                        break;
                    default:
                        result = theCell.InnerText.Trim();
                        break;
                }

            return result;
        }

        public DataSet GetDataSet(params string[] sheetNames)
        {
            DataSet dSet = new DataSet();

            foreach (var sheetItem in sheetNames)
            {
                DataTable dTable = new DataTable(sheetItem);
                Sheet sheet = _workBookPart.Workbook.Descendants<Sheet>().Where(s => s.Name == sheetItem).FirstOrDefault();
                WorksheetPart workSheetPart = _workBookPart.GetPartById(sheet.Id) as WorksheetPart;

                var stringTable = _workBookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
                foreach (var rowItem in workSheetPart.Worksheet.Descendants<Row>())
                {
                    if (rowItem.RowIndex == 1)
                    {
                        foreach (var columnItem in rowItem.ChildElements.OfType<Cell>())
                        {
                            dTable.Columns.Add(new DataColumn() { ColumnName = GetValue(columnItem), Caption = columnItem.CellReference });
                        }
                    }
                    else
                    {
                        List<object> colValues = new List<object>(dTable.Columns.Count);
                        foreach (DataColumn columnItem in dTable.Columns)
                        {
                            string colindex = columnItem.Caption.Substring(0, columnItem.Caption.Length - 1);

                            var cell = rowItem.ChildElements.OfType<Cell>().SingleOrDefault(c => c.CellReference == string.Format("{0}{1}", colindex, rowItem.RowIndex));

                            colValues.Add(cell == null ? string.Empty : GetValue(cell));

                        }
                        dTable.Rows.Add(colValues.ToArray());
                    }
                }
                dSet.Tables.Add(dTable);
            }

            return dSet;
        }

        public void Dispose()
        {
            if (_document != null)
                _document.Close();
        }
    }
}
