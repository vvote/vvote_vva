﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.vVote.Reader
{
    public class DelimitedFileReader : IDisposable
    {
        //public DataTable Data { get; set; }

        //public void ImportCsvFile(string filename)
        //{
        //    FileInfo file = new FileInfo(filename);
        //    string connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties='text;HDR=No;FMT=Delimited(,)';Data Source='" + file.DirectoryName + "';";
        //    using (OleDbConnection con = new OleDbConnection(connectionString))
        //    {
        //        using (OleDbCommand cmd = new OleDbCommand(string.Format("SELECT * FROM [{0}]", file.Name), con))
        //        {
        //            con.Open();

        //            // Using a DataTable to process the data
        //            using (OleDbDataAdapter adp = new OleDbDataAdapter(cmd))
        //            {
        //                Data = new DataTable(file.Name);
        //                adp.Fill(Data);
        //            }
        //        }
        //    }
        //}

        public List<string[]> ImportCsvFile(string fileName)
        {
            List<string[]> result = new List<string[]>();

            FileInfo fileInfo = new FileInfo(fileName);
            using (StreamReader file = new StreamReader(fileInfo.FullName))
            {
                string line;

                while ((line = file.ReadLine()) != null)
                {
                    if (line.Trim().Length > 0)
                    {
                        string[] array = line.Split(new string[] { "," }, StringSplitOptions.None);
                        result.Add(array);
                    }
                }
            }
            return result;
        }

        public void Dispose()
        {
            //if (Data != null)
            //    Data.Dispose();
        }
    }
}
