﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Vec.Properties;

namespace Vec.Apps.vVote.Common
{
    public sealed class Synthesizer
    {
        public static byte[] SynthesizeText(string text)
        {
            byte[] file = null;
            string remoteUri = string.Format(Settings.Default.GoogleAudioUrl, text.ToLowerInvariant());

            using (WebClient myWebClient = new WebClient())
            {
                file = myWebClient.DownloadData(remoteUri);
            }

            return file;
        }
    }
}
