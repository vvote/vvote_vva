﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.vVote.Common.Signature
{
    public interface ISignatureFactory
    {
        void GenerateKeyPair();

        byte[] PrivateKey { get; set; }

        byte[] PublicKey { get; set; }

        byte[] Sign(byte[] data);

        bool Verify(byte[] data, byte[] signature);
    }

}
