﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.vVote.Common.Signature
{
    public class DSAFactory : ISignatureFactory
    {
        public void GenerateKeyPair()
        {
            using (DSACryptoServiceProvider DSA = new DSACryptoServiceProvider())
            {
                string publicKey = DSA.ToXmlString(false);
                string privateKey = DSA.ToXmlString(true);

                PublicKey = publicKey.GetBytes();
                PrivateKey = privateKey.GetBytes();
            }
        }

        public byte[] PrivateKey { get; set; }

        public byte[] PublicKey { get; set; }

        public byte[] Sign(byte[] data)
        {
            using (DSACryptoServiceProvider DSA = new DSACryptoServiceProvider())
            {
                DSA.FromXmlString(PrivateKey.GetString());

                return DSA.SignData(data);
            }
        }

        public bool Verify(byte[] data, byte[] signature)
        {
            using (DSACryptoServiceProvider DSA = new DSACryptoServiceProvider())
            {
                DSA.FromXmlString(PublicKey.GetString());

                return DSA.VerifyData(data, signature);
            }
        }
    }
}
