﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using Org.BouncyCastle.Bcpg;
using Org.BouncyCastle.Bcpg.OpenPgp;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;
using System;
using System.IO;
using Vec.Properties;

namespace Vec.Apps.vVote.Common.Signature
{
    //TODO: convert this to an interface class for unit tests...
    public sealed class PGP
    {
        public static void CreateKeys(string secreteKeyPath, string publicKeyPath, char[] passphrase)
        {
            IAsymmetricCipherKeyPairGenerator dsaKpg = GeneratorUtilities.GetKeyPairGenerator("DSA");
            DsaParametersGenerator pGen = new DsaParametersGenerator();
            pGen.Init(1024, 80, new SecureRandom());
            DsaParameters dsaParams = pGen.GenerateParameters();
            DsaKeyGenerationParameters kgp = new DsaKeyGenerationParameters(new SecureRandom(), dsaParams);
            dsaKpg.Init(kgp);

            //
            // this takes a while as the key generator has to Generate some DSA parameters
            // before it Generates the key.
            //
            AsymmetricCipherKeyPair dsaKp = dsaKpg.GenerateKeyPair();

            using (Stream secreteOut = File.Create(secreteKeyPath), publicOut = File.Create(publicKeyPath))
            {
                Stream sOut = new ArmoredOutputStream(secreteOut);
                Stream pOut = new ArmoredOutputStream(publicOut);

                PgpKeyPair dsaKeyPair = new PgpKeyPair(PublicKeyAlgorithmTag.Dsa, dsaKp, DateTime.UtcNow);
                PgpKeyRingGenerator keyRingGen = new PgpKeyRingGenerator(PgpSignature.PositiveCertification, dsaKeyPair, Settings.Default.KeyIdentifier, SymmetricKeyAlgorithmTag.Aes256, passphrase, true, null, null, new SecureRandom());

                keyRingGen.GenerateSecretKeyRing().Encode(sOut);
                keyRingGen.GeneratePublicKeyRing().Encode(pOut);

                sOut.Close();
                pOut.Close();
            }
        }

        public static void SignFile(string filePath, string privateKeyFilePath, char[] passphrase)
        {
            Func<Stream, PgpSecretKey> readSecretKey = delegate(Stream input)
            {
                PgpSecretKeyRingBundle pgpSec = new PgpSecretKeyRingBundle(
                   PgpUtilities.GetDecoderStream(input));

                //
                // we just loop through the collection till we find a key suitable for encryption, in the real
                // world you would probably want to be a bit smarter about this.
                //
                foreach (PgpSecretKeyRing keyRing in pgpSec.GetKeyRings())
                {
                    foreach (PgpSecretKey key in keyRing.GetSecretKeys())
                    {
                        if (key.IsSigningKey)
                        {
                            return key;
                        }
                    }
                }

                throw new ArgumentException("Can't find signing key in key ring.");
            };

            using (Stream oStream = File.Create(filePath + ".bpg"))
            {
                Stream outputStream = new ArmoredOutputStream(oStream);
                Stream keyIn = File.OpenRead(privateKeyFilePath);
                PgpSecretKey pgpSec = readSecretKey(keyIn);
                PgpPrivateKey pgpPrivKey = pgpSec.ExtractPrivateKey(passphrase);
                PgpSignatureGenerator sGen = new PgpSignatureGenerator(pgpSec.PublicKey.Algorithm, HashAlgorithmTag.Sha1);

                sGen.InitSign(PgpSignature.BinaryDocument, pgpPrivKey);

                BcpgOutputStream bOut = new BcpgOutputStream(outputStream);

                byte[] fIn = File.ReadAllBytes(filePath);
                sGen.Update(fIn);

                sGen.Generate().Encode(bOut);

                outputStream.Close();
            }
        }

        public static bool VerifyFile(string filePath, string pubicKeyFilePath)
        {
            bool result = false;
            using (Stream signInputStream = File.OpenRead(filePath + ".bpg"))
            {
                Stream inputStream = PgpUtilities.GetDecoderStream(signInputStream);

                PgpObjectFactory pgpFact = new PgpObjectFactory(inputStream);
                PgpSignatureList p3 = null;
                PgpObject o = pgpFact.NextPgpObject();
                if (o is PgpCompressedData)
                {
                    PgpCompressedData c1 = (PgpCompressedData)o;
                    pgpFact = new PgpObjectFactory(c1.GetDataStream());

                    p3 = (PgpSignatureList)pgpFact.NextPgpObject();
                }
                else
                {
                    p3 = (PgpSignatureList)o;
                }

                using (FileStream pubFs = File.OpenRead(pubicKeyFilePath))
                {
                    PgpPublicKeyRingBundle pgpPubRingCollection = new PgpPublicKeyRingBundle(PgpUtilities.GetDecoderStream(pubFs));

                    PgpSignature sig = p3[0];
                    PgpPublicKey key = pgpPubRingCollection.GetPublicKey(sig.KeyId);
                    sig.InitVerify(key);

                    byte[] dIn = File.ReadAllBytes(filePath);
                    sig.Update(dIn);

                    result = sig.Verify();
                }
            }
            return result;
        }
    }
}
