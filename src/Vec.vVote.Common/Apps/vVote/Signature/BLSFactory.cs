﻿using Newtonsoft.Json.Linq;
using org.json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using uk.ac.surrey.cs.tvs.utils.crypto;
using uk.ac.surrey.cs.tvs.utils.crypto.signing;
using uk.ac.surrey.cs.tvs.utils.crypto.signing.bls;
using uk.ac.surrey.cs.tvs.utils.io;
using Vec.Apps.vVote.Common;

namespace Vec.Apps.vVote.Common.Signature
{
    public class BLSFactory : ISignatureFactory
    {
        public void GenerateKeyPair()
        {
            var keyPair = BLSKeyPair.generateKeyPair();
            var privateKeyJson = keyPair.getPrivateKey().toJSON();
            var publicKeyJson = keyPair.getPublicKey().toJSON();
            var privateKeyString = privateKeyJson.toString();
            var publicKeyString = publicKeyJson.toString();

            PrivateKey = privateKeyString.GetBytes();
            PublicKey = publicKeyString.GetBytes();
        }

        public byte[] PrivateKey { get; set; }

        public byte[] PublicKey { get; set; }

        public byte[] Sign(byte[] data)
        {
            TVSSignature sig = new TVSSignature(SignatureType.BLS, BLSPrivateKey);
            sig.update(data.GetString());

            var sigString = sig.signAndEncode(EncodingType.BASE64);
            return sigString.GetBytes();
        }

        public bool Verify(byte[] data, byte[] signature)
        {
            TVSSignature sigVerify = new TVSSignature(BLSPublicKey, false);
            sigVerify.update(data.GetString());

            return sigVerify.verify(IOUtils.decodeData(EncodingType.BASE64, signature.GetString()));
        }

        protected BLSPrivateKey BLSPrivateKey
        {
            get
            {
                var privateKeyString = PrivateKey.GetString();
                var privateKeyJson = new JSONObject(privateKeyString);

                return new BLSPrivateKey(privateKeyJson);
            }
        }
        protected BLSPublicKey BLSPublicKey
        {
            get
            {
                var publicKeyString = PublicKey.GetString();
                var publicKeyJson = new JSONObject(publicKeyString);

                return new BLSPublicKey(publicKeyJson);
            }
        }
    }
}
