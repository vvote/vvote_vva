﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.vVote.Common.Signature
{
    public interface ISignature
    {
        byte[] PrivateKey { get; set; }
        byte[] PublicKey { get; set; }

        byte[] Sign(byte[] data);

        bool Verify(byte[] data, byte[] signature);
    }

    public class Signature<T> : ISignature
        where T : ISignatureFactory, new()
    {
        protected internal T SignatureFactory { get; set; }

        public Signature()
        {
            SignatureFactory = new T();
        }

        public byte[] PrivateKey
        {
            get { return SignatureFactory.PrivateKey; }
            set { SignatureFactory.PrivateKey = value; }
        }

        public byte[] PublicKey
        {
            get { return SignatureFactory.PublicKey; }
            set { SignatureFactory.PublicKey = value; }
        }

        public byte[] Sign(byte[] data)
        {
            return SignatureFactory.Sign(data);
        }

        public bool Verify(byte[] data, byte[] signature)
        {
            return SignatureFactory.Verify(data, signature);
        }
    }
}
