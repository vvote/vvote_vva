﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.vVote.Writer
{
    public sealed class SpreadsheetWriter : IDisposable
    {
        SpreadsheetDocument _document { get; set; }
        WorkbookPart _workbookPart { get; set; }
        SharedStringTablePart _stringTablePart { get; set; }
        WorkbookStylesPart _stylePart { get; set; }
        Sheets _sheets { get; set; }

        public SpreadsheetWriter(string filePath)
        {
            _document = SpreadsheetDocument.Create(filePath, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook);
            _workbookPart = _document.AddWorkbookPart();
            _document.WorkbookPart.Workbook = new Workbook();

            _stringTablePart = _document.WorkbookPart.AddNewPart<SharedStringTablePart>();

            _sheets = _document.WorkbookPart.Workbook.AppendChild<Sheets>(new Sheets());

            //_stylePart = _document.WorkbookPart.AddNewPart<WorkbookStylesPart>();
            //_stylePart.Stylesheet = new Stylesheet();

            //// blank font list
            //Font font = new Font();
            //font.Append(new Bold());
            //_stylePart.Stylesheet.Fonts = new Fonts();
            //_stylePart.Stylesheet.Fonts.Count = 1;
            //_stylePart.Stylesheet.Fonts.AppendChild(font);

            //// create fills
            //_stylePart.Stylesheet.Fills = new Fills();

            //// create a solid red fill
            //var solidRed = new PatternFill() { PatternType = PatternValues.Solid };
            //solidRed.ForegroundColor = new ForegroundColor { Rgb = HexBinaryValue.FromString("FFFFFF") };
            //solidRed.BackgroundColor = new BackgroundColor { Rgb = HexBinaryValue.FromString("C0C0C0") };

            //_stylePart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = new PatternFill { PatternType = PatternValues.None } }); // required, reserved by Excel
            //_stylePart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = new PatternFill { PatternType = PatternValues.Gray125 } }); // required, reserved by Excel
            //_stylePart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = solidRed });
            //_stylePart.Stylesheet.Fills.Count = 3;

            //// blank border list
            //_stylePart.Stylesheet.Borders = new Borders();
            //_stylePart.Stylesheet.Borders.Count = 1;
            //_stylePart.Stylesheet.Borders.AppendChild(new Border());

            // blank cell format list
            //_stylePart.Stylesheet.CellStyleFormats = new CellStyleFormats();
            //_stylePart.Stylesheet.CellStyleFormats.Count = 1;
            //_stylePart.Stylesheet.CellStyleFormats.AppendChild(new CellFormat());

            //// cell format list
            //_stylePart.Stylesheet.CellFormats = new CellFormats();
            //// empty one for index 0, seems to be required
            //_stylePart.Stylesheet.CellFormats.AppendChild(new CellFormat());
            //// cell format references style format 0, font 0, border 0, fill 2 and applies the fill
            //_stylePart.Stylesheet.CellFormats.AppendChild(new CellFormat { FormatId = 0, FontId = 0, BorderId = 0, FillId = 2, ApplyFill = true }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Center });
            //_stylePart.Stylesheet.CellFormats.Count = 2;

            //_stylePart.Stylesheet.Save();
        }

        public void SetDataSet(DataSet dataSet)
        {

            for (int i = 0; i < dataSet.Tables.Count; i++)
            {
                WorksheetPart worksheetPart = _document.WorkbookPart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet(new SheetData());
                Sheet sheet = new Sheet()
                {
                    Id = _document.WorkbookPart.GetIdOfPart(worksheetPart),
                    SheetId = (uint)i + 1,
                    Name = dataSet.Tables[i].TableName
                };
                _sheets.Append(sheet);

                char column = 'A';
                foreach (DataColumn dataColumnItem in dataSet.Tables[i].Columns)
                {
                    int index = InsertSharedStringItem(dataColumnItem.ColumnName);
                    Cell cell = GetCell(column.ToString(), 1, worksheetPart.Worksheet);

                    // Set the value of cell A1.
                    cell.CellValue = new CellValue(index.ToString());
                    cell.DataType = new EnumValue<CellValues>(CellValues.SharedString);
                    column++;
                }

                for (int j = 0, k = 2; j < dataSet.Tables[i].Rows.Count; j++, k++)
                {
                    column = 'A';
                    foreach (DataColumn colItem in dataSet.Tables[i].Columns)
                    {
                        Cell cell = null;
                        if (colItem.DataType == typeof(int))
                        {
                            cell = GetCell(column.ToString(), (uint)k, worksheetPart.Worksheet);
                            cell.CellValue = new CellValue(dataSet.Tables[i].Rows[j][colItem].ToString());
                            cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                        }
                        else
                        {
                            int index = InsertSharedStringItem(dataSet.Tables[i].Rows[j][colItem].ToString());
                            cell = GetCell(column.ToString(), (uint)k, worksheetPart.Worksheet);
                            cell.CellValue = new CellValue(index.ToString());
                            cell.DataType = new EnumValue<CellValues>(CellValues.SharedString);
                        }

                        column++;
                    }
                }
            }
            _workbookPart.Workbook.Save();
        }

        public void Dispose()
        {
            if (_document != null)
                _document.Close();
        }

        private int InsertSharedStringItem(string text)
        {
            // If the part does not contain a SharedStringTable, create one.
            if (_stringTablePart.SharedStringTable == null)
            {
                _stringTablePart.SharedStringTable = new SharedStringTable();
            }

            int i = 0;

            // Iterate through all the items in the SharedStringTable. If the text already exists, return its index.
            foreach (SharedStringItem item in _stringTablePart.SharedStringTable.Elements<SharedStringItem>())
            {
                if (item.InnerText == text)
                {
                    return i;
                }

                i++;
            }

            // The text does not exist in the part. Create the SharedStringItem and return its index.
            _stringTablePart.SharedStringTable.AppendChild(new SharedStringItem(new DocumentFormat.OpenXml.Spreadsheet.Text(text)));
            _stringTablePart.SharedStringTable.Save();

            return i;
        }

        private Cell GetCell(string columnName, uint rowIndex, Worksheet worksheet)
        {
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Where(c => c.CellReference.Value == columnName + rowIndex).Count() > 0)
            {
                return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
            }
            else
            {
                // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
                Cell refCell = null;
                foreach (Cell cell in row.Elements<Cell>())
                {
                    if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
                    {
                        refCell = cell;
                        break;
                    }
                }

                Cell newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                worksheet.Save();
                return newCell;
            }
        }
    }
}
