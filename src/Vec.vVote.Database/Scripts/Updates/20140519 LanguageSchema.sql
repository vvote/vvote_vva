﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

GO
PRINT N'Altering [dbo].[Language]...';


IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Language]') AND type in (N'U'))
BEGIN
	
	IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE NAME = N'SortOrder' AND OBJECT_ID = OBJECT_ID(N'Language'))
	BEGIN
		ALTER TABLE [dbo].[Language] ADD [SortOrder] INT NOT NULL DEFAULT 0;
	END 
	
	IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE NAME = N'LongName' AND OBJECT_ID = OBJECT_ID(N'Language'))
	BEGIN
		ALTER TABLE [dbo].[Language] ADD [LongName] NVARCHAR(100) NOT NULL DEFAULT '';
	END 
	
END
GO

GO
PRINT N'Update complete.';

GO
