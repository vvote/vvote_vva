﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

PRINT N'Create [dbo].[CommitFile]...';
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CommitFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CommitFile] (
	[Id]		INT				IDENTITY (1, 1) NOT NULL,
	[EventId]	UNIQUEIDENTIFIER	NOT NULL,
	[Name]		NVARCHAR(500)		NOT NULL,
	[CommitDateTime] DATETIME2		NOT NULL,
	[ProcessedDateTime]	DATETIME2	NOT NULL,
	CONSTRAINT [PK_CommitFile] PRIMARY KEY ([Id] ASC),
    CONSTRAINT [FK_CommitFile_Event] FOREIGN KEY (EventId) REFERENCES [Event]([Id]) ON DELETE CASCADE 
)
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProcessedCommit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProcessedCommit] (
	[Id]		BIGINT				IDENTITY (1, 1) NOT NULL,
	[EventId]	UNIQUEIDENTIFIER	NOT NULL,
	[DeviceId] [nvarchar](50) NOT NULL,
	[SerialNo] [nvarchar](100) NOT NULL,
	[Type] [smallint] NOT NULL,
	[District] [nvarchar](100) NOT NULL,
	[VoteType] [smallint] NULL,
	[CommitDateTime] DATETIME2		NOT NULL,
	CONSTRAINT [PK_ProcessedCommit] PRIMARY KEY ([Id] ASC),
    CONSTRAINT [FK_ProcessedCommit_Event] FOREIGN KEY (EventId) REFERENCES [Event]([Id]) ON DELETE CASCADE 
)
END
GO

PRINT N'Update complete.';
GO

