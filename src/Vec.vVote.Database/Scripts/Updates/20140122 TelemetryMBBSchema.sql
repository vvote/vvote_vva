﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

GO
PRINT N'Create [dbo].[TelemetryMBB]...';

GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TelemetryMBB]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TelemetryMBB] (
	[Id]			BIGINT				IDENTITY (1, 1) NOT NULL,
	[EventId]		UNIQUEIDENTIFIER	NOT NULL,
	[DateTime]		DATETIME2			NOT NULL DEFAULT GETUTCDATE(),
	[Level]			NVARCHAR(50)		NOT NULL,
	[From]			NVARCHAR(100)		NOT NULL,
	[MessageType]	NVARCHAR (50)		NOT NULL,	
	[Message]		NVARCHAR (2000)		NOT NULL,		
	CONSTRAINT [PK_TelemetryMBB] PRIMARY KEY ([Id] ASC),
    CONSTRAINT [FK_TelemetryMBB_Event] FOREIGN KEY (EventId) REFERENCES [Event]([Id]) ON DELETE CASCADE 
)
END	

GO
PRINT N'Update complete.';

GO
