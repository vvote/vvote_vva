﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 1)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (1,'English', 'eng')

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 2)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (2,'Amharic', 'amh')

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 3)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (3,'Arabic', 'ara')

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 4)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (4,'Bosnian', 'bos')

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 5)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (5,'Cambodian', 'khm')

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 6)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (6,'Cantonese', 'can')

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 7)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (7,'Croatian', 'hrv')

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 8)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (8,'Dari', 'fas')

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 9)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (9,'Greek', 'grk')
	
IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 10)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (10,'Italian', 'ita')

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 11)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (11,'Korean', 'kor')

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 12)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (12,'Macedonian', 'mac')

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 13)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (13,'Mandarin', 'cmn')

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 14)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (14,'Persian', 'per')

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 15)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (15,'Russian', 'rus')

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 16)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (16,'Serbian', 'srp')

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 17)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (17,'Somali', 'som')

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 18)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (18,'Spanish', 'spa')

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 19)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (19,'Turkish', 'tur')

IF NOT EXISTS (SELECT Id FROM [Language] WHERE Id = 20)
	INSERT INTO [Language] (Id, Name, ShortName) VALUES (20,'Vietnamese', 'vie')
GO
