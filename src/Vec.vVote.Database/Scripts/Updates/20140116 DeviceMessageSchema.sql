﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

GO
PRINT N'Creating [dbo].[DeviceMessage]...';

GO

IF NOT EXISTS(SELECT * FROM SYS.TABLES WHERE NAME = N'DeviceMessage')
BEGIN
	CREATE TABLE [dbo].[DeviceMessage](
		[Id] [bigint] IDENTITY(1,1) NOT NULL,
		[EventDeviceId] [int] NOT NULL,
		[Type] [smallint] NOT NULL,
		[SerialNo] [nvarchar](50) NOT NULL,
		[SerialSig] [nvarchar](500) NOT NULL,
		[DeviceId] [int] NOT NULL,
		[DeviceSig] [nvarchar](500) NOT NULL,
		[District] [nvarchar](100) NOT NULL,
		[Response] [smallint] NOT NULL,
		[AuthId] [nvarchar](100) NULL,
		[AuthSig] [nvarchar](500) NULL,
		[DateTime] DATETIME2 NOT NULL, 
	 CONSTRAINT [PK_DeviceMessage] PRIMARY KEY CLUSTERED ([Id] ASC)
	)

	ALTER TABLE [dbo].[DeviceMessage]  WITH CHECK ADD  CONSTRAINT [FK_DeviceMessage_EventDevice] FOREIGN KEY([EventDeviceId])
	REFERENCES [dbo].[EventDevice] ([Id])
	ON DELETE CASCADE

	ALTER TABLE [dbo].[DeviceMessage] CHECK CONSTRAINT [FK_DeviceMessage_EventDevice]
END

GO

PRINT N'Update complete.';

GO