﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

GO
PRINT N'Create [dbo].[TelemetryMBBMessage]...';

GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TelemetryMBBMessage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TelemetryMBBMessage](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[EventId] [uniqueidentifier] NOT NULL,
	[SerialNo] [nvarchar](100) NOT NULL,
	[VPSId] [nvarchar](50) NOT NULL,
	[EVMId] [nvarchar](50) NULL,
	[DateTime] [datetime2](7) NOT NULL,
	[CommitTime] [datetime2](7) NOT NULL,
	[VoteDistrict] [nvarchar](100) NULL,
	[IsBlankLA] bit null,
	[IsBlankATL] bit null,
	[IsBlankBTL] bit null,
	[Type] [smallint] NOT NULL,
	CONSTRAINT [PK_TelemetryMBBMessage] PRIMARY KEY ([Id] ASC),
    CONSTRAINT [FK_TelemetryMBBMessage_Event] FOREIGN KEY (EventId) REFERENCES [Event]([Id]) ON DELETE CASCADE 
)
END	

GO
PRINT N'Update complete.';

GO