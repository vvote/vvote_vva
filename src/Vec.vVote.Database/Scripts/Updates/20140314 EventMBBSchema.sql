﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

GO
PRINT N'Creating [dbo].[EventMBB]...';

GO

IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE NAME = N'Name' AND OBJECT_ID = OBJECT_ID(N'EventMBB'))
BEGIN
	ALTER TABLE [dbo].[EventMBB] ADD [Name] NVARCHAR(50); 
END
IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE NAME = N'Port' AND OBJECT_ID = OBJECT_ID(N'EventMBB'))
BEGIN
	ALTER TABLE [dbo].[EventMBB] ADD [Port] INT NOT NULL DEFAULT 0;
END
IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE NAME = N'PublicKeyEntry' AND OBJECT_ID = OBJECT_ID(N'EventMBB'))
BEGIN
	ALTER TABLE [dbo].[EventMBB] ADD [PublicKeyEntry] NVARCHAR(1000);
END
GO

PRINT N'Update complete.';

GO