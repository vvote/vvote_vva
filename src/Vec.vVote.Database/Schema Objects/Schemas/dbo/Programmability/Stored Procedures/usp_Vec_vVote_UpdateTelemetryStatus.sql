﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

/****** Object:  StoredProcedure [dbo].[usp_Vec_vVote_UpdateTelemetryStatus]    Script Date: 08/27/2014 13:04:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Vec_vVote_UpdateTelemetryStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Vec_vVote_UpdateTelemetryStatus]
GO

-- =============================================
-- Author:		Jayadev D
-- Create date: 21-08-2014
-- Description:	Stored procedure that updates the telemetry
-- status frequently, this status is needed by the reporting system
-- to identify the accuracy of the report
-- =============================================
CREATE PROCEDURE [dbo].[usp_Vec_vVote_UpdateTelemetryStatus] 
	-- Add the parameters for the stored procedure here
	@Level NVARCHAR(10),
	@Logger NVARCHAR(400),
	@Message NVARCHAR(4000),
	@EventId UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS(SELECT Id FROM Telemetry WHERE EventId = @EventId)
	BEGIN
		UPDATE Telemetry 
		SET [Level] = @Level,
			[Logger] = @Logger,
			[Message] = @Message,
			[DateTime] = GETUTCDATE()
		WHERE EventId = @EventId 
	END
	ELSE
	BEGIN
		INSERT Telemetry([EventId], [Level], [Logger], [Message], [DateTime])
		VALUES(@EventId, @Level, @Logger, @Message, GETUTCDATE())
	END

END
GO

--Grant permissions------------------------------------------------------------------
GRANT EXEC ON [dbo].[usp_Vec_vVote_UpdateTelemetryStatus] TO PUBLIC
GO
