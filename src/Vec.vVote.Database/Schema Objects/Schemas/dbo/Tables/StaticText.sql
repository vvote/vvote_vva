﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

CREATE TABLE [dbo].[StaticText]
(
	[Id] INT NOT NULL IDENTITY, 
    [EventStaticTextId] INT NOT NULL, 
    [Name] NVARCHAR(2000) NULL,
	[LanguageId] int NOT NULL, 
	[Error] NVARCHAR(500) NULL,
    [DateTimeStamp] DATETIME2 NULL, 
	CONSTRAINT [PK_StaticText] PRIMARY KEY ([Id] ASC),
    CONSTRAINT [FK_StaticText_EventStaticText] FOREIGN KEY ([EventStaticTextId]) REFERENCES [EventStaticText](Id) ON DELETE CASCADE,
    CONSTRAINT [FK_StaticText_Language] FOREIGN KEY (LanguageId) REFERENCES [Language]([Id])
)
