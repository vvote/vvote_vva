﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

CREATE TABLE [dbo].[EventText]
(
	[Id] INT NOT NULL IDENTITY,
	[EventId] UNIQUEIDENTIFIER NOT NULL, 
    [Name] NVARCHAR(500) NOT NULL,
	[LanguageId] int NOT NULL, 
	[Error] NVARCHAR(500) NULL,
    [DateTimeStamp] DATETIME2 NULL, 
	CONSTRAINT [PK_EventText] PRIMARY KEY ([Id] ASC),
    CONSTRAINT [FK_EventText_Event] FOREIGN KEY ([EventId]) REFERENCES [Event]([Id]) ON DELETE CASCADE, 
    CONSTRAINT [FK_EventText_Language] FOREIGN KEY (LanguageId) REFERENCES [Language]([Id])
)
