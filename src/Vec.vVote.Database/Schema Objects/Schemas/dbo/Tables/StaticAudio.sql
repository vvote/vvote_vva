﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

CREATE TABLE [dbo].[StaticAudio]
(
	[Id] INT NOT NULL IDENTITY, 
	[EventStaticAudioId] INT not null,
	[FileName] NVARCHAR (200) NULL,
    [Title]   NVARCHAR(2000)            NULL,
    [Author]   NVARCHAR(200)            NULL,
    [Comment] NVARCHAR(MAX)  NULL,
    [Genre] NVARCHAR(100) NULL, 
	[LanguageId] int NOT NULL, 
	[Error] NVARCHAR(500) NULL,
    [DateTimeStamp] DATETIME2 NULL,
    [FileContent] VARBINARY(MAX) NULL, 
	CONSTRAINT [PK_StaticAudio] PRIMARY KEY ([Id] ASC),
    CONSTRAINT [FK_StaticAudio_EventStaticAudio] FOREIGN KEY ([EventStaticAudioId]) REFERENCES [EventStaticAudio](Id) ON DELETE CASCADE,
    CONSTRAINT [FK_StaticAudio_Langauge] FOREIGN KEY (LanguageId) REFERENCES [Language]([Id])
)