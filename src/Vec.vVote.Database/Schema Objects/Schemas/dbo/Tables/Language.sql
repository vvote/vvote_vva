﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

CREATE TABLE [dbo].[Language]
(
	[Id] INT NOT NULL, 
    [Name] NVARCHAR(100) NOT NULL, 
    [ShortName] NVARCHAR(10) NOT NULL, 
    [LocalName] NVARCHAR(100) NULL, 
    [Direction] CHAR(3) NULL, 
    [Font] NVARCHAR(50) NULL, 
    [FontSize] NVARCHAR(10) NULL, 
    [StyleSheetLarge] NVARCHAR(500) NULL, 
    [StyleSheetMedium] NVARCHAR(500) NULL,
	CONSTRAINT [PK_Language] PRIMARY KEY ([Id] ASC),
	CONSTRAINT [UQ_Language] UNIQUE ([Name])
)
