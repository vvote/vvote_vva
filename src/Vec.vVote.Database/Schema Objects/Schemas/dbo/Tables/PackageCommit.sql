﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

CREATE TABLE [dbo].[PackageCommit]
(
	[Id] BIGINT NOT NULL IDENTITY,
	[EventId] UNIQUEIDENTIFIER NOT NULL,
    [DeviceId] NVARCHAR(50) NOT NULL, 
    [SerialNo] NVARCHAR(100) NOT NULL, 
    [Type] SMALLINT NOT NULL, 
    [District] NVARCHAR(100) NOT NULL, 
    [CommitTime] DATETIME2 NULL, 
    [Preferences] NVARCHAR(2000) NULL, 
    [BallotReductions] NVARCHAR(MAX) NULL,
    [DeviceRaceName] NVARCHAR(100) NULL, 
    [DeviceRaceId] UNIQUEIDENTIFIER NULL, 
    [VoteDistrictRaceName] NVARCHAR(100) NULL, 
    [VoteDistrictRaceId] UNIQUEIDENTIFIER NULL, 
    [VoteRegionRaceName] NVARCHAR(100) NULL, 
    [VoteRegionRaceId] UNIQUEIDENTIFIER NULL, 
	[ElectionStartTime] DATETIME2 NULL,
    [VoteType] SMALLINT NULL, 
    CONSTRAINT [PK_PackageCommit] PRIMARY KEY ([Id] ASC),
    CONSTRAINT [FK_PackageCommit_Event] FOREIGN KEY (EventId) REFERENCES [Event]([Id]) ON DELETE CASCADE
)
