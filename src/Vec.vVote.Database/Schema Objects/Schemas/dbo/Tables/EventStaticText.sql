﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

CREATE TABLE [dbo].[EventStaticText]
(
	[Id] INT NOT NULL IDENTITY, 
    [EventId] UNIQUEIDENTIFIER NOT NULL,    
	[Name] NVARCHAR(100) NOT NULL, 
    [Description] NVARCHAR(500) NULL, 
    [SizeLimit] NVARCHAR(10) NULL, 
    [ConversionLabel] NVARCHAR(100) NULL,
	CONSTRAINT [PK_EventStaticText] PRIMARY KEY ([Id] ASC),
    CONSTRAINT [FK_EventStaticText_Event] FOREIGN KEY (EventId) REFERENCES [Event](Id) ON DELETE CASCADE
)
