﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

CREATE TABLE [dbo].[EventMBB]
(
	[Id] INT NOT NULL IDENTITY, 
    [EventId] UNIQUEIDENTIFIER NOT NULL, 
    [IP] NVARCHAR(15) NOT NULL, 
	CONSTRAINT [PK_EventMBB] PRIMARY KEY ([Id] ASC),
    CONSTRAINT [FK_EventMBB_Event] FOREIGN KEY (EventId) REFERENCES [Event](Id) ON DELETE CASCADE
)
