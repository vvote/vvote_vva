﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

CREATE TABLE [dbo].[Event]
(
	[Id] UNIQUEIDENTIFIER NOT NULL , 
    [Name] NVARCHAR(200) NOT NULL, 
    [WBBServiceHostName] NVARCHAR(500) NULL, 
    [WBBMServiceHostName] NVARCHAR(500) NULL, 
    [VVPubKeyPath] NVARCHAR(500) NULL, 
    [EBPubKeyPath] NVARCHAR(500) NULL, 
    [MBBPubKeyPath] NVARCHAR(500) NULL, 
    [PollTime] NVARCHAR(50) NULL, 
    [ProofTime] NVARCHAR(50) NULL, 
    [DraftBundlePath] NVARCHAR(MAX) NULL, 
    [FinalBundlePath] NVARCHAR(MAX) NULL, 
    [SignatureFile] VARBINARY(MAX) NULL, 
    CONSTRAINT [PK_Event] PRIMARY KEY ([Id])
)
