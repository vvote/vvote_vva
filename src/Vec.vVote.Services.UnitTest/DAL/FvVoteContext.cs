﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/
 
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vec.Apps.vVote.DAL;
using Vec.TestHelpers.Fake;

namespace Vec.vVote.Services.UnitTest.DAL
{
    public class FvVoteContext : IvVoteContext
    {
        public IDbSet<Event> Events { get; set; }
        public IDbSet<Language> Languages { get; set; }
        public IDbSet<PartyAudio> PartyAudios { get; set; }
        public IDbSet<Party> Parties { get; set; }
        public IDbSet<CandidateAudio> CandidateAudios { get; set; }
        public IDbSet<PartyText> PartyTexts { get; set; }
        public IDbSet<EventAudio> EventAudios { get; set; }
        public IDbSet<LocationAudio> LocationAudios { get; set; }
        public IDbSet<EventText> EventTexts { get; set; }
        public IDbSet<EventDevice> EventDevices { get; set; }
        public IDbSet<EventElection> EventElections { get; set; }
        public IDbSet<EventMBB> EventMBBs { get; set; }
        public IDbSet<StaticAudio> StaticAudios { get; set; }
        public IDbSet<StaticText> StaticTexts { get; set; }
        public IDbSet<EventStaticAudio> EventStaticAudios { get; set; }
        public IDbSet<EventStaticText> EventStaticTexts { get; set; }
        public IDbSet<EventLocation> EventLocations { get; set; }
        public IDbSet<TelemetryWBB> TelemetryWBBs { get; set; }
        public IDbSet<TelemetryMBB> TelemetryMBBs { get; set; }
        public IDbSet<PackageCommit> PackageCommits { get; set; }
        public IDbSet<DeviceMessage> DeviceMessages { get; set; }
        public IDbSet<TelemetryMBBMessage> TelemetryMBBMessages { get; set; }
        public IDbSet<Telemetry> Telemetries { get; set; }
        public IDbSet<CommitFile> CommitFiles {get;set;}
        public IDbSet<ProcessedCommit> ProcessedCommits { get; set; }

        public FvVoteContext()
        {
            Events = new FakeDbSet<Event>();
            Languages = new FakeDbSet<Language>();
            PartyAudios = new FakeDbSet<PartyAudio>();
            Parties = new FakeDbSet<Party>();
            CandidateAudios = new FakeDbSet<CandidateAudio>();
            PartyTexts = new FakeDbSet<PartyText>();
            LocationAudios = new FakeDbSet<LocationAudio>();
            EventAudios = new FakeDbSet<EventAudio>();
            EventTexts = new FakeDbSet<EventText>();
            EventDevices = new FakeDbSet<EventDevice>();
            EventElections = new FakeDbSet<EventElection>();
            EventMBBs = new FakeDbSet<EventMBB>();
            StaticAudios = new FakeDbSet<StaticAudio>();
            StaticTexts = new FakeDbSet<StaticText>();
            EventStaticAudios = new FakeDbSet<EventStaticAudio>();
            EventStaticTexts = new FakeDbSet<EventStaticText>();
            EventLocations = new FakeDbSet<EventLocation>();
            TelemetryWBBs = new FakeDbSet<TelemetryWBB>();
            TelemetryMBBs = new FakeDbSet<TelemetryMBB>();
            PackageCommits = new FakeDbSet<PackageCommit>();
            DeviceMessages = new FakeDbSet<DeviceMessage>();
            TelemetryMBBMessages = new FakeDbSet<TelemetryMBBMessage>();
            Telemetries = new FakeDbSet<Telemetry>();
            CommitFiles = new FakeDbSet<CommitFile>();
            ProcessedCommits = new FakeDbSet<ProcessedCommit>();
        }

        public int SaveChanges()
        {
            return 0;
        }

        public int usp_Vec_vVote_UpdateTelemetryStatus(string level, string logger, string message, Nullable<Guid> eventId)
        {
            var record = Telemetries.SingleOrDefault(t => t.EventId == eventId);
            if (record == null)
            {
                record = new Telemetry() { EventId = eventId.Value, Level = level, Logger = logger, Message = message, DateTime = DateTime.UtcNow };
                Telemetries.Add(record);
            }
            else
            {
                record.Level = level;
                record.Logger = logger;
                record.Message = message;
                record.DateTime = DateTime.UtcNow;
            }
            return SaveChanges(); ;
        }
    }
}
