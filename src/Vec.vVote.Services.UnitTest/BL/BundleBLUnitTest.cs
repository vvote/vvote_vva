﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vec.Apps.vVote.DAL;
using Vec.vVote.Services.UnitTest.DAL;
using Vec.Apps.vVote.BL;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Vec.Apps.vVote.Entities;
using Vec.TuringElectionService;
using System.IO;
using Vec.TuringNominationService;
using Vec.WbbFileService;
using Vec.Apps.vVote.BL.Common;
using Vec.Apps.vVote.Converter;

namespace Vec.vVote.Services.UnitTest.BL
{
    [TestClass]
    public class BundleBLUnitTest
    {
        IvVoteContext vVoteContext { get; set; }
        Guid EventId { get; set; }
        Mock<IEventBL> DefaultEventBL { get; set; }
        Mock<IElectionBL> DefaultElectionBL { get; set; }


        Mock<IFileServiceHelper> DefaultFileServiceHelper { get; set; }
        Mock<IAudioConverter> DefaultAudioConverter{ get; set; }

        [TestInitialize]
        public void TestSetUp()
        {
            vVoteContext = new FvVoteContext();
            DefaultEventBL = new Mock<IEventBL>();
            DefaultElectionBL = new Mock<IElectionBL>();
            DefaultFileServiceHelper = new Mock<IFileServiceHelper>();
            DefaultAudioConverter = new Mock<IAudioConverter>();

            EventId = Guid.Parse("57474254-4f91-4125-8b89-ad4113387f78");

            var eve = vVoteContext.Events.Add(new Event() { Id = EventId, Name = "Test Event" });

            eve.EventLocations.Add(new EventLocation() { Id = 1, EventId = EventId, Type = (int)Vec.Apps.vVote.Entities.LocationType.District });
            eve.EventLocations.Add(new EventLocation() { Id = 2, EventId = EventId, Type = (int)Vec.Apps.vVote.Entities.LocationType.District });
            eve.EventLocations.Add(new EventLocation() { Id = 3, EventId = EventId, Type = (int)Vec.Apps.vVote.Entities.LocationType.District });
            eve.EventLocations.Add(new EventLocation() { Id = 4, EventId = EventId, Type = (int)Vec.Apps.vVote.Entities.LocationType.District });
            eve.EventLocations.Add(new EventLocation() { Id = 5, EventId = EventId, Type = (int)Vec.Apps.vVote.Entities.LocationType.District });
            eve.EventLocations.Add(new EventLocation() { Id = 6, EventId = EventId, Type = (int)Vec.Apps.vVote.Entities.LocationType.District });
            eve.EventLocations.Add(new EventLocation() { Id = 7, EventId = EventId, Type = (int)Vec.Apps.vVote.Entities.LocationType.District });
            eve.EventLocations.Add(new EventLocation() { Id = 8, EventId = EventId, Type = (int)Vec.Apps.vVote.Entities.LocationType.IS });
            eve.EventLocations.Add(new EventLocation() { Id = 9, EventId = EventId, Type = (int)Vec.Apps.vVote.Entities.LocationType.OS });
            eve.EventLocations.Add(new EventLocation() { Id = 10, EventId = EventId, Type = (int)Vec.Apps.vVote.Entities.LocationType.SPARE });
            eve.EventLocations.Add(new EventLocation() { Id = 10, EventId = EventId, Type = (int)Vec.Apps.vVote.Entities.LocationType.PROOF });
        }

        private BundleBL GetService(IvVoteContext vVoteContext, IEventBL eventBL)
        {
            return new BundleBL(vVoteContext, DefaultElectionBL.Object, eventBL, DefaultFileServiceHelper.Object, DefaultAudioConverter.Object);
        }

        [TestMethod]
        public void GetStagingLocation_WhenInvalidEventId_ThenZeroRecordsReturned()
        {
            // Arrange
            IBundleBL bundleBL = GetService(vVoteContext, DefaultEventBL.Object);

            try
            {
                // Act
                var result = bundleBL.GetStagingLocation(Guid.NewGuid());
                // Assert
                Assert.Fail("Sequence contains no matching elements.");
            }
            catch (InvalidOperationException)
            {
                
            }            
        }
        
        [TestMethod]
        public void GetStagingLocation_WhenValidEventId_Then7RecordsReturned()
        {
            // Arrange
            IBundleBL bundleBL = GetService(vVoteContext, DefaultEventBL.Object);

            // Act
            var result = bundleBL.GetStagingLocation(EventId);

            // Assert
            Assert.AreEqual(result.Count(), 9);
        }

        [TestCleanup]
        public void CleanUp()
        {
        }
    }
}
