﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using Vec.Apps.vVote.BL;
using Vec.Apps.vVote.BL.Common;
using Vec.Apps.vVote.DAL;
using Vec.Apps.vVote.Entities;
using Vec.TuringNominationService;
using Vec.vVote.Services.UnitTest.DAL;
using Vec.TuringElectionService;
using Vec.Apps.vVote.Common;
using Ionic.Zip;

namespace Vec.vVote.Services.UnitTest.BL
{
    [TestClass]
    public class VoteDecryptBLUnitTest
    {
        IvVoteContext vVoteContext { get; set; }
        Mock<IElectionBL> DefaultElectionBL { get; set; }
        Mock<IEventBL> DefaultEventBL { get; set; }
        Mock<IFileServiceHelper> DefaultFileService { get; set; }
        Mock<VotePackingServiceHelper> DefaultVotePackingService { get; set; }

        public Guid ElectionId { get; set; }
        public DirectoryInfo TestPath { get; set; }

        [TestInitialize]
        public void TestInit()
        {
            vVoteContext = new FvVoteContext();
            DefaultEventBL = new Mock<IEventBL>();
            DefaultElectionBL = new Mock<IElectionBL>();
            DefaultFileService = new Mock<IFileServiceHelper>();
            DefaultVotePackingService = new Mock<VotePackingServiceHelper>();

            ElectionId = Guid.Parse("57474254-4f91-4125-8b89-ad4113387f78");

            TestPath = Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Guid.NewGuid().ToString()));

            var candidates = new List<BallotDrawnCandidatesForElectionElectorateVacancyEntity>();
            var ballotDrawCandidates = new BallotDrawnCandidatesForElectionElectorateVacancyEntity()
            {
                BallotDrawnCandidates = new BallotDrawnNominationEntity[3]{
                    new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 1", BallotPaperName = "Name 1", BallotPaperPosition =1, Id =1, NominationStatus = NS.Active, OrderWithinGroupOrTeam=1, PreferredRPPName ="RPP 1"},
                    new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 2", BallotPaperName = "Name 2", BallotPaperPosition =2, Id =2, NominationStatus = NS.Active, OrderWithinGroupOrTeam=2, PreferredRPPName ="RPP 2"}, 
                    new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 3", BallotPaperName = "Name 3", BallotPaperPosition =3, Id =3, NominationStatus = NS.Active, OrderWithinGroupOrTeam=3, PreferredRPPName ="RPP 3"} 
                }
            };
            candidates.Add(ballotDrawCandidates);

            var parties = new List<BallotDrawnGroupsForElectionElectorateVacancy>();
            var ballotDrawGroups = new BallotDrawnGroupsForElectionElectorateVacancy()
            {
                BallotDrawnGroups = new BallotDrawnGroupEntity[5] {
                        new BallotDrawnGroupEntity() { BallotPaperLetter="A", BallotPaperPosition = 1, Id =1, Name = "Party 1", Ungrouped = false, Unnamed = false, Nominations = new BallotDrawnNominationEntity[3] {
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 1", BallotPaperName = "Name 1", BallotPaperPosition =1, Id =1, NominationStatus = NS.Active, OrderWithinGroupOrTeam=1, PreferredRPPName ="RPP 1"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 2", BallotPaperName = "Name 2", BallotPaperPosition =2, Id =2, NominationStatus = NS.Active, OrderWithinGroupOrTeam=2, PreferredRPPName ="RPP 2"}, 
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 3", BallotPaperName = "Name 3", BallotPaperPosition =3, Id =3, NominationStatus = NS.Active, OrderWithinGroupOrTeam=3, PreferredRPPName ="RPP 3"} 
                            }
                        },
                        new BallotDrawnGroupEntity() { BallotPaperLetter="B", BallotPaperPosition = 2, Id =2, Name = "Party 2", Ungrouped = false, Unnamed = false, Nominations = new BallotDrawnNominationEntity[3] {
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 4", BallotPaperName = "Name 4", BallotPaperPosition =4, Id =4, NominationStatus = NS.Active, OrderWithinGroupOrTeam=1, PreferredRPPName ="RPP 1"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 5", BallotPaperName = "Name 5", BallotPaperPosition =5, Id =5, NominationStatus = NS.Active, OrderWithinGroupOrTeam=2, PreferredRPPName ="RPP 2"}, 
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 6", BallotPaperName = "Name 6", BallotPaperPosition =6, Id =6, NominationStatus = NS.Active, OrderWithinGroupOrTeam=3, PreferredRPPName ="RPP 3"} 
                            }
                        },
                         new BallotDrawnGroupEntity() { BallotPaperLetter="C", BallotPaperPosition = 3, Id =3, Name = "Party 3", Ungrouped = false, Unnamed = false, Nominations = new BallotDrawnNominationEntity[3] {
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 7", BallotPaperName = "Name 7", BallotPaperPosition =7, Id =7, NominationStatus = NS.Active, OrderWithinGroupOrTeam=1, PreferredRPPName ="RPP 1"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 8", BallotPaperName = "Name 8", BallotPaperPosition =8, Id =8, NominationStatus = NS.Active, OrderWithinGroupOrTeam=2, PreferredRPPName ="RPP 2"}, 
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 9", BallotPaperName = "Name 9", BallotPaperPosition =9, Id =9, NominationStatus = NS.Active, OrderWithinGroupOrTeam=3, PreferredRPPName ="RPP 3"} 
                            }
                        }, 
                        new BallotDrawnGroupEntity() { BallotPaperLetter="", BallotPaperPosition = 4, Id =4, Name = "Unnamed", Ungrouped = false, Unnamed = true, Nominations = new BallotDrawnNominationEntity[3] {
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 10", BallotPaperName = "Name 10", BallotPaperPosition =10, Id =10, NominationStatus = NS.Active, OrderWithinGroupOrTeam=1, PreferredRPPName ="RPP 1"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 11", BallotPaperName = "Name 11", BallotPaperPosition =11, Id =11, NominationStatus = NS.Active, OrderWithinGroupOrTeam=2, PreferredRPPName ="RPP 2"}, 
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 12", BallotPaperName = "Name 12", BallotPaperPosition =12, Id =12, NominationStatus = NS.Active, OrderWithinGroupOrTeam=3, PreferredRPPName ="RPP 3"} 
                            }
                        },
                         new BallotDrawnGroupEntity() { BallotPaperLetter="", BallotPaperPosition = 5, Id =5, Name = "Ungrouped", Ungrouped = true, Unnamed = false, Nominations = new BallotDrawnNominationEntity[3] {
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 13", BallotPaperName = "Name 13", BallotPaperPosition =13, Id =13, NominationStatus = NS.Active, OrderWithinGroupOrTeam=1, PreferredRPPName ="RPP 1"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 14", BallotPaperName = "Name 14", BallotPaperPosition =14, Id =14, NominationStatus = NS.Active, OrderWithinGroupOrTeam=2, PreferredRPPName ="RPP 2"}, 
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 15", BallotPaperName = "Name 15", BallotPaperPosition =15, Id =15, NominationStatus = NS.Active, OrderWithinGroupOrTeam=3, PreferredRPPName ="RPP 3"} 
                            }
                        },

                }
            };
            parties.Add(ballotDrawGroups);

            List<MElectionLookup> elections = new List<MElectionLookup>();
            elections.Add(new MElectionLookup()
            {
                Id = ElectionId,
                Name = "State Election 2010",
                ElectionType = TuringElectionService.ElectionType.GeneralElection,
                DateRanges = new MElectionDateRange[] { new MElectionDateRange { ElectionId = ElectionId, DateRangeType = ElectionDateRangeType.ElectionDay, StartDateTime = DateTime.Today.AddDays(-10) } },
                ParentElectorates = new MElectorateLookup[1] { 
                    new MElectorateLookup() { 
                        Electorates = new MElectorateLookup[2] {
                            new MElectorateLookup(){  Name = "Altona District", Type= ElectorateType.District,
                                Locations = 
                                new MLocationLookup[1] { 
                                    new MLocationLookup() { Id = Guid.NewGuid(), Name ="Dummy Location 1" }
                                },
                                ElectorateVacancyConfiguration = new MElectionConfig(){ ElectionElectorateVacancyId = Guid.NewGuid(), ElectionElectorateVacancyStatus = TuringElectionService.ElectionElectorateVacancyStatus.Contested, ElectorateType = ElectorateType.District}
                            },
                            new MElectorateLookup(){ Name= "South West Metro Region", Type = ElectorateType.Region,
                                Locations = 
                                new MLocationLookup[1] { 
                                    new MLocationLookup() { Id = Guid.NewGuid(), Name ="Dummy Location 2" }
                                },
                                ElectorateVacancyConfiguration = new MElectionConfig(){ ElectionElectorateVacancyId = Guid.NewGuid(), ElectionElectorateVacancyStatus = TuringElectionService.ElectionElectorateVacancyStatus.Contested, ElectorateType = ElectorateType.Region }
                            }
                        } 
                    }
                }
            });

            DefaultElectionBL.Setup(e => e.GetElections(It.IsAny<List<Guid>>())).Returns(elections);
            DefaultElectionBL.Setup(e => e.BallotDrawCandidates(It.IsAny<List<Guid>>())).Returns(candidates);
            DefaultElectionBL.Setup(e => e.BallotDrawGroups(It.IsAny<List<Guid>>())).Returns(parties);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestPath.Delete(true);
        }

        private VoteDecryptBL GetService(IvVoteContext vVoteContext, Mock<IEventBL> eventBL, Mock<IElectionBL> electionBL, Mock<IFileServiceHelper> fileServiceHelper)
        {
            return new VoteDecryptBL(vVoteContext, eventBL.Object, electionBL.Object, fileServiceHelper.Object, DefaultVotePackingService.Object, new ECHelper());
        }

        [TestMethod]
        [Ignore]
        [DeploymentItem(@"Data\Signature.png", "Data")]
        [DeploymentItem(@"Data\LA_A.Altona.csv", "Data")]
        [DeploymentItem(@"Data\LA_E.Altona.csv", "Data")]
        [DeploymentItem(@"Data\LA_202.Altona.csv", "Data")]
        public void ProcessDistrictDecryptedVotes_When3PreferenceAnd5VotesEach_Then3FilesProcessedFor1PfdFileGeneration()
        {
            // Arrange
            Guid context = Guid.NewGuid();
            DirectoryInfo contextDirectory = TestPath.CreateSubdirectory(context.ToString());

            File.Copy(@"Data\Signature.png", Path.Combine(contextDirectory.FullName, "Signature.png"));
            File.Copy(@"Data\LA_202.Altona.csv", Path.Combine(contextDirectory.FullName, "1ba3792a-92b8-4362-bd58-92144259a329_LA_202.Altona.csv"));
            File.Copy(@"Data\LA_A.Altona.csv", Path.Combine(contextDirectory.FullName, "1ba3792a-92b8-4362-bd58-92144259a329_LA_A.Altona.csv"));
            File.Copy(@"Data\LA_E.Altona.csv", Path.Combine(contextDirectory.FullName, "1ba3792a-92b8-4362-bd58-92144259a329_LA_E.Altona.csv"));

            IVoteDecryptBL voteDecryptBL = GetService(new FvVoteContext(), DefaultEventBL, DefaultElectionBL, DefaultFileService);

            // Act
            var result = voteDecryptBL.DecryptDistrictVotes(contextDirectory);

            // Assert
            Assert.AreEqual(3, result.VoteDecryptFiles.Count);
            Assert.IsTrue(File.Exists(Path.Combine(contextDirectory.FullName, "ParsedVotes.json")), "Could not find ParsedVotes.json");
            Assert.IsTrue(File.Exists(Path.Combine(contextDirectory.FullName, "CandidateLookup.json")), "Could not find CandidateLookup.json");
        }

        [TestMethod]
        [Ignore]
        [DeploymentItem(@"Data\Signature.png", "Data")]
        [DeploymentItem(@"Data\DistrictVoteTemplate.pdf", "Data")]
        [DeploymentItem(@"Data\LA_A.Altona.csv", "Data")]
        [DeploymentItem(@"Data\LA_E.Altona.csv", "Data")]
        [DeploymentItem(@"Data\LA_202.Altona.csv", "Data")]
        public void GeneratDistrictPDF_When3PreferenceAnd5VotesEach_Then1PdfWithAbsentEarlyAndOrdinaryFileGenerated()
        {
            // Arrange
            Guid context = Guid.NewGuid();
            DirectoryInfo contextDirectory = TestPath.CreateSubdirectory(context.ToString());

            File.Copy(@"Data\Signature.png", Path.Combine(contextDirectory.FullName, "Signature.png"));
            File.Copy(@"Data\LA_202.Altona.csv", Path.Combine(contextDirectory.FullName, "1ba3792a-92b8-4362-bd58-92144259a329_LA_202.Altona.csv"));
            File.Copy(@"Data\LA_A.Altona.csv", Path.Combine(contextDirectory.FullName, "1ba3792a-92b8-4362-bd58-92144259a329_LA_A.Altona.csv"));
            File.Copy(@"Data\LA_E.Altona.csv", Path.Combine(contextDirectory.FullName, "1ba3792a-92b8-4362-bd58-92144259a329_LA_E.Altona.csv"));
            File.Copy(@"Data\DistrictVoteTemplate.pdf", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "DistrictVoteTemplate.pdf"));

            var voteDecryptBL = GetService(new FvVoteContext(), DefaultEventBL, DefaultElectionBL, DefaultFileService);
            var result = voteDecryptBL.DecryptDistrictVotes(contextDirectory);

            // Act
            voteDecryptBL.GenerateDistrictVotePDFs(contextDirectory, result.VoteDecryptFiles.Select(x => x.Id).ToList());

            File.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "DistrictVoteTemplate.pdf"));
            // Assert
            Assert.IsTrue(File.Exists(Path.Combine(contextDirectory.FullName, "Altona.pdf")), "Could not find the generated PDF: Altona.pdf");
        }

        [TestMethod]
        [Ignore]
        [DeploymentItem(@"Data\Signature.png", "Data")]
        [DeploymentItem(@"Data\DistrictVoteTemplate.pdf", "Data")]
        [DeploymentItem(@"Data\LA_A.Altona.csv", "Data")]
        [DeploymentItem(@"Data\LA_A.Altona_BLANK.csv", "Data")]
        [DeploymentItem(@"Data\LA_E.Altona.csv", "Data")]
        [DeploymentItem(@"Data\LA_202.Altona.csv", "Data")]
        public void GeneratDistrictPDF_When3Preference2BlanksAnd5VotesEach_Then1PdfWithAbsentEarlyAndOrdinaryFileGeneratedWithBlanks()
        {
            // Arrange
            Guid context = Guid.NewGuid();
            DirectoryInfo contextDirectory = TestPath.CreateSubdirectory(context.ToString());

            File.Copy(@"Data\Signature.png", Path.Combine(contextDirectory.FullName, "Signature.png"));
            File.Copy(@"Data\LA_202.Altona.csv", Path.Combine(contextDirectory.FullName, "1ba3792a-92b8-4362-bd58-92144259a329_LA_202.Altona.csv"));
            File.Copy(@"Data\LA_A.Altona.csv", Path.Combine(contextDirectory.FullName, "1ba3792a-92b8-4362-bd58-92144259a329_LA_A.Altona.csv"));
            File.Copy(@"Data\LA_A.Altona_BLANK.csv", Path.Combine(contextDirectory.FullName, "1ba3792a-92b8-4362-bd58-92144259a329_LA_A.Altona_BLANK.csv"));
            File.Copy(@"Data\LA_E.Altona.csv", Path.Combine(contextDirectory.FullName, "1ba3792a-92b8-4362-bd58-92144259a329_LA_E.Altona.csv"));
            File.Copy(@"Data\DistrictVoteTemplate.pdf", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "DistrictVoteTemplate.pdf"));

            var voteDecryptBL = GetService(new FvVoteContext(), DefaultEventBL, DefaultElectionBL, DefaultFileService);
            var result = voteDecryptBL.DecryptDistrictVotes(contextDirectory);

            // Act
            voteDecryptBL.GenerateDistrictVotePDFs(contextDirectory, result.VoteDecryptFiles.Select(x => x.Id).ToList());

            File.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "DistrictVoteTemplate.pdf"));
            // Assert
            Assert.IsTrue(File.Exists(Path.Combine(contextDirectory.FullName, "Altona.pdf")), "Could not find the generated PDF: Altona.pdf");
        }

        [TestMethod]
        [Ignore]
        [DeploymentItem(@"Data\Signature.png", "Data")]
        [DeploymentItem(@"Data\ATL_202.Western Metropolitan_Altona.csv", "Data")]
        [DeploymentItem(@"Data\ATL_A.Western Metropolitan_Altona.csv", "Data")]
        [DeploymentItem(@"Data\ATL_E.Western Metropolitan_Altona.csv", "Data")]
        public void ProcessRegionAtlDecryptedVotes_When3PreferenceAnd3VotesEach_Then3FilesProcessedFor1PfdFileGeneration()
        {
            // Arrange
            Guid context = Guid.NewGuid();
            DirectoryInfo contextDirectory = TestPath.CreateSubdirectory(context.ToString());

            File.Copy(@"Data\Signature.png", Path.Combine(contextDirectory.FullName, "Signature.png"));
            File.Copy(@"Data\ATL_202.Western Metropolitan_Altona.csv", Path.Combine(contextDirectory.FullName, "91e14ee3-d425-4814-beb3-20b7d01a1989-Altona_ATL_202.Western Metropolitan_Altona.csv"));
            File.Copy(@"Data\ATL_A.Western Metropolitan_Altona.csv", Path.Combine(contextDirectory.FullName, "91e14ee3-d425-4814-beb3-20b7d01a1989-Altona_ATL_A.Western Metropolitan_Altona.csv"));
            File.Copy(@"Data\ATL_E.Western Metropolitan_Altona.csv", Path.Combine(contextDirectory.FullName, "91e14ee3-d425-4814-beb3-20b7d01a1989-Altona_ATL_E.Western Metropolitan_Altona.csv"));

            IVoteDecryptBL voteDecryptBL = GetService(new FvVoteContext(), DefaultEventBL, DefaultElectionBL, DefaultFileService);

            // Act
            var result = voteDecryptBL.DecryptAtlVotes(contextDirectory);

            // Assert
            Assert.AreEqual(3, result.VoteDecryptFiles.Count);
            Assert.IsTrue(File.Exists(Path.Combine(contextDirectory.FullName, "ParsedVotes.json")), "Could not find ParsedVotes.json");
            Assert.IsTrue(File.Exists(Path.Combine(contextDirectory.FullName, "PartyLookup.json")), "Could not find PartyLookup.json");
        }

        [TestMethod]
        [Ignore]
        [DeploymentItem(@"Data\Signature.png", "Data")]
        [DeploymentItem(@"Data\ATLVoteTemplate.pdf", "Data")]
        [DeploymentItem(@"Data\ATL_202.Western Metropolitan_Altona.csv", "Data")]
        [DeploymentItem(@"Data\ATL_A.Western Metropolitan_Altona.csv", "Data")]
        [DeploymentItem(@"Data\ATL_E.Western Metropolitan_Altona.csv", "Data")]
        public void GeneratRegionAtlPDF_When3PreferenceAnd5VotesEach_Then1PdfWithAbsentEarlyAndOrdinaryFileGenerated()
        {
            // Arrange
            Guid context = Guid.NewGuid();
            DirectoryInfo contextDirectory = TestPath.CreateSubdirectory(context.ToString());

            File.Copy(@"Data\Signature.png", Path.Combine(contextDirectory.FullName, "Signature.png"));
            File.Copy(@"Data\ATL_202.Western Metropolitan_Altona.csv", Path.Combine(contextDirectory.FullName, "91e14ee3-d425-4814-beb3-20b7d01a1989-Altona_ATL_202.Western Metropolitan_Altona.csv"));
            File.Copy(@"Data\ATL_A.Western Metropolitan_Altona.csv", Path.Combine(contextDirectory.FullName, "91e14ee3-d425-4814-beb3-20b7d01a1989-Altona_ATL_A.Western Metropolitan_Altona.csv"));
            File.Copy(@"Data\ATL_E.Western Metropolitan_Altona.csv", Path.Combine(contextDirectory.FullName, "91e14ee3-d425-4814-beb3-20b7d01a1989-Altona_ATL_E.Western Metropolitan_Altona.csv"));
            File.Copy(@"Data\ATLVoteTemplate.pdf", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ATLVoteTemplate.pdf"));

            var voteDecryptBL = GetService(new FvVoteContext(), DefaultEventBL, DefaultElectionBL, DefaultFileService);
            var result = voteDecryptBL.DecryptAtlVotes(contextDirectory);

            // Act
            voteDecryptBL.GenerateAtlVotePDFs(contextDirectory, result.VoteDecryptFiles.Select(x => x.Id).ToList());

            File.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ATLVoteTemplate.pdf"));
            // Assert
            Assert.IsTrue(File.Exists(Path.Combine(contextDirectory.FullName, "Western_Metropolitan_ATL.pdf")), "Could not find the generated PDF: Western Metropolitan.pdf");
        }

        [TestMethod]
        [Ignore]
        [DeploymentItem(@"Data\Signature.png", "Data")]
        [DeploymentItem(@"Data\ATLVoteTemplate.pdf", "Data")]
        [DeploymentItem(@"Data\ATL_202.Western Metropolitan_Altona.csv", "Data")]
        [DeploymentItem(@"Data\ATL_A.Western Metropolitan_Altona.csv", "Data")]
        [DeploymentItem(@"Data\BLANK_ATL_A.Western Metropolitan.csv", "Data")]
        [DeploymentItem(@"Data\ATL_E.Western Metropolitan_Altona.csv", "Data")]
        public void GeneratRegionAtlPDF_When3Preference3BlanksAnd5VotesEach_Then1PdfWithAbsentEarlyAndOrdinaryFileGeneratedWithBlanks()
        {
            // Arrange
            Guid context = Guid.NewGuid();
            DirectoryInfo contextDirectory = TestPath.CreateSubdirectory(context.ToString());

            File.Copy(@"Data\Signature.png", Path.Combine(contextDirectory.FullName, "Signature.png"));
            File.Copy(@"Data\ATL_202.Western Metropolitan_Altona.csv", Path.Combine(contextDirectory.FullName, "91e14ee3-d425-4814-beb3-20b7d01a1989-Altona_ATL_202.Western Metropolitan_Altona.csv"));
            File.Copy(@"Data\ATL_A.Western Metropolitan_Altona.csv", Path.Combine(contextDirectory.FullName, "91e14ee3-d425-4814-beb3-20b7d01a1989-Altona_ATL_A.Western Metropolitan_Altona.csv"));
            File.Copy(@"Data\BLANK_ATL_A.Western Metropolitan.csv", Path.Combine(contextDirectory.FullName, "91e14ee3-d425-4814-beb3-20b7d01a1989-Altona_BLANK_ATL_A.Western Metropolitan_Altona_BLANK.csv"));
            File.Copy(@"Data\ATL_E.Western Metropolitan_Altona.csv", Path.Combine(contextDirectory.FullName, "91e14ee3-d425-4814-beb3-20b7d01a1989-Altona_ATL_E.Western Metropolitan_Altona.csv"));
            File.Copy(@"Data\ATLVoteTemplate.pdf", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ATLVoteTemplate.pdf"));

            var voteDecryptBL = GetService(new FvVoteContext(), DefaultEventBL, DefaultElectionBL, DefaultFileService);
            var result = voteDecryptBL.DecryptAtlVotes(contextDirectory);

            // Act
            voteDecryptBL.GenerateAtlVotePDFs(contextDirectory, result.VoteDecryptFiles.Select(x => x.Id).ToList());

            File.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ATLVoteTemplate.pdf"));
            // Assert
            Assert.IsTrue(File.Exists(Path.Combine(contextDirectory.FullName, "Western_Metropolitan_ATL.pdf")), "Could not find the generated PDF: Western Metropolitan.pdf");
        }

        [TestMethod]
        [Ignore]
        [DeploymentItem(@"Data\Signature.png", "Data")]
        [DeploymentItem(@"Data\BTL_202.Western Metropolitan_Altona.csv", "Data")]
        [DeploymentItem(@"Data\BTL_A.Western Metropolitan_Altona.csv", "Data")]
        [DeploymentItem(@"Data\BTL_E.Western Metropolitan_Altona.csv", "Data")]
        public void ProcessRegionBtlDecryptedVotes_When3PreferenceAnd3VotesEach_Then3FilesProcessedFor1PfdFileGeneration()
        {
            // Arrange
            Guid context = Guid.NewGuid();
            DirectoryInfo contextDirectory = TestPath.CreateSubdirectory(context.ToString());

            File.Copy(@"Data\Signature.png", Path.Combine(contextDirectory.FullName, "Signature.png"));
            File.Copy(@"Data\BTL_202.Western Metropolitan_Altona.csv", Path.Combine(contextDirectory.FullName, "91e14ee3-d425-4814-beb3-20b7d01a1989-Altona_BTL_202.Western Metropolitan_Altona.csv"));
            File.Copy(@"Data\BTL_A.Western Metropolitan_Altona.csv", Path.Combine(contextDirectory.FullName, "91e14ee3-d425-4814-beb3-20b7d01a1989-Altona_BTL_A.Western Metropolitan_Altona.csv"));
            File.Copy(@"Data\BTL_E.Western Metropolitan_Altona.csv", Path.Combine(contextDirectory.FullName, "91e14ee3-d425-4814-beb3-20b7d01a1989-Altona_BTL_E.Western Metropolitan_Altona.csv"));

            IVoteDecryptBL voteDecryptBL = GetService(new FvVoteContext(), DefaultEventBL, DefaultElectionBL, DefaultFileService);

            // Act
            var result = voteDecryptBL.DecryptBtlVotes(contextDirectory);

            // Assert
            Assert.AreEqual(3, result.VoteDecryptFiles.Count);
            Assert.IsTrue(File.Exists(Path.Combine(contextDirectory.FullName, "ParsedVotes.json")), "Could not find ParsedVotes.json");
            Assert.IsTrue(File.Exists(Path.Combine(contextDirectory.FullName, "PartyLookup.json")), "Could not find PartyLookup.json");
            Assert.IsTrue(File.Exists(Path.Combine(contextDirectory.FullName, "CandidateLookup.json")), "Could not find CandidateLookup.json");
        }

        [TestMethod]
        [Ignore]
        [DeploymentItem(@"Data\Signature.png", "Data")]
        [DeploymentItem(@"Data\BTLVoteTemplate.pdf", "Data")]
        [DeploymentItem(@"Data\BTLVoteTemplateA3.pdf", "Data")]
        [DeploymentItem(@"Data\LARGE_BTL_202.Western Metropolitan_Altona.csv", "Data")]
        [DeploymentItem(@"Data\LARGE_BTL_A.Western Metropolitan_Altona.csv", "Data")]
        [DeploymentItem(@"Data\LARGE_BTL_E.Western Metropolitan_Altona.csv", "Data")]
        public void GeneratRegionBtlPDF_When3PreferenceAnd5VotesEach_Then1A4PdfWithAbsentEarlyAndOrdinaryFileGenerated()
        {
            // Arrange
            Guid context = Guid.NewGuid();
            DirectoryInfo contextDirectory = TestPath.CreateSubdirectory(context.ToString());

            File.Copy(@"Data\Signature.png", Path.Combine(contextDirectory.FullName, "Signature.png"));
            File.Copy(@"Data\LARGE_BTL_202.Western Metropolitan_Altona.csv", Path.Combine(contextDirectory.FullName, "91e14ee3-d425-4814-beb3-20b7d01a1989-Altona_BTL_202.Western Metropolitan_Altona.csv"));
            File.Copy(@"Data\LARGE_BTL_A.Western Metropolitan_Altona.csv", Path.Combine(contextDirectory.FullName, "91e14ee3-d425-4814-beb3-20b7d01a1989-Altona_BTL_A.Western Metropolitan_Altona.csv"));
            File.Copy(@"Data\LARGE_BTL_E.Western Metropolitan_Altona.csv", Path.Combine(contextDirectory.FullName, "91e14ee3-d425-4814-beb3-20b7d01a1989-Altona_BTL_E.Western Metropolitan_Altona.csv"));
            File.Copy(@"Data\BTLVoteTemplate.pdf", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "BTLVoteTemplate.pdf"));
            File.Copy(@"Data\BTLVoteTemplateA3.pdf", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "BTLVoteTemplateA3.pdf"));

            var voteDecryptBL = GetService(new FvVoteContext(), DefaultEventBL, DefaultElectionBL, DefaultFileService);
            var result = voteDecryptBL.DecryptBtlVotes(contextDirectory);

            // Act
            voteDecryptBL.GenerateBtlVotePDFs(contextDirectory, result.VoteDecryptFiles.Select(x => x.Id).ToList());

            File.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "BTLVoteTemplate.pdf"));
            File.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "BTLVoteTemplateA3.pdf"));

            // Assert
            Assert.IsTrue(File.Exists(Path.Combine(contextDirectory.FullName, "Western_Metropolitan_BTL.pdf")), "Could not find the generated PDF: Western Metropolitan.pdf");
        }

        [TestMethod]
        [Ignore]
        [DeploymentItem(@"Data\Signature.png", "Data")]
        [DeploymentItem(@"Data\BTLVoteTemplate.pdf", "Data")]
        [DeploymentItem(@"Data\BTLVoteTemplateA3.pdf", "Data")]
        [DeploymentItem(@"Data\BTL_202.Western Metropolitan_Altona.csv", "Data")]
        [DeploymentItem(@"Data\BTL_A.Western Metropolitan_Altona.csv", "Data")]
        [DeploymentItem(@"Data\BTL_E.Western Metropolitan_Altona.csv", "Data")]
        public void GeneratRegionBtlPDF_When42Candidates3PreferenceAnd5VotesEach_Then1A3PdfWithAbsentEarlyAndOrdinaryFileGenerated()
        {
            // Arrange
            Guid context = Guid.NewGuid();
            DirectoryInfo contextDirectory = TestPath.CreateSubdirectory(context.ToString());

            File.Copy(@"Data\Signature.png", Path.Combine(contextDirectory.FullName, "Signature.png"));
            File.Copy(@"Data\BTL_202.Western Metropolitan_Altona.csv", Path.Combine(contextDirectory.FullName, "69BDEC3C-25E1-4481-9825-148109B8215E-Altona_BTL_202.Western Metropolitan_Altona.csv"));
            File.Copy(@"Data\BTL_A.Western Metropolitan_Altona.csv", Path.Combine(contextDirectory.FullName, "69BDEC3C-25E1-4481-9825-148109B8215E-Altona_BTL_A.Western Metropolitan_Altona.csv"));
            File.Copy(@"Data\BTL_E.Western Metropolitan_Altona.csv", Path.Combine(contextDirectory.FullName, "69BDEC3C-25E1-4481-9825-148109B8215E-Altona_BTL_E.Western Metropolitan_Altona.csv"));
            File.Copy(@"Data\BTLVoteTemplate.pdf", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "BTLVoteTemplate.pdf"));
            File.Copy(@"Data\BTLVoteTemplateA3.pdf", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "BTLVoteTemplateA3.pdf"));

            var parties = new List<BallotDrawnGroupsForElectionElectorateVacancy>();
            var ballotDrawGroups = new BallotDrawnGroupsForElectionElectorateVacancy()
            {
                BallotDrawnGroups = new BallotDrawnGroupEntity[5] {
                        new BallotDrawnGroupEntity() { BallotPaperLetter="A", BallotPaperPosition = 1, Id =1, Name = "Party 1", Ungrouped = false, Unnamed = false, Nominations = new BallotDrawnNominationEntity[3] {
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 1", BallotPaperName = "Name 1", BallotPaperPosition =1, Id =1, NominationStatus = NS.Active, OrderWithinGroupOrTeam=1, PreferredRPPName ="RPP 1"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 2", BallotPaperName = "Name 2", BallotPaperPosition =2, Id =2, NominationStatus = NS.Active, OrderWithinGroupOrTeam=2, PreferredRPPName ="RPP 2"}, 
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 3", BallotPaperName = "Name 3", BallotPaperPosition =3, Id =3, NominationStatus = NS.Active, OrderWithinGroupOrTeam=3, PreferredRPPName ="RPP 3"} 
                            }
                        },
                        new BallotDrawnGroupEntity() { BallotPaperLetter="B", BallotPaperPosition = 2, Id =2, Name = "Party 2", Ungrouped = false, Unnamed = false, Nominations = new BallotDrawnNominationEntity[3] {
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 4", BallotPaperName = "Name 4", BallotPaperPosition =4, Id =4, NominationStatus = NS.Active, OrderWithinGroupOrTeam=1, PreferredRPPName ="RPP 1"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 5", BallotPaperName = "Name 5", BallotPaperPosition =5, Id =5, NominationStatus = NS.Active, OrderWithinGroupOrTeam=2, PreferredRPPName ="RPP 2"}, 
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 6", BallotPaperName = "Name 6", BallotPaperPosition =6, Id =6, NominationStatus = NS.Active, OrderWithinGroupOrTeam=3, PreferredRPPName ="RPP 3"} 
                            }
                        },
                         new BallotDrawnGroupEntity() { BallotPaperLetter="C", BallotPaperPosition = 3, Id =3, Name = "Party 3", Ungrouped = false, Unnamed = false, Nominations = new BallotDrawnNominationEntity[3] {
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 7", BallotPaperName = "Name 7", BallotPaperPosition =7, Id =7, NominationStatus = NS.Active, OrderWithinGroupOrTeam=1, PreferredRPPName ="RPP 1"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 8", BallotPaperName = "Name 8", BallotPaperPosition =8, Id =8, NominationStatus = NS.Active, OrderWithinGroupOrTeam=2, PreferredRPPName ="RPP 2"}, 
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 9", BallotPaperName = "Name 9", BallotPaperPosition =9, Id =9, NominationStatus = NS.Active, OrderWithinGroupOrTeam=3, PreferredRPPName ="RPP 3"} 
                            }
                        }, 
                        new BallotDrawnGroupEntity() { BallotPaperLetter="", BallotPaperPosition = 4, Id =4, Name = "Unnamed", Ungrouped = false, Unnamed = true, Nominations = new BallotDrawnNominationEntity[3] {
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 10", BallotPaperName = "Name 10", BallotPaperPosition =10, Id =10, NominationStatus = NS.Active, OrderWithinGroupOrTeam=1, PreferredRPPName ="RPP 1"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 11", BallotPaperName = "Name 11", BallotPaperPosition =11, Id =11, NominationStatus = NS.Active, OrderWithinGroupOrTeam=2, PreferredRPPName ="RPP 2"}, 
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 12", BallotPaperName = "Name 12", BallotPaperPosition =12, Id =12, NominationStatus = NS.Active, OrderWithinGroupOrTeam=3, PreferredRPPName ="RPP 3"} 
                            }
                        },
                         new BallotDrawnGroupEntity() { BallotPaperLetter="", BallotPaperPosition = 5, Id =5, Name = "Ungrouped", Ungrouped = true, Unnamed = false, Nominations = new BallotDrawnNominationEntity[30] {
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 13", BallotPaperName = "Name 13", BallotPaperPosition =13, Id =13, NominationStatus = NS.Active, OrderWithinGroupOrTeam=13, PreferredRPPName ="RPP 13"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 14", BallotPaperName = "Name 14", BallotPaperPosition =14, Id =14, NominationStatus = NS.Active, OrderWithinGroupOrTeam=14, PreferredRPPName ="RPP 14"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 15", BallotPaperName = "Name 15", BallotPaperPosition =15, Id =15, NominationStatus = NS.Active, OrderWithinGroupOrTeam=15, PreferredRPPName ="RPP 15"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 16", BallotPaperName = "Name 16", BallotPaperPosition =16, Id =16, NominationStatus = NS.Active, OrderWithinGroupOrTeam=16, PreferredRPPName ="RPP 16"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 17", BallotPaperName = "Name 17", BallotPaperPosition =17, Id =17, NominationStatus = NS.Active, OrderWithinGroupOrTeam=17, PreferredRPPName ="RPP 17"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 18", BallotPaperName = "Name 18", BallotPaperPosition =18, Id =18, NominationStatus = NS.Active, OrderWithinGroupOrTeam=18, PreferredRPPName ="RPP 18"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 19", BallotPaperName = "Name 19", BallotPaperPosition =19, Id =19, NominationStatus = NS.Active, OrderWithinGroupOrTeam=19, PreferredRPPName ="RPP 19"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 20", BallotPaperName = "Name 20", BallotPaperPosition =20, Id =20, NominationStatus = NS.Active, OrderWithinGroupOrTeam=20, PreferredRPPName ="RPP 20"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 21", BallotPaperName = "Name 21", BallotPaperPosition =21, Id =21, NominationStatus = NS.Active, OrderWithinGroupOrTeam=21, PreferredRPPName ="RPP 21"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 22", BallotPaperName = "Name 22", BallotPaperPosition =22, Id =22, NominationStatus = NS.Active, OrderWithinGroupOrTeam=22, PreferredRPPName ="RPP 22"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 23", BallotPaperName = "Name 23", BallotPaperPosition =23, Id =23, NominationStatus = NS.Active, OrderWithinGroupOrTeam=23, PreferredRPPName ="RPP 23"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 24", BallotPaperName = "Name 24", BallotPaperPosition =24, Id =24, NominationStatus = NS.Active, OrderWithinGroupOrTeam=24, PreferredRPPName ="RPP 24"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 25", BallotPaperName = "Name 25", BallotPaperPosition =25, Id =25, NominationStatus = NS.Active, OrderWithinGroupOrTeam=25, PreferredRPPName ="RPP 25"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 26", BallotPaperName = "Name 26", BallotPaperPosition =26, Id =26, NominationStatus = NS.Active, OrderWithinGroupOrTeam=26, PreferredRPPName ="RPP 26"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 27", BallotPaperName = "Name 27", BallotPaperPosition =27, Id =27, NominationStatus = NS.Active, OrderWithinGroupOrTeam=27, PreferredRPPName ="RPP 27"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 28", BallotPaperName = "Name 28", BallotPaperPosition =28, Id =28, NominationStatus = NS.Active, OrderWithinGroupOrTeam=28, PreferredRPPName ="RPP 28"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 29", BallotPaperName = "Name 29", BallotPaperPosition =29, Id =29, NominationStatus = NS.Active, OrderWithinGroupOrTeam=29, PreferredRPPName ="RPP 29"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 30", BallotPaperName = "Name 30", BallotPaperPosition =30, Id =30, NominationStatus = NS.Active, OrderWithinGroupOrTeam=30, PreferredRPPName ="RPP 30"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 31", BallotPaperName = "Name 31", BallotPaperPosition =31, Id =31, NominationStatus = NS.Active, OrderWithinGroupOrTeam=31, PreferredRPPName ="RPP 31"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 32", BallotPaperName = "Name 32", BallotPaperPosition =32, Id =32, NominationStatus = NS.Active, OrderWithinGroupOrTeam=32, PreferredRPPName ="RPP 32"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 33", BallotPaperName = "Name 33", BallotPaperPosition =33, Id =33, NominationStatus = NS.Active, OrderWithinGroupOrTeam=33, PreferredRPPName ="RPP 33"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 34", BallotPaperName = "Name 34", BallotPaperPosition =34, Id =34, NominationStatus = NS.Active, OrderWithinGroupOrTeam=34, PreferredRPPName ="RPP 34"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 35", BallotPaperName = "Name 35", BallotPaperPosition =35, Id =35, NominationStatus = NS.Active, OrderWithinGroupOrTeam=35, PreferredRPPName ="RPP 35"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 36", BallotPaperName = "Name 36", BallotPaperPosition =36, Id =36, NominationStatus = NS.Active, OrderWithinGroupOrTeam=36, PreferredRPPName ="RPP 36"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 37", BallotPaperName = "Name 37", BallotPaperPosition =37, Id =37, NominationStatus = NS.Active, OrderWithinGroupOrTeam=37, PreferredRPPName ="RPP 37"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 38", BallotPaperName = "Name 38", BallotPaperPosition =38, Id =38, NominationStatus = NS.Active, OrderWithinGroupOrTeam=38, PreferredRPPName ="RPP 38"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 39", BallotPaperName = "Name 39", BallotPaperPosition =39, Id =39, NominationStatus = NS.Active, OrderWithinGroupOrTeam=39, PreferredRPPName ="RPP 39"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 40", BallotPaperName = "Name 40", BallotPaperPosition =40, Id =40, NominationStatus = NS.Active, OrderWithinGroupOrTeam=40, PreferredRPPName ="RPP 40"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 41", BallotPaperName = "Name 41", BallotPaperPosition =41, Id =41, NominationStatus = NS.Active, OrderWithinGroupOrTeam=41, PreferredRPPName ="RPP 41"},
                                new BallotDrawnNominationEntity() { BallotPaperLocality= "Locality 42", BallotPaperName = "Name 42", BallotPaperPosition =42, Id =42, NominationStatus = NS.Active, OrderWithinGroupOrTeam=42, PreferredRPPName ="RPP 42"}
                            }
                        },
                }
            };
            parties.Add(ballotDrawGroups);

            var customElectionBL = new Mock<IElectionBL>();
            customElectionBL.Setup(e => e.BallotDrawGroups(It.IsAny<List<Guid>>())).Returns(parties);

            var voteDecryptBL = GetService(new FvVoteContext(), DefaultEventBL, customElectionBL, DefaultFileService);
            var result = voteDecryptBL.DecryptBtlVotes(contextDirectory);

            // Act
            voteDecryptBL.GenerateBtlVotePDFs(contextDirectory, result.VoteDecryptFiles.Select(x => x.Id).ToList());

            File.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "BTLVoteTemplate.pdf"));
            File.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "BTLVoteTemplateA3.pdf"));

            // Assert
            Assert.IsTrue(File.Exists(Path.Combine(contextDirectory.FullName, "Western_Metropolitan_BTL.pdf")), "Could not find the generated PDF: Western Metropolitan.pdf");
        }

        //[TestMethod]
        [DeploymentItem(@"Data\commit.json", "Data")]
        [DeploymentItem(@"Data\cipherFile2790101.json", "Data")]
        [DeploymentItem(@"Data\LA_candidate_ids_plaintext.json", "Data")]
        [DeploymentItem(@"Data\LC_ATL_candidate_ids_plaintext.json", "Data")]
        [DeploymentItem(@"Data\LC_BTL_candidate_ids_plaintext.json", "Data")]
        [DeploymentItem(@"Data\PreMixTemplate.pdf", "Data")]
        public void ProcessCommitVotes_When5Votes_Then3PackFiles2BlankFilesWithCandidateIds()
        {
            // Arrange
            Guid context = Guid.NewGuid();
            DirectoryInfo contextDirectory = TestPath.CreateSubdirectory(context.ToString());

            File.Copy(@"Data\commit.json", Path.Combine(contextDirectory.FullName, "commit.json"));
            File.Copy(@"Data\cipherFile2790101.json", Path.Combine(contextDirectory.FullName, "cipherFile2790101.json"));
            File.Copy(@"Data\LA_candidate_ids_plaintext.json", Path.Combine(contextDirectory.FullName, "LA_candidate_ids_plaintext.json"));
            File.Copy(@"Data\LC_ATL_candidate_ids_plaintext.json", Path.Combine(contextDirectory.FullName, "LC_ATL_candidate_ids_plaintext.json"));
            File.Copy(@"Data\LC_BTL_candidate_ids_plaintext.json", Path.Combine(contextDirectory.FullName, "LC_BTL_candidate_ids_plaintext.json"));
            File.Copy(@"Data\PreMixTemplate.pdf", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PreMixTemplate.pdf"));

            IvVoteContext vcontext = new FvVoteContext();
            vcontext.Events.Add(new Event() { Id = context, WBBServiceHostName = "http://localhost", WBBMServiceHostName = "http://localhost" });
            var eve = vcontext.Events.Single(e => e.Id == context);
            eve.EventLocations.Add(new EventLocation()
            {
                EventId = context,
                LocationCode = 279,
                LocationSubCode = 01,
                RaceName = "Altona",
                ParentRaceName = "Western Metro Region",
                LocationName = "Altona Location",
                Type = 1,
                EventDevices = new List<EventDevice>(new EventDevice[2] { new EventDevice() { DeviceId = 2790101, Type = 1 }, new EventDevice() { DeviceId = 2790102, Type = 2 } })
            });


            IVoteDecryptBL voteDecryptBL = GetService(vcontext, DefaultEventBL, DefaultElectionBL, DefaultFileService);

            // Act
            voteDecryptBL.ProcessCommitVotes(context, contextDirectory, context);

            File.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PreMixTemplate.pdf"));

            // Assert
            Assert.AreEqual(10, eve.PackageCommits.Count);
        }

        //[TestMethod]
        [DeploymentItem(@"Data\commit.json", "Data")]
        [DeploymentItem(@"Data\cipherFile2790101.json", "Data")]
        [DeploymentItem(@"Data\LA_candidate_ids_plaintext.json", "Data")]
        [DeploymentItem(@"Data\LC_ATL_candidate_ids_plaintext.json", "Data")]
        [DeploymentItem(@"Data\LC_BTL_candidate_ids_plaintext.json", "Data")]
        [DeploymentItem(@"Data\PreMixTemplate.pdf", "Data")]
        public void PackageCommitVotes_When5Votes_Then3PackFiles2BlankFilesWithCandidateIds()
        {
            // Arrange
            Guid context = Guid.NewGuid();
            DirectoryInfo contextDirectory = TestPath.CreateSubdirectory(context.ToString());

            File.Copy(@"Data\commit.json", Path.Combine(contextDirectory.FullName, "commit.json"));
            File.Copy(@"Data\cipherFile2790101.json", Path.Combine(contextDirectory.FullName, "cipherFile2790101.json"));
            File.Copy(@"Data\LA_candidate_ids_plaintext.json", Path.Combine(contextDirectory.FullName, "LA_candidate_ids_plaintext.json"));
            File.Copy(@"Data\LC_ATL_candidate_ids_plaintext.json", Path.Combine(contextDirectory.FullName, "LC_ATL_candidate_ids_plaintext.json"));
            File.Copy(@"Data\LC_BTL_candidate_ids_plaintext.json", Path.Combine(contextDirectory.FullName, "LC_BTL_candidate_ids_plaintext.json"));
            File.Copy(@"Data\PreMixTemplate.pdf", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PreMixTemplate.pdf"));

            IvVoteContext vcontext = new FvVoteContext();
            vcontext.Events.Add(new Event() { Id = context, WBBServiceHostName = "http://localhost", WBBMServiceHostName = "http://localhost" });
            var eve = vcontext.Events.Single(e => e.Id == context);
            eve.EventLocations.Add(new EventLocation()
            {
                EventId = context,
                LocationCode = 279,
                LocationSubCode = 01,
                RaceName = "Altona",
                ParentRaceName = "Western Metro Region",
                LocationName = "Altona Location",
                Type = 1,
                EventDevices = new List<EventDevice>(new EventDevice[2] { new EventDevice() { DeviceId = 2790101, Type = 1 }, new EventDevice() { DeviceId = 2790102, Type = 2 } })
            });

            IVoteDecryptBL voteDecryptBL = GetService(vcontext, DefaultEventBL, DefaultElectionBL, DefaultFileService);

            // Act
            voteDecryptBL.ProcessCommitVotes(context, contextDirectory, context);
            voteDecryptBL.PackProcessedVotes(context, contextDirectory, context, new int[3] { 13, 3, 29 });

            File.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PreMixTemplate.pdf"));

            // Assert
            Assert.AreEqual(10, eve.PackageCommits.Count);
            // Candidate IDs exist
            Assert.AreEqual(3, Directory.GetFiles(contextDirectory.FullName, "*.cid").Length);
            // Packages exists
            Assert.AreEqual(3, Directory.GetFiles(contextDirectory.FullName, "*.blt").Length);
            // Blanks exists
            Assert.AreEqual(2, Directory.GetFiles(contextDirectory.FullName, "*.csv").Length);
        }

        [TestMethod]
        public void GetCommitFiles_WhenNoCommitsProcessed_Then3UnprocessedCommitFiles()
        {
            // Arrange
            Guid context = Guid.NewGuid();
            IvVoteContext vcontext = new FvVoteContext();
            var commitFiles = new List<string>();
            commitFiles.Add("1407999600000.json");
            commitFiles.Add("1407999600000_signature.json");
            commitFiles.Add("1408086000000.json");
            commitFiles.Add("1408086000000_signature.json");
            commitFiles.Add("1408604400000ext01.json");
            commitFiles.Add("1408604400000ext01_signature.json");

            vcontext.Events.Add(new Event() { Id = context, WBBServiceHostName = "http://localhost", WBBMServiceHostName = "http://localhost" });
            DefaultFileService.Setup(f => f.List(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(commitFiles);
            IVoteDecryptBL voteDecryptBL = GetService(vcontext, DefaultEventBL, DefaultElectionBL, DefaultFileService);

            // Act
            var result = voteDecryptBL.GetCommitFiles(context);

            // Assert
            Assert.AreEqual(3, result.Count);
        }

        [TestMethod]
        public void GetCommitFiles_When1CommitProcessed_Then2UnprocessedCommitFiles()
        {
            // Arrange
            Guid context = Guid.NewGuid();
            IvVoteContext vcontext = new FvVoteContext();
            var commitFiles = new List<string>();
            commitFiles.Add("1407999600000.json");
            commitFiles.Add("1407999600000_signature.json");
            commitFiles.Add("1408086000000.json");
            commitFiles.Add("1408086000000_signature.json");
            commitFiles.Add("1408604400000ext01.json");
            commitFiles.Add("1408604400000ext01_signature.json");

            var eve = new Event() { Id = context, WBBServiceHostName = "http://localhost", WBBMServiceHostName = "http://localhost" };
            vcontext.Events.Add(eve);
            eve.CommitFiles.Add(new CommitFile() { Id = 1, CommitDateTime = "1407999600000".GetDateTime(), EventId = context, Name = "1407999600000.json", ProcessedDateTime = DateTime.Now });
           
            DefaultFileService.Setup(f => f.List(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(commitFiles);
            IVoteDecryptBL voteDecryptBL = GetService(vcontext, DefaultEventBL, DefaultElectionBL, DefaultFileService);

            // Act
            var result = voteDecryptBL.GetCommitFiles(context);

            // Assert
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result[2].Id);
        }

        [TestMethod]
        public void ProcessCommitFiles_When1CommitToProcess_Then1CommitFileProcessed()
        {
            // Arrange
            Guid context = Guid.NewGuid();
            IvVoteContext vcontext = new FvVoteContext();
            var commitFiles = new List<CommitFileEntity>();
            commitFiles.Add(new CommitFileEntity(){ Name = "1407999600000.json", UnixCommitDateTime= "1407999600000" } );
            commitFiles.Add(new CommitFileEntity(){ Name = "1408086000000.json", UnixCommitDateTime= "1408086000000" } );
            commitFiles.Add(new CommitFileEntity(){ Name = "1408604400000ext01.json", UnixCommitDateTime= "1408604400000" } );

            var eve = new Event() { Id = context, WBBServiceHostName = "http://localhost", WBBMServiceHostName = "http://localhost" };
            vcontext.Events.Add(eve);

            using (MemoryStream ms = new MemoryStream())
            {
                ZipFile zf = new ZipFile();
                zf.Save(ms);

                DefaultFileService.Setup(f => f.GetListed(It.IsAny<string>(), It.IsAny<List<string>>())).Returns(ms.ToArray());
                IVoteDecryptBL voteDecryptBL = GetService(vcontext, DefaultEventBL, DefaultElectionBL, DefaultFileService);

                // Act
                voteDecryptBL.ProcessCommitFiles(context, commitFiles);
            }

            // Assert
            Assert.AreEqual(0, eve.CommitFiles.Count);
        }
    }
}
