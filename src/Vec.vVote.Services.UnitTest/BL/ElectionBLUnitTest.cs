﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vec.Apps.vVote.DAL;
using Vec.vVote.Services.UnitTest.DAL;
using Moq;
using Vec.TuringElectionService;
using Vec.Apps.vVote.BL;
using Vec.TuringNominationService;
using System.Collections.Generic;

namespace Vec.vVote.Services.UnitTest.BL
{
    [TestClass]
    public class ElectionBLUnitTest
    {
        IvVoteContext vVoteContext { get; set; }
        Guid ElectionId { get; set; }

        [TestInitialize]
        public void TestSetUp()
        {
            vVoteContext = new FvVoteContext();
            ElectionId = Guid.Parse("57474254-4f91-4125-8b89-ad4113387f78");
        }

        private ElectionBL GetService(Mock<IElectionLookup> electionLookup, Mock<INominationsExternalService> nominationLookup, IvVoteContext vVoteContext)
        {
            return new ElectionBL(electionLookup.Object, nominationLookup.Object, vVoteContext);
        }

        [TestMethod]
        public void GetElections_WhenGeneralElection_ThenElectionListIsReturned()
        {
            // Arrange
            Mock<IElectionLookup> electionLookup = new Mock<IElectionLookup>();
            Mock<INominationsExternalService> nominationLookup = new Mock<INominationsExternalService>();
            electionLookup.Setup(e => e.GetElection(It.IsAny<Guid>())).Returns(new MElectionLookup()
            {
                Id = ElectionId,
                Name = "State Election 2010",
                ElectionType = TuringElectionService.ElectionType.GeneralElection,
                DateRanges = new MElectionDateRange[] { new MElectionDateRange { ElectionId = ElectionId, DateRangeType = ElectionDateRangeType.ElectionDay, StartDateTime = DateTime.Today.AddDays(-10) } }
            });

            IElectionBL electionBL = GetService(electionLookup, nominationLookup, vVoteContext);
            
            // Act
            List<MElectionLookup> elections = electionBL.GetElections(ElectionType.GeneralElection);

            // Assert
            Assert.AreEqual(elections.Count, 0);
        }


        [TestMethod]
        public void GetElections_WhenElectionIdList_ThenElectionListIsReturned()
        {
            // Arrange
            List<Guid> electionIds = new List<Guid>();
            electionIds.Add(ElectionId);

            Mock<IElectionLookup> electionLookup = new Mock<IElectionLookup>();
            Mock<INominationsExternalService> nominationLookup = new Mock<INominationsExternalService>();
            electionLookup.Setup(e => e.GetElection(It.IsAny<Guid>())).Returns(new MElectionLookup()
            {
                Id = ElectionId,
                Name = "State Election 2010",
                ElectionType = TuringElectionService.ElectionType.GeneralElection,
                DateRanges = new MElectionDateRange[] { new MElectionDateRange { ElectionId = ElectionId, DateRangeType = ElectionDateRangeType.ElectionDay, StartDateTime = DateTime.Today.AddDays(-10) } }
            });

            IElectionBL electionBL = GetService(electionLookup, nominationLookup, vVoteContext);

            // Act
            List<MElectionLookup> elections = electionBL.GetElections(electionIds);

            // Assert
            Assert.AreEqual(elections.Count, 0);
        }

        [TestMethod]
        public void GetRaces_WhenElectionIdAndStatus_ThenMElectionLookupListIsReturned()
        {
            // Arrange
            List<Guid> electionIds = new List<Guid>();
            electionIds.Add(ElectionId);

            Mock<IElectionLookup> electionLookup = new Mock<IElectionLookup>();
            Mock<INominationsExternalService> nominationLookup = new Mock<INominationsExternalService>();
            MElectionLookup eleLookup = new MElectionLookup()
            {
                Id = ElectionId,
                Name = "State Election 2010",
                ElectionType = TuringElectionService.ElectionType.GeneralElection,
                DateRanges = new MElectionDateRange[] { new MElectionDateRange { ElectionId = ElectionId, DateRangeType = ElectionDateRangeType.ElectionDay, StartDateTime = DateTime.Today.AddDays(-10) } }
            };
            eleLookup.ParentElectorates = new MElectorateLookup[1] { 
                new MElectorateLookup() { 
                    Electorates = new MElectorateLookup[1] {
                        new MElectorateLookup(){ Locations = 
                            new MLocationLookup[1] { 
                                new MLocationLookup() { Id = Guid.NewGuid(), Name ="Dummy Location" }
                            },
                            ElectorateVacancyConfiguration = new MElectionConfig(){ ElectionElectorateVacancyStatus = TuringElectionService.ElectionElectorateVacancyStatus.Contested}
                        }
                    } 
                } 
            };

            electionLookup.Setup(e => e.GetElection(It.IsAny<Guid>())).Returns(eleLookup);

            IElectionBL electionBL = GetService(electionLookup, nominationLookup, vVoteContext);

            // Act
            List<MElectorateLookup> races = electionBL.GetRaces(electionIds, ElectionElectorateVacancyStatus.Contested);

            // Assert
            Assert.AreEqual(races.Count, 1);
        }

        [TestMethod]
        public void GetRaces_WhenElectionIdAndStatusIsNull_ThenMElectionLookupListIsReturned()
        {
            // Arrange
            List<Guid> electionIds = new List<Guid>();
            electionIds.Add(ElectionId);

            Mock<IElectionLookup> electionLookup = new Mock<IElectionLookup>();
            Mock<INominationsExternalService> nominationLookup = new Mock<INominationsExternalService>();
            MElectionLookup eleLookup = new MElectionLookup()
            {
                Id = ElectionId,
                Name = "State Election 2010",
                ElectionType = TuringElectionService.ElectionType.GeneralElection,
                DateRanges = new MElectionDateRange[] { new MElectionDateRange { ElectionId = ElectionId, DateRangeType = ElectionDateRangeType.ElectionDay, StartDateTime = DateTime.Today.AddDays(-10) } }
            };
            eleLookup.ParentElectorates = new MElectorateLookup[1] { 
                new MElectorateLookup() { 
                    Electorates = new MElectorateLookup[2] {
                        new MElectorateLookup(){ Locations = 
                            new MLocationLookup[1] { 
                                new MLocationLookup() { Id = Guid.NewGuid(), Name ="Dummy Location 1" }
                            },
                            ElectorateVacancyConfiguration = new MElectionConfig(){ ElectionElectorateVacancyStatus = TuringElectionService.ElectionElectorateVacancyStatus.Contested}
                        },
                        new MElectorateLookup(){ Locations = 
                            new MLocationLookup[1] { 
                                new MLocationLookup() { Id = Guid.NewGuid(), Name ="Dummy Location 2" }
                            },
                            ElectorateVacancyConfiguration = new MElectionConfig(){ ElectionElectorateVacancyStatus = TuringElectionService.ElectionElectorateVacancyStatus.Uncontested}
                        }
                    } 
                } 
            };

            electionLookup.Setup(e => e.GetElection(It.IsAny<Guid>())).Returns(eleLookup);

            IElectionBL electionBL = GetService(electionLookup, nominationLookup, vVoteContext);

            // Act
            List<MElectorateLookup> races = electionBL.GetRaces(electionIds, null);

            // Assert
            Assert.AreEqual(races.Count, 2);
        }

        [TestMethod]
        public void GetElectionLocations_WhenElectionIdList_ThenElectionLocationListIsReturned()
        {
            // Arrange
            List<Guid> electionIds = new List<Guid>();
            electionIds.Add(ElectionId);

            Mock<IElectionLookup> electionLookup = new Mock<IElectionLookup>();
            Mock<INominationsExternalService> nominationLookup = new Mock<INominationsExternalService>();
            MElectionLookup eleLookup = new MElectionLookup()
            {
                Id = ElectionId,
                Name = "State Election 2010",
                ElectionType = TuringElectionService.ElectionType.GeneralElection,
                DateRanges = new MElectionDateRange[] { new MElectionDateRange { ElectionId = ElectionId, DateRangeType = ElectionDateRangeType.ElectionDay, StartDateTime = DateTime.Today.AddDays(-10) } }
            };
            eleLookup.ParentElectorates = new MElectorateLookup[1] { 
                new MElectorateLookup() { 
                    Electorates = new MElectorateLookup[1] {
                        new MElectorateLookup(){ Locations = 
                            new MLocationLookup[1] { 
                                new MLocationLookup() { Id = Guid.NewGuid(), Name ="Dummy Location" }
                            }
                        }
                    } 
                } 
            };

            electionLookup.Setup(e => e.GetElection(It.IsAny<Guid>())).Returns(eleLookup);

            IElectionBL electionBL = GetService(electionLookup, nominationLookup, vVoteContext);

            // Act
            List<MLocationLookup> locatioins = electionBL.GetElectionLocations(electionIds);

            // Assert
            Assert.AreEqual(locatioins.Count, 1);
        }

        [TestMethod]
        public void GetNominations_WhenElectorateVacencyIdList_ThenCandidateInNominationOrderEntityListIsReturned()
        {
            // Arrange
            List<Guid> electionIds = new List<Guid>();
            electionIds.Add(ElectionId);

            Mock<IElectionLookup> electionLookup = new Mock<IElectionLookup>();
            Mock<INominationsExternalService> nominationLookup = new Mock<INominationsExternalService>();
            MElectionLookup eleLookup = new MElectionLookup()
            {
                Id = ElectionId,
                Name = "State Election 2010",
                ElectionType = TuringElectionService.ElectionType.GeneralElection,
                DateRanges = new MElectionDateRange[] { new MElectionDateRange { ElectionId = ElectionId, DateRangeType = ElectionDateRangeType.ElectionDay, StartDateTime = DateTime.Today.AddDays(-10) } }
            };
            eleLookup.ParentElectorates = new MElectorateLookup[1] { 
                new MElectorateLookup() { 
                    Electorates = new MElectorateLookup[1] {
                        new MElectorateLookup(){ Locations = 
                            new MLocationLookup[1] { 
                                new MLocationLookup() { Id = Guid.NewGuid(), Name ="Dummy Location" }
                            }
                        }
                    } 
                } 
            };

            electionLookup.Setup(e => e.GetElection(It.IsAny<Guid>())).Returns(eleLookup);

            nominationLookup.Setup(n => n.GetCandidatesInNominationOrder(It.IsAny<Guid[]>())).Returns(new CandidatesInNominationOrderForElectionElectorateVacancyEntity[1] {
                new CandidatesInNominationOrderForElectionElectorateVacancyEntity(){ ElectionElectorateVacancyId = Guid.NewGuid(), CandidatesInNominationOrder = 
                    new CandidateInNominationOrderEntity[1] {new CandidateInNominationOrderEntity() { Id = 1, BallotPaperName = "Dummy Candidate"}
                    }
                }
            });

            IElectionBL electionBL = GetService(electionLookup, nominationLookup, vVoteContext);

            List<Guid> electorateVacencyIds = new List<Guid>();
            electorateVacencyIds.Add(Guid.NewGuid());

            // Act
            List<CandidatesInNominationOrderForElectionElectorateVacancyEntity> locatioins = electionBL.GetNominations(electorateVacencyIds);

            // Assert
            Assert.AreEqual(locatioins.Count, 1);
        }

        [TestMethod]
        public void BallotDrawCandidates_WhenElectorateVacencyIdList_ThenCandidateInNominationOrderEntityListIsReturned()
        {
            // Arrange
            List<Guid> electionIds = new List<Guid>();
            electionIds.Add(ElectionId);

            Mock<IElectionLookup> electionLookup = new Mock<IElectionLookup>();
            Mock<INominationsExternalService> nominationLookup = new Mock<INominationsExternalService>();
            MElectionLookup eleLookup = new MElectionLookup()
            {
                Id = ElectionId,
                Name = "State Election 2010",
                ElectionType = TuringElectionService.ElectionType.GeneralElection,
                DateRanges = new MElectionDateRange[] { new MElectionDateRange { ElectionId = ElectionId, DateRangeType = ElectionDateRangeType.ElectionDay, StartDateTime = DateTime.Today.AddDays(-10) } }
            };
            eleLookup.ParentElectorates = new MElectorateLookup[1] { 
                new MElectorateLookup() { 
                    Electorates = new MElectorateLookup[1] {
                        new MElectorateLookup(){ Locations = 
                            new MLocationLookup[1] { 
                                new MLocationLookup() { Id = Guid.NewGuid(), Name ="Dummy Location" }
                            }
                        }
                    } 
                } 
            };

            electionLookup.Setup(e => e.GetElection(It.IsAny<Guid>())).Returns(eleLookup);

            nominationLookup.Setup(n => n.GetBallotDrawnCandidatesForElectionElectorateVacancyIds(It.IsAny<Guid[]>())).Returns(new  BallotDrawnCandidatesForElectionElectorateVacancyEntity[1] {
                new BallotDrawnCandidatesForElectionElectorateVacancyEntity(){ ElectionElectorateVacancyId = Guid.NewGuid(), BallotDrawnCandidates = 
                    new BallotDrawnNominationEntity[1] {new BallotDrawnNominationEntity() { Id = 1, BallotPaperName = "Dummy Candidate"}
                    }
                }
            });

            IElectionBL electionBL = GetService(electionLookup, nominationLookup, vVoteContext);

            List<Guid> electorateVacencyIds = new List<Guid>();
            electorateVacencyIds.Add(Guid.NewGuid());

            // Act
            List<BallotDrawnCandidatesForElectionElectorateVacancyEntity> candidates = electionBL.BallotDrawCandidates(electorateVacencyIds);

            // Assert
            Assert.AreEqual(candidates.Count, 1);
        }

        [TestMethod]
        public void BallotDrawGroups_WhenElectorateVacencyIdList_ThenCandidateInNominationOrderEntityListIsReturned()
        {
            // Arrange
            List<Guid> electionIds = new List<Guid>();
            electionIds.Add(ElectionId);

            Mock<IElectionLookup> electionLookup = new Mock<IElectionLookup>();
            Mock<INominationsExternalService> nominationLookup = new Mock<INominationsExternalService>();
            MElectionLookup eleLookup = new MElectionLookup()
            {
                Id = ElectionId,
                Name = "State Election 2010",
                ElectionType = TuringElectionService.ElectionType.GeneralElection,
                DateRanges = new MElectionDateRange[] { new MElectionDateRange { ElectionId = ElectionId, DateRangeType = ElectionDateRangeType.ElectionDay, StartDateTime = DateTime.Today.AddDays(-10) } }
            };
            eleLookup.ParentElectorates = new MElectorateLookup[1] { 
                new MElectorateLookup() { 
                    Electorates = new MElectorateLookup[1] {
                        new MElectorateLookup(){ Locations = 
                            new MLocationLookup[1] { 
                                new MLocationLookup() { Id = Guid.NewGuid(), Name ="Dummy Location" }
                            }
                        }
                    } 
                } 
            };

            electionLookup.Setup(e => e.GetElection(It.IsAny<Guid>())).Returns(eleLookup);

            nominationLookup.Setup(n => n.GetBallotDrawnGroupsForElectionElectorateVacancyIds(It.IsAny<Guid[]>())).Returns(new BallotDrawnGroupsForElectionElectorateVacancy[1] {
                new BallotDrawnGroupsForElectionElectorateVacancy(){ ElectionElectorateVacancyId = Guid.NewGuid(), BallotDrawnGroups = 
                    new BallotDrawnGroupEntity[1] {new BallotDrawnGroupEntity() { Id = 1, Name = "Dummy Party"}
                    }
                }
            });

            IElectionBL electionBL = GetService(electionLookup, nominationLookup, vVoteContext);

            List<Guid> electorateVacencyIds = new List<Guid>();
            electorateVacencyIds.Add(Guid.NewGuid());

            // Act
            List<BallotDrawnGroupsForElectionElectorateVacancy> groups = electionBL.BallotDrawGroups(electorateVacencyIds);

            // Assert
            Assert.AreEqual(groups.Count, 1);
        }
    }
}
