﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vec.Apps.vVote.DAL;
using Vec.vVote.Services.UnitTest.DAL;
using Vec.Apps.vVote.BL;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Vec.Apps.vVote.Entities;
using Vec.TuringElectionService;
using System.IO;
using Vec.TuringNominationService;
using Vec.WbbFileService;
using Vec.Apps.vVote.BL.Common;

namespace Vec.vVote.Services.UnitTest.BL
{
    [TestClass]
    public class TelemetryBLUnitTest
    {
        IvVoteContext vVoteContext { get; set; }
        Guid EventId { get; set; }
        Mock<IEventBL> DefaultEventBL { get; set; }
        
        [TestInitialize]
        public void TestSetUp()
        {
            vVoteContext = new FvVoteContext();
            DefaultEventBL = new Mock<IEventBL>();
            EventId = Guid.Parse("57474254-4f91-4125-8b89-ad4113387f78");

            vVoteContext.TelemetryMBBs.Add(new TelemetryMBB() { Id = 1, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Message = "Test", MessageType = "Test", Level = "INFO" });
            vVoteContext.TelemetryMBBs.Add(new TelemetryMBB() { Id = 2, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Message = "Test", MessageType = "Test", Level = "INFO" });
            vVoteContext.TelemetryMBBs.Add(new TelemetryMBB() { Id = 3, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Message = "Test", MessageType = "Test", Level = "INFO" });
            vVoteContext.TelemetryMBBs.Add(new TelemetryMBB() { Id = 4, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Message = "Test", MessageType = "Test", Level = "INFO" });
            vVoteContext.TelemetryMBBs.Add(new TelemetryMBB() { Id = 5, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Message = "Test", MessageType = "Test", Level = "INFO" });
            vVoteContext.TelemetryMBBs.Add(new TelemetryMBB() { Id = 6, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Message = "Test", MessageType = "Test", Level = "INFO" });
            vVoteContext.TelemetryMBBs.Add(new TelemetryMBB() { Id = 7, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Message = "Test", MessageType = "Test", Level = "INFO" });
            vVoteContext.TelemetryMBBs.Add(new TelemetryMBB() { Id = 8, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Message = "Test", MessageType = "Test", Level = "INFO" });
            vVoteContext.TelemetryMBBs.Add(new TelemetryMBB() { Id = 9, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Message = "Test", MessageType = "Test", Level = "INFO" });
            vVoteContext.TelemetryMBBs.Add(new TelemetryMBB() { Id = 10, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Message = "Test", MessageType = "Test", Level = "INFO" });

            vVoteContext.TelemetryWBBs.Add(new TelemetryWBB() { Id = 1, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Resource = "Test", Status = 200, Level = "INFO" });
            vVoteContext.TelemetryWBBs.Add(new TelemetryWBB() { Id = 2, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Resource = "Test", Status = 200, Level = "INFO" });
            vVoteContext.TelemetryWBBs.Add(new TelemetryWBB() { Id = 3, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Resource = "Test", Status = 200, Level = "INFO" });
            vVoteContext.TelemetryWBBs.Add(new TelemetryWBB() { Id = 4, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Resource = "Test", Status = 200, Level = "INFO" });
            vVoteContext.TelemetryWBBs.Add(new TelemetryWBB() { Id = 5, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Resource = "Test", Status = 200, Level = "INFO" });
            vVoteContext.TelemetryWBBs.Add(new TelemetryWBB() { Id = 6, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Resource = "Test", Status = 200, Level = "INFO" });
            vVoteContext.TelemetryWBBs.Add(new TelemetryWBB() { Id = 7, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Resource = "Test", Status = 200, Level = "INFO" });
            vVoteContext.TelemetryWBBs.Add(new TelemetryWBB() { Id = 8, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Resource = "Test", Status = 200, Level = "INFO" });
            vVoteContext.TelemetryWBBs.Add(new TelemetryWBB() { Id = 9, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Resource = "Test", Status = 200, Level = "INFO" });
            vVoteContext.TelemetryWBBs.Add(new TelemetryWBB() { Id = 10, EventId = EventId, DateTime = DateTime.UtcNow, From = "Test", Resource = "Test", Status = 200, Level = "INFO" });
        }

        private TelemetryBL GetService(IvVoteContext vVoteContext, IEventBL eventBL)
        {
            return new TelemetryBL(vVoteContext, eventBL);
        }

        [TestMethod]
        public void GetMBBTelemetryData_When1Record_ThenTop1RecordsReturned()
        {
            // Arrange
            ITelemetryBL telemetryBL = GetService(vVoteContext, DefaultEventBL.Object);

            // Act
            var mbbTelemetryRecords = telemetryBL.GetMBBTelemetryData(EventId, 1);

            // Assert
            Assert.AreEqual(mbbTelemetryRecords.Count(), 1);
        }
        
        [TestMethod]
        public void GetMBBTelemetryData_When10Record_ThenTop10RecordsReturned()
        {
            // Arrange
            ITelemetryBL telemetryBL = GetService(vVoteContext, DefaultEventBL.Object);

            // Act
            var mbbTelemetryRecords = telemetryBL.GetMBBTelemetryData(EventId, 10);

            // Assert
            Assert.AreEqual(mbbTelemetryRecords.Count(), 10);
        }

        [TestMethod]
        public void GetMBBTelemetryData_WhenInvalidEventId_ThenTop0RecordsReturned()
        {
            // Arrange
            ITelemetryBL telemetryBL = GetService(vVoteContext, DefaultEventBL.Object);

            // Act
            var mbbTelemetryRecords = telemetryBL.GetMBBTelemetryData(Guid.NewGuid(), 10);

            // Assert
            Assert.AreEqual(mbbTelemetryRecords.Count(), 0);
        }

        [TestMethod]
        public void GetWBBTelemetryData_When1Record_ThenTop1RecordsReturned()
        {
            // Arrange
            ITelemetryBL telemetryBL = GetService(vVoteContext, DefaultEventBL.Object);

            // Act
            var wbbTelemetryRecords = telemetryBL.GetWBBTelemetryData(EventId, 1);

            // Assert
            Assert.AreEqual(wbbTelemetryRecords.Count(), 1);
        }

        [TestMethod]
        public void GetWBBTelemetryData_When10Record_ThenTop10RecordsReturned()
        {
            // Arrange
            ITelemetryBL telemetryBL = GetService(vVoteContext, DefaultEventBL.Object);

            // Act
            var wbbTelemetryRecords = telemetryBL.GetWBBTelemetryData(EventId, 10);

            // Assert
            Assert.AreEqual(wbbTelemetryRecords.Count(), 10);
        }

        [TestMethod]
        public void GetWBBTelemetryData_WhenInvalidEventId_ThenTop0RecordsReturned()
        {
            // Arrange
            ITelemetryBL telemetryBL = GetService(vVoteContext, DefaultEventBL.Object);

            // Act
            var wbbTelemetryRecords = telemetryBL.GetWBBTelemetryData(Guid.NewGuid(), 10);

            // Assert
            Assert.AreEqual(wbbTelemetryRecords.Count(), 0);
        }

        [TestCleanup]
        public void CleanUp()
        {
        }
    }
}
