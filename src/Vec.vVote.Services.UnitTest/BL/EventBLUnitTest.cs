﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vec.Apps.vVote.DAL;
using Vec.vVote.Services.UnitTest.DAL;
using Vec.Apps.vVote.BL;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Vec.Apps.vVote.Entities;
using Vec.TuringElectionService;
using System.IO;
using Vec.TuringNominationService;
using Vec.WbbFileService;
using Vec.Apps.vVote.BL.Common;

namespace Vec.vVote.Services.UnitTest.BL
{
    [TestClass]
    public class EventBLUnitTest
    {
        IvVoteContext vVoteContext { get; set; }
        Guid ElectionId { get; set; }
        Mock<IElectionBL> DefaultElectionBL { get; set; }
        Mock<IFileServiceHelper> DefaultFileService { get; set; }
        Mock<IAudioHelper> DefaultAudioHelper { get; set; }

        DirectoryInfo TestPath { get; set; }
        //DirectoryInfo ContextDirectory { get; set; }
        //Guid Context { get; set; }

        [TestInitialize]
        public void TestSetUp()
        {
            //Context = Guid.NewGuid();
            //if (!Directory.Exists(Path.Combine(@"C:\Temp\vVote", Context.ToString())))
            //    Directory.CreateDirectory(Path.Combine(@"C:\Temp\vVote", Context.ToString()));
            //ContextDirectory = new DirectoryInfo(Path.Combine(@"C:\Temp\vVote", Context.ToString()));
            vVoteContext = new FvVoteContext();
            DefaultElectionBL = new Mock<IElectionBL>();
            DefaultFileService = new Mock<IFileServiceHelper>();
            DefaultAudioHelper = new Mock<IAudioHelper>();

            TestPath = Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Guid.NewGuid().ToString()));
            ElectionId = Guid.Parse("57474254-4f91-4125-8b89-ad4113387f78");

            List<MElectionLookup> elections = new List<MElectionLookup>();
            elections.Add(new MElectionLookup()
            {
                Id = ElectionId,
                Name = "State Election 2010",
                ElectionType = TuringElectionService.ElectionType.GeneralElection,
                DateRanges = new MElectionDateRange[] { new MElectionDateRange { ElectionId = ElectionId, DateRangeType = ElectionDateRangeType.ElectionDay, StartDateTime = DateTime.Today.AddDays(-10) } },
                ParentElectorates = new MElectorateLookup[1] { 
                    new MElectorateLookup() { 
                        Electorates = new MElectorateLookup[2] {
                            new MElectorateLookup(){ Locations = 
                                new MLocationLookup[1] { 
                                    new MLocationLookup() { Id = Guid.NewGuid(), Name ="Dummy Location 1" }
                                },
                                ElectorateVacancyConfiguration = new MElectionConfig(){ ElectionElectorateVacancyId = Guid.NewGuid(), ElectionElectorateVacancyStatus = TuringElectionService.ElectionElectorateVacancyStatus.Contested}
                            },
                            new MElectorateLookup(){ Locations = 
                                new MLocationLookup[1] { 
                                    new MLocationLookup() { Id = Guid.NewGuid(), Name ="Dummy Location 2" }
                                },
                                ElectorateVacancyConfiguration = new MElectionConfig(){ ElectionElectorateVacancyId = Guid.NewGuid(), ElectionElectorateVacancyStatus = TuringElectionService.ElectionElectorateVacancyStatus.Uncontested}
                            }
                        } 
                    }
                }
            });

            var candidateNomList = new List<CandidatesInNominationOrderForElectionElectorateVacancyEntity>();
            var candidate = new CandidatesInNominationOrderForElectionElectorateVacancyEntity()
            {
                ElectionElectorateVacancyId = new Guid(),
                CandidatesInNominationOrder = (new CandidateInNominationOrderEntity[2] {
                    new CandidateInNominationOrderEntity() { Id = 1, BallotPaperName = "Dummy Candidate 1", NominationDateTime = DateTime.Now.AddDays(-10) },
                    new CandidateInNominationOrderEntity() { Id = 1, BallotPaperName = "Dummy Candidate 2", NominationDateTime = DateTime.Now.AddDays(-5) }
            })
            };
            candidateNomList.Add(candidate);

            DefaultElectionBL.Setup(e => e.GetElections(It.IsAny<List<Guid>>())).Returns(elections);
            DefaultElectionBL.Setup(e => e.GetRaces(It.IsAny<List<Guid>>(), null)).Returns(elections.First().ParentElectorates.First().Electorates.ToList());
            DefaultElectionBL.Setup(e => e.GetNominations(It.IsAny<List<Guid>>())).Returns(candidateNomList);
            DefaultElectionBL.Setup(e => e.BallotDrawGroups(It.IsAny<List<Guid>>())).Returns((new BallotDrawnGroupsForElectionElectorateVacancy[1] {
                new BallotDrawnGroupsForElectionElectorateVacancy(){ ElectionElectorateVacancyId = Guid.NewGuid(), BallotDrawnGroups = 
                    new BallotDrawnGroupEntity[1] {new BallotDrawnGroupEntity() { Id = 1, Name = "Dummy Party"}
                    }
                }
            }).ToList());
        }

        private EventBL GetService(IvVoteContext vVoteContext, IElectionBL electionBL)
        {
            return new EventBL(vVoteContext, electionBL, DefaultFileService.Object, DefaultAudioHelper.Object);
        }

        [TestMethod]
        public void GetEvents_WhenNothing_ThenEventEntityListIsReturned()
        {
            // Arrange
            vVoteContext.Events.Add(new Event() { Id = Guid.NewGuid(), Name = "State Election 2010" });
            IEventBL eventBL = GetService(vVoteContext, DefaultElectionBL.Object);

            // Act
            List<EventEntity> events = eventBL.GetEvents();

            // Assert
            Assert.AreEqual(events.Count, 1);
        }

        [TestMethod]
        public void GetEvent_WhenEventId_ThenEventEntityIsReturned()
        {
            // Arrange
            Guid eventId = Guid.NewGuid();
            vVoteContext.Events.Add(new Event() { Id = eventId, Name = "State Election 2010" });

            IEventBL eventBL = GetService(vVoteContext, DefaultElectionBL.Object);

            // Act
            EventEntity eve = eventBL.GetEvent(eventId);

            // Assert
            Assert.AreEqual(eve.Id, eventId);
        }

        [TestMethod]
        public void GetOrCreateEvent_WhenElectionIdsAndContextDirectory_ThenEventIdIsReturned()
        {
            // Arrange
            IEventBL eventBL = GetService(vVoteContext, DefaultElectionBL.Object);
            List<MElectionLookup> elections = DefaultElectionBL.Object.GetElections(It.IsAny<List<Guid>>());
            var electionIds = from ele in elections
                              select ele.Id;

            //// Act
            //Guid eventId = eventBL.GetOrCreateEvent(electionIds.ToList(), ContextDirectory);

            //// Assert
            //Assert.AreNotEqual(Guid.Empty, eventId);
        }

        [TestMethod]
        public void UpdateEvent_WhenEventEntity_ThenEventEntityIsReturned()
        {
            // Arrange
            IEventBL eventBL = GetService(vVoteContext, DefaultElectionBL.Object);
            List<MElectionLookup> elections = DefaultElectionBL.Object.GetElections(It.IsAny<List<Guid>>());
            var electionIds = from ele in elections
                              select ele.Id;
            //Guid expectedEventId = eventBL.GetOrCreateEvent(electionIds.ToList(), ContextDirectory);

            //// Act
            //EventEntity eventEntity = eventBL.UpdateEvent(new EventEntity { Id = expectedEventId, Name = "Dummy Event" });

            //// Assert
            //Assert.AreEqual(expectedEventId, eventEntity.Id);
        }

        [TestMethod]
        public void UpdateEventSignature_WhenEventEntity_ThenEventEntityIsReturned()
        {
            // Arrange
            IEventBL eventBL = GetService(vVoteContext, DefaultElectionBL.Object);
            List<MElectionLookup> elections = DefaultElectionBL.Object.GetElections(It.IsAny<List<Guid>>());
            var electionIds = from ele in elections
                              select ele.Id;
            //Guid expectedEventId = eventBL.GetOrCreateEvent(electionIds.ToList(), ContextDirectory);

            //// Act
            //eventBL.UpdateEventSignature(expectedEventId, new byte[1] { 23 });

            // Assert
            // Nothing
        }

        [TestMethod]
        public void GetParties_WhenEventIdAndDirectoryContext_ThenPartyEntityListIsReturned()
        {
            // Arrange
            IEventBL eventBL = GetService(vVoteContext, DefaultElectionBL.Object);
            List<MElectionLookup> elections = DefaultElectionBL.Object.GetElections(It.IsAny<List<Guid>>());
            var electionIds = from ele in elections
                              select ele.Id;
            //Guid eventId = eventBL.GetOrCreateEvent(electionIds.ToList(), ContextDirectory);

            //// Act
            //List<PartyEntity> parties = eventBL.GetParties(eventId, ContextDirectory);

            //// Assert
            //Assert.AreEqual(parties.Count, 1);
        }

        [TestMethod]
        public void GetCandidates_WhenEventIdAndDirectoryContext_ThenCandidateEntityListIsReturned()
        {    // Arrange
            IEventBL eventBL = GetService(vVoteContext, DefaultElectionBL.Object);
            List<MElectionLookup> elections = DefaultElectionBL.Object.GetElections(It.IsAny<List<Guid>>());
            var electionIds = from ele in elections
                              select ele.Id;
            //Guid eventId = eventBL.GetOrCreateEvent(electionIds.ToList(), ContextDirectory);

            //// Act
            //List<CandidateEntity> candidates = eventBL.GetCandidates(eventId, ContextDirectory);

            //// Assert
            //Assert.AreEqual(candidates.Count, 2);
        }

        [TestMethod]
        public void GetEventDevices_WhenEventId_ThenEventDeviceEntityListIsReturned()
        {
            // Arrange
            IEventBL eventBL = GetService(vVoteContext, DefaultElectionBL.Object);
            List<MElectionLookup> elections = DefaultElectionBL.Object.GetElections(It.IsAny<List<Guid>>());
            var electionIds = from ele in elections
                              select ele.Id;
            //Guid eventId = eventBL.GetOrCreateEvent(electionIds.ToList(), ContextDirectory);
            //vVoteContext.Events.First().EventDevices.Add(new EventDevice() { Id = Guid.NewGuid(), EventId = eventId, Location = new Location() { Id = Guid.NewGuid(), Name = String.Empty }, Type = 1 });

            //// Act
            //List<EventDeviceEntity> eventDevices = eventBL.GetEventDevices(eventId);

            //// Assert
            //Assert.AreEqual(eventDevices.Count, 1);
        }

        [TestMethod]
        [Ignore]
        [DeploymentItem(@"Data\eb-private.bpg", "Data")]
        [DeploymentItem(@"Data\eb-public.bpg", "Data")]
        [DeploymentItem(@"Data\Quarantine.xlsx", "Data")]
        [DeploymentItem(@"Data\Quarantine.xlsx.bpg", "Data")]
        public void UploadQuarentine_WhenValidSignatureAnd15SerialNo_ThenSignatureValidAndUploadedToWbb()
        {
            // Arrange
            Guid context = Guid.NewGuid();
            DirectoryInfo contextDirectory = TestPath.CreateSubdirectory(context.ToString());

            File.Copy(@"Data\eb-private.bpg", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "eb-private.bpg"));
            File.Copy(@"Data\eb-public.bpg", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "eb-public.bpg"));
            File.Copy(@"Data\Quarantine.xlsx", Path.Combine(contextDirectory.FullName, "Quarantine.xlsx"));
            File.Copy(@"Data\Quarantine.xlsx.bpg", Path.Combine(contextDirectory.FullName, "Quarantine.xlsx.bpg"));

            IvVoteContext vcontext = new FvVoteContext();
            vcontext.Events.Add(new Event() { Id = context, WBBServiceHostName = "http://localhost", WBBMServiceHostName = "http://localhost" });
            var eve = vcontext.Events.Single(e => e.Id == context);

            IEventBL eventBL = GetService(vcontext, DefaultElectionBL.Object);

            // Act
            string result = eventBL.UploadQuarantine(context, Path.Combine(contextDirectory.FullName, "Quarantine.xlsx"), Path.Combine(contextDirectory.FullName, "Quarantine.xlsx.bpg"));

            File.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "eb-private.bpg"));
            File.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "eb-public.bpg"));

            // Assert
            Assert.AreEqual("http://localhost/WbbServices/data/Quarantine.xlsx", result);
        }

        [TestCleanup]
        public void CleanUp()
        {
            TestPath.Delete(true);
        }
    }
}
