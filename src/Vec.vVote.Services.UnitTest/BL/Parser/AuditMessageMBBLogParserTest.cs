﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vec.Apps.vVote.DAL;
using Vec.vVote.Services.UnitTest.DAL;
using Moq;
using Vec.Apps.vVote.Entities;
using Vec.Apps.vVote.BL.Handler;

namespace Vec.vVote.Services.UnitTest.BL.Parser
{
    [TestClass]
    public class AuditMessageMBBLogParserTest
    {
        IvVoteContext vVoteContext { get; set; }
        Guid EventId { get; set; }

        [TestInitialize]
        public void TestSetUp()
        {
            vVoteContext = new FvVoteContext();
            EventId = Guid.Parse("57474254-4f91-4125-8b89-ad4113387f78");
        }

        [TestMethod]
        public void ParseAudit_WhenValidAuditMessage_ThenDBEntryToTelemetryMBB()
        {
            // Arrange
            //{"commitWitness":"t7M53RJepmX6KUA4NzQ3xL0c6u3a9IP7Zlk14mO8Zq8=","_reducedPerms":"9,12,5,1,7,8,11,6,3,4,10,2,0:3,4,5,1,2,6,0:31,4,49,46,0,40,20,1,19,32,16,12,14,29,21,24,33,17,34,25,2,18,48,8,11,15,35,27,50,3,6,47,28,30,53,13,10,41,42,43,7,52,26,45,44,22,37,38,36,39,9,23,5,51:","commitTime":"1407913200000","district":"Yuroke","boothSig":"TeoV1Dd+QPsTEFaJiS6FNwvm77UQxET9ZEDUMp1ywzi2X99ISyzKZQ==","serialSig":"NiACEXVKEAA3QggfdXSuuYjGqWxc+n/9bLJMUARtO6GWD07HIoO/NQ==","type":"audit","boothID":"5150101","serialNo":"5150101:14","permutation":"18,13,16,9,12,5,15,14,1,7,8,19,17,11,6,3,4,10,2,0:19,3,14,4,5,17,7,18,8,15,1,13,2,9,6,10,11,12,16,0:78,31,4,49,59,65,46,91,96,95,0,40,69,77,20,98,1,92,93,82,19,32,16,12,14,66,29,56,99,58,68,21,24,86,33,89,88,73,87,17,57,34,25,71,64,2,18,48,8,70,11,15,35,74,27,81,72,50,90,3,6,47,80,62,28,30,53,13,10,55,41,97,42,43,7,79,85,52,26,45,44,22,76,54,75,83,67,37,84,38,36,94,60,39,9,63,23,61,5,51:"}
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "Peer1",
                Level = "INFO",
                MessageType = "MSGCANCEL_MESSAGE",
                Message = "{\"commitWitness\":\"t7M53RJepmX6KUA4NzQ3xL0c6u3a9IP7Zlk14mO8Zq8=\",\"_reducedPerms\":\"9,12,5,1,7,8,11,6,3,4,10,2,0:3,4,5,1,2,6,0:31,4,49,46,0,40,20,1,19,32,16,12,14,29,21,24,33,17,34,25,2,18,48,8,11,15,35,27,50,3,6,47,28,30,53,13,10,41,42,43,7,52,26,45,44,22,37,38,36,39,9,23,5,51:\",\"commitTime\":\"1407913200000\",\"district\":\"Yuroke\",\"boothSig\":\"TeoV1Dd+QPsTEFaJiS6FNwvm77UQxET9ZEDUMp1ywzi2X99ISyzKZQ==\",\"serialSig\":\"NiACEXVKEAA3QggfdXSuuYjGqWxc+n/9bLJMUARtO6GWD07HIoO/NQ==\",\"type\":\"audit\",\"boothID\":\"5150101\",\"serialNo\":\"5150101:14\",\"permutation\":\"18,13,16,9,12,5,15,14,1,7,8,19,17,11,6,3,4,10,2,0:19,3,14,4,5,17,7,18,8,15,1,13,2,9,6,10,11,12,16,0:78,31,4,49,59,65,46,91,96,95,0,40,69,77,20,98,1,92,93,82,19,32,16,12,14,66,29,56,99,58,68,21,24,86,33,89,88,73,87,17,57,34,25,71,64,2,18,48,8,70,11,15,35,74,27,81,72,50,90,3,6,47,80,62,28,30,53,13,10,55,41,97,42,43,7,79,85,52,26,45,44,22,76,54,75,83,67,37,84,38,36,94,60,39,9,63,23,61,5,51:\"}"
            };

            // Act
            var auditLogParser = new AuditMessageMBBLogHandler(vVoteContext);
            auditLogParser.DoHandleLog(logEntity);

            // Assert
            Assert.AreEqual(1, vVoteContext.TelemetryMBBMessages.Count());
        }
    }
}
