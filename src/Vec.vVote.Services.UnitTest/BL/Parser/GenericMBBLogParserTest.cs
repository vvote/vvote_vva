﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vec.Apps.vVote.DAL;
using Vec.vVote.Services.UnitTest.DAL;
using Moq;
using Vec.Apps.vVote.Entities;
using Vec.Apps.vVote.BL.Handler;

namespace Vec.vVote.Services.UnitTest.BL.Parser
{
    [TestClass]
    public class GenericMBBLogParserTest
    {
        IvVoteContext vVoteContext { get; set; }
        Guid EventId { get; set; }

        [TestInitialize]
        public void TestSetUp()
        {
            vVoteContext = new FvVoteContext();
            EventId = Guid.Parse("57474254-4f91-4125-8b89-ad4113387f78");
        }

        [TestMethod]
        public void ParseGeneric_WhenValidStartEVMMessage_ThenDBEntryToTelemetryMBB()
        {
            // Arrange
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "Peer1",
                Level = "INFO",
                MessageType = "MSGSTARTEVM",
                Message = "{\"serialNo\":\"2020101:3\",\"boothID\":\"2020102\",\"commitTime\":\"1393437600000\",\"boothSig\":\"Lc9k80uWp0EVrcrZB95R60LOnB0RLb3QtJfhaYjHg+X71hi3BmH6og==\",\"serialSig\":\"D+8q36NUPvLE/Hx8+lBeCPMHhXMLZTNWZwUSFmaVCzr+CinqGAgXxg==\",\"type\":\"startevm\",\"district\":\"Altona\",\"_uuid\":\"187d54d0-207d-44cf-b9f2-e4e49cba69f3\",\"_fromPeer\":\"/127.0.0.1:50133\"}"
            };

            // Act
            var genericLogParser = new GenericMBBLogHandler(vVoteContext);
            genericLogParser.DoHandleLog(logEntity);

            // Assert
            Assert.AreEqual(1, vVoteContext.TelemetryMBBs.Count());
        }
    }
}
