﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vec.Apps.vVote.DAL;
using Vec.vVote.Services.UnitTest.DAL;
using Moq;
using Vec.Apps.vVote.Entities;
using Vec.Apps.vVote.BL.Handler;

namespace Vec.vVote.Services.UnitTest.BL.Parser
{
    [TestClass]
    public class HeartbeatMessageMBBLogParserTest
    {
        IvVoteContext vVoteContext { get; set; }
        Guid EventId { get; set; }

        [TestInitialize]
        public void TestSetUp()
        {
            vVoteContext = new FvVoteContext();
            EventId = Guid.Parse("57474254-4f91-4125-8b89-ad4113387f78");
        }

        [TestMethod]
        public void ParseHeartbeat_WhenValidHeartbeatMessage_ThenOneDBEntryToTelemetry()
        {
            // Arrange
            //{"Message":"Telemetry Heartbeat#1408410157968#false","EventId":"98f54476-94b2-4f63-af41-e9a6378823fe","Level":"DEBUG","MessageType":"TELEMETRYINFO","From":"Peer1"}
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "Peer1",
                Level = "DEBUG",
                MessageType = "TELEMETRYINFO",
                Message = "{\"Message\":\"Telemetry Heartbeat#1408410157968#false\",\"EventId\":\"98f54476-94b2-4f63-af41-e9a6378823fe\",\"Level\":\"DEBUG\",\"MessageType\":\"TELEMETRYINFO\",\"From\":\"Peer1\"}"
            };

            // Act
            var heartbeatLogParser = new HeartbeatMBBLogHandler(vVoteContext);
            heartbeatLogParser.DoHandleLog(logEntity);

            // Assert
            Assert.AreEqual(1, vVoteContext.Telemetries.Count());
        }

        [TestMethod]
        public void ParseHeartbeat_WhenQueuedHeartbeatMessage_ThenOneDBEntryToTelemetry()
        {
            // Arrange
            //{"Message":"Telemetry Heartbeat#1408410157968#false","EventId":"98f54476-94b2-4f63-af41-e9a6378823fe","Level":"DEBUG","MessageType":"TELEMETRYINFO","From":"Peer1"}
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "Peer1",
                Level = "DEBUG",
                MessageType = "TELEMETRYINFO",
                Message = "{\"Message\":\"Telemetry Heartbeat#1408410157968#true\",\"EventId\":\"98f54476-94b2-4f63-af41-e9a6378823fe\",\"Level\":\"DEBUG\",\"MessageType\":\"TELEMETRYINFO\",\"From\":\"Peer1\"}"
            };

            // Act
            var heartbeatLogParser = new HeartbeatMBBLogHandler(vVoteContext);
            heartbeatLogParser.DoHandleLog(logEntity);

            // Assert
            Assert.AreEqual(1, vVoteContext.Telemetries.Count());
        }

        [TestMethod]
        public void ParseHeartbeat_WhenTaintedHeartbeatMessage_ThenOneDBEntryToTelemetry()
        {
            // Arrange
            //{"Message":"Telemetry Tainted#1408410157968","EventId":"Unknown","Level":"DEBUG","MessageType":"TELEMETRYINFO","From":"Unknown"}
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "Peer1",
                Level = "DEBUG",
                MessageType = "TELEMETRYINFO",
                Message = "{\"Message\":\"Telemetry Tainted#1408410157968\",\"EventId\":\"Unknown\",\"Level\":\"DEBUG\",\"MessageType\":\"TELEMETRYINFO\",\"From\":\"Unknown\"}"
            };

            // Act
            var heartbeatLogParser = new HeartbeatMBBLogHandler(vVoteContext);
            heartbeatLogParser.DoHandleLog(logEntity);

            // Assert
            Assert.AreEqual(1, vVoteContext.Telemetries.Count());
        }
    }
}
