﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vec.Apps.vVote.DAL;
using Vec.vVote.Services.UnitTest.DAL;
using Moq;
using Vec.Apps.vVote.Entities;
using Vec.Apps.vVote.BL.Handler;

namespace Vec.vVote.Services.UnitTest.BL.Parser
{
    [TestClass]
    public class StartEVMMessageMBBLogParserTest
    {
        IvVoteContext vVoteContext { get; set; }
        Guid EventId { get; set; }

        [TestInitialize]
        public void TestSetUp()
        {
            vVoteContext = new FvVoteContext();
            EventId = Guid.Parse("57474254-4f91-4125-8b89-ad4113387f78");
        }

        [TestMethod]
        public void ParseStartEVM_WhenValidStartEVMMessage_ThenDBEntryToTelemetryMBB()
        {
            // Arrange
            //{"serialNo":"TestDeviceOne:9","boothID":"TestEVMOne","commitTime":"1393524000000","boothSig":"ILiIjLOm6OxM6oFmPVPg3QIVXzQUrSc1Fm3VcGmo0vSaAKrQMUwH7A==","serialSig":"BRgOis6u4mJF6Yubrrx03aXeaXs2XbYljtwDgJv1TWD/94hG+VgKQA==","type":"startevm","district":"Altona","_uuid":"ef96cafe-2fdd-46ca-acad-b98b658c7ba5","_fromPeer":"/127.0.0.1:53491"}
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "Peer1",
                Level = "INFO",
                MessageType = "MSGSTARTEVM",
                Message = "{\"serialNo\":\"TestDeviceOne:9\",\"boothID\":\"TestEVMOne\",\"commitTime\":\"1393524000000\",\"boothSig\":\"ILiIjLOm6OxM6oFmPVPg3QIVXzQUrSc1Fm3VcGmo0vSaAKrQMUwH7A==\",\"serialSig\":\"BRgOis6u4mJF6Yubrrx03aXeaXs2XbYljtwDgJv1TWD/94hG+VgKQA==\",\"type\":\"startevm\",\"district\":\"Altona\",\"_uuid\":\"ef96cafe-2fdd-46ca-acad-b98b658c7ba5\",\"_fromPeer\":\"/127.0.0.1:53491\"}"
            };

            // Act
            var startEVMLogParser = new StartEVMMessageMBBLogHandler(vVoteContext);
            startEVMLogParser.DoHandleLog(logEntity);

            // Assert
            Assert.AreEqual(1, vVoteContext.TelemetryMBBs.Count());
        }
    }
}
