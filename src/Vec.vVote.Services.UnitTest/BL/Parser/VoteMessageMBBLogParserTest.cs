﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vec.Apps.vVote.DAL;
using Vec.vVote.Services.UnitTest.DAL;
using Moq;
using Vec.Apps.vVote.Entities;
using Vec.Apps.vVote.BL.Handler;

namespace Vec.vVote.Services.UnitTest.BL.Parser
{
    [TestClass]
    public class VoteMessageMBBLogParserTest
    {
        IvVoteContext vVoteContext { get; set; }
        Guid EventId { get; set; }

        [TestInitialize]
        public void TestSetUp()
        {
            vVoteContext = new FvVoteContext();
            EventId = Guid.Parse("57474254-4f91-4125-8b89-ad4113387f78");
        }

        [TestMethod]
        public void ParseVote_WhenValidVoteMessage_ThenDBEntryToTelemetryMBBMessage()
        {
            // Arrange
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "Peer1",
                Level = "INFO",
                MessageType = "MSGVOTE_MESSAGE",
                Message = "{\"serialNo\":\"2020101:3\",\"boothID\":\"2020102\",\"startEVMSig\":\"J5n8QlT4g3YoSF+zv7s9lFFZF5ZQEH/74VACs5PqsM4eDIzWyVPmiw==\",\"commitTime\":\"1393437600000\",\"boothSig\":\"YRsQiIco9i1P2QFYkyuo9O3nBntaCB9eJG5ga9Gx4dIPZAllaWC6Nw==\",\"races\":[{\"id\":\"LA\",\"preferences\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\"]},{\"id\":\"LC_ATL\",\"preferences\":[\" \",\" \",\" \"]},{\"id\":\"LC_BTL\",\"preferences\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\",\"15\",\"16\",\"17\",\"18\",\"19\",\"20\",\"21\",\"22\",\"23\",\"24\",\"25\",\"26\",\"27\",\"28\"]}],\"serialSig\":\"D+8q36NUPvLE/Hx8+lBeCPMHhXMLZTNWZwUSFmaVCzr+CinqGAgXxg==\",\"type\":\"vote\",\"district\":\"Altona\",\"_uuid\":\"50e5876a-58e1-41f2-9539-259c4efb70b0\",\"_fromPeer\":\"/127.0.0.1:50141\",\"_vPrefs\":\"1,2,3,4,5,6,7,8,9,10,11,12: , , :1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28:\"}"
            };

            // Act
            var voteLogParser = new VoteMessageMBBLogHandler(vVoteContext);
            voteLogParser.DoHandleLog(logEntity);

            // Assert
            Assert.AreEqual(1, vVoteContext.TelemetryMBBMessages.Count());
        }

        [TestMethod]
        public void ParseVote_WhenValidVoteWithIncompleteMessage_ThenDBEntryToTelemetryMBBMessage()
        {
            // Arrange
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "Peer1",
                Level = "INFO",
                MessageType = "MSGVOTE_MESSAGE",
                Message = "{\"serialNo\":\"TestDeviceOne:34\",\"boothID\":\"TestEVMOne\",\"startEVMSig\":\"B2Te2hx8DNCOZKdBH/MZP1LDVZVcutS9rx4NrbhUG3d+gvjNotdW+w==\",\"commitTime\":\"1393524000000\",\"boothSig\":\"HDdXwZEzM6/xN1B0nDU+MKhQHV5ea5IxQl1tPbbzBToxfOgJ1xg7yw==\",\"races\":[{\"id\":\"LA\",\"preferences\":[]},{\"id\":\"LC_ATL\",\"preferences\":[]},{\"id\":\"LC_BTL\",\"preferences\":[\"21\",\"16\",\"5\",\"11\",\"17\",\"1\",\"13\",\"12\",\"7\",\"8\",\"2\",\"14\",\"6\",\"3\",\"19\",\"4\",\"10\",\"9\",\"18\",\"15\",\"20\"]}],\"serialSig\":\"KLEiyUZZEp6rFgQiNU6ASMX5gVBmLI1smafCHkKS0iENFqTcUpfdbw==\",\"type\":\"vote\",\"district\":\"Bass\",\"_uuid\":\"c317e39b-2fc7-4e28-bcf8-86a881e77f93\",\"_fromPeer\":\"/127.0.0.1:54259\",\"_vPrefs\":\"::21,16,5,11,17,1,13,12,7,8,2,14,6,3,19,4,10,9,18,15,20:\"}"
            };

            // Act
            var voteLogParser = new VoteMessageMBBLogHandler(vVoteContext);
            voteLogParser.DoHandleLog(logEntity);

            // Assert
            Assert.AreEqual(1, vVoteContext.TelemetryMBBMessages.Count());
        }
    }
}
