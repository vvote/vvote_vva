﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vec.Apps.vVote.DAL;
using Vec.vVote.Services.UnitTest.DAL;
using Moq;
using Vec.Apps.vVote.Entities;
using Vec.Apps.vVote.BL.Handler;

namespace Vec.vVote.Services.UnitTest.BL.Parser
{
    [TestClass]
    public class CancelMessageMBBLogParserTest
    {
        IvVoteContext vVoteContext { get; set; }
        Guid EventId { get; set; }

        [TestInitialize]
        public void TestSetUp()
        {
            vVoteContext = new FvVoteContext();
            EventId = Guid.Parse("57474254-4f91-4125-8b89-ad4113387f78");
        }

        [TestMethod]
        public void ParseCancel_WhenValidCancelMessage_ThenDBEntryToTelemetryMBB()
        {
            // Arrange
            //{"boothID":"TestDeviceOne","serialNo":"TestDeviceOne:16","cancelAuthSig":"XGXYI73ep5MZ7sojzZ7Cvl0HPUckUNjsPTN4uQlWz1ZLUE9ZzbAA2Q==","commitTime":"1386180000000","cancelAuthID":"CancelAuth","boothSig":"Ue3tDC2YMMdYYm5Eu6dMUde9lpknRXfN4C263FnTdQLGPChoJe9lxg==","serialSig":"TcyZEoFT24wLJ+tYf3DKPN7k285Ww/2mZpfGwg8S2RN/ore5L0TTgg==","district":"Northcote","type":"cancel"}
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "Peer1",
                Level = "INFO",
                MessageType = "MSGCANCEL_MESSAGE",
                Message = "{\"boothID\":\"TestDeviceOne\",\"serialNo\":\"TestDeviceOne:16\",\"cancelAuthSig\":\"XGXYI73ep5MZ7sojzZ7Cvl0HPUckUNjsPTN4uQlWz1ZLUE9ZzbAA2Q==\",\"commitTime\":\"1386180000000\",\"cancelAuthID\":\"CancelAuth\",\"boothSig\":\"Ue3tDC2YMMdYYm5Eu6dMUde9lpknRXfN4C263FnTdQLGPChoJe9lxg==\",\"serialSig\":\"TcyZEoFT24wLJ+tYf3DKPN7k285Ww/2mZpfGwg8S2RN/ore5L0TTgg==\",\"district\":\"Northcote\",\"type\":\"cancel\"}"
            };

            // Act
            var cancelLogParser = new CancelMessageMBBLogHandler(vVoteContext);
            cancelLogParser.DoHandleLog(logEntity);

            // Assert
            Assert.AreEqual(1, vVoteContext.TelemetryMBBMessages.Count());
        }
    }
}
