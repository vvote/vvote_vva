﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vec.Apps.vVote.DAL;
using Vec.vVote.Services.UnitTest.DAL;
using Moq;
using Vec.Apps.vVote.Entities;
using Vec.Apps.vVote.BL.Handler;

namespace Vec.vVote.Services.UnitTest.BL.Parser
{
    [TestClass]
    public class ConnectionFailMessageMBBLogParserTest
    {
        IvVoteContext vVoteContext { get; set; }
        Guid EventId { get; set; }

        [TestInitialize]
        public void TestSetUp()
        {
            vVoteContext = new FvVoteContext();
            EventId = Guid.Parse("57474254-4f91-4125-8b89-ad4113387f78");
        }

        [TestMethod]
        public void ParseConnectionFail_WhenValidConnectionFailMessage_ThenOneDBEntryToTelemetry()
        {
            // Arrange
            //{"Message":"140362ff[SSL_NULL_WITH_NULL_NULL: Socket[unconnected]],IOException:Connection refused: connect","EventId":"98f54476-94b2-4f63-af41-e9a6378823fe","Level":"INFO","MessageType":"CONNFAIL","From":"Peer1"}
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "Peer1",
                Level = "DEBUG",
                MessageType = "TELEMETRYINFO",
                Message = "{\"Message\":\"140362ff[SSL_NULL_WITH_NULL_NULL: Socket[unconnected]],IOException:Connection refused: connect\",\"EventId\":\"98f54476-94b2-4f63-af41-e9a6378823fe\",\"Level\":\"INFO\",\"MessageType\":\"CONNFAIL\",\"From\":\"Peer1\"}"
            };

            // Act
            var connectionFailLogParser = new ConnectionFailMBBLogHandler(vVoteContext);
            connectionFailLogParser.DoHandleLog(logEntity);

            // Assert
            Assert.AreEqual(1, vVoteContext.Telemetries.Count());
        }
    }
}
