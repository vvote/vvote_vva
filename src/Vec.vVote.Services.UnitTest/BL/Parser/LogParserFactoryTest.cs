﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vec.Apps.vVote.DAL;
using Vec.vVote.Services.UnitTest.DAL;
using Moq;
using Vec.Apps.vVote.Entities;
using Vec.Apps.vVote.BL.Handler;

namespace Vec.vVote.Services.UnitTest.BL.Parser
{
    [TestClass]
    public class LogParserFactoryTest
    {
        IvVoteContext vVoteContext { get; set; }
        Guid EventId { get; set; }

        [TestInitialize]
        public void TestSetUp()
        {
            vVoteContext = new FvVoteContext();
            EventId = Guid.Parse("57474254-4f91-4125-8b89-ad4113387f78");
        }

        [TestMethod]
        public void GetParser_WhenValidWBBRequestMessage_ThenGenericWBBLogParser()
        {
            // Arrange
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "localhost",
                Level = "INFO",
                MessageType = "WBB_RESOURCE_REQUEST",
                Message = "http://localhost/SomeDummyResource|200"
            };
            ILogHandlerFactory factory = new LogHandlerFactory(vVoteContext);

            // Act
            var parser = factory.GetWBBLogHandlers(logEntity.MessageType);

            // Assert
            Assert.IsInstanceOfType(parser[0], typeof(GenericWBBLogHandler));
        }

        [TestMethod]
        public void GetParser_WhenValidPeerStartMessage_ThenGenericMBBLogParser()
        {
            // Arrange
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "Peer1",
                Level = "INFO",
                MessageType = "PEERINIT",
                Message = "Peer has started"
            };
            ILogHandlerFactory factory = new LogHandlerFactory(vVoteContext);

            // Act
            var parser = factory.GetMBBLogHandlers(logEntity.MessageType);

            // Assert
            Assert.IsInstanceOfType(parser[0], typeof(GenericMBBLogHandler));
        }

        [TestMethod]
        public void GetParser_WhenValidStartEVMMessage_ThenStartEVMMessageMBBLogParser()
        {
            // Arrange
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "Peer1",
                Level = "INFO",
                MessageType = "MSGSTARTEVM",
                Message = "{\"serialNo\":\"2020101:3\",\"boothID\":\"2020102\",\"commitTime\":\"1393437600000\",\"boothSig\":\"Lc9k80uWp0EVrcrZB95R60LOnB0RLb3QtJfhaYjHg+X71hi3BmH6og==\",\"serialSig\":\"D+8q36NUPvLE/Hx8+lBeCPMHhXMLZTNWZwUSFmaVCzr+CinqGAgXxg==\",\"type\":\"startevm\",\"district\":\"Altona\",\"_uuid\":\"187d54d0-207d-44cf-b9f2-e4e49cba69f3\",\"_fromPeer\":\"/127.0.0.1:50133\"}"
            };
            ILogHandlerFactory factory = new LogHandlerFactory(vVoteContext);

            // Act
            var parser = factory.GetMBBLogHandlers(logEntity.MessageType);

            // Assert
            Assert.IsInstanceOfType(parser[0], typeof(StartEVMMessageMBBLogHandler));
        }

        [TestMethod]
        public void GetParser_WhenValidVoteMessage_ThenVoteMessageMBBLogParser()
        {
            // Arrange
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "Peer1",
                Level = "INFO",
                MessageType = "MSGVOTE_MESSAGE",
                Message = "{\"serialNo\":\"2020101:3\",\"boothID\":\"2020102\",\"startEVMSig\":\"J5n8QlT4g3YoSF+zv7s9lFFZF5ZQEH/74VACs5PqsM4eDIzWyVPmiw==\",\"commitTime\":\"1393437600000\",\"boothSig\":\"YRsQiIco9i1P2QFYkyuo9O3nBntaCB9eJG5ga9Gx4dIPZAllaWC6Nw==\",\"races\":[{\"id\":\"LA\",\"preferences\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\"]},{\"id\":\"LC_ATL\",\"preferences\":[\" \",\" \",\" \"]},{\"id\":\"LC_BTL\",\"preferences\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\",\"15\",\"16\",\"17\",\"18\",\"19\",\"20\",\"21\",\"22\",\"23\",\"24\",\"25\",\"26\",\"27\",\"28\"]}],\"serialSig\":\"D+8q36NUPvLE/Hx8+lBeCPMHhXMLZTNWZwUSFmaVCzr+CinqGAgXxg==\",\"type\":\"vote\",\"district\":\"Altona\",\"_uuid\":\"50e5876a-58e1-41f2-9539-259c4efb70b0\",\"_fromPeer\":\"/127.0.0.1:50141\",\"_vPrefs\":\"1,2,3,4,5,6,7,8,9,10,11,12: , , :1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28:\"}"
            };
            ILogHandlerFactory factory = new LogHandlerFactory(vVoteContext);

            // Act
            var parser = factory.GetMBBLogHandlers(logEntity.MessageType);

            // Assert
            Assert.IsInstanceOfType(parser[0], typeof(VoteMessageMBBLogHandler));
        }

        [TestMethod]
        public void GetParser_WhenValidPODMessage_ThenPODMessageMBBLogParser()
        {
            // Arrange
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "Peer1",
                Level = "INFO",
                MessageType = "MSGPOD_MESSAGE",
                Message = "{\"serialNo\":\"TestDeviceOne:10\",\"boothID\":\"TestDeviceOne\",\"ballotReductions\":[[{\"index\":39,\"candidateIndex\":29,\"randomness\":\"zgt3XepkKygzxWicBC7Ev0xhlg4gvK6mLkDEhQ+y5QE=\"},{\"index\":38,\"candidateIndex\":30,\"randomness\":\"886XvGrbF9/ok9JT1/hvAqme2xn1iN6UNRIcOIhy3n8=\"},{\"index\":35,\"candidateIndex\":33,\"randomness\":\"2Saj2yLf/VGDALr/Yhc1ctwD40kACp4mRiydoQdIhBw=\"},{\"index\":34,\"candidateIndex\":36,\"randomness\":\"8Z1DkrNMslgpjr1SGFk/u2t0aV1x/HytdXT5AM23VbQ=\"},{\"index\":33,\"candidateIndex\":19,\"randomness\":\"v5aqaszHYimAOsuDZLdwJ7lXrFSmV/1l3SCtNG4dvBU=\"},{\"index\":32,\"candidateIndex\":35,\"randomness\":\"PQ/0/5QnDsyyI9sRYBTP4+kNw1VSGRi/52BhBqDQsrM=\"},{\"index\":31,\"candidateIndex\":15,\"randomness\":\"KblOpDoVoibNrzw/2PWK1CKJ8nvbCksIkSywwiPF//4=\"},{\"index\":30,\"candidateIndex\":20,\"randomness\":\"5ipoULMPao/3onGCYkouHGYDtUT+7yts7cMpwn7fAiY=\"},{\"index\":29,\"candidateIndex\":26,\"randomness\":\"5liOtsmEL4d3B7Dcv8+qsDEA4YPMBCyYQTWW0XiwezY=\"},{\"index\":28,\"candidateIndex\":22,\"randomness\":\"aE2DdezXbdSuyxx2gb0gpmxJNVe14keoRVQgh5QfKE4=\"},{\"index\":27,\"candidateIndex\":28,\"randomness\":\"HhdUO64oKSiTFj2yoB7nA13Vi3J//JqCEwGeQ6xxQ3A=\"},{\"index\":25,\"candidateIndex\":12,\"randomness\":\"T4lBkYp7nnZsTN2jxj303KUl55hwJBHZZ8pjXHuc7cI=\"},{\"index\":24,\"candidateIndex\":34,\"randomness\":\"OFwzSex7u3vCvOXs0pKPGUMV1/TKY9bqoEXvHZ25Z+Y=\"},{\"index\":23,\"candidateIndex\":37,\"randomness\":\"THmOK5r5gS/25vsCPEPq+AnsMEz1coRABSTsJZv50tw=\"},{\"index\":22,\"candidateIndex\":14,\"randomness\":\"qyErb66+3D2pBXWRy59s0WUTno0rkS/aYIYZSqnFsjA=\"},{\"index\":20,\"candidateIndex\":21,\"randomness\":\"S+lM4ZPOgvwNFWfksR8mqb/zR1UeG3i2bZOtrWNSzm4=\"},{\"index\":19,\"candidateIndex\":31,\"randomness\":\"6NFL2HQAvawR74tQZmg216usRONW12o7P3/YPQ+OziM=\"},{\"index\":18,\"candidateIndex\":18,\"randomness\":\"McLikUrMFb7X8E1m9p7QOn5FHc34jLHX+wyXqRkcoBY=\"},{\"index\":17,\"candidateIndex\":25,\"randomness\":\"Jk2xPdXMD515wiW62i0exEAeCc/JZXTqt0YnKp4/GjE=\"},{\"index\":14,\"candidateIndex\":13,\"randomness\":\"gsPEmGKX58xMKzoJ4I7oIpzxcYgYOc4qTCkdf7vWy/E=\"},{\"index\":13,\"candidateIndex\":32,\"randomness\":\"ZGVfGZGdwq9i1qLbydkNfvUBzb+4SseMNRGxi919hB8=\"},{\"index\":12,\"candidateIndex\":16,\"randomness\":\"KMNgwD2DUybxnXOdMrcSNrcemo8+B22zUEI9jmwRZgI=\"},{\"index\":10,\"candidateIndex\":27,\"randomness\":\"m6615MS3I/Yvwmera/oZnR8ak64oiA6LTRC6rub1/IM=\"},{\"index\":9,\"candidateIndex\":17,\"randomness\":\"5pyuzb9BwQWi8W9AM6ncJDdL/vhge0GOmOIrFac0ReU=\"},{\"index\":6,\"candidateIndex\":24,\"randomness\":\"4CIy7fAKkT8sw5Mo0T6scGdfaF+dm34oRQDC38kYsxw=\"},{\"index\":5,\"candidateIndex\":23,\"randomness\":\"hF3Fgs73VM5KCDDf9Bfh9OZqAVcqbwesuitmmTrCBIU=\"},{\"index\":4,\"candidateIndex\":39,\"randomness\":\"fAx3sAeuclh7rvHB3bZPI5ljzM/bmjtjxQLFtjXAHmM=\"},{\"index\":0,\"candidateIndex\":38,\"randomness\":\"gcsJF2637fmnndTFI+v5k6m5BqFvJqNJvezNhjNfmrE=\"}],[{\"index\":5,\"candidateIndex\":5,\"randomness\":\"/cyOys7Mlitt7FTsoAZA7dhYZXJMiwf7zuft748v+Fk=\"},{\"index\":2,\"candidateIndex\":6,\"randomness\":\"75klNtlGkUsgPhI7xnkTi07peE2+y6kcv8BrfgZ7PHs=\"},{\"index\":1,\"candidateIndex\":3,\"randomness\":\"JwIlNB+k2pMFDvtpQnKQYmC79s2M9vc2Qa1rZp5M8hE=\"},{\"index\":0,\"candidateIndex\":4,\"randomness\":\"txeE9DWJy58fG977Fuvjpmqw+9Y33389gco0ifNECL0=\"}],[{\"index\":30,\"candidateIndex\":30,\"randomness\":\"g2U/QNZlLiec0psoVWZztdqHzZwmJiBQ1kdQCqychQM=\"},{\"index\":21,\"candidateIndex\":34,\"randomness\":\"mzuZfNgCBmyVqQWuzMSN/T8MjHc6HjWMZM5d5sjLQJc=\"},{\"index\":20,\"candidateIndex\":31,\"randomness\":\"7BOSAM5SDL04CLkQdnje4pxt4hxgredBSTfAZS981q8=\"},{\"index\":17,\"candidateIndex\":28,\"randomness\":\"uojbgng+riFjPnLO/nJmAwlO8QI/t1xt8ltrZvJYbKg=\"},{\"index\":15,\"candidateIndex\":32,\"randomness\":\"JMUYZQ/57RR+0KXPeZFNrG7luTC0QE1Es+BSvNArG5Q=\"},{\"index\":13,\"candidateIndex\":33,\"randomness\":\"H6fkjY7iXdbkdn1fCaOsOAXfxoP4Y1m9faexZddP/mk=\"},{\"index\":5,\"candidateIndex\":29,\"randomness\":\"Qiqh48z6nEuQt7PKGDwDi/35DN7kfDI69HNjXIFCLII=\"}]],\"commitTime\":\"1393524000000\",\"boothSig\":\"a+vAl+Fh+66R1HZMuov0OJ1WlvhXZ3+ViV2RdeuNQv6WcAMVs7AbDQ==\",\"district\":\"Altona\",\"type\":\"pod\",\"_uuid\":\"bd18573d-a281-4a7b-a21d-377790983569\""
            };
            ILogHandlerFactory factory = new LogHandlerFactory(vVoteContext);

            // Act
            var parser = factory.GetMBBLogHandlers(logEntity.MessageType);

            // Assert
            Assert.IsInstanceOfType(parser[0], typeof(PODMessageMBBLogHandler));
        }

        [TestMethod]
        public void GetParser_WhenValidCancelMessage_ThenCancelMessageMBBLogParser()
        {
            // Arrange
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "Peer1",
                Level = "INFO",
                MessageType = "MSGCANCEL_MESSAGE",
                Message = "{\"boothID\":\"TestDeviceOne\",\"serialNo\":\"TestDeviceOne:16\",\"cancelAuthSig\":\"XGXYI73ep5MZ7sojzZ7Cvl0HPUckUNjsPTN4uQlWz1ZLUE9ZzbAA2Q==\",\"commitTime\":\"1386180000000\",\"cancelAuthID\":\"CancelAuth\",\"boothSig\":\"Ue3tDC2YMMdYYm5Eu6dMUde9lpknRXfN4C263FnTdQLGPChoJe9lxg==\",\"serialSig\":\"TcyZEoFT24wLJ+tYf3DKPN7k285Ww/2mZpfGwg8S2RN/ore5L0TTgg==\",\"district\":\"Northcote\",\"type\":\"cancel\"}"
            };
            ILogHandlerFactory factory = new LogHandlerFactory(vVoteContext);

            // Act
            var parser = factory.GetMBBLogHandlers(logEntity.MessageType);

            // Assert
            Assert.IsInstanceOfType(parser[0], typeof(CancelMessageMBBLogHandler));
        }

        [TestMethod]
        public void GetParser_WhenValidAuditMessage_ThenAuditMessageMBBLogParser()
        {
            // Arrange
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "Peer1",
                Level = "INFO",
                MessageType = "MSGAUDIT_MESSAGE",
                Message = "{\"boothID\":\"TestDeviceOne\",\"serialNo\":\"TestDeviceOne:16\",\"cancelAuthSig\":\"XGXYI73ep5MZ7sojzZ7Cvl0HPUckUNjsPTN4uQlWz1ZLUE9ZzbAA2Q==\",\"commitTime\":\"1386180000000\",\"cancelAuthID\":\"CancelAuth\",\"boothSig\":\"Ue3tDC2YMMdYYm5Eu6dMUde9lpknRXfN4C263FnTdQLGPChoJe9lxg==\",\"serialSig\":\"TcyZEoFT24wLJ+tYf3DKPN7k285Ww/2mZpfGwg8S2RN/ore5L0TTgg==\",\"district\":\"Northcote\",\"type\":\"cancel\"}"
            };
            ILogHandlerFactory factory = new LogHandlerFactory(vVoteContext);

            // Act
            var parser = factory.GetMBBLogHandlers(logEntity.MessageType);

            // Assert
            Assert.IsInstanceOfType(parser[0], typeof(AuditMessageMBBLogHandler));
        }

        [TestMethod]
        public void GetParser_WhenValidHeartbeatMessage_ThenHeartbeatMessageMBBLogParser()
        {
            // Arrange
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "Peer1",
                Level = "INFO",
                MessageType = "TELEMETRYINFO",
                Message = "{\"boothID\":\"TestDeviceOne\",\"serialNo\":\"TestDeviceOne:16\",\"cancelAuthSig\":\"XGXYI73ep5MZ7sojzZ7Cvl0HPUckUNjsPTN4uQlWz1ZLUE9ZzbAA2Q==\",\"commitTime\":\"1386180000000\",\"cancelAuthID\":\"CancelAuth\",\"boothSig\":\"Ue3tDC2YMMdYYm5Eu6dMUde9lpknRXfN4C263FnTdQLGPChoJe9lxg==\",\"serialSig\":\"TcyZEoFT24wLJ+tYf3DKPN7k285Ww/2mZpfGwg8S2RN/ore5L0TTgg==\",\"district\":\"Northcote\",\"type\":\"cancel\"}"
            };
            ILogHandlerFactory factory = new LogHandlerFactory(vVoteContext);

            // Act
            var parser = factory.GetMBBLogHandlers(logEntity.MessageType);

            // Assert
            Assert.IsInstanceOfType(parser[0], typeof(HeartbeatMBBLogHandler));
        }

        [TestMethod]
        public void GetParser_WhenValidConnectionFailMessage_ThenConnectionFailMessageMBBLogParser()
        {
            // Arrange
            var logEntity = new LogEntity()
            {
                EventId = EventId,
                DateTime = DateTime.UtcNow.ToString("o"),
                From = "Peer1",
                Level = "INFO",
                MessageType = "CONNFAIL",
                Message = "{\"boothID\":\"TestDeviceOne\",\"serialNo\":\"TestDeviceOne:16\",\"cancelAuthSig\":\"XGXYI73ep5MZ7sojzZ7Cvl0HPUckUNjsPTN4uQlWz1ZLUE9ZzbAA2Q==\",\"commitTime\":\"1386180000000\",\"cancelAuthID\":\"CancelAuth\",\"boothSig\":\"Ue3tDC2YMMdYYm5Eu6dMUde9lpknRXfN4C263FnTdQLGPChoJe9lxg==\",\"serialSig\":\"TcyZEoFT24wLJ+tYf3DKPN7k285Ww/2mZpfGwg8S2RN/ore5L0TTgg==\",\"district\":\"Northcote\",\"type\":\"cancel\"}"
            };
            ILogHandlerFactory factory = new LogHandlerFactory(vVoteContext);

            // Act
            var parser = factory.GetMBBLogHandlers(logEntity.MessageType);

            // Assert
            Assert.IsInstanceOfType(parser[0], typeof(ConnectionFailMBBLogHandler));
            Assert.IsInstanceOfType(parser[1], typeof(GenericMBBLogHandler));
        }
    }
}
