﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vec.Apps.Wbb.BL;

namespace Vec.Wbb.Services.UnitTest
{
    [TestClass]
    public class VotePackingBLUnitTest
    {
        public IVotePackingBL DefaultVotePackingBL{ get; set; }
        
        [TestInitialize]
        public void TestSetUp()
        {
            DefaultVotePackingBL = new VotePackingBL(); //TODO:mock...
        }

        //[TestMethod]
        public void LocateCiphers_WhenNothing_ThenEmptyListReturned()
        {
            // Arrange

            string basePath = @"C:\Projects\Turing\Releases\vVote\Dev\vVote\Vec.Wbb.Host";
            // Act
            var result = DefaultVotePackingBL.GetCipherDevices(basePath);

            // Assert
            Assert.AreEqual(0, result.Count);
        }
    }
}
