﻿/*
    This file is part of vVote from the Victorian Electoral Commission.

    vVote is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    vVote is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vVote.  If not, see <http://www.gnu.org/licenses/>.

    Contact Craig Burton   craig.burton@vec.vic.gov.au
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Vec.Apps.Entities
{
    [DataContract]
    public class Receipt
    {
        public Receipt()
        {
            Races = new List<Race>();
        }

        [DataMember(Name = "boothID")]
        public string BoothId { get; set; }

        [DataMember(Name = "serialNo")]
        public string SerialNumber { get; set; }

        [DataMember(Name = "startEVMSig")]
        public string StartSignature { get; set; }

        [DataMember(Name = "commitTime")]
        public string CommitTime { get; set; }

        [DataMember(Name = "boothSig")]
        public string BoothSignature { get; set; }

        [DataMember(Name = "races")]
        public List<Race> Races { get; set; }

        [DataMember(Name = "serialSig")]
        public string SerialSignature { get; set; }

        [DataMember(Name = "district")]
        public string District { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "_vPrefs")]
        public string VotePreferences { get; set; }

        [DataMember(Name = "voting_centre")]
        public string VotingCentre { get; set; }

        [DataMember(Name = "region")]
        public string Region { get; set; }
    }
}
