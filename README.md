vVote_VVA
==========

Server application to provide configuration, monitoring and reporting for vVote
--------------------------------------------------------------------------------------------------
 
This is part of the vVote design.  It has been implemented by the Victorian Electoral Commission.
It consists of .NET IIS services and SQL databases.

It relies on the VEC's EMS2 application to provide:
  
  * Conversion of EMS2 electoral event content to XML for vVote clients
  * RDL services for reporting

It relies on third party tools to configure Android devices:
  * ADB (Android Debug Bridge) 
  
The VVA software provides:
  
  * Creation of parallel election events inherited from EMS2
  * Creation of MP3 and OGG audio for populating vVote clients and WBB website
  * Creation of JSON configuration files for vVote clients and servers
  * Population of vVote client devices with content files and execution of client-side scripts for key generation
  * Fleet management for vVote client devices including spares deployment
  * Reporting of system events via collection and processing of telemetry data
  * Post-processing of collected receipt files to prepare them for decryption
  * Post-processing of emitted CSV files from decryption and conversion to PDF vote files
  
Notes:
  
  * VVA cannot be built from sources provided here.  Key steps in system configuration such as vote packing should be built from the suVote sources.
  * VVA is built with the entire VEC EMS2 application.  EMS2 is not Open Source.